﻿using System;
using System.Collections.Generic;
using System.Text;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Options;
using BloodBank.Server.Extensions;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using BloodBank.Server.Resources;
using Microsoft.AspNetCore.Http;
using Amazon.S3.Transfer;
using Amazon.Runtime;
using Amazon;
using BloodBank.Server.Entities;

namespace BloodBank.Server.Repositories
{
    public class S3Repository
    {
        public IOptions<S3Settings> s3Settings;

        string accessKey = Environment.GetEnvironmentVariable("ACCESS_KEY");
        string secretKey = Environment.GetEnvironmentVariable("SECRET_KEY");
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast1;

        private IAmazonS3 amazonS3;

        public S3Repository(
            IOptions<S3Settings> s3Settings,
            IAmazonS3 amazonS3)
        {
            this.s3Settings = s3Settings;
            this.amazonS3 = amazonS3;
        }

        public async Task<UploadPhotoModel> UploadObject(
            IOptions<S3Settings> s3Settings,
            string ImageBase64String,
            string BucketChildKey,
            string mediaType = "image/jpg",
            string mediaFileName = "1.jpg")
        {
            string Bucket = s3Settings.Value.BucketName;
            //string region = s3Settings.AWSRegion;

            // connecting to the client
            //var client = this.amazonS3;
            BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
            var client = new AmazonS3Client(awsCreds, bucketRegion);

            // get the file and convert it to the byte[]
            byte[] fileBytes = Convert.FromBase64String(ImageBase64String);

            // create unique file name for prevent the mess
            // Create full file name as - SKO.BUTImages/<BucketKey>/<UniqueGUID>.jpg
            var FullFileName = s3Settings.Value.BucketParentKey + "/" +
                                BucketChildKey + "/" +
                                Guid.NewGuid() + mediaFileName.Trim();
            FullFileName = StringExtensions.RemoveSpaces(FullFileName);

            //Create full url
            string ConstructedURL = s3Settings.Value.AWSBaseURL + FullFileName;

            PutObjectResponse response = null;

            using (client)
            {
                var request = new PutObjectRequest
                {
                    BucketName = Bucket,
                    Key = FullFileName,
                    CannedACL = S3CannedACL.PublicRead,
                    ContentType = mediaType
                };

                using (var ms = new MemoryStream(fileBytes))
                {
                    request.InputStream = ms;
                    response = await client.PutObjectAsync(request);
                }

            };

            if (response.HttpStatusCode == HttpStatusCode.OK)
            {
                // this model is up to you, in my case I have to use it following;
                return new UploadPhotoModel
                {
                    Success = true,
                    FileName = ConstructedURL
                };
            }
            else
            {
                // this model is up to you, in my case I have to use it following;
                return new UploadPhotoModel
                {
                    Success = false,
                    FileName = ""
                };
            }
        }




        //OLD upload Objectfunction before Core3.0
        //public async Task<UploadPhotoModel> UploadObjectOld(
        //    IOptions<S3Settings> s3Settings,
        //    string ImageBase64String,
        //    string BucketChildKey,
        //    string mediaType = "image/jpg",
        //    string mediaFileName = "1.jpg")
        //{
        //    string Bucket = s3Settings.Value.BucketName;
        //    //string region = s3Settings.AWSRegion;

        //    // connecting to the client
        //    var client = this.amazonS3;

        //    // get the file and convert it to the byte[]
        //    byte[] fileBytes = Convert.FromBase64String(ImageBase64String);

        //    // create unique file name for prevent the mess
        //    // Create full file name as - SKO.BUTImages/<BucketKey>/<UniqueGUID>.jpg
        //    var FullFileName = s3Settings.Value.BucketParentKey + "/" +
        //                        BucketChildKey + "/" +
        //                        Guid.NewGuid() + mediaFileName.Trim();
        //    FullFileName = StringExtensions.RemoveSpaces(FullFileName);

        //    //Create full url
        //    string ConstructedURL = s3Settings.Value.AWSBaseURL + FullFileName;

        //    PutObjectResponse response = null;

        //    using (client)
        //    {
        //        var request = new PutObjectRequest
        //        {
        //            BucketName = Bucket,
        //            Key = FullFileName,
        //            CannedACL = S3CannedACL.PublicRead,
        //            ContentType = mediaType
        //        };

        //        using (var ms = new MemoryStream(fileBytes))
        //        {
        //            request.InputStream = ms;
        //            response = await client.PutObjectAsync(request);
        //        }

        //    };

        //    if (response.HttpStatusCode == HttpStatusCode.OK)
        //    {
        //        // this model is up to you, in my case I have to use it following;
        //        return new UploadPhotoModel
        //        {
        //            Success = true,
        //            FileName = ConstructedURL
        //        };
        //    }
        //    else
        //    {
        //        // this model is up to you, in my case I have to use it following;
        //        return new UploadPhotoModel
        //        {
        //            Success = false,
        //            FileName = ""
        //        };
        //    }
        //}

        // try uploading image Base64 to API and return URL

        public async Task<UploadPhotoModel> UploadObjectUsingByteArray(
      IOptions<S3Settings> s3Settings,
      //byte[] fileBytes,
      IFormFile file,
      string BucketChildKey)
        {
            string Bucket = s3Settings.Value.BucketName;
            //string region = s3Settings.AWSRegion;

            // connecting to the client
            var client = this.amazonS3;

            // create unique file name for prevent the mess
            // Create full file name as - SKO.BUTImages/<BucketKey>/<UniqueGUID>.jpg
            var FullFileName = s3Settings.Value.BucketParentKey + "/" +
                                BucketChildKey + "/" +
                                Guid.NewGuid() + "12345" + file.FileName.Trim();
            FullFileName = StringExtensions.RemoveSpaces(FullFileName);
          
            //Create full url
            string ConstructedURL = s3Settings.Value.AWSBaseURL + FullFileName;

            PutObjectResponse response = null;



            //new code

            //var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                string contentAsString = reader.ReadToEnd();
                byte[] bytes = new byte[contentAsString.Length * sizeof(char)];
                System.Buffer.BlockCopy(contentAsString.ToCharArray(), 0, bytes, 0, bytes.Length);
            }

            // end new code


            using (var newMemoryStream = new MemoryStream())
            {
                await file.CopyToAsync(newMemoryStream);

                var uploadRequest = new TransferUtilityUploadRequest
                {
                    InputStream = newMemoryStream,
                    Key = FullFileName,
                    BucketName = Bucket,
                    CannedACL = S3CannedACL.PublicRead,
                    ContentType = "image/jpeg"
                };

                var fileTransferUtility = new TransferUtility(client);
                await fileTransferUtility.UploadAsync(uploadRequest);
            }

            //using (client)
            //{
            //    var request = new PutObjectRequest
            //    {
            //        BucketName = Bucket,
            //        Key = FullFileName,
            //        CannedACL = S3CannedACL.PublicRead,
            //    };

            //    using (var ms = new MemoryStream())
            //    {
            //        file.CopyTo(ms);
            //        request.InputStream = ms;
            //        response = await client.PutObjectAsync(request);
            //    }

            //};

            // if (response.HttpStatusCode == HttpStatusCode.OK)
            // {
            // this model is up to you, in my case I have to use it following;
            return new UploadPhotoModel
            {
                Success = true,
                FileName = ConstructedURL
            };
            // }
            // else
            // {
            // this model is up to you, in my case I have to use it following;
            //    return new UploadPhotoModel
            //   {
            //        Success = false,
            //       FileName = ""
            //   };
            // }
        }


        public async Task<UploadPhotoModel> UploadObjectAsync(
      IOptions<S3Settings> s3Settings,
      //byte[] fileBytes,
      IFormFile file,
      string BucketChildKey)
        {
            string Bucket = s3Settings.Value.BucketName;
            // Create list to store upload part responses.
            List<UploadPartResponse> uploadResponses = new List<UploadPartResponse>();

            // connecting to the client
            var client = this.amazonS3;

            // create unique file name for prevent the mess
            // Create full file name as - SKO.BUTImages/<BucketKey>/<UniqueGUID>.jpg
            var FullFileName = s3Settings.Value.BucketParentKey + "/" +
                                BucketChildKey + "/" +
                                Guid.NewGuid() + "12345" + file.FileName.Trim();
            FullFileName = StringExtensions.RemoveSpaces(FullFileName);

            //Create full url
            string ConstructedURL = s3Settings.Value.AWSBaseURL + FullFileName;

            // Setup information required to initiate the multipart upload.
            InitiateMultipartUploadRequest initiateRequest = new InitiateMultipartUploadRequest
            {
                BucketName = Bucket,
                Key = FullFileName,
                CannedACL = S3CannedACL.PublicRead,
                ContentType = file.ContentType
            };

            // Initiate the upload.
            InitiateMultipartUploadResponse initResponse =
                await client.InitiateMultipartUploadAsync(initiateRequest);

            // Upload parts.
            long contentLength = file.Length;
            long partSize = 5 * (long)Math.Pow(2, 20); // 5 MB

            try
            {
                Console.WriteLine("Uploading parts");

                long filePosition = 0;
                for (int i = 1; filePosition < contentLength; i++)
                {
                    UploadPartRequest uploadRequest = new UploadPartRequest
                    {
                        BucketName = Bucket,
                        Key = FullFileName,
                        UploadId = initResponse.UploadId,
                        PartNumber = i,
                        PartSize = partSize,
                        FilePosition = filePosition,
                        InputStream = file.OpenReadStream()
                    };
                     
                    // Track upload progress.
                    uploadRequest.StreamTransferProgress +=
                        new EventHandler<StreamTransferProgressArgs>(UploadPartProgressEventCallback);

                    // Upload a part and add the response to our list.
                    uploadResponses.Add(await client.UploadPartAsync(uploadRequest));

                    filePosition += partSize;
                }

                // Setup to complete the upload.
                CompleteMultipartUploadRequest completeRequest = new CompleteMultipartUploadRequest
                {
                    BucketName = Bucket,
                    Key = FullFileName,
                    UploadId = initResponse.UploadId
                };
                completeRequest.AddPartETags(uploadResponses);

                // Complete the upload.
                CompleteMultipartUploadResponse completeUploadResponse =
                    await client.CompleteMultipartUploadAsync(completeRequest);

                if (completeUploadResponse.HttpStatusCode == HttpStatusCode.OK)
                {
                    return new UploadPhotoModel
                    {
                        Success = true,
                        FileName = ConstructedURL
                    };
                }
                else
                {
                    return new UploadPhotoModel
                    {
                        Success = false,
                        FileName = ""
                    };
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("An AmazonS3Exception was thrown: { 0}", exception.Message);

                // Abort the upload.
                AbortMultipartUploadRequest abortMPURequest = new AbortMultipartUploadRequest
                {
                    BucketName = Bucket,
                    Key = FullFileName,
                    UploadId = initResponse.UploadId
                };
                await client.AbortMultipartUploadAsync(abortMPURequest);

                return new UploadPhotoModel
                {
                    Success = false,
                    FileName = ""
                };
            }

        }


        public async Task<UploadPhotoModel> UploadObjectWithFormFile(
           IOptions<S3Settings> s3Settings,
           IFormFile file,
           string BucketChildKey)
        {
            string Bucket = s3Settings.Value.BucketName;
            //string region = s3Settings.AWSRegion;

            // connecting to the client
            var client = this.amazonS3;

            // get the file and convert it to the byte[]
            byte[] fileBytes = new Byte[file.Length];
            file.OpenReadStream().Read(fileBytes, 0, Int32.Parse(file.Length.ToString()));


            // get the file and convert it to the byte[]
           // byte[] fileBytes = Convert.FromBase64String(base64);

            // create unique file name for prevent the mess
            // Create full file name as - SKO.BUTImages/<BucketKey>/<UniqueGUID>.jpg
            var FullFileName = s3Settings.Value.BucketParentKey + "/" +
                                BucketChildKey + "/" +
                                Guid.NewGuid() + ""+ file.FileName.Trim();
            FullFileName = StringExtensions.RemoveSpaces(FullFileName);

            //Create full url
            string ConstructedURL = s3Settings.Value.AWSBaseURL + FullFileName;

            PutObjectResponse response = null;
         
            using (client)
            {
                //var request = new PutObjectRequest
                //{
                //    BucketName = Bucket,
                //    Key = FullFileName,
                //    CannedACL = S3CannedACL.PublicRead,
                //    ServerSideEncryptionMethod = ServerSideEncryptionMethod.None
                //};


                using (var stream = new MemoryStream(fileBytes))
                {
                    var request = new PutObjectRequest
                    {
                        BucketName = Bucket,
                        Key = FullFileName,
                        InputStream = stream,
                        ContentType = file.ContentType,
                        CannedACL = S3CannedACL.PublicRead
                    };

                    response = await client.PutObjectAsync(request);
                };
                //using (var ms = new MemoryStream(fileBytes))
                //{
                //    request.InputStream = ms;
                //    response = await client.PutObjectAsync(request);
                //}

            };

            if (response.HttpStatusCode == HttpStatusCode.OK)
            {
                // this model is up to you, in my case I have to use it following;
                return new UploadPhotoModel
                {
                    Success = true,
                    FileName = ConstructedURL,
                    //data = "base64Original = " + (base64 != null ? base64 : "null") + " ------ response.ServerSideEncryptionMethod.Value = " + (response.ServerSideEncryptionMethod != null && response.ServerSideEncryptionMethod.Value != null ? response.ServerSideEncryptionMethod.Value : "null") +  "----- fileBytesData.len = "+ (fileBytesData != null ? fileBytesData.Length+"" : "null") + " -----------  fileBytes.len = " + (fileBytes != null ? fileBytes.Length + "" : "null")
                    data = ""
                };
            }
            else
            {
                // this model is up to you, in my case I have to use it following;
                return new UploadPhotoModel
                {
                    Success = false,
                    FileName = "",
                    data = ""
                };
            }
        }


        // Test API
        public async Task<UploadPhotoModel> UploadObjectVideo(IOptions<S3Settings> s3Settings, string BucketChildKey, string mediaFileName, string mediaType, IFormFile formFile)
        {
            string Bucket = s3Settings.Value.BucketName;
            //string region = s3Settings.AWSRegion;

            // connecting to the client
            var client = this.amazonS3;


            //byte[] fileBytes = Convert.FromBase64String(ImageBase64String);
            byte[] uploadvideobytes = null;
            using (var fs = new MemoryStream())
            {
                formFile.CopyTo(fs);
                var fileBytes = fs.ToArray();
                string s = Convert.ToBase64String(fileBytes);
                uploadvideobytes = Convert.FromBase64String(s);
            }

            // create unique file name for prevent the mess  // Bucket Name - testing74 && ARN - arn:aws:s3:::testing74/*
            // Create full file name as - SKO.BUTImages/<BucketKey>/<UniqueGUID>.jpg
            var FullFileName = s3Settings.Value.BucketParentKey + "/" +
                                    BucketChildKey + "/" +
                                    Guid.NewGuid() + mediaFileName.Trim();
            FullFileName = StringExtensions.RemoveSpaces(FullFileName);

            //Create full url
            string ConstructedURL = s3Settings.Value.AWSBaseURL + FullFileName;

            PutObjectResponse response = null;

            using (client)
            {
                var request = new PutObjectRequest
                {
                    BucketName = Bucket,
                    Key = FullFileName,
                    CannedACL = S3CannedACL.PublicRead,
                    ContentType = mediaType
                };


                if (uploadvideobytes != null && uploadvideobytes.Length > 0)
                {
                    using (var ms = new MemoryStream(uploadvideobytes))
                    {
                        request.InputStream = ms;
                        response = await client.PutObjectAsync(request);
                    }
                }
            };

            if (response.HttpStatusCode == HttpStatusCode.OK)
            {
                // this model is up to you, in my case I have to use it following;
                return new UploadPhotoModel
                {
                    Success = true,
                    FileName = ConstructedURL
                };
            }
            else
            {
                // this model is up to you, in my case I have to use it following;
                return new UploadPhotoModel
                {
                    Success = false,
                    FileName = ""
                };
            }
        }


        // Test 2
        public UploadPhotoModel sendMyFileToS3(IOptions<S3Settings> s3Settings, string BucketChildKey, System.IO.Stream localFilePath, string bucketName, string subDirectoryInBucket, string fileNameInS3, string contentType)
        {
            //IAmazonS3 client = new AmazonS3Client(RegionEndpoint.APSouth1);
            
            var client = this.amazonS3;
            TransferUtility utility = new TransferUtility(client);
            bucketName = s3Settings.Value.BucketName;

            // Create full file name as - SKO.BUTImages/<BucketKey>/<UniqueGUID>.jpg
            var FullFileName = s3Settings.Value.BucketParentKey + "/" +
                                BucketChildKey + "/" +
                                Guid.NewGuid() + fileNameInS3.Trim();
            FullFileName = StringExtensions.RemoveSpaces(FullFileName);

            //Create full url
            string ConstructedURL = s3Settings.Value.AWSBaseURL + FullFileName;

            TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();
            if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
            {
                request.BucketName = bucketName; //no subdirectory just bucket name  
            }
            else
            {   // subdirectory and bucket name  
                request.BucketName = bucketName + @"/" + subDirectoryInBucket;
            }
            request.Key = FullFileName; //file name up in S3  
            request.InputStream = localFilePath;
            request.CannedACL = S3CannedACL.PublicRead;
            request.ContentType = contentType;
            utility.Upload(request); //commensing the transfer  


            return new UploadPhotoModel
            {
                Success = true,
                FileName = ConstructedURL
            };

        }

        public static void UploadPartProgressEventCallback(object sender, StreamTransferProgressArgs e)
        {
            // Process event. 
            Console.WriteLine("{0}/{1}", e.TransferredBytes, e.TotalBytes);
        }


    }
}
