﻿using BloodBank.Server.Identity;
using Microsoft.AspNetCore.Identity;
using BloodBank.Server.Messages;
using BloodBank.Server.Messages.Donor;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using BloodBank.Server.Entities;
using TimeZoneConverter;
using BloodBank.Server.Extensions;

namespace BloodBank.Server.Repositories
{
 public  class DonorRepository:BaseRepository
    {
        private BloodBankDBContext DBContext;
        private S3Repository s3Repository;
        private string sResponse;
        private readonly UserManager<AlcoholUser> userManager;

        public DonorRepository(BloodBankDBContext dbContext,
                               UserManager<AlcoholUser> userManager,
                               S3Repository s3Repository)
        {
            this.DBContext = dbContext;
            this.userManager = userManager;
            this.s3Repository = s3Repository;
        }

        public DateTime GetLocalDateTime()
        {
            DateTime UtcNowTime = DateTime.UtcNow;
            //DateTime localDateTime =TimeZoneInfo.ConvertTimeFromUtc(UtcNowTime, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time")); //for converting to IST
            //DateTime localDateTime = TimeZoneInfo.ConvertTimeFromUtc(UtcNowTime, TimeZoneInfo.FindSystemTimeZoneById("Asia/Calcutta")); //for converting to IST
            DateTime localDateTime = TimeZoneInfo.ConvertTimeFromUtc(UtcNowTime, TZConvert.GetTimeZoneInfo("India Standard Time"));
            return localDateTime;
        }



        public AddBloodOrderResponse AddBloodOrder(AddBloodOrderRequest request)
        {
            AddBloodOrderResponse response = new AddBloodOrderResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName.Contains(UserName));
                if (userProfileRecord != null)
                {
                    BloodRequest blood = new BloodRequest();
                    blood.ContactPersonName = request.contactPersonName;
                    blood.ContactPersonMobileNumber = string.IsNullOrEmpty(request.contactPersonMobileNumber) ? "" : request.contactPersonMobileNumber;
                    blood.PatientName = string.IsNullOrEmpty(request.patientName) ? "" : request.patientName;
                    blood.HospitalName = string.IsNullOrEmpty(request.hospitalName) ? "" : request.hospitalName;
                    blood.BloodGroup = string.IsNullOrEmpty(request.bloodGroup) ? "" : request.bloodGroup;
                    blood.ProductName = string.IsNullOrEmpty(request.productName) ? "" : request.productName;
                    blood.Status = "pending";
                    blood.RequestDate= GetLocalDateTime();
                    this.DBContext.BloodRequest.Add(blood);
                   
                    int affectedRows = this.DBContext.SaveChanges();
                    response.Success = affectedRows == 1;
                    if (response.Success)
                    {
                        response.Message = "Blood Order inserted successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Failed to Add Blood order";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }

                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "AddBloodOrder Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public GetAllBloodOrderResponse GetAllBloodOrder(GetAllBloodOrderRequest request)
        {
            GetAllBloodOrderResponse response = new GetAllBloodOrderResponse();

            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                string id = this.DBContext.Users.FirstOrDefault(x => x.UserName.Contains(UserName)).Id;
                List<BloodRequest> bloodRequests = this.DBContext.BloodRequest.Where(x=>x.Status=="pending").ToList();
                if (bloodRequests != null && bloodRequests.Count > 0)
                {
                    foreach (var item in bloodRequests)
                    {
                        BloodOrderList bloodOrderList = new BloodOrderList();
                        bloodOrderList.requestId = item.RequestId;
                        bloodOrderList.contactPersonName = item.ContactPersonName;
                        bloodOrderList.contactPersonMobileNumber = string.IsNullOrEmpty(item.ContactPersonMobileNumber) ? "" : item.ContactPersonMobileNumber;
                        bloodOrderList.patientName = string.IsNullOrEmpty(item.PatientName) ? "" : item.PatientName;
                        bloodOrderList.hospitalName = string.IsNullOrEmpty(item.HospitalName) ? "" : item.HospitalName;
                        bloodOrderList.bloodGroup= string.IsNullOrEmpty(item.BloodGroup) ? "" : item.BloodGroup;
                        bloodOrderList.productName = string.IsNullOrEmpty(item.ProductName) ? "" : item.ProductName;
                        // if(bloodOrderList.requestId=
                        BloodInterest bloodInterest = this.DBContext.BloodInterest.FirstOrDefault(x => x.FkRequestId == item.RequestId && x.UserId == id);
                       if(bloodInterest!=null)
                        {
                            bloodOrderList.isavilableinterest = "yes";
                        }
                        else
                        {
                            bloodOrderList.isavilableinterest = "no";
                        }
                        response.bloodOrderLists.Add(bloodOrderList);

                    }
                    response.Message = "All Blood Order fetched successfully";
                    response.Success = true;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 0;
                }
                else
                {
                    response.Message = "Blood Order list not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "GetAllBloodOrder Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }



        public AddBloodDonateInterestResponse AddBloodDonateInterest(AddBloodDonateInterestRequest request)
        {
            AddBloodDonateInterestResponse response = new AddBloodDonateInterestResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName.Contains(UserName));
                if (userProfileRecord != null)
                {
                    BloodRequest bloodRequest = this.DBContext.BloodRequest.FirstOrDefault(x => x.RequestId == request.requestId);
                    if (bloodRequest != null)
                    {
                        BloodInterest interest = new BloodInterest();
                        interest.UserId = userProfileRecord.Id;
                        interest.FkRequestId = request.requestId;
                        interest.InterestDate= GetLocalDateTime();
                        this.DBContext.BloodInterest.Add(interest);
                        int affectedRows = this.DBContext.SaveChanges();
                        response.Success = affectedRows == 1;
                        if (response.Success)
                        {
                            response.Message = "Add Blood Donate Interest successfully";
                            response.Success = true;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "Failed to Add Blood Donate Interest";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }

                    }
                    else
                    {
                        response.Message = "Request not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;

                    }

                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "AddBloodDonorInterest Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }


        public UpdateDonorProfileResponse UpdateDonorProfile(UpdateDonorProfileRequest request)
        {
            UpdateDonorProfileResponse response = new UpdateDonorProfileResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName.Contains(UserName));
                if (userProfileRecord != null)
                {
                    string PostImageUrlAwsS3 = "";
                    string PostImageBase64 = request.sign;
                    if (StringExtensions.HasValue(PostImageBase64))
                    {
                        string BucketKeyPosts = s3Repository.s3Settings.Value.BucketChildKeyPosts;
                        UploadPhotoModel uploadPhotoResponse = s3Repository.UploadObject(
                            s3Repository.s3Settings,
                            PostImageBase64,
                            BucketKeyPosts).Result;
                        PostImageUrlAwsS3 = uploadPhotoResponse.FileName;
                    }

                    Donor donor = this.DBContext.Donor.FirstOrDefault(x => x.UserId == userProfileRecord.Id);
                    if (donor != null)
                    {
                        donor.DonorName = string.IsNullOrEmpty(request.name) ? "" : request.name;
                        donor.Address = string.IsNullOrEmpty(request.address) ? "" : request.address;
                        donor.Contact = string.IsNullOrEmpty(request.contactNo) ? "" : request.contactNo;
                        donor.Email = string.IsNullOrEmpty(request.email) ? "" : request.email;
                        donor.Age = request.age;
                        donor.Dob = request.dob;
                        donor.BloodGroup= string.IsNullOrEmpty(request.bloodGroup) ? "" : request.bloodGroup;
                        donor.Gender = string.IsNullOrEmpty(request.gender) ? "" : request.gender;
                        donor.Weight = request.weight;
                        donor.Height = string.IsNullOrEmpty(request.height) ? "" : request.height;
                        donor.Sign = PostImageUrlAwsS3;
                        int affectedRows = this.DBContext.SaveChanges();
                       // response.Success = affectedRows == 1;
                        
                         response.Message = "Update Profile successfully";
                         response.Success = true;
                         response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                         response.ResponseCode = 0;
                        

                    }
                    else
                    {
                        response.Message = "Donor Detils  not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;

                    }

                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "UpdateDonorProfile Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }





        protected override void Disposing()
        {
            this.DBContext.Dispose();
        }
    }
}
