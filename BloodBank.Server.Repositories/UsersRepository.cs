﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using BloodBank.Server.Identity;
using System;
using System.Net;
using BloodBank.Server.Entities;
using Amazon.S3;
using Microsoft.Extensions.Options;
using BloodBank.Server.Messages;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using BloodBank.Server.Extensions;

namespace BloodBank.Server.Repositories
{
    public class UsersRepository : BaseRepository
    {
        private readonly UserManager<AlcoholUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IPasswordHasher<AlcoholUser> passwordHasher;
        private readonly IConfiguration configuration;
        private BloodBankDBContext DBContext;
        private S3Repository s3Repository;
        private String fromEmailAddress = "support@beerustogether.com";
        private String smtp_Username = "AKIARC6MAVW4ZGMB5HPL";
        private String smtp_Password = "BDd5RnDXco1XPIDfUUZBTMbtaAbXk44RpUJJGpN7MlR0";

        public UsersRepository(BloodBankDBContext dbContext,
                              UserManager<AlcoholUser> userManager,
                              RoleManager<IdentityRole> roleManager,
                              IPasswordHasher<AlcoholUser> passwordHasher,
                              IConfiguration configuration,
                              S3Repository s3Repository)
        {
            this.DBContext = dbContext;
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.passwordHasher = passwordHasher;
            this.configuration = configuration;
            this.s3Repository = s3Repository;
        }

        public CreateUserResponse CreateUser(CreateUserRequest request)
        {
            string emailId = request.EmailId;
            string password = request.Password;
            string firstName = request.FirstName;
            string lastName = request.LastName;
            string loginType = request.LoginType;
            string role = "Donor";
            CreateUserResponse response = null;
            AlcoholUser alcoholUser = null;

            try
            {
                //Check if user exists using emailId, if yes then return 409:Conflicts
                alcoholUser = this.userManager.FindByNameAsync(emailId).Result;
                if (alcoholUser != null && StringExtensions.HasValue(alcoholUser.UserName))
                {
                    return new CreateUserResponse
                    {
                        Success = false,
                        Message = "Email Id already exists.",
                        ResponseCode = 1,
                        HttpStatusCode = System.Net.HttpStatusCode.OK
                    };
                }


                if (request.LoginType.ToLower() == "donor")
                {
                    // Morph into AlcoholUser            
                    alcoholUser = new AlcoholUser
                    {
                        UserName = emailId,
                        Email = emailId,
                        FirstName = firstName,
                        LastName = lastName,
                        PhoneNumber = request.MobileNo,
                        IsDelete = 0,

                        PasswordHash = password,
                        NormalizedEmail = emailId.ToUpper(),
                        NormalizedUserName = emailId.ToUpper(),
                        InitialPasswordChanged = false,
                        Logintype = loginType,


                    };
                    this.DBContext.Add(alcoholUser);

                    int affectedRows = this.DBContext.SaveChanges();

                    if (affectedRows == 1)
                    {
                        Donor donor = new Donor();
                        donor.DonorName = firstName + " " + lastName;
                        donor.Address = " ";
                        donor.DonationDate = DateTime.Now;
                        donor.Dob = DateTime.Now;
                        donor.Contact = request.MobileNo;
                        donor.Email = emailId;
                        donor.Age = 0;
                        donor.Gender = "";
                        donor.Weight = 0;
                        donor.Height = "";
                        donor.BloodGroup = "";
                        donor.Bp = "";
                        donor.Sign = "";
                        donor.UserId = alcoholUser.Id;
                        this.DBContext.Donor.Add(donor);

                        int affectedRows1 = this.DBContext.SaveChanges();

                        if (affectedRows1 == 1)
                        {
                            _ = this.userManager.AddToRoleAsync(alcoholUser, role).Result;

                            response = new CreateUserResponse();
                            response.userName = alcoholUser.UserName;
                            response.password = alcoholUser.PasswordHash;
                            response.Message = $"Donor Created {firstName + " " + lastName}";
                            response.Success = true;
                            response.HttpStatusCode = HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "failed to add details Donor ";
                            response.Success = false;
                            response.HttpStatusCode = HttpStatusCode.OK;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        //User Not created
                        response.Message = "User Not created ";
                        response.Success = false;
                        response.HttpStatusCode = HttpStatusCode.OK;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Please Provide Valid login Type";
                    response.Success = true;
                    response.HttpStatusCode = HttpStatusCode.OK;
                    response.ResponseCode = 0;
                }
            }
            catch (Exception ex)
            {
                response.Message = "create user Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public ChangePasswordResponse ChangePassword(ChangePasswordRequest request)
        {
            ChangePasswordResponse response = new ChangePasswordResponse();
            try
            {
                response.newPassword = "Test@123";
                response.Message = "Password changed successfully.";
                response.Success = true;
                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.Message = "ChangePassword Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
            }
            return response;
        }
        protected override void Disposing()
        {
            this.userManager.Dispose();
            this.roleManager.Dispose();
        }
    }
}
