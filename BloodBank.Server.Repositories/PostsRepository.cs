﻿using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using BloodBank.Server.Identity;
using BloodBank.Server.Entities;
using BloodBank.Server.Extensions;

using BloodBank.Server.Logging;
using BloodBank.Server.Messages;
using BloodBank.Server.Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BloodBank.Server.Repositories
{
    public class PostsRepository : BaseRepository
    {
        private BloodBankDBContext dbContext;
        private UserManager<AlcoholUser> userManager;
        private S3Repository s3Repository;
        
        public PostsRepository(
            BloodBankDBContext dbContext,
            UserManager<AlcoholUser> userManager,
            S3Repository s3Repository)
        {
            this.dbContext = dbContext;
            this.userManager = userManager;
            this.s3Repository = s3Repository;
        }

       

        protected override void Disposing()
        {
            this.dbContext.Dispose();
        }
    }
}
