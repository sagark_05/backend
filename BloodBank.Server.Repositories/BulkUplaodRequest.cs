﻿using BloodBank.Server.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Repositories
{
    public class BulkUplaodRequest : BaseRequest
    {
        public string startNumber { get; set; }
        public string endNumber { get; set; }
        //  public string[] component { get; set; }

        public string bloodbankId { get; set; }
        public bool isBagDiscard { get; set; }
        public string discardBag { get; set; }
        public string ifOtherRemark { get; set; }

      

        public DateTime collectionDate { get; set; }

        public string solution { get; set; }    //added by sk on 11may2021 

        public List<componantDetails> componant { get; set; }

        public BulkUplaodRequest()
        {
            this.componant = new List<componantDetails>();
        }
    }
}
