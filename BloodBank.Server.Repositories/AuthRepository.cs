﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using BloodBank.Server.Entities;
using BloodBank.Server.Identity;
using BloodBank.Server.Messages;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Net;
using TimeZoneConverter;


namespace BloodBank.Server.Repositories
{
    public class AuthRepository : BaseRepository
    {
        private readonly UserManager<AlcoholUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IPasswordHasher<AlcoholUser> passwordHasher;
        private readonly IConfiguration configuration;
        private BloodBankDBContext DBContext;
        public string sResponse;
        public AuthRepository(BloodBankDBContext dbContext,
                              UserManager<AlcoholUser> userManager,
                              RoleManager<IdentityRole> roleManager,
                              IPasswordHasher<AlcoholUser> passwordHasher,
                              IConfiguration configuration)
        {
            this.DBContext = dbContext;
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.passwordHasher = passwordHasher;
            this.configuration = configuration;
        }

        public LoginResponse Login(LoginRequest request)
        {
            LoginResponse response = null;
            AlcoholUser user = userManager.FindByNameAsync(request.Username).Result;
            if (user == null)
            {
                return LoginFailedforInccorectUserName(request);
            }
            //if (CryptoEngine.DecryptString(user.PasswordHash) != request.Password)
            //{
            //    return LoginFailedforIncorrectPassword(request);
            //}
            if (user.PasswordHash != request.Password)
            {
                return LoginFailedforIncorrectPassword(request);
            }
            if (user.IsDelete == 1)
            {
                return LoginFailedforDeleteAccount(request);
            }
            //if (user.EmailConfirmed == false)
            //{
            //    return PleaseVerifyMail(request);
            //}
            user.DeviceId = request.DeviceId;
            int affectedRows = this.DBContext.SaveChanges();

            return LoginSucceed(user);
        }
        private LoginResponse LoginSucceed(AlcoholUser user)
        {
            IEnumerable<Claim> claims = this.GetClaims(user);
            LoginResponse response = null;

            // TODO: Use RNGCryptoProvider to create a random key.
            // TODO: Then store it in SecretManager or Environment variables
            // TODO: Not in configuration file
            var secret = configuration["Tokens:Key"];
            var issuer = configuration["Tokens:Issuer"];
            var audience = configuration["Tokens:Audience"];

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            // create token
            var token = new JwtSecurityToken(
                issuer: issuer,
                audience: audience,
                claims: claims,
                expires: DateTime.UtcNow.AddDays(90),
               signingCredentials: creds
            );

            // All Good, if we can till now!
            response = new LoginResponse();




            string Id = user.Id;

            response.Id = Id;
            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
            response.Message = "Login Successful";
            response.Token = new JwtSecurityTokenHandler().WriteToken(token);
            response.ValidTo = token.ValidTo;
            var roles = this.userManager.GetRolesAsync(user).Result;
            response.roleName = roles.FirstOrDefault();
            response.Success = true;
            return response;

        }

        private LoginResponse LoginFailed(LoginRequest request)
        {
            return new LoginResponse
            {
                Success = false,
                Message = "Login Failed",
                HttpStatusCode = System.Net.HttpStatusCode.BadRequest,
                ResponseCode = -1,
                Token = ""
            };
        }

        private LoginResponse LoginFailedforIncorrectPassword(LoginRequest request)
        {
            return new LoginResponse
            {
                Success = false,
                Message = "Login Failed ! Please Enter Valid Password",
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                ResponseCode = 1,
                Token = ""
            };
        }

        private LoginResponse LoginFailedforInccorectUserName(LoginRequest request)
        {
            return new LoginResponse
            {
                Success = false,
                Message = "Login Failed ! Please Enter valid User Name",
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                ResponseCode = 1,
                Token = ""
            };
        }

        private LoginResponse LoginFailedforDeleteAccount(LoginRequest request)
        {
            return new LoginResponse
            {
                Success = false,
                Message = "Login Failed ! Account is Deleted",
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                ResponseCode = 1,
                Token = ""
            };
        }

        private LoginResponse PleaseVerifyMail(LoginRequest request)
        {
            return new LoginResponse
            {
                Success = false,
                Message = "Please verify your account",
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                ResponseCode = 1,
                Token = ""
            };
        }

        public virtual IEnumerable<Claim> GetClaims(AlcoholUser user)
        {
            var userClaims = this.userManager.GetClaimsAsync(user).Result;

            // get claims from roles [start]
            // all this to collect claims from roles.
            // there must be out of the box way to do this.
            // todo: find that
            IList<string> roles = this.userManager.GetRolesAsync(user).Result;
            IEnumerable<Claim> claimRolesTotal = new List<Claim>();

            foreach (var role in roles)
            {
                var identityRole = roleManager.FindByNameAsync(role).Result;
                var roleClaims = this.roleManager.GetClaimsAsync(identityRole).Result;
                claimRolesTotal = claimRolesTotal.Union(roleClaims);
            }
            // get claims from roles [end]

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                // required for uniquenes of token
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                // required for User.Identity.Name
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
            }
            // any user specific claims. Note we don't use this way. We should use claims inherited from Roles the user is in
            .Union(userClaims)
            // any claims inherited from Roles
            .Union(claimRolesTotal)
            // this is for User.IsInRole to work. But we don't use this way. We user claim based and Policy based authorization
            // this is only for backward-style compatility
            .Union(roles.Select(role => new Claim(ClaimTypes.Role, role)));

            return claims;
        }

        public BaseResponse Test(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();

            response.Message = "Test API Message";
            response.ResponseCode = 0;
            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
            response.Success = true;
            return response;
        }


        public GetVersionDetailsResponse GetVersionDetails(GetVersionDetailsRequest request)
        {
            GetVersionDetailsResponse response = new GetVersionDetailsResponse();
            try
            {
                VersionDetails versionDetailsRecord = this.DBContext.VersionDetails.FirstOrDefault(x => x.DeviceType == request.DeviceType.ToLower() && x.AppType == request.Apptype.ToLower());
                if (versionDetailsRecord != null)
                {
                    response.VersionId = versionDetailsRecord.VersionId;
                    response.VersionName = versionDetailsRecord.VersionName;
                    response.VersionNumber = versionDetailsRecord.VersionNumber;
                    response.DeviceType = versionDetailsRecord.DeviceType;
                    response.ForceUpdateVerName = versionDetailsRecord.ForceUpdateVerName;
                    response.ForceUpdateVerNumber = versionDetailsRecord.ForceUpdateVerNumber;
                    response.AppType = versionDetailsRecord.AppType;
                    response.Message = "Version details fetch Succesfully";
                    response.Success = true;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 0;
                }
                else
                {
                    response.Message = "Version Details not Found ";
                    response.Success = false;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "GetVersionDetails exception : " + ex.Message;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.Success = false;
            }
            return response;
        }


        public GetOTPResponse GetOTP(GetOTPRequest request)
        {
            GetOTPResponse response = new GetOTPResponse();
            try
            {
                if (request.MobileNumber != null)
                {
                    string userMobile = request.MobileNumber;
                    string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
                    string sRandomOTP = GenerateRandomOTP(4, saAllowedCharacters);
                    string tempdatetime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");//2021-05-03 12:36:33 


                    //  string sURL = "https://otpsms.vision360solutions.in/api/sendhttp.php?authkey=350789A1TsTLhv75feed226P1&mobiles=" + userMobile + "&message=Dear User,OTP for Weeto Mobile App is is " + sRandomOTP + "&sender=Vision&route=4&country=91";

                    //New Sms getway for Otp 
                    string sURL = "https://otpsms.vision360solutions.in/api/sendhttp.php?authkey=350789A1TsTLhv75feed226P1&mobiles=" + userMobile + "&message=Dear%20User,%0AOTP%20for%20login%20to%20WEETO%20app%20is%20" + sRandomOTP + ".%20It%20is%20valid%20for%2015%20minutes.%20Please%20do%20not%20share%20this%20OTP%20with%20anyone.%0A%0ARegards,%0AWEETO%20Team.&sender=WEETOO&route=4&country=91&DLT_TE_ID=1207163488479078043";

                    string sResponse = GetResponse(sURL);


                    // string sResponse= GetResponse(sURL);
                    string TempOTP = sRandomOTP;
                    // AlcoholUser alcoholUser = null;
                    OtpTracking obj1 = new OtpTracking();
                    obj1.MobileNo = request.MobileNumber;
                    obj1.Otp = Convert.ToInt32(TempOTP);
                    obj1.DateTime = GetLocalDateTime();
                    obj1.Status = "pending";

                    this.DBContext.OtpTracking.Add(obj1);
                    int insertedOTPRows = this.DBContext.SaveChanges();
                    response.Success = insertedOTPRows == 1;
                    ////end        

                    response.Message = "OTP Sent successfully!";
                    response.Success = true;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 0;


                }
                else
                {
                    response.Message = "Please Enter Mobile Number";
                    response.Success = false;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "GetOTP exception : " + ex.Message;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
                response.Success = false;
            }
            return response;
        }

        public VerifyOTPResponse VerifyOTP(VerifyOTPRequest request)
        {
            VerifyOTPResponse response = new VerifyOTPResponse();
            string role = "Donor";
            int otp = Convert.ToInt32(request.OTP);
            // CreateUserResponse response = null;
            // AlcoholUser alcoholUser = null;
            try
            {
                //var orderByDescendingResult = from s in this.DBContext.OtpTracking
                //                              orderby s.OtpId descending
                //                              where s.MobileNo == request.MobileNumber && s.Otp==otp
                //                              select s;
                //OtpTracking otpTracking = orderByDescendingResult.FirstOrDefault();
                OtpTracking otpTracking = this.DBContext.OtpTracking.LastOrDefault(x => x.MobileNo == request.MobileNumber);
                if (otpTracking != null && otpTracking.Otp == otp)
                {

                    AlcoholUser alcoholUser = null;

                    //alcoholUser = this.userManager.FindByNameAsync(request.MobileNumber).Result;


                    var userInformation = (from userTbl in this.DBContext.Users
                                           join userRoleTbl in this.DBContext.UserRoles
                                           on userTbl.Id equals userRoleTbl.UserId into eGroup
                                           from userRoleTbl in eGroup.DefaultIfEmpty()
                                           join roleTbl in this.DBContext.Roles
                                            on userRoleTbl.RoleId equals roleTbl.Id into eGroup2
                                           from roleTbl in eGroup2.DefaultIfEmpty()
                                           where roleTbl.Name == "Donor" && userTbl.PhoneNumber == request.MobileNumber
                                           select new
                                           {
                                               _userId = userTbl.Id,
                                               _userRole = roleTbl.Name

                                           }).FirstOrDefault();

                    if (userInformation == null)
                    {
                        alcoholUser = new AlcoholUser
                        {
                            UserName = request.MobileNumber,
                            Email = request.MobileNumber,
                            FirstName = "",
                            LastName = "",
                            PhoneNumber = request.MobileNumber,
                            IsDelete = 0,

                            PasswordHash = "",
                            NormalizedEmail = request.MobileNumber,
                            NormalizedUserName = request.MobileNumber,
                            InitialPasswordChanged = false,
                            Logintype = "donor",


                        };
                        this.DBContext.Add(alcoholUser);

                        int affectedRows = this.DBContext.SaveChanges();

                        if (affectedRows == 1)
                        {
                            Donor donor = new Donor();
                            donor.DonorName = "";
                            donor.Address = " ";
                            donor.DonationDate = DateTime.Now;
                            donor.Dob = DateTime.Now;
                            donor.Contact = request.MobileNumber;
                            donor.Email = "";
                            donor.Age = 0;
                            donor.Gender = "";
                            donor.Weight = 0;
                            donor.Height = "";
                            donor.BloodGroup = "";
                            donor.Bp = "";
                            donor.Sign = "";
                            donor.UserId = alcoholUser.Id;
                            this.DBContext.Donor.Add(donor);

                            int affectedRows1 = this.DBContext.SaveChanges();

                            if (affectedRows1 == 1)
                            {
                                _ = this.userManager.AddToRoleAsync(alcoholUser, role).Result;

                                IEnumerable<Claim> claims = this.GetClaims(alcoholUser);
                                var secret = configuration["Tokens:Key"];
                                var issuer = configuration["Tokens:Issuer"];
                                var audience = configuration["Tokens:Audience"];
                                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));
                                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                                // create token
                                var token = new JwtSecurityToken(
                                    issuer: issuer,
                                    audience: audience,
                                    claims: claims,
                                    expires: DateTime.UtcNow.AddDays(90),
                                    signingCredentials: creds
                                );


                                response.UserId = alcoholUser.Id;
                                // response.fullName = alcoholUser.FirstName + " " + alcoholUser.LastName;
                                response.Role = userManager.GetRolesAsync(alcoholUser).Result.FirstOrDefault();
                                response.Token = new JwtSecurityTokenHandler().WriteToken(token);
                                response.ValidTo = token.ValidTo;

                                response.Message = "  Verfied SucessFully";
                                response.Success = true;
                                response.HttpStatusCode = HttpStatusCode.OK;
                                response.ResponseCode = 0;
                            }



                        }
                    }
                    else
                    {
                        alcoholUser = userManager.FindByIdAsync(userInformation._userId).Result;

                        IEnumerable<Claim> claims = this.GetClaims(alcoholUser);
                        var secret = configuration["Tokens:Key"];
                        var issuer = configuration["Tokens:Issuer"];
                        var audience = configuration["Tokens:Audience"];
                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));
                        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                        // create token
                        var token = new JwtSecurityToken(
                            issuer: issuer,
                            audience: audience,
                            claims: claims,
                            expires: DateTime.UtcNow.AddDays(90),
                            signingCredentials: creds
                        );


                        response.UserId = alcoholUser.Id;
                        // response.fullName = alcoholUser.FirstName + " " + alcoholUser.LastName;
                        response.Role = userManager.GetRolesAsync(alcoholUser).Result.FirstOrDefault();
                        response.Token = new JwtSecurityTokenHandler().WriteToken(token);
                        response.ValidTo = token.ValidTo;
                        response.Message = "Verify Sucessfully!";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;


                    }
                    //response.Message = "Otp Vefied successfully";
                    //response.Success = true;
                    //response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    //response.ResponseCode = 0;

                }
                else
                {
                    response.Message = "OTP Is Not Valid";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }


            }
            catch (Exception ex)
            {
                response.Message = "VerifyOTP exception : " + ex.Message;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.Success = false;
            }
            return response;
        }




        public DateTime GetLocalDateTime()
        {
            DateTime UtcNowTime = DateTime.UtcNow;
            //DateTime localDateTime =TimeZoneInfo.ConvertTimeFromUtc(UtcNowTime, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time")); //for converting to IST
            //DateTime localDateTime = TimeZoneInfo.ConvertTimeFromUtc(UtcNowTime, TimeZoneInfo.FindSystemTimeZoneById("Asia/Calcutta")); //for converting to IST
            DateTime localDateTime = TimeZoneInfo.ConvertTimeFromUtc(UtcNowTime, TZConvert.GetTimeZoneInfo("India Standard Time"));
            return localDateTime;
        }


        private string GenerateRandomOTP(int iOTPLength, string[] saAllowedCharacters)
        {
            string sOTP = String.Empty;
            string sTempChars = String.Empty;
            Random rand = new Random();
            for (int i = 0; i < iOTPLength; i++)
            {
                int p = rand.Next(0, saAllowedCharacters.Length);
                sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];
                sOTP += sTempChars;
            }
            return sOTP;
        }

        public string GetResponse(string sURL)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURL);
            request.MaximumAutomaticRedirections = 4;
            request.Credentials = CredentialCache.DefaultCredentials;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string sRes = readStream.ReadToEnd();
            }
            catch (Exception ex)
            {

            }
            return sResponse;
        }


        protected override void Disposing()
        {
            this.userManager.Dispose();
            this.roleManager.Dispose();
        }
    }
}
