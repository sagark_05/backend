﻿using BloodBank.Server.Entities;
using BloodBank.Server.Extensions;
using BloodBank.Server.Identity;
using BloodBank.Server.Messages;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;
using Nest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using TimeZoneConverter;
using System.Text.RegularExpressions;

namespace BloodBank.Server.Repositories
{
    public class StockRepository : BaseRepository
    {
        private BloodBankDBContext DBContext;
        private S3Repository s3Repository;
        private string sResponse;
        private readonly UserManager<AlcoholUser> userManager;

        public StockRepository(BloodBankDBContext dbContext,
                               UserManager<AlcoholUser> userManager,
                               S3Repository s3Repository)
        {
            this.DBContext = dbContext;
            this.userManager = userManager;
            this.s3Repository = s3Repository;
        }



        public GetUserMenuListResponse GetUserMenuList(GetUserMenuListRequest request)
        {
            GetUserMenuListResponse response = new GetUserMenuListResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    List<Usermainmenumapping> usermainmenumappings = this.DBContext.Usermainmenumapping.Where(x => x.UserId == userProfileRecord.Id && x.PlatformType.ToLower() == "app").ToList();
                    if(usermainmenumappings!=null &&usermainmenumappings.Count>0)
                    {
                        foreach (var item in usermainmenumappings)
                        {
                            MainMenuInfo mainMenuInfo = new MainMenuInfo();
                            mainMenuInfo.mainMenuId = (int)item.FkMainMenuId;
                            mainMenuInfo.mainMenuName = this.DBContext.Mainmenu.FirstOrDefault(x => x.MainMenuId == item.FkMainMenuId && x.PlatformType.ToLower() == "app").MainMenuName;

                            List<Usersubmenumapping> usersubmenumappings = (from submenu in this.DBContext.Submenu
                                                                            join usersubmenumapping in this.DBContext.Usersubmenumapping on submenu.SubMenuId equals usersubmenumapping.FkSubMenuId
                                                                            where usersubmenumapping.UserId == userProfileRecord.Id &&usersubmenumapping.PlatformType.ToLower()=="app" &&submenu.PlatformType.ToLower()=="app" &&submenu.FkMainMenuId==(int)item.FkMainMenuId
                                                                            select usersubmenumapping)
                                   .ToList();

                            if (usersubmenumappings != null & usersubmenumappings.Count > 0)
                            {
                                foreach (var sumenuitem in usersubmenumappings)
                                {
                                    SubMenuInfo subMenuInfo = new SubMenuInfo();
                                    subMenuInfo.subMenuId =(int)sumenuitem.FkSubMenuId;
                                    subMenuInfo.subMenuName = this.DBContext.Submenu.FirstOrDefault(x => x.SubMenuId == sumenuitem.FkSubMenuId).SubMenuName;
                                    mainMenuInfo.subMenuInfos.Add(subMenuInfo);

                                }
                            }
                            response.mainMenuInfos.Add(mainMenuInfo);
                        }

                    }

                        response.Message = "Usermenu fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
  
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "GetUserMenuList Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;

        }

        public UserBloodbankListResponse UserBloodbankList(UserBloodbankListRequest request)
        {
            UserBloodbankListResponse response = new UserBloodbankListResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    List<UsersbloodbankMaster> usersbloodbankMaster = this.DBContext.UsersbloodbankMaster.Where(x => x.UserId == userProfileRecord.Id).ToList();
                    if (usersbloodbankMaster != null && usersbloodbankMaster.Count > 0)
                    {
                        foreach (var item in usersbloodbankMaster)
                        {
                            UserBloodbankListInfo userBloodbankListInfo = new UserBloodbankListInfo();
                            userBloodbankListInfo.bloodbankId = item.FkBloodBankId;
                            userBloodbankListInfo.bloodbankName = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == item.FkBloodBankId).BloodbankName;
                            userBloodbankListInfo.bloodbankLogo = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == item.FkBloodBankId).Logo;
                            response.userBloodbankListInfos.Add(userBloodbankListInfo);
                        }
                        response.Message = "UserBloodbankList fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }

                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "UserBloodbankList Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }



        public AddStockResponse AddStock(AddStockRequest request)
        {

            AddStockResponse response = new AddStockResponse();
            try
            {
                string _bloodbankId = request.bloodbankId;
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == _bloodbankId);
                    if (bloodbankMaster != null)
                    {
                        bool updateFlag = false;
                        List<StockDetails> stockDetailsObjectList = new List<StockDetails>();
                        foreach (var item in request.componant)
                        {
                            string _bagNumber = request.barcodeNumber.ToLower().Trim();
                            int _productId = this.DBContext.ProductMaster.FirstOrDefault(x => x.ProductName.ToLower() == item.componantName.ToLower()).ProductId;


                            StockDetails stockExist = CheckStockExistOrNot(_bagNumber, _productId, _bloodbankId);
                            if (stockExist != null)
                            {
                                // Update
                                stockExist.BagNo = _bagNumber;
                                stockExist.Segment = request.segment;
                                stockExist.ProductId = _productId;
                                //stockExist.CollectionDate = DateTime.Now; //Current DateTime
                                stockExist.CollectionDate = request.collectionDate;// AU Added on 17March2021
                                stockExist.TestDate = request.collectionDate;// AU Added on 15 April 2021
                                                                             //  stockExist.ExpiryDate = GetExpiryDateByProductId(_productId);
                                DateTime collectiondate = request.collectionDate;   //Sk added on 26 april 2021
                                stockExist.Solution = request.solution.ToLower();
                                string solution = request.solution;
                                stockExist.ExpiryDate = GetExpiryDateByProductId(_productId, collectiondate, solution);  //Sk added on 26 april 2021 using collection date achive expiydate


                                int updatedStockRows = this.DBContext.SaveChanges();
                                updateFlag = updatedStockRows == 1;
                            }
                            else
                            {
                                // Insert
                                StockDetails stockDetailsObject = new StockDetails();
                                stockDetailsObject.BagNo = _bagNumber;
                                stockDetailsObject.ProductId = _productId;
                                stockDetailsObject.BloodGroup = "";
                                stockDetailsObject.Segment = request.segment;
                                //stockDetailsObject.CollectionDate = GetLocalDateTime();// AU Added on 25Jan2021
                                stockDetailsObject.CollectionDate = request.collectionDate;// AU Added on 01Feb2021
                                stockDetailsObject.Solution = request.solution.ToLower();         //added by sk on 11May2021
                                string solution = request.solution.ToLower();
                                DateTime collectiondate = request.collectionDate;
                                stockDetailsObject.ExpiryDate = GetExpiryDateByProductId(_productId, collectiondate, solution);
                                stockDetailsObject.TestResult = request.isBagDiscard.ToString().ToLower();
                                stockDetailsObject.DiscardReason = request.isBagDiscard == true ? request.discardBag : "";

                                //stockDetailsObject.TestDate = stockDetailsObject.CollectionDate;//GetLocalDateTime();// AU Added on 25Jan2021
                                stockDetailsObject.TestDate = request.collectionDate;// AU Added on 01Feb2021

                                stockDetailsObject.Remark = request.discardBag.Trim().ToLower() == "other" ? request.ifOtherRemark : "";
                                stockDetailsObject.IssueFlag = request.isBagDiscard == true ? "unavailable" : "available";
                                stockDetailsObject.Volume = item.volume;
                                stockDetailsObject.CollectedBy = userProfileRecord.UserName;
                                stockDetailsObject.TestedBy = "";
                                stockDetailsObject.FkBloodbankId = request.bloodbankId;
                                stockDetailsObjectList.Add(stockDetailsObject);
                            }
                        }

                        if (updateFlag)
                        {
                            response.Success = true;
                        }
                        if (stockDetailsObjectList.Count > 0)
                        {
                            this.DBContext.StockDetails.AddRange(stockDetailsObjectList);
                            int affectedStockRows = this.DBContext.SaveChanges();
                            response.Success = affectedStockRows >= 1;
                        }


                        if (response.Success)
                        {
                            response.Message = "Stocks added/updated successfully";
                            response.Success = true;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "Failed to add stocks";
                            response.Success = false;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "BloodBank Not Found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }


            }
            catch (Exception ex)
            {
                response.Message = "AddStock Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        // The "CheckStockExistOrNot" function is used in AddStock & BulkUplaod APIs.
        public StockDetails CheckStockExistOrNot(string _bagNumber, int _productId, string _bloodbankId)
        {
            StockDetails stockExist = this.DBContext.StockDetails.FirstOrDefault(x => x.BagNo.ToLower() == _bagNumber.ToLower() && x.ProductId == _productId && x.FkBloodbankId == _bloodbankId);
            return stockExist;
        }

      
        public DateTime GetExpiryDateByProductId(int _productId, DateTime collectiondate, string solution)
        {

            DateTime _expiryDate = new DateTime();
            if (_productId == 1 || _productId == 4)
            {
                // PCV ProductId is 1 and WB ProductId is 4
                //_expiryDate = DateTime.Now.AddDays(35);
                if (solution.ToLower() == "plain")
                {
                    _expiryDate = collectiondate.AddDays(35);
                }
                else if (solution.ToLower() == "sagm")
                {
                    _expiryDate = collectiondate.AddDays(42);
                }
                // _expiryDate = GetLocalDateTime().AddDays(35);
            }
            else if (_productId == 2)
            {
                // FFP ProductId is 2
                //_expiryDate = DateTime.Now.AddYears(1);
                // _expiryDate = GetLocalDateTime().AddYears(1);
                //_expiryDate = collectiondate.AddYears(1);    /added by sk 8 may 2021
                _expiryDate = collectiondate.AddYears(1).AddDays(-1);
            }
            else if (_productId == 3)
            {
                // RDP ProductId is 3
                //_expiryDate = DateTime.Now.AddDays(7);
                // _expiryDate = GetLocalDateTime().AddDays(7);
                //  _expiryDate = collectiondate.AddDays(7);  //added by sk 8 may 2021
                _expiryDate = collectiondate.AddDays(5);
            }
            else if (_productId == 5)
            {
                // SDP ProductId is 5 
                //_expiryDate = DateTime.Now.AddDays(7);
                // _expiryDate = GetLocalDateTime().AddDays(7);
                _expiryDate = collectiondate.AddDays(5);
            }
            else if (_productId == 6)
            {
                //and Plasma ProductId is 6
                //_expiryDate = DateTime.Now.AddYears(1);
                //_expiryDate = GetLocalDateTime().AddYears(1);
                _expiryDate = collectiondate.AddYears(1).AddDays(-1);

            }
            return _expiryDate;
        }

        public BulkUplaodResponse BulkUplaod(BulkUplaodRequest request)
        {
            BulkUplaodResponse response = new BulkUplaodResponse();
            try
            {
                string _bloodbankId = request.bloodbankId;
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == _bloodbankId);
                    if (bloodbankMaster != null)
                    {
                        string startnumber = "";
                        string endnumber = "";
                        var prefixvalue = "";
                        int charcount = 0;
                        charcount = Regex.Matches(request.startNumber, @"[a-zA-Z]").Count;
                        if (charcount > 0)
                        {
                            var pattern = new Regex("(.*?)(\\d+$)");
                            var match = pattern.Match(request.startNumber);
                            var match2 = pattern.Match(request.endNumber);
                            prefixvalue = match.Groups[1].Value;
                            startnumber = match.Groups[2].Value;
                            endnumber = match2.Groups[2].Value;
                        }
                        else
                        {
                            startnumber = request.startNumber;
                            endnumber = request.endNumber; 
                        }
                        int count = 1; 

                        bool updateFlag = false;
                        int _endNumber = Convert.ToInt32(endnumber);
                        List<StockDetails> stockDetailsObjectList = new List<StockDetails>();

                        for (int _statrtNumber = Convert.ToInt32(startnumber); _statrtNumber <= _endNumber; _statrtNumber++)
                        {
                            //only 100 bag can insert
                            if (count <= 100)
                            {
                                count++;
                                foreach (var item in request.componant)
                                {
                                    string _bagNumber = "";
                                    if (charcount > 0)
                                        _bagNumber = (prefixvalue + _statrtNumber).ToString().Trim();
                                    else
                                        _bagNumber = _statrtNumber.ToString().Trim();

                                    // string _bagNumber = _statrtNumber.ToString().ToLower().Trim();
                                    int _productId = this.DBContext.ProductMaster.FirstOrDefault(x => x.ProductName.ToLower() == item.componantName.ToLower()).ProductId;

                                    StockDetails stockExist = CheckStockExistOrNot(_bagNumber, _productId, _bloodbankId);
                                    if (stockExist != null)
                                    {
                                        // Update
                                        stockExist.BagNo = _bagNumber;
                                        stockExist.ProductId = _productId;
                                        //stockExist.CollectionDate = DateTime.Now; //Current DateTime
                                        // stockExist.CollectionDate = GetLocalDateTime();
                                        stockExist.CollectionDate = request.collectionDate; //added by SK on 27/04/2021
                                        stockExist.TestDate = request.collectionDate;      //added by sk on 28/04/2021

                                        DateTime collectiondate = request.collectionDate;   //Sk added on 26 april 2021
                                        stockExist.Solution = request.solution.ToLower();   //added by sk on 11may2021
                                        string solution = request.solution;                  //added by sk on 11may2021
                                        stockExist.ExpiryDate = GetExpiryDateByProductId(_productId, collectiondate, solution);  //Sk added on 26 april 2021 using collection date achive expiydate



                                        int updatedStockRows = this.DBContext.SaveChanges();
                                        updateFlag = updatedStockRows == 1;
                                    }
                                    else
                                    {
                                        // Insert
                                        StockDetails stockDetailsObject = new StockDetails();
                                        stockDetailsObject.BagNo = _bagNumber;
                                        stockDetailsObject.ProductId = _productId;
                                        stockDetailsObject.BloodGroup = "";
                                        //stockDetailsObject.CollectionDate = DateTime.Now; //Current DateTime
                                        //  stockDetailsObject.CollectionDate = GetLocalDateTime();
                                        stockDetailsObject.CollectionDate = request.collectionDate;   //added by sk on 27/04/2021
                                        stockDetailsObject.Solution = request.solution.ToLower();     //added by sk on 11may2021
                                        DateTime collectiondate = request.collectionDate;             //added by sk on 27/04/2021
                                        string solution = request.solution;                           //added by sk on 11may2021
                                        stockDetailsObject.ExpiryDate = GetExpiryDateByProductId(_productId, collectiondate, solution);
                                        stockDetailsObject.TestResult = request.isBagDiscard.ToString().ToLower();      //added by sk on 27/04/2021

                                        stockDetailsObject.DiscardReason = request.isBagDiscard == true ? request.discardBag : "";    //added by sk on 27/04/2021
                                                                                                                                      //  stockDetailsObject.TestDate = null;
                                        stockDetailsObject.TestDate = request.collectionDate;          //added by sk on 27/04/2021
                                        stockDetailsObject.Remark = request.discardBag.Trim().ToLower() == "other" ? request.ifOtherRemark : "";   //added by sk on 27/04/2021
                                        stockDetailsObject.IssueFlag = request.isBagDiscard == true ? "unavailable" : "available";                 //added by sk on 27/04/2021
                                        stockDetailsObject.Volume = item.volume;        //added by sk on 27/04/2021
                                        stockDetailsObject.CollectedBy = userProfileRecord.UserName;          //added by sk on 27/04/2021
                                        stockDetailsObject.TestedBy = "";
                                        stockDetailsObject.FkBloodbankId = request.bloodbankId;
                                        stockDetailsObjectList.Add(stockDetailsObject);
                                    }
                                }//End of Foreach loop
                            }//end of if loop
                        }//End of For loop


                        if (updateFlag)
                        {
                            response.Success = true;
                        }
                        if (stockDetailsObjectList.Count > 0)
                        {
                            this.DBContext.StockDetails.AddRange(stockDetailsObjectList);
                            int affectedSurveyRows = this.DBContext.SaveChanges();
                            response.Success = affectedSurveyRows >= 1;
                        }


                        if (response.Success)
                        {
                            response.Message = "BulkUplaod stocks added/updated successfully";
                            response.Success = true;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "Failed to add/update Bulkupload stocks";
                            response.Success = false;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "Bloodbank Not Found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }

                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }


            }
            catch (Exception ex)
            {
                response.Message = "BulkUplaod Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public AddTestingResponse AddTesting(AddTestingRequest request)
        {
            AddTestingResponse response = new AddTestingResponse();
            try
            {
                string bloodbankid = request.bloodbankId;
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankid);
                    if (bloodbankMaster != null)
                    {
                        List<StockDetails> stockDetailsList = this.DBContext.StockDetails.Where(x => x.BagNo.ToLower().Trim() == request.barcodeNumber.ToLower().Trim() && x.FkBloodbankId == bloodbankid).ToList();
                        if (stockDetailsList != null && stockDetailsList.Count > 0)
                        {
                            //stockDetailsList.ForEach(x =>
                            //{
                            //    x.BloodGroup = request.bloodGroup;
                            //    x.TestResult = request.discardBag.ToString().ToLower().Trim();
                            //    x.DiscardReason = request.discardBag == true ? request.reason : null;
                            //    x.TestDate = DateTime.Now;
                            //    x.Remark = request.remark;
                            //});

                            stockDetailsList.ForEach(x =>
                            {
                                x.BloodGroup = request.bloodGroup;
                                x.TestResult = request.isdiscardBag.ToString().ToLower().Trim();
                                x.DiscardReason = request.isdiscardBag == true ? request.discardBag : "";
                                //x.TestDate = DateTime.Now;
                                //x.TestDate = GetLocalDateTime();//AU commented this line on 30Jan2021
                                x.IssueFlag = request.isdiscardBag == true ? "unavailable" : "available";// Added on 11Jan2021
                                x.Remark = request.discardBag.Trim().ToLower() == "other" ? request.otherRemark : "";
                                x.TestedBy = userProfileRecord.UserName;// Added on 02Jan2021
                            });

                            int affectedRows = this.DBContext.SaveChanges();
                            response.Success = affectedRows >= 1;
                            if (response.Success)
                            {
                                //bool b = request.discardBag; 
                                response.Message = "AddTesting data updated successfully";
                                response.Success = true;
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.ResponseCode = 0;
                            }
                            else
                            {
                                response.Message = "Failed to update test data";
                                response.Success = false;
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.ResponseCode = 1;
                            }
                        }
                        else
                        {
                            response.Message = "BagNumber not found";
                            response.Success = false;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "Bloodbank not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "AddTesting Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }



        public GetCallListResponse GetCallList(GetCallListRequest request)
        {
            string bloodbankId = request.bloodbankId;
            GetCallListResponse response = new GetCallListResponse();
            List<CallListDetails> callListDetailsList = new List<CallListDetails>();
            int pageSize = 10;
            DateTime todaydate = GetLocalDateTime();
            string UserName = GetCurrentIdentityUser(request).Name;
            AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
            var roles = userManager.GetRolesAsync(userProfileRecord).Result;
            try
            {
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {

                    if (request.pageNumber >= 1)
                    {

                        List<ScheduleCall> scheduleCallsList = new List<ScheduleCall>();
                        if (!String.IsNullOrEmpty(request.status))
                        {
                            //if (request.status.ToLower() == "all")
                            //{
                            //    scheduleCallsList = this.DBContext.ScheduleCall.ToList();
                            //}
                            //else
                            //{
                            //    scheduleCallsList = this.DBContext.ScheduleCall.Where(x => x.Status.ToLower() == request.status.ToLower()).ToList();
                            //}
                            if (roles.Contains("Serviceboy"))
                            {
                                scheduleCallsList = this.DBContext.ScheduleCall.Where(x => x.Status.ToLower() == request.status.ToLower() && x.AssignedUserId == userProfileRecord.Id && x.FkBloodbankId == bloodbankId && x.CallDateTime >= todaydate.AddDays(-3)).ToList();
                                List<ScheduleCall> notassignedList = this.DBContext.ScheduleCall.Where(x => x.Status.ToLower() == request.status.ToLower() && x.AssignedUserId.ToLower() == "na" && x.FkBloodbankId == bloodbankId &&x.CallDateTime >= todaydate.AddDays(-3)).ToList();
                                scheduleCallsList = scheduleCallsList.Concat(notassignedList).ToList();
                            }
                            else
                            {
                                scheduleCallsList = this.DBContext.ScheduleCall.Where(x => x.Status.ToLower() == request.status.ToLower() && x.FkBloodbankId == bloodbankId && x.CallDateTime >= todaydate.AddDays(-3)).ToList();
                            }
                            scheduleCallsList = scheduleCallsList.OrderByDescending(x => x.CallDateTime).ToList();
                            response.totalCount = scheduleCallsList.Count();
                            scheduleCallsList = scheduleCallsList.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
                            // scheduleCallsList= scheduleCallsList.OrderByDescending(x=>x.CallDateTime).ToList();
                            if (scheduleCallsList != null && scheduleCallsList.Count > 0)
                            {
                                //callListDetailsList = GetCallListDetails(scheduleCallsList);
                                //response.callList = callListDetailsList;
                                foreach (var item in scheduleCallsList)
                                {
                                    CallListDetails callListDetailsObject = new CallListDetails();
                                    callListDetailsObject.callId = item.CallId;
                                    callListDetailsObject.callListDetailsInfos = GetCallDetailsListInfo(item);

                                    response.callList.Add(callListDetailsObject);
                                }
                                response.Message = "CallList fetched successfully";
                                response.Success = true;
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.ResponseCode = 0;
                            }
                            else
                            {
                                response.Message = "Schedule calls not found";
                                response.Success = false;
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.ResponseCode = 1;
                            }
                        }
                        else
                        {
                            response.Message = "Please provide valid status";
                            response.Success = false;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "Page number must be greater than zero(0)";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "BloodBank Not Found";
                    response.Success = false;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "GetCallList Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }


        public List<CallListDetailsInfo> GetCallDetailsListInfo(ScheduleCall scheduleCall)
        {
            List<CallListDetailsInfo> callListDetailsInfos = new List<CallListDetailsInfo>();

            List<CallDetails> callDetailsRecordList = this.DBContext.CallDetails.Where(x => x.FkCallId == scheduleCall.CallId).ToList();
            if (callDetailsRecordList != null && callDetailsRecordList.Count > 0)
            {
                foreach (var item in callDetailsRecordList)
                {
                    CallListDetailsInfo callListDetailsObject = new CallListDetailsInfo();
                    callListDetailsObject.callDetailsId = item.CallDetailsId;// AU Added on 22JAN2021
                    callListDetailsObject.isSuccess = true;
                    callListDetailsObject.requisitionStatus = "";
                    //callListDetailsObject.hospitalName = this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == scheduleCall.CallId).FkHospitalId == 0 ? "" : this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == scheduleCall.CallId).FkHospitalId).HospitalName;
                    if (item.ReqBillType.ToLower() == "officework")
                    {
                        callListDetailsObject.hospitalName = item.Address;
                    }
                    else
                    {
                        callListDetailsObject.hospitalName = item.FkHospitalId == 0 ? "" : this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == item.FkHospitalId).HospitalName;
                    }
                    callListDetailsObject.details = item.CallDetails1;
                    callListDetailsObject.callType = item.ReqBillType;
                    callListDetailsObject.callTime = scheduleCall.CallDateTime;
                    callListDetailsObject.isAccept = scheduleCall.AssignedUserId.ToLower() == "na" ? false : true;
                    callListDetailsObject.status = scheduleCall.Status;
                    callListDetailsObject.patientName = item.PatientName;
                    callListDetailsObject.serviceBoyName = scheduleCall.AssignedUserId.ToLower() == "na" ? "" : scheduleCall.AssignedUserId;
                    callListDetailsObject.createdByUserId = string.IsNullOrEmpty(item.CreatedUserId) ? "" : item.CreatedUserId;
                    callListDetailsObject.createdByUserName = string.IsNullOrEmpty(item.CreatedUserId) ? "" : this.DBContext.Users.FirstOrDefault(x => x.Id == item.CreatedUserId).UserName;
                    callListDetailsObject.assignedUserId = scheduleCall.AssignedUserId.ToLower() == "na" ? "" : scheduleCall.AssignedUserId;
                    callListDetailsObject.assignedUserName = scheduleCall.AssignedUserId.ToLower() == "na" ? "" : this.DBContext.Users.FirstOrDefault(x => x.Id == scheduleCall.AssignedUserId).UserName;
                    callListDetailsObject.patientStatus = string.IsNullOrEmpty(item.PatientStatus) ? "" : item.PatientStatus.ToLower();
                    // The below line fetch the username
                    //createdByUserId = string.IsNullOrEmpty(callDetailsRecord.CreatedUserId) ? "" : this.DBContext.Users.FirstOrDefault(x => x.Id == callDetailsRecord.CreatedUserId).UserName

                    callListDetailsInfos.Add(callListDetailsObject);
                }
            }

            return callListDetailsInfos;
        }

        //public GetCallListResponse GetCallList(GetCallListRequest request)
        //{
        //    GetCallListResponse response = new GetCallListResponse();
        //    List<CallListDetails> callListDetailsList = new List<CallListDetails>();

        //    try
        //    {
        //        List<ScheduleCall> scheduleCallsList;
        //        if (String.IsNullOrEmpty(request.status))
        //        {
        //            scheduleCallsList = this.DBContext.ScheduleCall.Where(x => x.Status.ToLower() == "pending" || x.Status.ToLower() == "inprogress").ToList();
        //            if (scheduleCallsList != null && scheduleCallsList.Count > 0)
        //            {
        //                callListDetailsList = GetCallListDetails(scheduleCallsList);

        //                response.callList = callListDetailsList;
        //                response.Message = "CallList fetched successfully";
        //                response.Success = true;
        //                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //                response.ResponseCode = 0;
        //            }
        //            else
        //            {
        //                response.Message = "Schedule calls not found";
        //                response.Success = false;
        //                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //                response.ResponseCode = 1;
        //            }
        //        }
        //        else 
        //        {
        //            scheduleCallsList = this.DBContext.ScheduleCall.Where(x => x.Status.ToLower() == request.status.ToLower()).ToList();
        //            if (request.date.HasValue)
        //            {
        //                scheduleCallsList = this.DBContext.ScheduleCall.Where(x => x.CallDateTime == request.date).ToList();
        //            }
        //            if(request.hospitalId >= 1)
        //            {
        //                scheduleCallsList = this.DBContext.ScheduleCall.Where(x => x.HospitalId == request.hospitalId).ToList();
        //            }

        //            //Now check scheduleCallsList is null or not.
        //            if (scheduleCallsList != null && scheduleCallsList.Count > 0)
        //            {
        //                callListDetailsList = GetCallListDetails(scheduleCallsList);
        //                response.callList = callListDetailsList;
        //                response.Message = "CallList fetched successfully";
        //                response.Success = true;
        //                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //                response.ResponseCode = 0;
        //            }
        //            else
        //            {
        //                response.Message = "Schedule calls not found";
        //                response.Success = false;
        //                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //                response.ResponseCode = 1;
        //            }
        //        }//End Of Outer ELSE Block
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Message = "GetCallList Exception : " + ex.Message;
        //        response.Success = false;
        //        response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
        //        response.ResponseCode = 1;
        //    }
        //    return response;
        //}

        // The "GetCallListDetails" function is used in "GetCallList" API.
        //public List<CallListDetails> GetCallListDetails(List<ScheduleCall> scheduleCallsList)
        //{
        //    List<CallListDetails> callListDetailsList = new List<CallListDetails>();

        //    foreach (var item in scheduleCallsList)
        //    {
        //        CallDetails callDetailsRecord = this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == item.CallId);
        //        if (callDetailsRecord != null)
        //        {
        //            CallListDetails callListDetailsObject = new CallListDetails
        //            {
        //                //callId = item.CallId,
        //                isSuccess = true,
        //                requisitionStatus = "",
        //                hospitalName = this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == item.CallId).FkHospitalId == 0 ? "" : this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == item.CallId).FkHospitalId).HospitalName,
        //                details = callDetailsRecord.CallDetails1,
        //                callType = callDetailsRecord.ReqBillType,
        //                callTime = item.CallDateTime,
        //                isAccept = item.AssignedUserId.ToLower() == "na" ? false : true,
        //                status = item.Status,
        //                patientName = callDetailsRecord.PatientName,
        //                serviceBoyName = item.AssignedUserId.ToLower() == "na" ? "" : item.AssignedUserId,
        //                createdByUserId = string.IsNullOrEmpty(callDetailsRecord.CreatedUserId) ? "" : callDetailsRecord.CreatedUserId
        //                // The below line fetch the username
        //                //createdByUserId = string.IsNullOrEmpty(callDetailsRecord.CreatedUserId) ? "" : this.DBContext.Users.FirstOrDefault(x => x.Id == callDetailsRecord.CreatedUserId).UserName
        //            };
        //            callListDetailsList.Add(callListDetailsObject);
        //        }
        //    }
        //    return callListDetailsList;
        //}

        //GS API




        public GetCallDetailsResponse GetCallDetails(GetCallDetailsRequest request)
        {
            GetCallDetailsResponse response = new GetCallDetailsResponse();
            List<Quantity> quantityList = new List<Quantity>();

            try
            {

                List<CallDetails> callDetailseRecords = this.DBContext.CallDetails.Where(x => x.FkCallId == request.CallId).ToList();
                if (callDetailseRecords != null && callDetailseRecords.Count > 0)
                {
                    foreach (var item in callDetailseRecords)
                    {
                        GetCallDetailsInfo getCallDetailsResponse = new GetCallDetailsInfo();
                        if (item.ReqBillType.ToLower() == "officework")
                        {
                            getCallDetailsResponse.hospitalName = item.Address;
                            getCallDetailsResponse.CallDetails = item.CallDetails1;
                            getCallDetailsResponse.CallType = item.ReqBillType;
                            getCallDetailsResponse.patientName = "";
                            getCallDetailsResponse.mobileNumber = "";
                            getCallDetailsResponse.IsNat = "";
                            getCallDetailsResponse.Amount = 0;
                            getCallDetailsResponse.patientBloodGroup = "";
                            getCallDetailsResponse.patientGender = "";
                            getCallDetailsResponse.patientAge = "";
                            getCallDetailsResponse.btcharge = "";
                            getCallDetailsResponse.servicecharge = "";
                            getCallDetailsResponse.discount = 0;
                            getCallDetailsResponse.callDetailsId = item.CallDetailsId;
                        }
                        else if (item.ReqBillType.ToLower() == "samplecollection")
                        {
                            getCallDetailsResponse.patientName = item.PatientName;
                            getCallDetailsResponse.mobileNumber = item.PatientContact;
                            getCallDetailsResponse.hospitalName = item.FkHospitalId == 0 ? "" : this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == item.FkHospitalId).HospitalName;
                            getCallDetailsResponse.CallDetails = item.CallDetails1;
                            getCallDetailsResponse.CallType = item.ReqBillType;
                            getCallDetailsResponse.IsNat = "";
                            getCallDetailsResponse.Amount = 0;
                            getCallDetailsResponse.patientBloodGroup = "";
                            getCallDetailsResponse.patientGender = "";
                            getCallDetailsResponse.patientAge = "";
                            getCallDetailsResponse.btcharge = "";
                            getCallDetailsResponse.servicecharge = "";
                            getCallDetailsResponse.discount = 0;
                            getCallDetailsResponse.callDetailsId = item.CallDetailsId;
                        }
                        else if (item.ReqBillType.ToLower() == "issuebag")
                        {
                            Bill billRecord = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.BillReqId);
                            if (billRecord != null)
                            {
                                Quantity QntyObject = new Quantity();
                                QntyObject.pcv = this.DBContext.BillDetails.Where(x => x.FkProductId == 1 && x.FkBillId == item.BillReqId).Count(); //item.ProductId;
                                QntyObject.ffp = this.DBContext.BillDetails.Where(x => x.FkProductId == 2 && x.FkBillId == item.BillReqId).Count(); //item.ProductId;
                                QntyObject.rdp = this.DBContext.BillDetails.Where(x => x.FkProductId == 3 && x.FkBillId == item.BillReqId).Count(); //item.ProductId;
                                QntyObject.wb = this.DBContext.BillDetails.Where(x => x.FkProductId == 4 && x.FkBillId == item.BillReqId).Count(); //item.ProductId;
                                QntyObject.sdp = this.DBContext.BillDetails.Where(x => x.FkProductId == 5 && x.FkBillId == item.BillReqId).Count(); //item.ProductId;
                                QntyObject.plasma = this.DBContext.BillDetails.Where(x => x.FkProductId == 6 && x.FkBillId == item.BillReqId).Count(); //item.ProductId;

                                quantityList.Add(QntyObject);

                                getCallDetailsResponse.quantityRequired = quantityList;
                                getCallDetailsResponse.patientName = item.PatientName;
                                getCallDetailsResponse.mobileNumber = item.PatientContact;
                                getCallDetailsResponse.hospitalName = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == item.FkHospitalId).HospitalName;
                                getCallDetailsResponse.CallDetails = item.CallDetails1;
                                getCallDetailsResponse.CallType = item.ReqBillType;
                                getCallDetailsResponse.Amount = billRecord.NetAmount;
                                getCallDetailsResponse.IsNat = billRecord.IsNat;
                                getCallDetailsResponse.patientBloodGroup = item.BloodGroup;
                                getCallDetailsResponse.patientGender = item.Gender;
                                getCallDetailsResponse.patientAge = item.Age;
                                getCallDetailsResponse.btcharge = billRecord.BtSetCharge;
                                getCallDetailsResponse.servicecharge = billRecord.ServiceCharge;
                                getCallDetailsResponse.discount = (int)billRecord.Discount;
                                getCallDetailsResponse.callDetailsId = item.CallDetailsId;
                            }
                        }

                        response.getCallDetailsInfos.Add(getCallDetailsResponse);
                    }
                    response.Message = "GetCallDetails successfully";
                    response.Success = true;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 0;
                    // ---------------------------------------------------------------------------------------------
                    //if (callDetailseRecord.ReqBillType.ToLower() == "officework")
                    //{
                    //    response.hospitalName = callDetailseRecord.Address;
                    //    response.CallDetails = callDetailseRecord.CallDetails1;
                    //    response.CallType = callDetailseRecord.ReqBillType;
                    //    response.patientName = "";
                    //    response.mobileNumber = "";
                    //    response.IsNat = "";
                    //    response.Amount = 0;
                    //    response.callDetailsId = callDetailseRecord.CallDetailsId;
                    //}
                    //else if (callDetailseRecord.ReqBillType.ToLower() == "samplecollection")
                    //{
                    //    response.patientName = callDetailseRecord.PatientName;
                    //    response.mobileNumber = callDetailseRecord.PatientContact;
                    //    response.hospitalName = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetailseRecord.FkHospitalId).HospitalName;
                    //    response.CallDetails = callDetailseRecord.CallDetails1;
                    //    response.CallType = callDetailseRecord.ReqBillType;
                    //    response.IsNat = "";
                    //    response.Amount = 0;
                    //    /response.callDetailsId = callDetailseRecord.CallDetailsId;
                    //}
                    //else if (callDetailseRecord.ReqBillType.ToLower() == "issuebag")
                    //{
                    //    Bill billRecord = this.DBContext.Bill.FirstOrDefault(x => x.BillId == callDetailseRecord.BillReqId);
                    //    if (billRecord != null)
                    //    {
                    //        Quantity QntyObject = new Quantity();
                    //        QntyObject.pcv = this.DBContext.BillDetails.Where(x => x.FkProductId == 1 && x.FkBillId == callDetailseRecord.BillReqId).Count(); //item.ProductId;
                    //        QntyObject.ffp = this.DBContext.BillDetails.Where(x => x.FkProductId == 2 && x.FkBillId == callDetailseRecord.BillReqId).Count(); //item.ProductId;
                    //        QntyObject.rdp = this.DBContext.BillDetails.Where(x => x.FkProductId == 3 && x.FkBillId == callDetailseRecord.BillReqId).Count(); //item.ProductId;
                    //        QntyObject.wb = this.DBContext.BillDetails.Where(x => x.FkProductId == 4 && x.FkBillId == callDetailseRecord.BillReqId).Count(); //item.ProductId;
                    //        QntyObject.sdp = this.DBContext.BillDetails.Where(x => x.FkProductId == 5 && x.FkBillId == callDetailseRecord.BillReqId).Count(); //item.ProductId;
                    //        QntyObject.plasma = this.DBContext.BillDetails.Where(x => x.FkProductId == 6 && x.FkBillId == callDetailseRecord.BillReqId).Count(); //item.ProductId;

                    //        quantityList.Add(QntyObject);

                    //        response.quantityRequired = quantityList;
                    //        response.patientName = callDetailseRecord.PatientName;
                    //        response.mobileNumber = callDetailseRecord.PatientContact;
                    //        response.hospitalName = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetailseRecord.FkHospitalId).HospitalName;
                    //        response.CallDetails = callDetailseRecord.CallDetails1;
                    //        response.CallType = callDetailseRecord.ReqBillType;
                    //        response.Amount = billRecord.NetAmount;
                    //        response.IsNat = billRecord.IsNat;
                    //        response.patientBloodGroup = callDetailseRecord.BloodGroup;
                    //        response.patientGender = callDetailseRecord.Gender;
                    //        response.patientAge = callDetailseRecord.Age;
                    //        response.callDetailsId = callDetailseRecord.CallDetailsId;
                    //    }
                    //    else
                    //    {
                    //        //bill record not found
                    //        response.Message = "Bill record not found";
                    //        response.Success = false;
                    //        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    //        response.ResponseCode = 1;
                    //    }

                    //}
                    //response.Message = "GetCallDetails successfully";
                    //response.Success = true;
                    //response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    //response.ResponseCode = 0;
                }
                else
                {
                    //call details  not found
                    response.Message = "Call record not found";
                    response.Success = false;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "GetCallDetails Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }


        //public ViewStockResponse ViewStock(ViewStockRequest request)
        //{
        //    ViewStockResponse response = new ViewStockResponse();
        //    List<Stock> stockList = new List<Stock>();

        //    try
        //    {
        //        int _productId = this.DBContext.ProductMaster.FirstOrDefault(x => x.ProductName.ToLower() == request.productType.ToLower()).ProductId;

        //        List<StockDetails> stockDetailsList =
        //            this.DBContext.StockDetails.Where(x => x.BloodGroup.ToLower() == request.bloodGroup.ToLower() && x.ProductId == _productId).ToList();
        //        if (stockDetailsList != null && stockDetailsList.Count > 0)
        //        {
        //            foreach (var item in stockDetailsList.OrderBy(x => x.ExpiryDate))
        //            {
        //                Stock stockObject = new Stock();
        //                stockObject.isSuccess = true;
        //                stockObject.bloodGroup = item.BloodGroup;
        //                stockObject.bagNumber = item.BagNo;
        //                stockObject.productID = item.ProductId.Value;
        //                stockObject.expiryDate = item.ExpiryDate.Value.ToString("dd/MM/yyyy");
        //                //stockObject.collectionDate = item.CollectionDate.Value; (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        //                stockObject.collectionDate = (Int32)(item.CollectionDate.Value.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

        //                stockObject.PCV = _productId == 1 ? 1 : 0;
        //                stockObject.FFP = _productId == 2 ? 1 : 0;
        //                stockObject.RDP = _productId == 3 ? 1 : 0;
        //                stockObject.WB = _productId == 4 ? 1 : 0;
        //                stockObject.SDP = _productId == 5 ? 1 : 0;
        //                stockObject.Plasma = _productId == 6 ? 1 : 0;

        //                stockList.Add(stockObject);
        //            }
        //            response.stockCount = stockList;
        //            response.Message = "ViewStock successfully";
        //            response.Success = true;
        //            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //            response.ResponseCode = 0;
        //        }
        //        else
        //        {
        //            response.Message = "Stocks not found";
        //            response.Success = false;
        //            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //            response.ResponseCode = 1;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Message = "ViewStock Exception : " + ex.Message;
        //        response.Success = false;
        //        response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
        //        response.ResponseCode = 1;
        //    }
        //    return response;
        //}

        public ViewStockResponse ViewStock(ViewStockRequest request)
        {
            string bloodbankId = request.bloodbankId;
            ViewStockResponse response = new ViewStockResponse();
            List<MainStockList> stockList = new List<MainStockList>();
            DateTime todaydate = GetLocalDateTime();
            try
            {
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    List<ProductMaster> productMastersList = this.DBContext.ProductMaster.ToList();
                    if (productMastersList != null && productMastersList.Count > 0)
                    {
                        foreach (var item in productMastersList)
                        {
                            MainStockList stockObject = new MainStockList();
                            stockObject.ProductID = item.ProductId;
                            stockObject.ProductName = item.ProductName;
                            stockObject.ProductCount =
                                this.DBContext.StockDetails.Where(x => x.ProductId == item.ProductId && x.IssueFlag.ToLower() == "available" && x.ExpiryDate.Value.Date >= todaydate.Date && x.FkBloodbankId == bloodbankId).Count();

                            stockList.Add(stockObject);
                        }



                        //        // int _productId = this.DBContext.ProductMaster.FirstOrDefault().ProductId;
                        //        var productIdList = new int?[] { 1, 2, 3, 4, 5, 6};
                        ////List<StockDetails> stockDetailsList =
                        ////    this.DBContext.StockDetails.Where(productIdList.Contains(x=>x.Value)).ToList();
                        //////
                        //List<StockDetails> stockDetailsList =
                        //          this.DBContext.StockDetails.
                        //               GroupBy(x => new { x.ProductId}).Select(x => x.OrderBy(x => x.ProductId,sum).FirstOrDefault()).ToList();


                        ////
                        //if (stockDetailsList != null && stockDetailsList.Count > 0)
                        //{
                        //    foreach (var item in stockDetailsList.OrderBy(x => x.ExpiryDate))
                        //    {
                        //        Stock stockObject = new Stock();
                        //        stockObject.isSuccess = true;
                        //        stockObject.bloodGroup = item.BloodGroup;
                        //        stockObject.bagNumber = item.BagNo;
                        //        stockObject.productID = item.ProductId.Value;
                        //        stockObject.expiryDate = item.ExpiryDate.Value.ToString("dd/MM/yyyy");
                        //        //stockObject.collectionDate = item.CollectionDate.Value; (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        //        stockObject.collectionDate = (Int32)(item.CollectionDate.Value.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

                        //        stockObject.PCV = _productId == 1 ? 1 : 0;
                        //        stockObject.FFP = _productId == 2 ? 1 : 0;
                        //        stockObject.RDP = _productId == 3 ? 1 : 0;
                        //        stockObject.WB = _productId == 4 ? 1 : 0;
                        //        stockObject.SDP = _productId == 5 ? 1 : 0;
                        //        stockObject.Plasma = _productId == 6 ? 1 : 0;

                        //        stockList.Add(stockObject);
                        //    }
                        response.mainStockLists = stockList;
                        response.Message = "ViewStock successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Stocks not found";
                        response.Success = false;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "BloodBank not found";
                    response.Success = false;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "ViewStock Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        //GS API
        public ViewStockbyProductTypeResponse ViewStockbyProductId(ViewStockbyProductTypeRequest request)
        {
            string bloodbankId = request.bloodbankId;
            ViewStockbyProductTypeResponse response = new ViewStockbyProductTypeResponse();
            List<StocksListbyProductType> stockList = new List<StocksListbyProductType>();
            DateTime todaydate = GetLocalDateTime();
            try
            {
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {

                    //List<StockDetails> stockDetailsList = new List<StockDetails>();
                    //if (!string.IsNullOrEmpty(request.issueFlag))
                    //{
                    //    if (request.issueFlag.ToLower() == "available" || request.issueFlag.ToLower() == "unavailable" ||
                    //        request.issueFlag.ToLower() == "expired" || request.issueFlag.ToLower() == "bill_issue")
                    //    {
                    //        stockDetailsList = this.DBContext.StockDetails.Where(x => x.IssueFlag.ToLower() == request.issueFlag.ToLower()).ToList();
                    //    }
                    //}
                    //else
                    //{
                    //    stockDetailsList = this.DBContext.StockDetails.Where(x => x.IssueFlag.ToLower() == "available").ToList();
                    //}
                    List<StockDetails> stockDetailsList = this.DBContext.StockDetails.Where(x => x.FkBloodbankId == bloodbankId).ToList();
                    if (stockDetailsList != null && stockDetailsList.Count > 0)
                    {
                        // 1.Here We applied IssueFlag filter
                        if (!string.IsNullOrEmpty(request.issueFlag))
                        {
                            if (request.issueFlag.ToLower() == "unavailable" || request.issueFlag.ToLower() == "bill_issue")
                            {
                                stockDetailsList = stockDetailsList.Where(x => x.IssueFlag.ToLower() == request.issueFlag.ToLower()).ToList();
                            }
                            else if (request.issueFlag.ToLower() == "available")
                            {
                                stockDetailsList = stockDetailsList.Where(x => x.IssueFlag.ToLower() == "available" && x.ExpiryDate.Value.Date >= todaydate.Date).ToList();
                            }
                            else if (request.issueFlag.ToLower() == "expired")
                            {
                                stockDetailsList = stockDetailsList.Where(x => x.IssueFlag.ToLower() == "available" && x.ExpiryDate.Value.Date < todaydate.Date || x.IssueFlag.ToLower() == "expired").ToList();

                            }
                        }
                        else
                        {
                            stockDetailsList = stockDetailsList.Where(x => x.IssueFlag.ToLower() == "available" && x.ExpiryDate.Value.Date >= todaydate.Date).ToList();
                        }

                        // 2. Here We applied date filter
                        if (request.date.HasValue)
                        {
                            DateTime dateTime = request.date.Value;
                            stockDetailsList = stockDetailsList.Where(x => x.CollectionDate.Value.Date == dateTime.Date).ToList();
                        }

                        // 3. Here We applied bloodgroup filter
                        if (!string.IsNullOrEmpty(request.bloodGroup))
                        {
                            stockDetailsList = stockDetailsList.Where(x => x.BloodGroup.ToLower() == request.bloodGroup.ToLower()).ToList();
                        }

                        // 4. Here We applied ProductID filter
                        if (request.productId > 0 && request.productId <= 6)
                        {
                            stockDetailsList = stockDetailsList.Where(x => x.ProductId == request.productId).ToList();

                        }

                        foreach (var item in stockDetailsList)
                        {
                            StocksListbyProductType stockObject = new StocksListbyProductType();
                            stockObject.isSuccess = true;
                            stockObject.bloodGroup = item.BloodGroup;
                            stockObject.bagNumber = item.BagNo;
                            stockObject.productID = item.ProductId.Value;
                            // stockObject.expiryDate = item.ExpiryDate.Value.Date;
                            stockObject.expiryDate = item.ExpiryDate.Value.ToString("dd/MM/yyyy");
                            //stockObject.collectionDate = (Int32)(item.CollectionDate.Value.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                            // stockObject.collectionDate = item.CollectionDate.Value.Date;

                            stockObject.collectionDate = item.CollectionDate.Value.ToString("dd/MM/yyyy");
                            stockObject.discardReason = item.DiscardReason;
                            stockObject.collectedBy = item.CollectedBy;// AU Added on 22JAN2021

                            stockList.Add(stockObject);
                        }

                        // response.stocksListbyProductTypes = stockList;
                        //response.stocksListbyProductTypes = stockList.OrderBy(x => x.collectionDate).ThenBy(x => x.bagNumber).ToList();
                        // if (request.sortType.ToLower() == "all")
                        // {
                        //  response.stocksListbyProductTypes = stockList.OrderBy(x => x.expiryDate).ThenBy(x => x.collectionDate).ThenBy(x => x.bagNumber).ToList();// AU Added on 11Jan2021
                        //  }
                        //Below If Else Runing on only Server
                        if (!string.IsNullOrEmpty(request.sortType))
                        {
                            if (request.sortType.ToLower() == "expiry")
                            {
                                response.stocksListbyProductTypes = stockList.OrderBy(x => DateTime.ParseExact(x.expiryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)).ThenBy(x => x.bagNumber).ToList();
                                //response.stocksListbyProductTypes = stockList.OrderByDescending(x => x.expiryDate).ThenBy(x => x.bagNumber).ToList();
                            }
                            else if (request.sortType.ToLower() == "collection")
                            {
                                response.stocksListbyProductTypes = stockList.OrderByDescending(x => DateTime.ParseExact(x.collectionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)).ThenBy(x => x.bagNumber).ToList();
                            }
                        }
                        else
                        {
                            response.stocksListbyProductTypes = stockList.OrderByDescending(x => DateTime.ParseExact(x.collectionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)).ThenBy(x => x.bagNumber).ToList();
                        }



                        response.Message = "View stock details by product type fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Stocks not found";
                        response.Success = false;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Bloodbank Not Found";
                    response.Success = false;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "ViewStockbyProductType Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public GetAccountResponse GetAccount(GetAccountRequest request)
        {
            GetAccountResponse response = new GetAccountResponse();
            List<Transaction> transactionsList = new List<Transaction>();

            try
            {
                Transaction today = new Transaction
                {
                    customerName = "ABC",
                    customerProfileImage = "Test",
                    detail = "test",
                    amount = "150",
                    date = new DateTime()
                };
                transactionsList.Add(today);

                response.totalBalance = 0.0f;
                response.income = 0.0f;
                response.expenses = 0.0f;
                response.recentTransaction = transactionsList;


                response.Message = "GetAccount successfully";
                response.Success = true;
                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                response.ResponseCode = 0;
            }
            catch (Exception ex)
            {
                response.Message = "GetAccount Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public AddCallResponse AddCall(AddCallRequest request)
        {
            string bloodbankId = request.bloodbankId;
            AddCallResponse response = new AddCallResponse();
            try
            {
                DateTime dateTime = GetLocalDateTime();
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {
                        if (request.callID == 0)
                        {
                            // Insert Entry
                            if (request.callType.ToLower() == "samplecollection" || request.callType.ToLower() == "officework")
                            {
                                ScheduleCall scheduleCallObject = new ScheduleCall();
                                scheduleCallObject.AssignedUserId = String.IsNullOrEmpty(request.assignedUserId) ? "NA" : request.assignedUserId;
                                //scheduleCallObject.CallDateTime = DateTime.Now;//request.callDateTime;
                                scheduleCallObject.CallDateTime = GetLocalDateTime();// AU change Added on 25Jan2021
                                scheduleCallObject.Status = "pending";
                                scheduleCallObject.FkBloodbankId = bloodbankId;
                                this.DBContext.ScheduleCall.Add(scheduleCallObject);
                                int affectedStockRows = this.DBContext.SaveChanges();
                                response.Success = affectedStockRows == 1;

                                if (response.Success)
                                {
                                    CallDetails callDetailsObj = new CallDetails();
                                    callDetailsObj.FkCallId = scheduleCallObject.CallId;
                                    callDetailsObj.BillReqId = null;
                                    //callDetailsObj.DateTime = DateTime.Now;
                                    callDetailsObj.DateTime = GetLocalDateTime();// AU change Added on 25Jan2021
                                    callDetailsObj.ReqBillType = request.callType.ToLower();
                                    callDetailsObj.FkHospitalId = request.hospitalId;
                                    callDetailsObj.CallDetails1 = request.details;
                                    callDetailsObj.PatientName = request.patientName;
                                    callDetailsObj.PatientContact = request.mobileNumber;
                                    callDetailsObj.Address = request.address;
                                    callDetailsObj.CreatedUserId = userProfileRecord.Id;
                                    callDetailsObj.BloodGroup = "";
                                    callDetailsObj.Gender = "";
                                    callDetailsObj.Age = null;
                                    callDetailsObj.PatientStatus = "pending";

                                    this.DBContext.CallDetails.Add(callDetailsObj);
                                    int affectedcallRows = this.DBContext.SaveChanges();
                                    response.Success = affectedcallRows == 1;

                                    if (response.Success)
                                    {
                                        var TempTitle = "";
                                        var Tempbody = "";
                                        if (request.callType.ToLower() == "samplecollection")
                                        {
                                            var hospitalName = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == request.hospitalId).HospitalName;
                                            TempTitle = "New Sample Collection Call ";
                                            Tempbody = "New call generated for " + hospitalName + ", Please check!";
                                        }
                                        else
                                        {
                                            TempTitle = "New Office Work Call ";
                                            Tempbody = "New call generated for Office Work Please check!";
                                        }
                                        //if serviceboy assign then send notification of call to that particular serviceboy and if not assingn then to all serviceboy notifcation of call 
                                        if (!String.IsNullOrEmpty(request.assignedUserId))
                                        {
                                            Attendance attendance = this.DBContext.Attendance.FirstOrDefault(x => x.AttendanceDateTime.Value.Date == dateTime.Date && x.InTime.HasValue && !x.OutTime.HasValue && x.UserId == request.assignedUserId &&x.FkBloodbankId==bloodbankId);
                                            if (attendance != null)
                                            {
                                                var userDeviceId = this.DBContext.Users.FirstOrDefault(x => x.Id == request.assignedUserId && x.IsDelete == 0).DeviceId;
                                                if (userDeviceId != null)
                                                {
                                                    NotificationFunctionForAndroid(userDeviceId, TempTitle, Tempbody);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var serviceBoyUsers = userManager.GetUsersInRoleAsync("Serviceboy").Result;
                                            serviceBoyUsers = serviceBoyUsers.Where(x => x.IsDelete == 0).ToList();
                                            serviceBoyUsers = (from serviceboy in serviceBoyUsers
                                                               join userbloodbank in this.DBContext.UsersbloodbankMaster on serviceboy.Id equals userbloodbank.UserId
                                             where userbloodbank.FkBloodBankId == bloodbankId
                                             select serviceboy).ToList();
                                            if (serviceBoyUsers != null && serviceBoyUsers.Count > 0)
                                            {
                                                foreach (var serviceboyeitem in serviceBoyUsers)
                                                {
                                                    Attendance attendance = this.DBContext.Attendance.FirstOrDefault(x => x.AttendanceDateTime.Value.Date == dateTime.Date && x.InTime.HasValue && !x.OutTime.HasValue && x.UserId == serviceboyeitem.Id &&x.FkBloodbankId==bloodbankId);
                                                    if (attendance != null)
                                                    {
                                                        var userDeviceId = this.DBContext.Users.FirstOrDefault(x => x.Id == serviceboyeitem.Id).DeviceId;
                                                        if (userDeviceId != null)
                                                        {
                                                            NotificationFunctionForAndroid(userDeviceId, TempTitle, Tempbody);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        response.Message = "Schedule call added successfully";
                                        response.Success = true;
                                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                        response.ResponseCode = 0;
                                    }
                                    else
                                    {
                                        response.Message = "Failed to add schedule call";
                                        response.Success = false;
                                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                        response.ResponseCode = 1;
                                    }
                                }
                                else
                                {
                                    response.Message = "Failed to add schedule call";
                                    response.Success = false;
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.ResponseCode = 1;
                                }
                            }
                            else
                            {
                                response.Message = "Please provide valid type";
                                response.Success = false;
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.ResponseCode = 1;
                            }
                        }
                        else
                        {
                            // Update Entry
                            ScheduleCall scheduleCall = this.DBContext.ScheduleCall.FirstOrDefault(x => x.CallId == request.callID);
                            if (scheduleCall != null)
                            {
                                CallDetails callDetails = new CallDetails();
                                callDetails.FkCallId = request.callID;
                                callDetails.BillReqId = null;
                                //callDetails.DateTime = DateTime.Now;
                                callDetails.DateTime = GetLocalDateTime();// AU change Added on 25Jan2021
                                callDetails.ReqBillType = request.callType.ToLower();
                                callDetails.FkHospitalId = request.hospitalId;
                                callDetails.CallDetails1 = request.details;
                                callDetails.PatientName = request.patientName;
                                callDetails.PatientContact = request.mobileNumber;
                                callDetails.Address = request.address;
                                callDetails.CreatedUserId = userProfileRecord.Id;
                                callDetails.BloodGroup = "";
                                callDetails.Gender = "";
                                callDetails.Age = null;
                                callDetails.PatientStatus = "pending";

                                this.DBContext.CallDetails.Add(callDetails);
                                int affectedCallDetailsRows = this.DBContext.SaveChanges();
                                response.Success = affectedCallDetailsRows == 1;

                                if (response.Success)
                                {
                                    response.Message = "CallDetails added successfully";
                                    response.Success = true;
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.ResponseCode = 0;
                                }
                                else
                                {
                                    response.Message = "Failed to add CallDetails";
                                    response.Success = false;
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.ResponseCode = 1;
                                }

                            }
                            else
                            {
                                response.Message = "Call details not found";
                                response.Success = false;
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.ResponseCode = 1;
                            }

                        }//Outer Else Block
                    }
                    else
                    {
                        response.Message = "BloodBank not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "AddCall Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }


        public DateTime GetLocalDateTime()
        {
            DateTime UtcNowTime = DateTime.UtcNow;
            //DateTime localDateTime =TimeZoneInfo.ConvertTimeFromUtc(UtcNowTime, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time")); //for converting to IST
            //DateTime localDateTime = TimeZoneInfo.ConvertTimeFromUtc(UtcNowTime, TimeZoneInfo.FindSystemTimeZoneById("Asia/Calcutta")); //for converting to IST
            DateTime localDateTime = TimeZoneInfo.ConvertTimeFromUtc(UtcNowTime, TZConvert.GetTimeZoneInfo("India Standard Time"));
            return localDateTime;
        }

        public void NotificationFunctionForAndroid(string DevicedId, string notiTitle, string notiBody)
        {
            var serverKey = "AAAAWGGzy18:APA91bEd_4Pk53GcpuqH02tHoASIEPQ08wGRDP4ADkeCmYGoNGVyla_Q8uHn-3WZtrOcHb-OrgLyzuMzO4KUpy_Pdi3NxaGAz-3jMZN614xJV3ExOPG81cBhViYJ1UdQBmoMxwq3LFj-";
            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            tRequest.ContentType = "application/json";

            var payload = new
            {
                to = DevicedId,
                priority = "high",
                content_available = true, // sound = "notification_bell.mp3",

                data = new
                {
                    body = notiBody,
                    title = notiTitle,
                    // badge = 1,
                    // tag = ,
                },
            };

            string postbody = JsonConvert.SerializeObject(payload).ToString();
            Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = tRequest.GetResponse())
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                //result.Response = sResponseFromServer;
                            }
                    }
                }
            }
        }






        public AddExpensesResponse AddExpenses(AddExpensesRequest request)
        {
            string bloodbankId = request.bloodbankId;
            AddExpensesResponse response = new AddExpensesResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {

                        string PostImageUrlAwsS3 = "";
                        string PostImageBase64 = request.image;
                        if (StringExtensions.HasValue(PostImageBase64))
                        {
                            string BucketKeyPosts = s3Repository.s3Settings.Value.BucketChildKeyPosts;
                            UploadPhotoModel uploadPhotoResponse = s3Repository.UploadObject(
                                s3Repository.s3Settings,
                                PostImageBase64,
                                BucketKeyPosts).Result;
                            PostImageUrlAwsS3 = uploadPhotoResponse.FileName;
                        }


                        Expense expenseObject = new Expense();
                        expenseObject.Amount = request.amount;
                        expenseObject.DateTime = request.expenseDate;
                        expenseObject.ExpenseOn = request.expenseOn;
                        expenseObject.ExpenseBy = userProfileRecord.Id;
                        expenseObject.Image = PostImageUrlAwsS3;
                        expenseObject.FkBloodbankId = bloodbankId;
                        this.DBContext.Expense.AddRange(expenseObject);
                        int affectedStockRows = this.DBContext.SaveChanges();
                        response.Success = affectedStockRows == 1;
                        if (response.Success)
                        {
                            response.Message = "Expenses inserted successfully";
                            response.Success = true;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "Failed to add expenses";
                            response.Success = false;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "BloodBank not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "AddExpensesResponse Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public AssignResponse Assign(AssignRequest request)
        {
            DateTime dateTime = GetLocalDateTime();
            string bloodbankId = request.bloodbankId;
            AssignResponse response = new AssignResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {
                        ScheduleCall scheduleCall = this.DBContext.ScheduleCall.FirstOrDefault(x => x.CallId == request.callID && x.FkBloodbankId == bloodbankId);
                        if (scheduleCall != null)
                        {
                            Attendance attendance = this.DBContext.Attendance.FirstOrDefault(x => x.AttendanceDateTime.Value.Date == dateTime.Date && x.InTime.HasValue && !x.OutTime.HasValue && x.UserId == userProfileRecord.Id &&x.FkBloodbankId==bloodbankId);
                            if (attendance != null)
                            {
                                scheduleCall.AssignedUserId = userProfileRecord.Id;
                                scheduleCall.Status = "inprogress";

                                int updatedRows = this.DBContext.SaveChanges();
                                response.Success = updatedRows == 1;

                                if (response.Success)
                                {
                                    response.status = scheduleCall.Status;
                                    response.Message = "Call assigned successfully";
                                    response.Success = true;
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.ResponseCode = 0;
                                }
                                else
                                {
                                    response.status = "";
                                    response.Message = "Failed to update a call";
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.Success = false;
                                    response.ResponseCode = 1;
                                }
                            }
                            else
                            {
                                response.Message = "Please mark attendance/you are already out";
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.Success = false;
                                response.ResponseCode = 1;
                            }
                        }
                        else
                        {
                            response.Message = "Schedule call not found";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "Bloodbank not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Assign Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        //public AddRequisitionResponse AddRequisition(AddRequisitionRequest request)
        //{
        //    AddRequisitionResponse response = new AddRequisitionResponse();
        //    try
        //    {
        //        Requisition requisition = new Requisition();
        //        requisition.RequisitionDate = DateTime.Now;
        //        requisition.FkCallId = request.callId;
        //        requisition.PaymentAmount = request.Amount;
        //        requisition.PaidAmount = request.Amount;
        //        requisition.PaymentType = request.paymentType;
        //        requisition.IsActive = "active";
        //        requisition.Discount = 0;
        //        requisition.Remark = null;
        //        requisition.AuthorisedById = null;
        //        requisition.IsNat = request.isNat.ToLower();

        //        this.DBContext.Requisition.Add(requisition);
        //        int affectedRequisitionRows = this.DBContext.SaveChanges();
        //        response.Success = affectedRequisitionRows == 1;
        //        if (response.Success)
        //        {
        //            List<RequisitionProducts> allRequisitionProducts = new List<RequisitionProducts>();
        //            foreach (var quantityRequiredItem in request.quantityRequired)
        //            {
        //                RequisitionProducts requisitionProducts = new RequisitionProducts();
        //                requisitionProducts.FkRequisitionId = requisition.RequisitionId;
        //                requisitionProducts.ProductId = this.DBContext.ProductMaster.FirstOrDefault(x => x.ProductName.ToLower() == quantityRequiredItem.productName).ProductId;
        //                requisitionProducts.Quantity = quantityRequiredItem.productCount;
        //                requisitionProducts.IssueQuantity = 0;

        //                allRequisitionProducts.Add(requisitionProducts);
        //            }

        //            this.DBContext.RequisitionProducts.AddRange(allRequisitionProducts);
        //            int affectedRequisitionProductsRows = this.DBContext.SaveChanges();
        //            response.Success = affectedRequisitionProductsRows >= 1;
        //            if (response.Success)
        //            {
        //                List<RequisitionPaymentAttachment> allRequisitionPaymentAttachments = new List<RequisitionPaymentAttachment>();
        //                foreach (var billImageItem in request.billImages)
        //                {
        //                    //New code
        //                    string PostImageUrlAwsS3 = "";
        //                    string PostImageBase64 = billImageItem.billImage;
        //                    if (StringExtensions.HasValue(PostImageBase64))
        //                    {
        //                        string BucketKeyPosts = s3Repository.s3Settings.Value.BucketChildKeyPosts;
        //                        UploadPhotoModel uploadPhotoResponse = s3Repository.UploadObject(
        //                            s3Repository.s3Settings,
        //                            PostImageBase64,
        //                            BucketKeyPosts).Result;
        //                        PostImageUrlAwsS3 = uploadPhotoResponse.FileName;
        //                    }
        //                    //New Code end

        //                    RequisitionPaymentAttachment requisitionPaymentAttachment = new RequisitionPaymentAttachment();
        //                    requisitionPaymentAttachment.FkRequisitionId = requisition.RequisitionId;
        //                    requisitionPaymentAttachment.ImageUrl = PostImageUrlAwsS3;//billImageItem.billImage;

        //                    allRequisitionPaymentAttachments.Add(requisitionPaymentAttachment);
        //                }

        //                this.DBContext.RequisitionPaymentAttachment.AddRange(allRequisitionPaymentAttachments);
        //                int affectedRequisitionPaymentAttachementRows = this.DBContext.SaveChanges();
        //                response.Success = affectedRequisitionPaymentAttachementRows >= 1;
        //                if (response.Success)
        //                {
        //                    // Update SchduleCall Details
        //                    ScheduleCall scheduleCall = this.DBContext.ScheduleCall.FirstOrDefault(x => x.CallId == request.callId);
        //                    if (scheduleCall != null)
        //                    {
        //                        //scheduleCall.PatientName = "";
        //                        //scheduleCall.PatientContact = "";
        //                        //scheduleCall.CallDetails = "";
        //                        //scheduleCall.HospitalId = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalName.ToLower() == request.hospital.ToLower()).HospitalMasterId;
        //                        scheduleCall.Status = "pendingsettlement";//pending settlement

        //                        int updatedScheduleCallRows = this.DBContext.SaveChanges();
        //                        response.Success = updatedScheduleCallRows == 1;
        //                        if (response.Success)
        //                        {
        //                            response.Message = "Requisition added successfully";
        //                            response.Success = true;
        //                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //                            response.ResponseCode = 0;
        //                        }
        //                        else
        //                        {
        //                            response.Message = "Failed to add requisition details";
        //                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //                            response.Success = false;
        //                            response.ResponseCode = 1;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        response.Message = "Schedule call not found";
        //                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //                        response.Success = false;
        //                        response.ResponseCode = 1;
        //                    }
        //                }
        //                else
        //                {
        //                    response.Message = "Failed to add requisition payment attachment";
        //                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //                    response.Success = false;
        //                    response.ResponseCode = 1;
        //                }
        //            }
        //            else
        //            {
        //                response.Message = "Failed to add requisition products";
        //                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //                response.Success = false;
        //                response.ResponseCode = 1;
        //            }
        //        }
        //        else
        //        {
        //            response.Message = "Failed to add requisition";
        //            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //            response.Success = false;
        //            response.ResponseCode = 1;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Message = "AddRequition Exception : " + ex.Message;
        //        response.Success = false;
        //        response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
        //        response.ResponseCode = 1;
        //    }
        //    return response;
        //}


        public AddRequisitionResponse AddRequisition(AddRequisitionRequest request)
        {
            string bloodbankId = request.bloodbankId;
            AddRequisitionResponse response = new AddRequisitionResponse();
            try
            {
                DateTime dateTime1 = GetLocalDateTime();
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {
                        if (request.callType.ToLower() == "samplecollection")
                        {
                            DateTime dateTime = DateTime.Now.AddMinutes(-60);
                            var query = from req in this.DBContext.Requisition
                                        join call in this.DBContext.CallDetails on req.FkCallId equals call.CallDetailsId
                                        where call.PatientName.ToLower() == request.patientName.ToLower() && call.PatientContact == request.mobileNumber && call.ReqBillType.ToLower() == "samplecollection" &&req.FkBloodbankId==bloodbankId
                                        && req.RequisitionDate.Value > dateTime
                                        select new { call.PatientName, req.RequisitionDate };

                            if (!query.Any())
                            {

                                Requisition requisition = new Requisition();
                                //requisition.RequisitionDate = DateTime.Now;
                                requisition.RequisitionDate = GetLocalDateTime();
                                //requisition.FkCallId = request.callId;
                                requisition.FkCallId = request.callDetailsId;
                                requisition.ServiceBoyId = userProfileRecord.Id;
                                requisition.PaymentAmount = request.Amount;
                                requisition.PaidAmount = request.Amount;
                                requisition.PaymentType = request.paymentType;
                                requisition.IsActive = "active";
                                requisition.Discount = 0;
                                requisition.Remark = "";
                                requisition.AuthorisedById = "";
                                requisition.IsNat = request.isNat.ToLower();
                                requisition.ServiceCharge = request.serviceCharge;
                                if (request.paymentType.ToLower() == "cash" || request.paymentType.ToLower() == "upi")
                                {
                                    requisition.PaidDate = GetLocalDateTime();
                                }
                                else if (request.paymentType.ToLower() == "credit")
                                {
                                    requisition.PaidDate = null;
                                }
                                requisition.UpiId = request.paymentType.ToLower() == "upi" ? request.upiId : "";
                                requisition.FkBloodbankId = bloodbankId;    //added by sk on 11/11/2021
                                requisition.RequisitionNumber = GetNewRequisitionNumber(bloodbankId);
                                this.DBContext.Requisition.Add(requisition);
                                int affectedRequisitionRows = this.DBContext.SaveChanges();
                                response.Success = affectedRequisitionRows == 1;
                                if (response.Success)
                                {
                                    InvoiceNo invoiceNo = new InvoiceNo();
                                    invoiceNo.InvoiceNumber = GetNewInvoiceNumber((this.DBContext.InvoiceNo.Where(x => x.FkBloodbankId == bloodbankId &&x.InvoiceDate.Value.Year==dateTime1.Year).ToList().Count + 1), bloodbankId);
                                    invoiceNo.Type = "requisition";
                                    invoiceNo.TypeId = requisition.RequisitionId;
                                    invoiceNo.IsCancelled = 0; // 0 means No
                                    invoiceNo.InvoiceDate = GetLocalDateTime();
                                    invoiceNo.FkBloodbankId = bloodbankId;      //added by sk on 11/11/2021
                                    this.DBContext.InvoiceNo.Add(invoiceNo);
                                    int insertedInvoiceRows = this.DBContext.SaveChanges();
                                    if (insertedInvoiceRows == 1)
                                    {
                                        List<RequisitionProducts> allRequisitionProducts = new List<RequisitionProducts>();
                                        foreach (var quantityRequiredItem in request.quantityRequired)
                                        {
                                            RequisitionProducts requisitionProducts = new RequisitionProducts();
                                            requisitionProducts.FkRequisitionId = requisition.RequisitionId;
                                            requisitionProducts.ProductId = this.DBContext.ProductMaster.FirstOrDefault(x => x.ProductName.ToLower() == quantityRequiredItem.productName).ProductId;
                                            requisitionProducts.Quantity = quantityRequiredItem.productCount;
                                            requisitionProducts.IssueQuantity = 0;

                                            allRequisitionProducts.Add(requisitionProducts);
                                        }

                                        this.DBContext.RequisitionProducts.AddRange(allRequisitionProducts);
                                        int affectedRequisitionProductsRows = this.DBContext.SaveChanges();
                                        response.Success = affectedRequisitionProductsRows >= 1;
                                        if (response.Success)
                                        {
                                            List<RequisitionPaymentAttachment> allRequisitionPaymentAttachments = new List<RequisitionPaymentAttachment>();

                                            string PostImageUrlAwsS3 = "";
                                            string PostImageBase64 = request.BillImages;
                                            if (StringExtensions.HasValue(PostImageBase64))
                                            {
                                                string BucketKeyPosts = s3Repository.s3Settings.Value.BucketChildKeyPosts;
                                                UploadPhotoModel uploadPhotoResponse = s3Repository.UploadObject(
                                                    s3Repository.s3Settings,
                                                    PostImageBase64,
                                                    BucketKeyPosts).Result;
                                                PostImageUrlAwsS3 = uploadPhotoResponse.FileName;
                                            }

                                            RequisitionPaymentAttachment requisitionPaymentAttachment = new RequisitionPaymentAttachment();
                                            requisitionPaymentAttachment.FkRequisitionId = requisition.RequisitionId;
                                            requisitionPaymentAttachment.ImageUrl = PostImageUrlAwsS3;//billImageItem.billImage;

                                            allRequisitionPaymentAttachments.Add(requisitionPaymentAttachment);
                                            this.DBContext.RequisitionPaymentAttachment.AddRange(allRequisitionPaymentAttachments);
                                            int affectedRequisitionPaymentAttachementRows = this.DBContext.SaveChanges();
                                            response.Success = affectedRequisitionPaymentAttachementRows >= 1;
                                            if (response.Success)
                                            {

                                                //CallDetails callDetailsRecord = this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == request.callId && x.PatientName.ToLower() == request.patientName.ToLower());
                                                CallDetails callDetailsRecord = this.DBContext.CallDetails.FirstOrDefault(x => x.CallDetailsId == request.callDetailsId && x.FkCallId == request.callId);
                                                if (callDetailsRecord != null)
                                                {
                                                    List<CallDetails> callDetailsPendingList = this.DBContext.CallDetails.Where(x => x.FkCallId == request.callId && x.PatientStatus.ToLower() == "pending").ToList();
                                                    if (callDetailsPendingList != null)
                                                    {
                                                        callDetailsRecord.PatientStatus = "complete";
                                                        callDetailsRecord.PatientContact = request.mobileNumber;// Au added on 01Jan2021
                                                        callDetailsRecord.BloodGroup = request.bloodroup;// Au added on 22Jan2021
                                                        callDetailsRecord.Gender = request.gender;// Au added on 22Jan2021
                                                        callDetailsRecord.Age = request.age;// Au added on 22Jan2021
                                                        callDetailsRecord.PatientName = request.patientName;// Au added on 22Jan2021
                                                        callDetailsRecord.FkHospitalId = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalName.ToLower() == request.hospital.ToLower()).HospitalMasterId;// Au added on 22Jan2021
                                                        int affectedcallRows = this.DBContext.SaveChanges();
                                                        response.Success = affectedcallRows == 1;
                                                        if (response.Success)
                                                        {
                                                           // SendSMS("requisition", requisition.RequisitionId, request.mobileNumber);

                                                            List<CallDetails> callDetailscompleteList = this.DBContext.CallDetails.Where(x => x.FkCallId == request.callId && x.PatientStatus.ToLower() == "pending").ToList();
                                                            if (callDetailscompleteList.Count == 0)
                                                            {
                                                                // Update SchduleCall Details
                                                                ScheduleCall scheduleCall = this.DBContext.ScheduleCall.FirstOrDefault(x => x.CallId == request.callId && x.FkBloodbankId == bloodbankId);
                                                                if (scheduleCall != null)
                                                                {
                                                                    scheduleCall.Status = "pendingsettlement";//pending settlement

                                                                    int updatedScheduleCallRows = this.DBContext.SaveChanges();
                                                                    response.Success = updatedScheduleCallRows == 1;
                                                                    if (response.Success)
                                                                    {

                                                                        response.Message = "Requisition added and ScduleCall updated successfully";
                                                                        response.Success = true;
                                                                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                                        response.ResponseCode = 0;
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                response.Message = "Requisition added successfully";
                                                                response.Success = true;
                                                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                                response.ResponseCode = 0;
                                                                //response.Message = "Call details pending records not found";
                                                                //response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                                //response.Success = false;
                                                                //response.ResponseCode = 1;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            response.Message = "Failed to add Call details";
                                                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                            response.Success = false;
                                                            response.ResponseCode = 1;

                                                        }
                                                    }
                                                    else
                                                    {
                                                        response.Message = "Call Details Record not found";
                                                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                        response.Success = false;
                                                        response.ResponseCode = 1;
                                                    }
                                                }
                                                else
                                                {
                                                    response.Message = "Call Details Record not found";
                                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                    response.Success = false;
                                                    response.ResponseCode = 1;
                                                }
                                            }
                                            else
                                            {
                                                response.Message = "Failed to add requisition payment attachment";
                                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                response.Success = false;
                                                response.ResponseCode = 1;
                                            }
                                        }
                                        else
                                        {
                                            response.Message = "Failed to add requisition products";
                                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                            response.Success = false;
                                            response.ResponseCode = 1;
                                        }
                                    }
                                    else
                                    {
                                        response.Message = "Failed to add invoice number";
                                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                        response.Success = false;
                                        response.ResponseCode = 1;
                                    }
                                }
                                else
                                {
                                    response.Message = "Failed to add requisition";
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.Success = false;
                                    response.ResponseCode = 1;
                                }

                            }
                            else
                            {
                                response.Message = "Already Added Requisition Of Patient Please Wait For 1hr";
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.Success = false;
                                response.ResponseCode = 1;
                            }
                        }
                        else if (request.callType.ToLower() == "issuebag")
                        {
                            //CallDetails callDetailsRecord = this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == request.callId && x.PatientName.ToLower() == request.patientName.ToLower());
                            CallDetails callDetailsRecord = this.DBContext.CallDetails.FirstOrDefault(x => x.CallDetailsId == request.callDetailsId && x.FkCallId == request.callId);
                            if (callDetailsRecord != null)
                            {
                                Bill billRecored = this.DBContext.Bill.FirstOrDefault(x => x.BillId == callDetailsRecord.BillReqId);
                                if (billRecored != null)
                                {
                                    //New code
                                    string PostImageUrlAwsS3 = "";
                                    string PostImageBase64 = request.BillImages;
                                    if (StringExtensions.HasValue(PostImageBase64))
                                    {
                                        string BucketKeyPosts = s3Repository.s3Settings.Value.BucketChildKeyPosts;
                                        UploadPhotoModel uploadPhotoResponse = s3Repository.UploadObject(
                                            s3Repository.s3Settings,
                                            PostImageBase64,
                                            BucketKeyPosts).Result;
                                        PostImageUrlAwsS3 = uploadPhotoResponse.FileName;
                                    }
                                    billRecored.ServiceBoyId = userProfileRecord.Id;
                                    billRecored.AttachmentUrl = PostImageUrlAwsS3;
                                    billRecored.PaymentType = request.paymentType;
                                    billRecored.UpiId = request.paymentType.ToLower() == "upi" ? request.upiId : "";
                                    int affectedbillRows = this.DBContext.SaveChanges();
                                    response.Success = affectedbillRows == 1;
                                    if (response.Success)
                                    {
                                        List<CallDetails> callDetailsPendingList = this.DBContext.CallDetails.Where(x => x.FkCallId == request.callId && x.PatientStatus.ToLower() == "pending").ToList();
                                        if (callDetailsPendingList != null)
                                        {
                                            callDetailsRecord.PatientStatus = "complete";
                                            callDetailsRecord.PatientContact = request.mobileNumber;// Au added on 01Jan2021
                                            callDetailsRecord.Gender = request.gender;// Au added on 22Jan2021
                                            callDetailsRecord.Age = request.age;// Au added on 22Jan2021
                                            callDetailsRecord.PatientName = request.patientName;// Au added on 22Jan2021
                                            callDetailsRecord.FkHospitalId = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalName.ToLower() == request.hospital.ToLower()).HospitalMasterId;// Au added on 22Jan2021
                                            int affectedcallRows = this.DBContext.SaveChanges();
                                            response.Success = affectedcallRows == 1;
                                            if (response.Success)
                                            {
                                              //  SendSMS("bill", billRecored.BillId, request.mobileNumber);

                                                List<CallDetails> callDetailscompleteList = this.DBContext.CallDetails.Where(x => x.FkCallId == request.callId && x.PatientStatus.ToLower() == "pending").ToList();
                                                if (callDetailscompleteList.Count == 0)
                                                {
                                                    // Update SchduleCall Details
                                                    ScheduleCall scheduleCall = this.DBContext.ScheduleCall.FirstOrDefault(x => x.CallId == request.callId &&x.FkBloodbankId==bloodbankId);
                                                    if (scheduleCall != null)
                                                    {
                                                        scheduleCall.Status = "pendingsettlement";//pending settlement

                                                        int updatedScheduleCallRows = this.DBContext.SaveChanges();
                                                        response.Success = updatedScheduleCallRows == 1;
                                                        if (response.Success)
                                                        {
                                                            response.Message = "Bill & schedule call updated successfully";
                                                            response.Success = true;
                                                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                            response.ResponseCode = 0;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    response.Message = "Bill updated successfully";
                                                    response.Success = true;
                                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                    response.ResponseCode = 0;
                                                }
                                            }
                                            else
                                            {
                                                response.Message = "Failed to add Call details";
                                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                response.Success = false;
                                                response.ResponseCode = 1;

                                            }
                                        }
                                    }
                                    else
                                    {
                                        response.Message = "Failed to update approve call settlement";
                                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                        response.Success = false;
                                        response.ResponseCode = 1;
                                    }
                                    //New Code end
                                }
                            }

                        }
                    }
                    else
                    {
                        response.Message = "Bloodbank not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "AddRequition Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public string GetNewInvoiceNumber(int id,string bloodbankId)
        {
            string bloodbankshortName = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId).BloodbankShortName;
            return bloodbankshortName + id.ToString("D5");
        }

        public string GetNewRequisitionNumber(string bloodbankId)
        {
            DateTime dateTime = GetLocalDateTime();
            string requisitionNumber = (this.DBContext.Requisition.Where(x => x.FkBloodbankId == bloodbankId &&x.RequisitionDate.Value.Year==dateTime.Year).ToList().Count + 1).ToString();
            return requisitionNumber;
        }

        public FilteredStockResponse FilteredStock(FilteredStockRequest request)
        {
            string bloodbankId = request.bloodbankId;
            FilteredStockResponse response = new FilteredStockResponse();
            int pageSize = 10;

            try
            {
                if (request.pageNumber >= 1)
                {
                    var filteresStockList = this.DBContext.StockDetails.Where(x => x.FkBloodbankId == bloodbankId).ToList();
                    if (!string.IsNullOrEmpty(request.bloodGroup))
                    {
                        filteresStockList = filteresStockList.Where(x => x.BloodGroup.ToLower() == request.bloodGroup.ToLower()).ToList();
                    }
                    if (!string.IsNullOrEmpty(request.productType))
                    {
                        int _productId = this.DBContext.ProductMaster.FirstOrDefault(x => x.ProductName.ToLower() == request.productType.ToLower()).ProductId;
                        filteresStockList = filteresStockList.Where(x => x.ProductId == _productId).ToList();
                    }
                    if (request.fromDate.HasValue && request.toDate.HasValue)
                    {
                        DateTime a = request.fromDate.Value;
                        DateTime b = request.toDate.Value.AddDays(1);
                        //filteresStockList = filteresStockList.Where(x => x.ExpiryDate >= DateTime.Parse(request.fromDate) && x.ExpiryDate <= DateTime.Parse(request.toDate).AddDays(1)).ToList();
                        filteresStockList = filteresStockList.Where(x => x.ExpiryDate >= request.fromDate.Value && x.ExpiryDate <= request.toDate.Value.AddDays(1)).ToList();
                    }
                    if (!string.IsNullOrEmpty(request.bagNumber))
                    {
                        filteresStockList = filteresStockList.Where(x => x.BagNo.ToLower() == request.bagNumber.ToLower()).ToList();
                    }

                    //----------------
                    if (filteresStockList != null && filteresStockList.Count > 0)
                    {
                        List<Stock> stockList = new List<Stock>();
                        filteresStockList = filteresStockList.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
                        foreach (var item in filteresStockList.OrderBy(x => x.ExpiryDate))
                        {
                            Stock stockObject = new Stock();
                            stockObject.isSuccess = true;
                            stockObject.bloodGroup = item.BloodGroup;
                            stockObject.bagNumber = item.BagNo;
                            stockObject.productID = item.ProductId.Value;
                            stockObject.expiryDate = item.ExpiryDate.Value.ToString("dd/MM/yyyy");
                            //stockObject.collectionDate = item.CollectionDate.Value; (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                            stockObject.collectionDate = (Int32)(item.CollectionDate.Value.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

                            stockObject.PCV = item.ProductId == 1 ? 1 : 0;
                            stockObject.FFP = item.ProductId == 2 ? 1 : 0;
                            stockObject.RDP = item.ProductId == 3 ? 1 : 0;
                            stockObject.WB = item.ProductId == 4 ? 1 : 0;
                            stockObject.SDP = item.ProductId == 5 ? 1 : 0;
                            stockObject.Plasma = item.ProductId == 6 ? 1 : 0;

                            stockList.Add(stockObject);
                        }
                        response.stockCount = stockList;
                        response.Message = "Stocks filtered successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Filtered stocks not found";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Page number must be greater than zero(0)";
                    response.Success = true;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "FilteredStock Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public GetQuantityAmountResponse GetQuantityAmount(GetQuantityAmountRequest request)
        {
            GetQuantityAmountResponse response = new GetQuantityAmountResponse();
            try
            {
                response.wb_Normal = 10;
                response.pcv_Normal = 10;
                response.ffp_Normal = 10;
                response.rdp_Normal = 10;
                response.sdp_Normal = 10;
                response.plasma_Normal = 10;
                response.wb_Nat = 10;
                response.pcv_Nat = 10;
                response.ffp_Nat = 10;
                response.rdp_Nat = 10;
                response.sdp_Nat = 10;
                response.plasma_Nat = 10;

                response.Message = "QuantityAmount fetched successfully";
                response.Success = true;
                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                response.ResponseCode = 0;
            }
            catch (Exception ex)
            {
                response.Message = "GetQuantityAmount Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public GetAllHospitalsResponse GetAllHospitals(GetAllHospitalsRequest request)
        {
            GetAllHospitalsResponse response = new GetAllHospitalsResponse();
            List<HospitalInfo> allHospitalsList = new List<HospitalInfo>();
            try
            {
                List<HospitalMaster> hospitalMastersList = this.DBContext.HospitalMaster.ToList();
                if (hospitalMastersList != null && hospitalMastersList.Count > 0)
                {
                    foreach (var item in hospitalMastersList)
                    {
                        HospitalInfo hospitalInfo = new HospitalInfo();
                        hospitalInfo.hospitalId = item.HospitalMasterId;
                        hospitalInfo.hospitalName = item.HospitalName;

                        allHospitalsList.Add(hospitalInfo);
                    }

                    response.allHospitals = allHospitalsList;
                    response.Message = "All hospitals fetched successfully";
                    response.Success = true;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 0;
                }
                else
                {
                    response.Message = "Hospitals not found";
                    response.Success = false;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "GetAllHospitals Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public GetAllAssignedUserResponse GetAllAssignedUser(GetAllAssignedUserRequest request)
        {
            string bloodbankId = request.bloodbankId;
            GetAllAssignedUserResponse response = new GetAllAssignedUserResponse();
            List<AssignedUserInfo> assignedUserInfoList = new List<AssignedUserInfo>();
            try
            {
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    var allUsers = userManager.GetUsersInRoleAsync("Serviceboy").Result;
                    allUsers = allUsers.Where(x => x.IsDelete == 0).ToList();
                    if (allUsers != null && allUsers.Count > 0)
                    {
                        foreach (var item in allUsers)
                        {
                            UsersbloodbankMaster usersbloodbankMaster = this.DBContext.UsersbloodbankMaster.FirstOrDefault(x => x.FkBloodBankId == bloodbankId && x.UserId == item.Id);
                            if (usersbloodbankMaster != null)
                            {
                                AssignedUserInfo assignedUserInfo = new AssignedUserInfo();
                                assignedUserInfo.assignUserId = item.Id;
                                assignedUserInfo.assignUserName = item.UserName;

                                assignedUserInfoList.Add(assignedUserInfo);
                            }
                        }

                        response.assignedUserInfos = assignedUserInfoList;
                        response.Message = "All service boys fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "user not found";
                        response.Success = false;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "BloodBank not found";
                    response.Success = false;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "GetAllAssignedUser Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public GetAttendanceStatusResponse GetAttendanceStatus(GetAttendanceStatusRequest request)
        {
            string bloodbankId = request.bloodbankId;
            GetAttendanceStatusResponse response = new GetAttendanceStatusResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {
                        DateTime currentDateTime = DateTime.Now.Date;
                        Attendance attendance = this.DBContext.Attendance.FirstOrDefault(x => x.UserId == userProfileRecord.Id && Convert.ToDateTime(x.AttendanceDateTime).Date == DateTime.Now.Date && x.FkBloodbankId == bloodbankId);
                        if (attendance != null)
                        {
                            //if (!string.IsNullOrEmpty(attendance.InTime) && !string.IsNullOrEmpty(attendance.OutTime))
                            if (attendance.InTime.HasValue && attendance.OutTime.HasValue)
                            {
                                response.attendanceStatus = "inout";
                                response.inTime = attendance.InTime.Value;
                                response.outTime = attendance.OutTime.Value;
                            }
                            else
                            {
                                response.attendanceStatus = "out";
                                response.inTime = attendance.InTime.Value;
                                response.outTime = new DateTime();//Default DateTime Value
                            }
                        }
                        else
                        {
                            response.attendanceStatus = "in";
                            response.inTime = new DateTime();//Default DateTime Value
                            response.outTime = new DateTime();//Default DateTime Value
                        }

                        response.Message = "Attendance status fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Bloodbank not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "GetAttendanceStatus Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public MarkAttendanceResponse MarkAttendance(MarkAttendanceRequest request)
        {
            string bloodbankId = request.bloodbankId;
            MarkAttendanceResponse response = new MarkAttendanceResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {

                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {
                        string PostImageUrlAwsS3 = "";
                        string PostImageBase64 = request.image;
                        if (StringExtensions.HasValue(PostImageBase64))
                        {
                            string BucketKeyPosts = s3Repository.s3Settings.Value.BucketChildKeyPosts;
                            UploadPhotoModel uploadPhotoResponse = s3Repository.UploadObject(
                                s3Repository.s3Settings,
                                PostImageBase64,
                                BucketKeyPosts).Result;
                            PostImageUrlAwsS3 = uploadPhotoResponse.FileName;
                        }

                        Attendance userAttendanceRecord = this.DBContext.Attendance.FirstOrDefault(x => x.UserId == userProfileRecord.Id && Convert.ToDateTime(x.AttendanceDateTime).Date == DateTime.Now.Date && x.FkBloodbankId == bloodbankId);
                        if (userAttendanceRecord != null)
                        {
                            // Update Attendance
                            //if (string.IsNullOrEmpty(userAttendanceRecord.OutTime))
                            if (!userAttendanceRecord.OutTime.HasValue)
                            {
                                //userAttendanceRecord.OutTime = String.Format("{0:hh:mm:ss tt}", DateTime.Now);
                                //userAttendanceRecord.OutTime = DateTime.Now;
                                userAttendanceRecord.OutTime = GetLocalDateTime();
                                //  userAttendanceRecord.ApproveAttendanceStatus = "pending";
                                int updtaedAttendanceRows = this.DBContext.SaveChanges();
                                response.Success = updtaedAttendanceRows == 1;
                                if (response.Success)
                                {
                                    response.Message = "Attendance updated successfully";
                                    response.Success = true;
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.ResponseCode = 0;
                                }
                                else
                                {
                                    response.Message = "Failed to update attendance";
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.Success = false;
                                    response.ResponseCode = 1;
                                }
                            }
                            else
                            {
                                response.Message = "User is already logged out";
                                response.Success = true;
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.ResponseCode = 0;
                            }
                        }
                        else
                        {
                            // Insert Attendance
                            Attendance attendance = new Attendance();
                            attendance.UserId = userProfileRecord.Id;
                            //attendance.AttendanceDateTime = DateTime.Now;
                            attendance.AttendanceDateTime = GetLocalDateTime();
                            //attendance.InTime = DateTime.Now;
                            attendance.InTime = GetLocalDateTime();
                            attendance.OutTime = null;
                            attendance.Image = PostImageUrlAwsS3;
                            attendance.Location = "";
                            attendance.ApproveAttendanceStatus = "pending";
                            attendance.FkBloodbankId = bloodbankId;
                            this.DBContext.Attendance.Add(attendance);
                            int affectedRows = this.DBContext.SaveChanges();
                            response.Success = affectedRows == 1;
                            if (response.Success)
                            {
                                response.Message = "Attendance inserted successfully";
                                response.Success = true;
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.ResponseCode = 0;
                            }
                            else
                            {
                                response.Message = "Failed to insert attendance";
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.Success = false;
                                response.ResponseCode = 1;
                            }
                        }
                    }
                    else
                    {
                        response.Message = "Bloodbank not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "MarkAttendance Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }


        public GetAllAttendanceListResponse GetAllAttendanceList(GetAllAttendanceListRequest request)
        {
            string bloodbankId = request.bloodbankId;
            GetAllAttendanceListResponse response = new GetAllAttendanceListResponse();
            List<AttendanceList> attendanceLists = new List<AttendanceList>();
            List<AbsentList> absentLists = new List<AbsentList>();
            DateTime todayDate = GetLocalDateTime();
            try
            {
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    var alladminuser = userManager.GetUsersInRoleAsync("Admin").Result;
                    var allserviceBoyuser = userManager.GetUsersInRoleAsync("Serviceboy").Result;
                    var alltechnicianuser = userManager.GetUsersInRoleAsync("Technician").Result;
                    var allstockuser = userManager.GetUsersInRoleAsync("Stock").Result;
                    var allaccountuser = userManager.GetUsersInRoleAsync("Accountant").Result;
                    var alltestinguser = userManager.GetUsersInRoleAsync("Testing").Result;
                    var allUsers = alladminuser.Concat(allserviceBoyuser);
                    allUsers = allUsers.Concat(alltechnicianuser);
                    allUsers = allUsers.Concat(allstockuser);
                    allUsers = allUsers.Concat(allaccountuser);
                    allUsers = allUsers.Concat(alltestinguser);

                    allUsers = allUsers.Where(x => x.IsDelete == 0).ToList();
                    if (allUsers.Any())
                    {
                        foreach (var item in allUsers)
                        {
                            UsersbloodbankMaster usersbloodbankMaster = this.DBContext.UsersbloodbankMaster.FirstOrDefault(x => x.FkBloodBankId == bloodbankId && x.UserId == item.Id);
                            if (usersbloodbankMaster != null)
                            {
                                Attendance attendance = this.DBContext.Attendance.FirstOrDefault(x => x.AttendanceDateTime.Value.Date == todayDate.Date && x.UserId == usersbloodbankMaster.UserId && x.FkBloodbankId == bloodbankId);
                                if (attendance != null)
                                {
                                    AttendanceList attendanceList = new AttendanceList();
                                    attendanceList.name = item.FirstName + " " + item.LastName;
                                    attendanceList.intime = !attendance.InTime.HasValue ? "" : attendance.InTime.Value.ToString("dd/MM/yyyy hh:mm tt");
                                    attendanceList.outtime = !attendance.OutTime.HasValue ? "" : attendance.OutTime.Value.ToString("dd/MM/yyyy hh:mm tt");
                                    if (attendance.InTime.HasValue && attendance.OutTime.HasValue)
                                    {
                                        TimeSpan totalhrs = attendance.OutTime.Value.TimeOfDay - attendance.InTime.Value.TimeOfDay;
                                        attendanceList.workhrs = totalhrs.ToString(@"hh\:mm\:ss");
                                    }
                                    else
                                    {
                                        attendanceList.workhrs = "";
                                    }
                                    attendanceList.imageurl = attendance.Image;
                                    attendanceLists.Add(attendanceList);
                                }
                                else
                                {
                                    AbsentList absentList = new AbsentList();
                                    absentList.name = item.FirstName + " " + item.LastName;
                                    absentLists.Add(absentList);

                                }
                            }
                        }


                        response.attendanceLists = attendanceLists;
                        response.absentLists = absentLists;
                        response.Message = "All User Attendace Fetch Successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = " users not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = " Bloodbank not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "GetAllAttendanceList Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }



        public AddCallSettlementResponse AddCallSettlement(AddCallSettlementRequest request)
        {
            AddCallSettlementResponse response = new AddCallSettlementResponse();
            try
            {
                string bloodbankId = request.bloodbankId;
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    ScheduleCall scheduleCall = this.DBContext.ScheduleCall.FirstOrDefault(x => x.CallId == request.callId &&x.FkBloodbankId==bloodbankId);
                    if (scheduleCall != null)
                    {
                        string PostImageUrlAwsS3 = "";
                        string PostImageBase64 = request.imageUrl;
                        if (StringExtensions.HasValue(PostImageBase64))
                        {
                            string BucketKeyPosts = s3Repository.s3Settings.Value.BucketChildKeyPosts;
                            UploadPhotoModel uploadPhotoResponse = s3Repository.UploadObject(
                                s3Repository.s3Settings,
                                PostImageBase64,
                                BucketKeyPosts).Result;
                            PostImageUrlAwsS3 = uploadPhotoResponse.FileName;
                        }



                        CallSettlement callSettlement = new CallSettlement();
                        callSettlement.FkCallId = request.callId;
                        callSettlement.Km = request.km;
                        callSettlement.ImageUrl = PostImageUrlAwsS3;
                        callSettlement.IsKmapproved = "false";
                        callSettlement.ApprovedById = null;
                        //callSettlement.DateTime = DateTime.Now;
                        callSettlement.DateTime = GetLocalDateTime();
                        callSettlement.Amount = null;
                        callSettlement.SettlementStatus = null;
                        callSettlement.CollectionSettlementStatus = null;
                        callSettlement.FkBloodbankId = bloodbankId;
                        this.DBContext.CallSettlement.Add(callSettlement);
                        int affectedRows = this.DBContext.SaveChanges();
                        response.Success = affectedRows == 1;
                        if (response.Success)
                        {
                            scheduleCall.Status = "pendingapprove";
                            int updatedRows = this.DBContext.SaveChanges();
                            response.Success = updatedRows == 1;
                            if (response.Success)
                            {
                                response.Message = "CallSettlement inserted and schedule call updated successfully";
                                response.Success = true;
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.ResponseCode = 0;
                            }
                            else
                            {
                                response.Message = "Failed to insert call settlement and updated schedule call";
                                response.Success = false;
                                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                                response.ResponseCode = 1;
                            }
                        }
                        else
                        {
                            response.Message = "Failed to insert call settlement";
                            response.Success = false;
                            response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "Call not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Bloodbank not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "AddCallSettlement Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public GetApproveKilometerListResponse GetApproveKilometerList(GetApproveKilometerListRequest request)
        {
            GetApproveKilometerListResponse response = new GetApproveKilometerListResponse();
            List<ApproveKilometerInfo> allApproveKilometerInfos = new List<ApproveKilometerInfo>();
            try
            {
                string bloodbankId = request.bloodbankId;
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    List<CallSettlement> allCallSettlements = this.DBContext.CallSettlement.Where(x => Convert.ToBoolean(x.IsKmapproved.ToLower()) == false &&x.FkBloodbankId==bloodbankId).ToList();
                    if (allCallSettlements != null && allCallSettlements.Count > 0)
                    {
                        foreach (var item in allCallSettlements)
                        {
                            ScheduleCall scheduleCall = this.DBContext.ScheduleCall.FirstOrDefault(x => x.CallId == item.FkCallId && x.FkBloodbankId==bloodbankId);
                            if (scheduleCall != null)
                            {
                                ApproveKilometerInfo approveKilometerInfoObject = new ApproveKilometerInfo();
                                approveKilometerInfoObject.callId = item.FkCallId;

                                int hospitalId = this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == item.FkCallId).FkHospitalId.Value;
                                //approveKilometerInfoObject.hospitalName = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == hospitalId).HospitalName;
                                string _address = this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == item.FkCallId).Address;
                                approveKilometerInfoObject.hospitalName = hospitalId == 0 ? _address : this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == hospitalId).HospitalName;
                                approveKilometerInfoObject.assignedUserId = scheduleCall.AssignedUserId;
                                approveKilometerInfoObject.assignedUserName = scheduleCall.AssignedUserId.ToLower() == "na" ? "" : this.DBContext.Users.FirstOrDefault(x => x.Id == scheduleCall.AssignedUserId).UserName;
                                approveKilometerInfoObject.callDateTime = scheduleCall.CallDateTime.Value;
                                approveKilometerInfoObject.kmImage = string.IsNullOrEmpty(item.ImageUrl) ? "" : item.ImageUrl;
                                approveKilometerInfoObject.KM = item.Km > 0 ? item.Km : 0;

                                allApproveKilometerInfos.Add(approveKilometerInfoObject);
                            }
                        }

                        response.approveKilometerList = allApproveKilometerInfos;
                        response.Message = "Approve kilometer list fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Call settlements not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Bloodbank not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "GetApproveKilometerList Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public ApproveCallSettlementResponse ApproveCallSettlement(ApproveCallSettlementRequest request)
        {
            ApproveCallSettlementResponse response = new ApproveCallSettlementResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName==UserName);
                if (userProfileRecord != null)
                {
                    string bloodbankId = request.bloodbankId;
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {

                        CallSettlement callSettlement = this.DBContext.CallSettlement.FirstOrDefault(x => x.FkCallId == request.CallId &&x.FkBloodbankId==bloodbankId);
                        if (callSettlement != null)
                        {
                            callSettlement.IsKmapproved = "true";
                            callSettlement.SettlementStatus = "pending";
                            callSettlement.CollectionSettlementStatus = "pending";
                            callSettlement.ApprovedById = userProfileRecord.Id;
                            decimal kmRate = (decimal)this.DBContext.Kmrate.Where(x => x.FkBloodbankId == bloodbankId).ToList().First().Rate;
                            decimal km = (decimal)callSettlement.Km;
                            callSettlement.Amount = ((int?)(km * kmRate));

                            int affectedStockRows = this.DBContext.SaveChanges();
                            response.Success = affectedStockRows == 1;
                            if (response.Success)
                            {
                                ScheduleCall scheduleCall = this.DBContext.ScheduleCall.FirstOrDefault(x => x.CallId == request.CallId &&x.FkBloodbankId== bloodbankId);
                                scheduleCall.Status = "complete";
                                int updatedRows = this.DBContext.SaveChanges();
                                response.Success = updatedRows == 1;
                                if (response.Success)
                                {
                                    response.Message = "Call settlement & schedule call updated successfully";
                                    response.Success = true;
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.ResponseCode = 0;
                                }
                                else
                                {
                                    response.Message = "Failed to update approve and schedule call";
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.Success = false;
                                    response.ResponseCode = 1;
                                }
                            }
                            else
                            {
                                response.Message = "Failed to update approve call settlement";
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.Success = false;
                                response.ResponseCode = 1;
                            }
                        }
                        else
                        {
                            response.Message = "Call settlement not found";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "Bloodbank not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "ApproveCallSettlement Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public CalculateRequisitionAmountResponse CalculateRequisitionAmount(CalculateRequisitionAmountRequest request)
        {
            CalculateRequisitionAmountResponse response = new CalculateRequisitionAmountResponse();
            try
            {
                string bloodbankId = request.bloodbankId;
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    int amountResult = 0;
                    if (Convert.ToBoolean(request.isNat))
                    {
                        foreach (var item in request.claculateRequisitionByProducts)
                        {
                            int productMasterId = this.DBContext.ProductMaster.FirstOrDefault(x => x.ProductName.ToLower() == item.productName.ToLower()).ProductId;
                            int? natRate = this.DBContext.ProductRate.FirstOrDefault(x => x.FkProductMasterId == productMasterId &&x.FkBloodbankId==bloodbankId).NatRate;
                            amountResult = amountResult + (item.quantity * natRate.Value);
                        }

                        response.amount = amountResult;
                        response.Message = "Requisition amount calculated successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        foreach (var item in request.claculateRequisitionByProducts)
                        {
                            int productMasterId = this.DBContext.ProductMaster.FirstOrDefault(x => x.ProductName.ToLower() == item.productName.ToLower()).ProductId;
                            int? rate = this.DBContext.ProductRate.FirstOrDefault(x => x.FkProductMasterId == productMasterId &&x.FkBloodbankId==bloodbankId).Rate;
                            amountResult = amountResult + (item.quantity * rate.Value);
                        }

                        response.amount = amountResult;
                        response.Message = "Requisition amount calculated successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                }
                else
                {
                    response.Message = "Bloodbank not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "CalculateRequisitionAmount Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public UserSettlementAmountResponse UserSettlementAmount(UserSettlementAmountRequest request)
        {
            UserSettlementAmountResponse response = new UserSettlementAmountResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName.Contains(UserName));
                if (userProfileRecord != null)
                {
                    //List<CallSettlement> callSettlementList = "";
                    //if (callSettlementList != null && callSettlementList.Count > 0)
                    //{
                    //    foreach (var item in callSettlementList)
                    //    {
                    //        string assignedUserId = this.DBContext.ScheduleCall.FirstOrDefault(x => x.CallId == item.FkCallId).AssignedUserId;
                    //        if (assignedUserId == userProfileRecord.Id)
                    //        {

                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    response.Message = "Call settlements not found";
                    //    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    //    response.Success = false;
                    //    response.ResponseCode = 1;
                    //}
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "UserSettlementAmount Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }
        //GM API
        public getCurrentStockDetailsResponse getCurrentStockDetails(getCurrentStockDetailsRequest request)
        {
            getCurrentStockDetailsResponse response = new getCurrentStockDetailsResponse();
            List<StockDetailsDataList> stockDetailsDataLists = new List<StockDetailsDataList>();
            DateTime todaydate = GetLocalDateTime();
            try
            {
                string bloodbankId = request.bloodbankId;
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    //StockDetailsDataList stoxdetailsInfo5 = new StockDetailsDataList();
                    //stoxdetailsInfo5.StockType = "PLASMA";
                    //stoxdetailsInfo5.OPositiveStock = "15";
                    //stoxdetailsInfo5.ONegativeStock = "15";
                    //stoxdetailsInfo5.APositiveStock = "15";
                    //stoxdetailsInfo5.ANegativeStock = "15";
                    //stoxdetailsInfo5.ABPositiveStock = "15";
                    //stoxdetailsInfo5.ABNegativeStock = "15";
                    //stoxdetailsInfo5.BPositiveStock = "15";
                    //stoxdetailsInfo5.BNegativeStock = "15";
                    //stoxdetailsInfo5.Total = "120";
                    //stockDetailsDataLists.Add(stoxdetailsInfo5);
                    //--------------------------------------------------------------------------------------


                    List<ProductMaster> productMastersList = this.DBContext.ProductMaster.ToList();
                    if (productMastersList != null && productMastersList.Count > 0)
                    {
                        foreach (var item in productMastersList)
                        {
                            StockDetailsDataList stoxdetailsInfo2 = new StockDetailsDataList();
                            stoxdetailsInfo2.StockType = item.ProductName.ToUpper();

                            stoxdetailsInfo2.OPositiveStock = this.DBContext.StockDetails.Where(x => x.ProductId == item.ProductId && x.IssueFlag.ToLower() == "available" && x.ExpiryDate.Value.Date >= todaydate.Date && x.BloodGroup.ToUpper().Trim() == "O+" && x.FkBloodbankId == bloodbankId).Count();
                            stoxdetailsInfo2.ONegativeStock = this.DBContext.StockDetails.Where(x => x.ProductId == item.ProductId && x.IssueFlag.ToLower() == "available" && x.ExpiryDate.Value.Date > todaydate.Date && x.BloodGroup.ToUpper().Trim() == "O-" && x.FkBloodbankId == bloodbankId).Count();

                            stoxdetailsInfo2.APositiveStock = this.DBContext.StockDetails.Where(x => x.ProductId == item.ProductId && x.IssueFlag.ToLower() == "available" && x.ExpiryDate.Value.Date >= todaydate.Date && x.BloodGroup.ToUpper().Trim() == "A+" && x.FkBloodbankId == bloodbankId).Count();
                            stoxdetailsInfo2.ANegativeStock = this.DBContext.StockDetails.Where(x => x.ProductId == item.ProductId && x.IssueFlag.ToLower() == "available" && x.ExpiryDate.Value.Date >= todaydate.Date && x.BloodGroup.ToUpper().Trim() == "A-" && x.FkBloodbankId == bloodbankId).Count();

                            stoxdetailsInfo2.ABPositiveStock = this.DBContext.StockDetails.Where(x => x.ProductId == item.ProductId && x.IssueFlag.ToLower() == "available" && x.ExpiryDate.Value.Date >= todaydate.Date && x.BloodGroup.ToUpper().Trim() == "AB+" && x.FkBloodbankId == bloodbankId).Count();
                            stoxdetailsInfo2.ABNegativeStock = this.DBContext.StockDetails.Where(x => x.ProductId == item.ProductId && x.IssueFlag.ToLower() == "available" && x.ExpiryDate.Value.Date >= todaydate.Date && x.BloodGroup.ToUpper().Trim() == "AB-" && x.FkBloodbankId == bloodbankId).Count();

                            stoxdetailsInfo2.BPositiveStock = this.DBContext.StockDetails.Where(x => x.ProductId == item.ProductId && x.IssueFlag.ToLower() == "available" && x.ExpiryDate.Value.Date >= todaydate.Date && x.BloodGroup.ToUpper().Trim() == "B+" && x.FkBloodbankId == bloodbankId).Count();
                            stoxdetailsInfo2.BNegativeStock = this.DBContext.StockDetails.Where(x => x.ProductId == item.ProductId && x.IssueFlag.ToLower() == "available" && x.ExpiryDate.Value.Date >= todaydate.Date && x.BloodGroup.ToUpper().Trim() == "B-" && x.FkBloodbankId == bloodbankId).Count();

                            stoxdetailsInfo2.Total = (stoxdetailsInfo2.OPositiveStock + stoxdetailsInfo2.ONegativeStock +
                                                     stoxdetailsInfo2.APositiveStock + stoxdetailsInfo2.ANegativeStock +
                                                     stoxdetailsInfo2.ABPositiveStock + stoxdetailsInfo2.ABNegativeStock +
                                                     stoxdetailsInfo2.BPositiveStock + stoxdetailsInfo2.BNegativeStock);

                            stockDetailsDataLists.Add(stoxdetailsInfo2);
                        }

                        response.Status = "True";
                        response.stockDetailsDataLists = stockDetailsDataLists;
                        response.Message = "All Current Stock Details fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Products not found";
                        response.Success = false;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "BloodBank not found";
                    response.Success = false;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "getCurrentStockDetails Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }
        //GM API
        public GetServiceBoysWithCallDetailsResponse GetServiceBoysWithCallDetails(GetServiceBoysWithCallDetailsRequest request)
        {
            GetServiceBoysWithCallDetailsResponse response = new GetServiceBoysWithCallDetailsResponse();
            List<ServiceBoyCallDetailsDataList> serviceBoyCallDetailsDataLists = new List<ServiceBoyCallDetailsDataList>();
            List<FreeServiceBoyCallDetailsDataList> freeServiceBoyCallDetailsDataLists = new List<FreeServiceBoyCallDetailsDataList>();
            List<NotAssignServiceBoyCallDetailsDataList> notAssignServiceBoyCallDetailsDataLists = new List<NotAssignServiceBoyCallDetailsDataList>();

            DateTime todayDate = GetLocalDateTime();
            try
            {
                string bloodbankId = request.bloodbankId;
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    // Fetch All Serviceboy Users
                    var serviceBoyUsers = userManager.GetUsersInRoleAsync("Serviceboy").Result;
                    serviceBoyUsers = serviceBoyUsers.Where(x => x.IsDelete == 0).ToList();
                    if (serviceBoyUsers.Count > 0)
                    {
                        foreach (var item in serviceBoyUsers)
                        {
                            UsersbloodbankMaster usersbloodbankMaster = this.DBContext.UsersbloodbankMaster.FirstOrDefault(x => x.FkBloodBankId == bloodbankId && x.UserId == item.Id);
                            if (usersbloodbankMaster != null)
                            {
                                Attendance attendance = this.DBContext.Attendance.FirstOrDefault(x => x.UserId == item.Id && x.AttendanceDateTime.Value.Date == todayDate.Date && x.InTime.HasValue && x.OutTime.Value == null && x.FkBloodbankId == bloodbankId);
                                if (attendance != null)
                                {
                                    List<ScheduleCall> scheduleCall = this.DBContext.ScheduleCall.Where(x => x.AssignedUserId == attendance.UserId && x.CallDateTime.Value.Date == todayDate.Date && x.FkBloodbankId == bloodbankId && x.Status != "complete").ToList();
                                    if (scheduleCall != null && scheduleCall.Count > 0)
                                    {
                                        foreach (var schedulecallitem in scheduleCall)
                                        {
                                            ServiceBoyCallDetailsDataList serviceboycallInfo = new ServiceBoyCallDetailsDataList();

                                            serviceboycallInfo.ServiceBoyName = item.FirstName + " " + item.LastName;
                                            //  serviceboycallInfo.IsFreeNow = schedulecallitem.Status.ToLower() == "inprogress" ? false : true;
                                            int _hospitalId = this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == schedulecallitem.CallId).FkHospitalId.Value;
                                            serviceboycallInfo.CallLocation = _hospitalId == 0 ? "" : this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == _hospitalId).HospitalName;//Hospital name
                                            serviceboycallInfo.CallTime = schedulecallitem.CallDateTime.Value.ToString("dd/MM/yyyy hh:mm tt");
                                            string patient_Name = this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == schedulecallitem.CallId).PatientName;
                                            serviceboycallInfo.PatientName = string.IsNullOrEmpty(patient_Name) ? "" : patient_Name;
                                            serviceboycallInfo.CallStatus = schedulecallitem.Status.ToLower();
                                            serviceBoyCallDetailsDataLists.Add(serviceboycallInfo);
                                        }
                                    }
                                    ScheduleCall scheduleCalls = this.DBContext.ScheduleCall.FirstOrDefault(x => x.AssignedUserId == attendance.UserId && x.CallDateTime.Value.Date == todayDate.Date && x.Status.ToLower() == "inprogress" && x.FkBloodbankId == bloodbankId);
                                    if (scheduleCalls == null)
                                    {
                                        FreeServiceBoyCallDetailsDataList FreeServiceBoy = new FreeServiceBoyCallDetailsDataList();
                                        FreeServiceBoy.ServiceBoyName = item.FirstName + " " + item.LastName;
                                        FreeServiceBoy.todayTotalAssigncalls = this.DBContext.ScheduleCall.Where(x => x.AssignedUserId == attendance.UserId && x.CallDateTime.Value.Date == todayDate.Date &&x.FkBloodbankId==bloodbankId).Count();
                                        freeServiceBoyCallDetailsDataLists.Add(FreeServiceBoy);

                                    }

                                }
                            }
                        }


                        List<ScheduleCall> scheduleCalls1 = this.DBContext.ScheduleCall.Where(x => x.AssignedUserId.ToLower() == "na" && x.CallDateTime.Value.Date == todayDate.Date && x.Status == "pending").ToList();
                        if (scheduleCalls1 != null && scheduleCalls1.Count > 0)
                        {
                            foreach (var scheduleCalls1item in scheduleCalls1)
                            {
                                NotAssignServiceBoyCallDetailsDataList notAssignServiceBoyCallDetailsInfo = new NotAssignServiceBoyCallDetailsDataList();

                                notAssignServiceBoyCallDetailsInfo.ServiceBoyName = "";
                                notAssignServiceBoyCallDetailsInfo.CallStatus = "pending";
                                int _hospitalId = this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == scheduleCalls1item.CallId).FkHospitalId.Value;
                                notAssignServiceBoyCallDetailsInfo.CallLocation = _hospitalId == 0 ? "" : this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == _hospitalId).HospitalName;//Hospital name
                                notAssignServiceBoyCallDetailsInfo.CallTime = scheduleCalls1item.CallDateTime.Value.ToString("dd/MM/yyyy hh:mm tt");
                                string patient_Name = this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == scheduleCalls1item.CallId).PatientName;
                                notAssignServiceBoyCallDetailsInfo.PatientName = string.IsNullOrEmpty(patient_Name) ? "" : patient_Name;
                                notAssignServiceBoyCallDetailsInfo.CallStatus = scheduleCalls1item.Status.ToLower();
                                notAssignServiceBoyCallDetailsDataLists.Add(notAssignServiceBoyCallDetailsInfo);
                            }


                        }

                        response.Status = "True";
                        response.serviceBoyCallDetailsDataLists = serviceBoyCallDetailsDataLists;
                        response.freeServiceBoyCallDetailsDataLists = freeServiceBoyCallDetailsDataLists;
                        response.notAssignServiceBoyCallDetailsDataLists = notAssignServiceBoyCallDetailsDataLists;
                        response.Message = "Service Boys With Call Details fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Serviceboy users not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Bloodbank not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "GetServiceBoysWithCallDetails Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        //GM API
        public GetProductRateResponse GetProductRate(GetProductRateRequest request)
        {
            GetProductRateResponse response = new GetProductRateResponse();
            List<ProductRateList> tempRateLists = new List<ProductRateList>();
            try
            {
                string bloodbankId = request.bloodbankId;
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    List<ProductRate> productRatesList = this.DBContext.ProductRate.Where(x => x.FkBloodbankId == bloodbankId).ToList();
                    if (productRatesList != null && productRatesList.Count > 0)
                    {
                        foreach (var item in productRatesList)
                        {
                            ProductRateList productRateListObj = new ProductRateList();
                            productRateListObj.ProductId = (int)item.FkProductMasterId;
                            productRateListObj.ProductName = this.DBContext.ProductMaster.FirstOrDefault(x => x.ProductId== item.FkProductMasterId).ProductName;
                            productRateListObj.Rate = item.Rate;
                            productRateListObj.NatRate = item.NatRate;
                            productRateListObj.CrossMatchRate = item.CrossMatchRate;
                            productRateListObj.CrossMatchNatRate = item.CrossMatchNatRate;

                            tempRateLists.Add(productRateListObj);
                        }
                        response.productRateLists = tempRateLists;
                        response.Message = "All product rate fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        //Product rate list not found
                        response.Message = "Product Rate list not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Bloodbank not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "GetProductRate Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }


        public CalculateCallSettlementResponse CalculateCallSettlement(CalculateCallSettlementRequest request)
        {
            CalculateCallSettlementResponse response = new CalculateCallSettlementResponse();
            try
            {
                string bloodbankId = request.bloodbankId;
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName== UserName);
                if (userProfileRecord != null)
                {
                    var roles = userManager.GetRolesAsync(userProfileRecord).Result;
                    List<ScheduleCall> scheduleCallList = new List<ScheduleCall>();
                    if (roles.Contains("Admin"))
                    {
                        DateTime currentDateTime = DateTime.Now;
                        //DateTime dt = currentDateTime.Date;
                        //DateTime dt2 = currentDateTime.Date.AddDays(-1);
                        //scheduleCallList = this.DBContext.ScheduleCall.Where(x => x.CallDateTime.Value.Date == currentDateTime.Date.AddDays(-1)).ToList();
                        //scheduleCallList = this.DBContext.ScheduleCall.Where(x => x.CallDateTime.Value.Date == currentDateTime.Date).ToList();
                        scheduleCallList = (from scheduecall in this.DBContext.ScheduleCall
                                            join callSettlement in this.DBContext.CallSettlement on scheduecall.CallId equals callSettlement.FkCallId
                                            where callSettlement.IsKmapproved.ToLower() == "true" && (callSettlement.SettlementStatus.ToLower() == "pending"|| callSettlement.CollectionSettlementStatus.ToLower()=="pending")&& scheduecall.Status.ToLower() == "complete" &&scheduecall.FkBloodbankId==bloodbankId &&callSettlement.FkBloodbankId==bloodbankId
                                            select scheduecall).ToList();
                    }
                    else if (roles.Contains("Serviceboy"))
                    {
                        scheduleCallList = (from scheduecall in this.DBContext.ScheduleCall
                                            join callSettlement in this.DBContext.CallSettlement on scheduecall.CallId equals callSettlement.FkCallId
                                            where callSettlement.IsKmapproved.ToLower() == "true" && (callSettlement.SettlementStatus.ToLower() == "pending" || callSettlement.CollectionSettlementStatus.ToLower() == "pending") &&   scheduecall.Status.ToLower() == "complete" && scheduecall.AssignedUserId == userProfileRecord.Id && scheduecall.FkBloodbankId == bloodbankId && callSettlement.FkBloodbankId == bloodbankId
                                            select scheduecall).ToList();
                    }

                    if (scheduleCallList != null && scheduleCallList.Count > 0)
                    {
                        response.callListDetailsInfos = CallSettelementCalculation(scheduleCallList,bloodbankId);
                        if (!string.IsNullOrEmpty(request.serviceBoyName))
                        {
                            response.callListDetailsInfos = response.callListDetailsInfos.Where(x => x.serviceBoyName.ToLower() == request.serviceBoyName.ToLower()).ToList();
                            response.collectedAmount = response.callListDetailsInfos.Sum(x=>x.callcollectioninfo.Sum(x=>x.collectedAmount));
                            response.petrolAmount = response.callListDetailsInfos.Sum(x => x.petrolAmount);
                            response.balance = (response.collectedAmount - response.petrolAmount);
                        }
                        else
                        {
                            response.collectedAmount = response.callListDetailsInfos.Sum(x => x.callcollectioninfo.Sum(x => x.collectedAmount));
                            // response.collectedAmount = response.callListDetailsInfos.Sum(x => x.collectedAmount);
                            response.petrolAmount = response.callListDetailsInfos.Sum(x => x.petrolAmount);
                            response.balance = (response.collectedAmount - response.petrolAmount);
                        }
                        response.Message = "Call settlement calculated successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Schedule calls not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "CalculateCallSettlement Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public List<CallSettlementInfo> CallSettelementCalculation(List<ScheduleCall> scheduleCallList,string bloodbankId)
        {
            List<CallSettlementInfo> callSettlements = new List<CallSettlementInfo>();
            List<CallCollectionInfo> callCollectionInList = new List<CallCollectionInfo>();
            foreach (var item in scheduleCallList)
            {
                CallSettlement callSettlement = this.DBContext.CallSettlement.FirstOrDefault(x => x.FkCallId == item.CallId && (x.SettlementStatus.ToLower() == "pending" ||x.CollectionSettlementStatus.ToLower()=="pending") && x.IsKmapproved.ToLower() == "true" &&x.FkBloodbankId==bloodbankId);
                if (callSettlement != null)
                {
                    CallSettlementInfo callSettlementInfoObject = new CallSettlementInfo();
                    callSettlementInfoObject.callId = item.CallId;
                    callSettlementInfoObject.serviceBoyUserId = item.AssignedUserId;
                    callSettlementInfoObject.serviceBoyName = this.DBContext.Users.FirstOrDefault(x => x.Id == item.AssignedUserId).UserName;
                    if (callSettlement.SettlementStatus.ToLower() == "pending")
                    {
                        callSettlementInfoObject.petrolAmount = callSettlement.Amount.Value;
                    }

                    //CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.FkCallId == item.CallId);
                    List<CallDetails> callDetailList = this.DBContext.CallDetails.Where(x => x.FkCallId == item.CallId).ToList();
                    foreach (var callDetailsitem in callDetailList)
                    {
                        if (callSettlement != null && callDetailList != null && callDetailList.Count > 0)
                        {
                            CallCollectionInfo callCollectionInfo = new CallCollectionInfo();

                            callCollectionInfo.hospitalName = callDetailsitem.FkHospitalId == 0 ? "" : this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetailsitem.FkHospitalId).HospitalName;
                            if (callSettlement.CollectionSettlementStatus.ToLower() == "pending")
                            {
                                if (callDetailsitem.ReqBillType.ToLower() == "samplecollection")
                                {
                                    callCollectionInfo.collectedAmount = this.DBContext.Requisition.FirstOrDefault(x => x.FkCallId == callDetailsitem.CallDetailsId).PaidAmount.Value;
                                }
                                else if (callDetailsitem.ReqBillType.ToLower() == "issuebag")
                                {
                                    //int _requisitionId = this.DBContext.Requisition.FirstOrDefault(x => x.FkCallId == item.CallId).RequisitionId;
                                    //callCollectionInfo.collectedAmount = this.DBContext.Bill.FirstOrDefault(x => x.FkRequisitionId == _requisitionId).TotalAmount.Value;
                
                                    callCollectionInfo.collectedAmount = this.DBContext.Bill.FirstOrDefault(x => x.BillId ==callDetailsitem.BillReqId).NetAmount.Value;

                                }
                                else
                                {
                                    callCollectionInfo.collectedAmount = 0;
                                }
                            }



                            callCollectionInfo.callDetailsId = callDetailsitem.CallDetailsId;
                            callCollectionInfo.patientName = "";
                            callSettlementInfoObject.callcollectioninfo.Add(callCollectionInfo);
                        }
                    }
                    //callSettlementInfoObject.callcollectioninfo = callCollectionInList;
                    callSettlements.Add(callSettlementInfoObject);

                }


            }//End Of Foreach
            
            return callSettlements;
        }

        public void SendSMS(string type, int req_Bill_id, string userMobile1)
        {
            //SMS Code
            string URL = string.Empty;
            string smsUrl = string.Empty;
            if (type.ToLower() == "requisition")
            {
               
                //for centralized dev
                URL = "http://65.1.247.197:92/Home/MR?id=" + req_Bill_id;
                
                //for weeto production
               // URL = "http://ec2-65-1-247-197.ap-south-1.compute.amazonaws.com:2202/Home/MR?id=" + req_Bill_id;
                smsUrl = shortUrlName(URL);
            }
            else
            {
                // This URL is used for Bill for centralized
                URL = "http://65.1.247.197:92/Home/PatientBilldetails?id=" + req_Bill_id;


                //this Url is  for bill weeto production
              //  URL = "http://ec2-65-1-247-197.ap-south-1.compute.amazonaws.com:2202/Home/PatientBilldetails?id=" + req_Bill_id;

                smsUrl = shortUrlName(URL);
            }

            string userMobile = userMobile1;
            //string _message = "Hi \n"
            //                 + "You can download your " + type.ToLower() + " here\n"
            //                 + URL + "\n"
            //                 + "Thanks\n"
            //                 + "PSI Blood Center";

            // string sURL = "https://otpsms.vision360solutions.in/api/sendhttp.php?authkey=350789A1TsTLhv75feed226P1&mobiles=" + userMobile + "&message=" + _message + "&sender=Vision&route=4&country=91";
            //DateTime otptime = DateTime.Now;
            //new register sms
            string sURL = "https://otpsms.vision360solutions.in/api/sendhttp.php?authkey=350789A1TsTLhv75feed226P1&mobiles=" + userMobile + "&message=Dear%20User%2C%0AYou%20can%20download%20your%20" + type.ToLower() + "%20here%20using%20following%20link%20-%20" + smsUrl + "%0ARegards%2C%0APSI%20Blood%20Center&sender=PSIBCB&route=4&country=91&DLT_TE_ID=1207163671571751402";

            DateTime otptime = GetLocalDateTime();
            string sResponse = GetResponse(sURL);
            //SMS Code End
        }


        public string shortUrlName(string url)
        {

            string shortUrl = "";
            string URL = "https://cutt.ly/api/api.php";
            string urlParameters = "?key=ebbd6049e8c21170557087d0df0f851df707c&short=" + url + "&&noTitle = 1";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {
                var json = response.Content.ReadAsStringAsync().Result;
                var jo = JObject.Parse(json);
                shortUrl = jo["url"]["shortLink"].ToString();
            }

          
            else
            {
                shortUrl = "";
            }

            //Make any other calls using HttpClient here.

            //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();

            return shortUrl;
        }


        //public AccountsResponse Accounts(AccountsRequest request)
        //{
        //    AccountsResponse response = new AccountsResponse();

        //    List<CashDetails> cashes = new List<CashDetails>();
        //    List<CreaditDetails> creadits = new List<CreaditDetails>();
        //    List<ExpenseDetails> expenses1 = new List<ExpenseDetails>();

        //    int pageSize = 10;

        //    try
        //    {
        //        if (request.pageNumber >= 1)
        //        {
        //            List<Expense> expenses = new List<Expense>();
        //            List<InvoiceNo> invoiceNos = new List<InvoiceNo>();
        //            if (request.fromDate.HasValue && request.toDate.HasValue)
        //            {
        //                DateTime startDate = request.fromDate.Value;
        //                DateTime endDate = request.toDate.Value;
        //                //    invoiceNos = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date >= startDate.Date && x.InvoiceDate.Value.Date <= endDate.Date).ToList();    added by sk 3/5/2021
        //                //    expenses = this.DBContext.Expense.Where(x => x.DateTime.Value.Date >= startDate.Date && x.DateTime.Value.Date <= endDate.Date).ToList();       //added by sk 3/5/2021
        //                invoiceNos = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date >= startDate.Date && x.InvoiceDate.Value.Date <= endDate.Date).OrderByDescending(x => x.InvoiceDate).ToList();
        //                expenses = this.DBContext.Expense.Where(x => x.DateTime.Value.Date >= startDate.Date && x.DateTime.Value.Date <= endDate.Date).OrderByDescending(x => x.DateTime).ToList();
        //            }
        //            else
        //            {
        //                DateTime startDate = request.fromDate.Value;
        //                invoiceNos = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date == startDate.Date).OrderByDescending(x => x.InvoiceDate).ToList();
        //                expenses = this.DBContext.Expense.Where(x => x.DateTime.Value.Date == startDate.Date).OrderByDescending(x => x.DateTime).ToList();
        //            }

        //            if (request.fromTime.HasValue && request.toTime.HasValue)
        //            {
        //                DateTime dateTime1 = request.fromTime.Value;
        //                DateTime dateTime2 = request.toTime.Value;
        //                invoiceNos = invoiceNos.Where(x => x.InvoiceDate.Value.TimeOfDay >= dateTime1.TimeOfDay && x.InvoiceDate.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
        //                expenses = expenses.Where(x => x.DateTime.Value.TimeOfDay >= dateTime1.TimeOfDay && x.DateTime.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
        //            }
        //            if (invoiceNos != null && invoiceNos.Count > 0)
        //            {
        //                foreach (var item in invoiceNos)
        //                {
        //                    if (item.Type.ToLower() == "requisition")
        //                    {

        ////
        //                        Requisition requisitions = this.DBContext.Requisition.FirstOrDefault(x => x.RequisitionId == item.TypeId);
        //   if(requisitions !=null)
        //{
        //CallDetails calldetailsRecords=this.DBContext.CallDetails.FirstOrDefault(x => x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower()== "requisition");
        //                         HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == calldetailsRecords.FkHospitalId);
        //                        if (requisitions.PaymentType.ToLower() == "cash" || requisitions.PaymentType.ToLower() == "upi" || requisitions.PaymentType.ToLower() == "discount" || requisitions.PaymentType == "")
        //                        {

        //                             CashDetails Details = new CashDetails();
        //                             Details.patientName = calldetailsRecords.PatientName;
        //                                //this.DBContext.CallDetails.FirstOrDefault(x => x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower()== "requisition").PatientName;
        //                             Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
        //                                //(int)this.DBContext.CallDetails.FirstOrDefault(x => x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower()== "requisition").FkHospitalId;
        //                             Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
        //                                    //this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == Details.hospitalId).HospitalName;
        //                             Details.type = "cash";
        //                             Details.InvoiceDate = item.InvoiceDate;
        //                             Details.InvoiceNumber = item.InvoiceNumber;
        //                             Details.Billtype = "requisition";

        //                             Details.amount = requisitions.PaidAmount.Value;
        //                            // response.cashDetails.Add(Details);        //added by sk 03/05/2021
        //                             cashes.Add(Details);


        //                        }
        //                        else if (requisitions.PaymentType.ToLower() == "credit")
        //                        {
        //                            CreaditDetails Details = new CreaditDetails();
        //                            Details.patientName = calldetailsRecords.PatientName;
        //                                //this.DBContext.CallDetails.FirstOrDefault(x => x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower()== "requisition").PatientName;
        //                            Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
        //                            Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
        //                                    //this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == Details.hospitalId).HospitalName;
        //                            Details.type = "credit";
        //                            Details.Billtype = "requisition";
        //                            Details.InvoiceNumber = item.InvoiceNumber;
        //                            Details.InvoiceDate = item.InvoiceDate;
        //                            Details.amount = requisitions.PaidAmount.Value;
        //                            //  response.creaditDetails.Add(Details);   //added by sk 03/05/2021
        //                            creadits.Add(Details);
        //} 
        //  }
        //                    }


        //                    else if (item.Type.ToLower() == "bill")
        //                    {
        //                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId);
        //                        if (bills != null)
        //                        {
        //                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "issuebag");
        //                            HospitalMaster hospitalMaster =this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
        //                            if (bills.PaymentType.ToLower() == "cash" || bills.PaymentType.ToLower() == "upi" || bills.PaymentType.ToLower() == "discount" || bills.PaymentType == "")
        //                            {
        //                                CashDetails Details = new CashDetails();
        //                                Details.patientName = callDetails.PatientName;
        //                                //this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "issuebag").PatientName;
        //                                Details.hospitalId = (int)callDetails.FkHospitalId;
        //                                //(int)this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "issuebag").FkHospitalId;
        //                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
        //                                    //this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == Details.hospitalId).HospitalName;
        //                                Details.type = "cash";
        //                                Details.Billtype = "bill";
        //                                Details.InvoiceDate = item.InvoiceDate;
        //                                Details.InvoiceNumber = item.InvoiceNumber;
        //                                Details.amount = bills.NetAmount.Value;
        //                                // response.cashDetails.Add(Details);      //added by sk 03/05/2021
        //                                cashes.Add(Details);
        //                            }
        //                            else if (bills.PaymentType == "credit")
        //                            {
        //                                CreaditDetails Details = new CreaditDetails();
        //                                Details.patientName = callDetails.PatientName;
        //                                // this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "issuebag").PatientName;
        //                                Details.hospitalId = (int)callDetails.FkHospitalId;
        //                                //(int)this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "issuebag").FkHospitalId;
        //                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
        //                                    //this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == Details.hospitalId).HospitalName;
        //                                Details.type = "credit";
        //                                Details.Billtype = "bill";
        //                                Details.InvoiceNumber = item.InvoiceNumber;
        //                                Details.InvoiceDate = item.InvoiceDate;
        //                                Details.amount = bills.NetAmount.Value;
        //                                // response.creaditDetails.Add(Details);     //added by sk 03/05/2021
        //                                creadits.Add(Details);
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //            if (expenses != null && expenses.Count > 0)
        //            {
        //                foreach (var item3 in expenses)
        //                {
        //                    ExpenseDetails Details = new ExpenseDetails();
        //                    Details.ExpenseBy = item3.ExpenseBy;
        //                    Details.ExpenseOn = item3.ExpenseOn;
        //                    Details.type = "expense";
        //                    Details.Amount = item3.Amount.Value;
        //                    Details.ExpenseDate = item3.DateTime;
        //                    // response.expenseDetails.Add(Details);
        //                    expenses1.Add(Details);
        //                }
        //            }
        //            if (request.hospitalId != null && request.hospitalId > 0)
        //            {
        //                cashes = cashes.Where(x => x.hospitalId == request.hospitalId).ToList();
        //                creadits = creadits.Where(x => x.hospitalId == request.hospitalId).ToList();
        //            }
        //            if(!string.IsNullOrEmpty(request.Billtype))
        //            {
        //                if(request.Billtype.ToLower()== "requisition")
        //                {
        //                    cashes = cashes.Where(x => x.Billtype.ToLower() == "requisition").ToList();
        //                    creadits= creadits.Where(x => x.Billtype.ToLower() == "requisition").ToList();
        //                }
        //             else if (request.Billtype.ToLower() == "bill")
        //                {
        //                    cashes = cashes.Where(x => x.Billtype.ToLower() == "bill").ToList();
        //                    creadits = creadits.Where(x => x.Billtype.ToLower() == "bill").ToList();
        //                }
        //            }
        //            response.totalCash = cashes.Where(x => x.type.ToLower() == "cash").Sum(x => x.amount);
        //            response.totalCredit = creadits.Where(x => x.type.ToLower() == "credit").Sum(x => x.amount);
        //            response.totalExpense = expenses1.Where(x => x.type.ToLower() == "expense").Sum(x => x.Amount);

        //            if (!string.IsNullOrEmpty(request.type))
        //            {

        //                if (request.type.ToLower() == "cash")
        //                {
        //                    response.totalCount = cashes.Count();
        //                   response.cashDetails = cashes.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
        //                    response.type = "cash";
        //                }
        //                else if(request.type.ToLower()== "credit")
        //                {
        //                    response.totalCount = creadits.Count();
        //                    response.creaditDetails = creadits.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
        //                    response.type = "credit";
        //                }
        //                else if(request.type.ToLower()== "expense")
        //                {
        //                    response.totalCount = expenses1.Count();
        //                    response.expenseDetails = expenses1.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
        //                    response.type = "expense";
        //                }
        //            }
        //            else
        //            {
        //                response.totalCount = cashes.Count();
        //                response.cashDetails = cashes.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
        //                response.type = "cash";
        //            }



        //            // response.totalCash = response.cashDetails.Where(x => x.type.ToLower() == "cash").Sum(x => x.amount);      //added by sk 03/05/2021
        //            // response.totalCredit = response.creaditDetails.Where(x => x.type.ToLower() == "credit").Sum(x => x.amount);   //added by sk 03/05/2021
        //            // response.totalExpense = response.expenseDetails.Where(x => x.type.ToLower() == "expense").Sum(x => x.Amount);   //added by sk 03/05/2021

        //            response.Message = "Accounts fetched successfully";
        //            response.Success = true;
        //            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //            response.ResponseCode = 0;
        //        }
        //        else
        //        {
        //            response.Message = "Page number must be greater than zero(0)";
        //            response.Success = false;
        //            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //           response.ResponseCode = 1;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Message = "Accounts Exception : " + ex.Message;
        //        response.Success = false;
        //        response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
        //        response.ResponseCode = 1;
        //    }
        //    return response;
        //}


        public AccountsResponse Accounts(AccountsRequest request)
        {
            AccountsResponse response = new AccountsResponse();
            List<CashDetails> cashesList = new List<CashDetails>();
            List<CreaditDetails> creaditsList = new List<CreaditDetails>();
            List<UpiDetails> upiList = new List<UpiDetails>();
            List<ExpenseDetails> expenses1 = new List<ExpenseDetails>();
            List<StorageDetails> storageList = new List<StorageDetails>();
            int pageSize = 10;

            try
            {
                string bloodbankId = request.bloodbankId;
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    if (request.pageNumber >= 1)
                    {
                        List<InvoiceNo> invoiceNosList = new List<InvoiceNo>();
                        List<Expense> expensesList = new List<Expense>();
                        if (request.type.ToLower() == "cash")
                        {
                            if (request.fromDate.HasValue && request.toDate.HasValue)
                            {
                                DateTime startDate = request.fromDate.Value;
                                DateTime endDate = request.toDate.Value;
                                invoiceNosList = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date >= startDate.Date && x.InvoiceDate.Value.Date <= endDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.InvoiceDate).ToList();
                                expensesList = this.DBContext.Expense.Where(x => x.DateTime.Value.Date >= startDate.Date && x.DateTime.Value.Date <= endDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.DateTime).ToList();
                            }
                            else
                            {
                                DateTime startDate = request.fromDate.Value;
                                invoiceNosList = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date == startDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.InvoiceDate).ToList();
                                expensesList = this.DBContext.Expense.Where(x => x.DateTime.Value.Date == startDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.DateTime).ToList();
                            }

                            if (request.fromTime.HasValue && request.toTime.HasValue)
                            {
                                DateTime dateTime1 = request.fromTime.Value;
                                DateTime dateTime2 = request.toTime.Value;
                                invoiceNosList = invoiceNosList.Where(x => x.InvoiceDate.Value.TimeOfDay >= dateTime1.TimeOfDay && x.InvoiceDate.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
                                expensesList = expensesList.Where(x => x.DateTime.Value.TimeOfDay >= dateTime1.TimeOfDay && x.DateTime.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
                            }
                            if (invoiceNosList != null && invoiceNosList.Count > 0)
                            {
                                foreach (var item in invoiceNosList)
                                {
                                    if (item.Type.ToLower() == "requisition")
                                    {
                                        Requisition requisitions = this.DBContext.Requisition.FirstOrDefault(x => x.RequisitionId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (requisitions != null)
                                        {
                                            // CallDetails calldetailsRecords = this._bloodbank_DbContext.CallDetails.FirstOrDefault(x => (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "requisition") || (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "samplecollection"));
                                            CallDetails calldetailsRecords = this.DBContext.CallDetails.FirstOrDefault(x => (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "requisition") || (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "samplecollection"));
                                            HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == calldetailsRecords.FkHospitalId);
                                            if (requisitions.PaymentType.ToLower() == "cash" || requisitions.PaymentType.ToLower() == "discount" || requisitions.PaymentType == "")
                                            {

                                                CashDetails Details = new CashDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "cash";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.Billtype = "requisition";
                                                Details.amount = requisitions.PaidAmount.Value;
                                                cashesList.Add(Details);

                                            }
                                            else if (requisitions.PaymentType.ToLower() == "upi")
                                            {
                                                UpiDetails Details = new UpiDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = "";
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = "";
                                                Details.type = "upi";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.Billtype = "requisition";
                                                Details.amount = requisitions.PaidAmount.Value;
                                                upiList.Add(Details);
                                            }

                                            else if (requisitions.PaymentType.ToLower() == "credit")
                                            {
                                                CreaditDetails Details = new CreaditDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = "";//calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "credit";
                                                Details.Billtype = "requisition";
                                                Details.InvoiceNumber = "";// item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = requisitions.PaidAmount.Value;
                                                creaditsList.Add(Details);
                                                //totalCredits = totalCredits + requisitions.PaidAmount.Value;//requisitions.PaidAmount
                                                //totalReqCredits = totalCredits;
                                            }
                                        }
                                    }
                                    else if (item.Type.ToLower() == "bill")
                                    {
                                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (bills != null)
                                        {
                                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "issuebag");
                                            HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
                                            if (bills.PaymentType.ToLower() == "cash" || bills.PaymentType.ToLower() == "discount" || bills.PaymentType == "")
                                            {
                                                CashDetails Details = new CashDetails();
                                                Details.isCancel = " ";
                                                Details.patientName = callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "cash";
                                                Details.Billtype = "bill";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.amount = bills.NetAmount.Value;
                                                cashesList.Add(Details);
                                            }
                                            else if (bills.PaymentType.ToLower() == "upi")
                                            {
                                                UpiDetails Details = new UpiDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";   // callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = " ";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "upi";
                                                Details.Billtype = "bill";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.amount = bills.NetAmount.Value;
                                                upiList.Add(Details);

                                            }
                                            else if (bills.PaymentType == "credit")
                                            {
                                                CreaditDetails Details = new CreaditDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";// callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "credit";
                                                Details.Billtype = "bill";
                                                Details.InvoiceNumber = "";//item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = bills.NetAmount.Value;
                                                creaditsList.Add(Details);
                                            }
                                        }
                                    }
                                    else if (item.Type.ToLower() == "storage")
                                    {

                                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (bills != null)
                                        {
                                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "storage");
                                            // HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
                                            if (bills.PaymentType.ToLower() == "credit")
                                            {
                                                StorageDetails Details = new StorageDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = callDetails.PatientName;
                                                Details.Billtype = "storage";
                                                Details.type = "credit";
                                                //// Details.Billtype = item.Type.ToLower();
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = bills.NetAmount.Value;
                                                storageList.Add(Details);
                                            }

                                        }
                                    }
                                    else if (item.Type.ToLower() == "plasmafractionation")
                                    {
                                        PlasmaFractionation plasmaFractionation = this.DBContext.PlasmaFractionation.FirstOrDefault(x => x.PlasmaFractionationId == item.TypeId &&x.FkBloodbankId==bloodbankId);
                                        if (plasmaFractionation != null)
                                        {
                                            CreaditDetails Details = new CreaditDetails();
                                            Details.isCancel = "";
                                            Details.patientName = "";// callDetails.PatientName;
                                            Details.hospitalId = 0;
                                            Details.hospitalName = "";
                                            Details.type = "credit";
                                            Details.Billtype = "plasmafractionation";
                                            Details.InvoiceNumber = "";//item.InvoiceNumber;
                                            Details.InvoiceDate = item.InvoiceDate;
                                            Details.amount = (int)plasmaFractionation.NetAmount;
                                            creaditsList.Add(Details);
                                        }
                                    }

                                }
                            }
                            if (request.hospitalId != null && request.hospitalId > 0)
                            {
                                cashesList = cashesList.Where(x => x.hospitalId == request.hospitalId).ToList();
                                creaditsList = creaditsList.Where(x => x.hospitalId == request.hospitalId).ToList();
                                upiList = upiList.Where(x => x.hospitalId == request.hospitalId).ToList();
                            }
                            if (!string.IsNullOrEmpty(request.Billtype))
                            {
                                if (request.Billtype.ToLower() == "requisition")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    //totalCredits = totalReqCredits;
                                }
                                else if (request.Billtype.ToLower() == "bill")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                }
                                else if (request.Billtype.ToLower() == "plasmafractionation")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                }

                            }
                            response.totalStorage = storageList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCash = cashesList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCredit = creaditsList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalUpi = upiList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCount = cashesList.Count();
                            response.cashDetails = cashesList.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
                            response.type = "cash";
                            response.totalExpense = expensesList.Sum(x => x.Amount);


                        }
                        else if (request.type.ToLower() == "storage")
                        {
                            if (request.fromDate.HasValue && request.toDate.HasValue)
                            {
                                DateTime startDate = request.fromDate.Value;
                                DateTime endDate = request.toDate.Value;
                                invoiceNosList = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date >= startDate.Date && x.InvoiceDate.Value.Date <= endDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.InvoiceDate).ToList();
                                expensesList = this.DBContext.Expense.Where(x => x.DateTime.Value.Date >= startDate.Date && x.DateTime.Value.Date <= endDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.DateTime).ToList();
                            }
                            else
                            {
                                DateTime startDate = request.fromDate.Value;
                                invoiceNosList = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date == startDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.InvoiceDate).ToList();
                                expensesList = this.DBContext.Expense.Where(x => x.DateTime.Value.Date == startDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.DateTime).ToList();
                            }

                            if (request.fromTime.HasValue && request.toTime.HasValue)
                            {
                                DateTime dateTime1 = request.fromTime.Value;
                                DateTime dateTime2 = request.toTime.Value;
                                invoiceNosList = invoiceNosList.Where(x => x.InvoiceDate.Value.TimeOfDay >= dateTime1.TimeOfDay && x.InvoiceDate.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
                                expensesList = expensesList.Where(x => x.DateTime.Value.TimeOfDay >= dateTime1.TimeOfDay && x.DateTime.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
                            }
                            if (invoiceNosList != null && invoiceNosList.Count > 0)
                            {
                                foreach (var item in invoiceNosList)
                                {
                                    if (item.Type.ToLower() == "requisition")
                                    {
                                        Requisition requisitions = this.DBContext.Requisition.FirstOrDefault(x => x.RequisitionId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (requisitions != null)
                                        {
                                            // CallDetails calldetailsRecords = this.DBContext.CallDetails.FirstOrDefault(x => (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "requisition") || (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "samplecollection"));

                                            CallDetails calldetailsRecords = this.DBContext.CallDetails.FirstOrDefault(x => (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "requisition") || (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "samplecollection"));
                                            HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == calldetailsRecords.FkHospitalId);
                                            if (requisitions.PaymentType.ToLower() == "cash" || requisitions.PaymentType.ToLower() == "discount" || requisitions.PaymentType == "")
                                            {

                                                CashDetails Details = new CashDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "cash";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.Billtype = "requisition";
                                                Details.amount = requisitions.PaidAmount.Value;
                                                cashesList.Add(Details);

                                            }
                                            else if (requisitions.PaymentType.ToLower() == "upi")
                                            {
                                                UpiDetails Details = new UpiDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = "";
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = "";
                                                Details.type = "upi";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.Billtype = "requisition";
                                                Details.amount = requisitions.PaidAmount.Value;
                                                upiList.Add(Details);
                                            }

                                            else if (requisitions.PaymentType.ToLower() == "credit")
                                            {
                                                CreaditDetails Details = new CreaditDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = "";//calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "credit";
                                                Details.Billtype = "requisition";
                                                Details.InvoiceNumber = "";// item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = requisitions.PaidAmount.Value;
                                                creaditsList.Add(Details);
                                                //totalCredits = totalCredits + requisitions.PaidAmount.Value;//requisitions.PaidAmount
                                                //totalReqCredits = totalCredits;
                                            }
                                        }
                                    }
                                    else if (item.Type.ToLower() == "bill")
                                    {
                                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (bills != null)
                                        {
                                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "issuebag");
                                            HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
                                            if (bills.PaymentType.ToLower() == "cash" || bills.PaymentType.ToLower() == "discount" || bills.PaymentType == "")
                                            {
                                                CashDetails Details = new CashDetails();
                                                Details.isCancel = "";
                                                Details.patientName = callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "cash";
                                                Details.Billtype = "bill";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.amount = bills.NetAmount.Value;
                                                cashesList.Add(Details);
                                            }
                                            else if (bills.PaymentType.ToLower() == "upi")
                                            {
                                                UpiDetails Details = new UpiDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";   // callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = " ";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "upi";
                                                Details.Billtype = "bill";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.amount = bills.NetAmount.Value;
                                                upiList.Add(Details);

                                            }
                                            else if (bills.PaymentType == "credit")
                                            {
                                                CreaditDetails Details = new CreaditDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";// callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "credit";
                                                Details.Billtype = "bill";
                                                Details.InvoiceNumber = "";//item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = bills.NetAmount.Value;
                                                creaditsList.Add(Details);
                                            }
                                        }
                                    }
                                    else if (item.Type.ToLower() == "storage")
                                    {

                                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (bills != null)
                                        {
                                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "storage");
                                            // HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
                                            if (bills.PaymentType.ToLower() == "credit")
                                            {
                                                StorageDetails Details = new StorageDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = callDetails.PatientName;
                                                Details.Billtype = "storage";
                                                Details.type = "credit";
                                                //// Details.Billtype = item.Type.ToLower();
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = bills.NetAmount.Value;
                                                storageList.Add(Details);
                                            }

                                        }
                                    }
                                    else if (item.Type.ToLower() == "plasmafractionation")
                                    {
                                        PlasmaFractionation plasmaFractionation = this.DBContext.PlasmaFractionation.FirstOrDefault(x => x.PlasmaFractionationId == item.TypeId &&x.FkBloodbankId==bloodbankId);
                                        if (plasmaFractionation != null)
                                        {
                                            CreaditDetails Details = new CreaditDetails();
                                            Details.isCancel = "";
                                            Details.patientName = "";// callDetails.PatientName;
                                            Details.hospitalId = 0;
                                            Details.hospitalName = "";
                                            Details.type = "credit";
                                            Details.Billtype = "plasmafractionation";
                                            Details.InvoiceNumber = "";//item.InvoiceNumber;
                                            Details.InvoiceDate = item.InvoiceDate;
                                            Details.amount = (int)plasmaFractionation.NetAmount;
                                            creaditsList.Add(Details);
                                        }
                                    }

                                }
                            }
                            if (request.hospitalId != null && request.hospitalId > 0)
                            {
                                cashesList = cashesList.Where(x => x.hospitalId == request.hospitalId).ToList();
                                creaditsList = creaditsList.Where(x => x.hospitalId == request.hospitalId).ToList();
                                upiList = upiList.Where(x => x.hospitalId == request.hospitalId).ToList();
                            }
                            if (!string.IsNullOrEmpty(request.Billtype))
                            {
                                if (request.Billtype.ToLower() == "requisition")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    //totalCredits = totalReqCredits;
                                }
                                else if (request.Billtype.ToLower() == "bill")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "bill").ToList();

                                }
                                else if (request.Billtype.ToLower() == "plasmafractionation")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                }
                            }
                            response.totalStorage = storageList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCash = cashesList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCredit = creaditsList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalUpi = upiList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCount = storageList.Count();
                            response.storageDetails = storageList.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
                            response.type = "storage";
                            response.totalExpense = expensesList.Sum(x => x.Amount);

                        }
                        else if (request.type.ToLower() == "credit")
                        {
                            if (request.fromDate.HasValue && request.toDate.HasValue)
                            {
                                DateTime startDate = request.fromDate.Value;
                                DateTime endDate = request.toDate.Value;
                                invoiceNosList = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date >= startDate.Date && x.InvoiceDate.Value.Date <= endDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.InvoiceDate).ToList();
                                expensesList = this.DBContext.Expense.Where(x => x.DateTime.Value.Date >= startDate.Date && x.DateTime.Value.Date <= endDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.DateTime).ToList();
                            }
                            else
                            {
                                DateTime startDate = request.fromDate.Value;
                                invoiceNosList = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date == startDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.InvoiceDate).ToList();
                                expensesList = this.DBContext.Expense.Where(x => x.DateTime.Value.Date == startDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.DateTime).ToList();
                            }

                            if (request.fromTime.HasValue && request.toTime.HasValue)
                            {
                                DateTime dateTime1 = request.fromTime.Value;
                                DateTime dateTime2 = request.toTime.Value;
                                invoiceNosList = invoiceNosList.Where(x => x.InvoiceDate.Value.TimeOfDay >= dateTime1.TimeOfDay && x.InvoiceDate.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
                                expensesList = expensesList.Where(x => x.DateTime.Value.TimeOfDay >= dateTime1.TimeOfDay && x.DateTime.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
                            }
                            if (invoiceNosList != null && invoiceNosList.Count > 0)
                            {
                                foreach (var item in invoiceNosList)
                                {
                                    if (item.Type.ToLower() == "requisition")
                                    {
                                        Requisition requisitions = this.DBContext.Requisition.FirstOrDefault(x => x.RequisitionId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (requisitions != null)
                                        {
                                            CallDetails calldetailsRecords = this.DBContext.CallDetails.FirstOrDefault(x => (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "requisition") || (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "samplecollection"));
                                            HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == calldetailsRecords.FkHospitalId);
                                            if (requisitions.PaymentType.ToLower() == "cash" || requisitions.PaymentType.ToLower() == "discount" || requisitions.PaymentType == "")
                                            {

                                                CashDetails Details = new CashDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = "";// calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = " "; // Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "cash";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = " ";// item.InvoiceNumber;
                                                Details.Billtype = "requisition";
                                                Details.amount = requisitions.PaidAmount.Value;
                                                cashesList.Add(Details);

                                            }
                                            else if (requisitions.PaymentType.ToLower() == "upi")
                                            {
                                                UpiDetails Details = new UpiDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = "";// calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = " "; // Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "upi";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = " ";// item.InvoiceNumber;
                                                Details.Billtype = "requisition";
                                                Details.amount = requisitions.PaidAmount.Value;
                                                upiList.Add(Details);

                                            }
                                            else if (requisitions.PaymentType.ToLower() == "credit")
                                            {

                                                CreaditDetails Details = new CreaditDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "credit";
                                                Details.Billtype = "requisition";
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = requisitions.PaidAmount.Value;
                                                creaditsList.Add(Details);
                                            }
                                        }
                                    }
                                    else if (item.Type.ToLower() == "bill")
                                    {
                                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (bills != null)
                                        {
                                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "issuebag");
                                            HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
                                            if (bills.PaymentType.ToLower() == "cash" || bills.PaymentType.ToLower() == "discount" || bills.PaymentType == "")
                                            {
                                                CashDetails Details = new CashDetails();
                                                Details.isCancel = "";
                                                Details.patientName = ""; // callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = ""; // Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.Billtype = "bill";
                                                Details.type = "cash";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = ""; //item.InvoiceNumber;
                                                Details.amount = bills.NetAmount.Value;
                                                cashesList.Add(Details);
                                            }
                                            else if (bills.PaymentType.ToLower() == "upi")
                                            {
                                                UpiDetails Details = new UpiDetails();
                                                Details.isCancel = "";
                                                Details.patientName = " "; //callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = ""; // Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.Billtype = "bill";
                                                Details.type = "upi";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = ""; //item.InvoiceNumber;
                                                Details.amount = bills.NetAmount.Value;
                                                upiList.Add(Details);

                                            }

                                            else if (bills.PaymentType.ToLower() == "credit")
                                            {
                                                CreaditDetails Details = new CreaditDetails();
                                                Details.isCancel = "";
                                                Details.patientName = callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.Billtype = "bill";
                                                Details.type = "credit";
                                                //// Details.Billtype = item.Type.ToLower();
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = bills.NetAmount.Value;
                                                creaditsList.Add(Details);
                                            }

                                        }
                                    }
                                    else if (item.Type.ToLower() == "storage")
                                    {

                                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (bills != null)
                                        {
                                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "storage");
                                            // HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
                                            if (bills.PaymentType.ToLower() == "credit")
                                            {
                                                StorageDetails Details = new StorageDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = callDetails.PatientName;
                                                Details.Billtype = "storage";
                                                Details.type = "credit";
                                                //// Details.Billtype = item.Type.ToLower();
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = bills.NetAmount.Value;
                                                storageList.Add(Details);
                                            }

                                        }
                                    }
                                    else if (item.Type.ToLower() == "plasmafractionation")
                                    {
                                        PlasmaFractionation plasmaFractionation = this.DBContext.PlasmaFractionation.FirstOrDefault(x => x.PlasmaFractionationId == item.TypeId &&x.FkBloodbankId==bloodbankId);
                                        if (plasmaFractionation != null)
                                        {
                                            CreaditDetails Details = new CreaditDetails();
                                            Details.isCancel = "";
                                            Details.patientName = "";// callDetails.PatientName;
                                            Details.hospitalId = 0;
                                            Details.hospitalName = "";
                                            Details.type = "credit";
                                            Details.Billtype = "plasmafractionation";
                                            Details.InvoiceNumber = item.InvoiceNumber;
                                            Details.InvoiceDate = item.InvoiceDate;
                                            Details.amount = (int)plasmaFractionation.NetAmount;
                                            creaditsList.Add(Details);
                                        }
                                    }
                                }
                            }
                            if (request.hospitalId != null && request.hospitalId > 0)
                            {
                                creaditsList = creaditsList.Where(x => x.hospitalId == request.hospitalId).ToList();
                                cashesList = cashesList.Where(x => x.hospitalId == request.hospitalId).ToList();
                                upiList = upiList.Where(x => x.hospitalId == request.hospitalId).ToList();
                            }
                            if (!string.IsNullOrEmpty(request.Billtype))
                            {
                                if (request.Billtype.ToLower() == "requisition")
                                {
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                }
                                else if (request.Billtype.ToLower() == "bill")
                                {
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                }
                                else if (request.Billtype.ToLower() == "plasmafractionation")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                }

                            }
                            response.totalStorage = storageList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCash = cashesList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCredit = creaditsList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalUpi = upiList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCount = creaditsList.Count();
                            response.creaditDetails = creaditsList.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
                            response.type = "credit";
                            response.totalExpense = expensesList.Sum(x => x.Amount);
                        }
                        else if (request.type.ToLower() == "expense")
                        {
                            if (request.fromDate.HasValue && request.toDate.HasValue)
                            {
                                DateTime startDate = request.fromDate.Value;
                                DateTime endDate = request.toDate.Value;
                                invoiceNosList = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date >= startDate.Date && x.InvoiceDate.Value.Date <= endDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.InvoiceDate).ToList();
                                expensesList = this.DBContext.Expense.Where(x => x.DateTime.Value.Date >= startDate.Date && x.DateTime.Value.Date <= endDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.DateTime).ToList();
                            }
                            else
                            {
                                DateTime startDate = request.fromDate.Value;
                                invoiceNosList = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date == startDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.InvoiceDate).ToList();
                                expensesList = this.DBContext.Expense.Where(x => x.DateTime.Value.Date == startDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.DateTime).ToList();
                            }

                            if (request.fromTime.HasValue && request.toTime.HasValue)
                            {
                                DateTime dateTime1 = request.fromTime.Value;
                                DateTime dateTime2 = request.toTime.Value;
                                invoiceNosList = invoiceNosList.Where(x => x.InvoiceDate.Value.TimeOfDay >= dateTime1.TimeOfDay && x.InvoiceDate.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
                                expensesList = expensesList.Where(x => x.DateTime.Value.TimeOfDay >= dateTime1.TimeOfDay && x.DateTime.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
                            }
                            if (expensesList != null && expensesList.Count > 0)
                            {
                                foreach (var item3 in expensesList)
                                {
                                    ExpenseDetails Details = new ExpenseDetails();
                                    Details.ExpenseBy = item3.ExpenseBy;
                                    Details.ExpenseOn = item3.ExpenseOn;
                                    Details.type = "expense";
                                    Details.Amount = item3.Amount.Value;
                                    Details.ExpenseDate = item3.DateTime;
                                    expenses1.Add(Details);
                                }
                            }

                            //new code
                            if (invoiceNosList != null && invoiceNosList.Count > 0)
                            {
                                foreach (var item in invoiceNosList)
                                {
                                    if (item.Type.ToLower() == "requisition")
                                    {
                                        Requisition requisitions = this.DBContext.Requisition.FirstOrDefault(x => x.RequisitionId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (requisitions != null)
                                        {
                                            CallDetails calldetailsRecords = this.DBContext.CallDetails.FirstOrDefault(x => (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "requisition") || (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "samplecollection"));
                                            HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == calldetailsRecords.FkHospitalId);
                                            if (requisitions.PaymentType.ToLower() == "cash" || requisitions.PaymentType.ToLower() == "discount" || requisitions.PaymentType == "")
                                            {

                                                CashDetails Details = new CashDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = "";// calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "cash";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = "";// item.InvoiceNumber;
                                                Details.Billtype = "requisition";
                                                Details.amount = requisitions.PaidAmount.Value;
                                                cashesList.Add(Details);

                                            }
                                            else if (requisitions.PaymentType.ToLower() == "upi")
                                            {
                                                UpiDetails Details = new UpiDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = "";// calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "upi";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = "";// item.InvoiceNumber;
                                                Details.Billtype = "requisition";
                                                Details.amount = requisitions.PaidAmount.Value;
                                                upiList.Add(Details);

                                            }
                                            else if (requisitions.PaymentType.ToLower() == "credit")
                                            {
                                                CreaditDetails Details = new CreaditDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = "";//calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "credit";
                                                Details.Billtype = "requisition";
                                                Details.InvoiceNumber = "";// item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = requisitions.PaidAmount.Value;
                                                creaditsList.Add(Details);
                                                //totalCredits = totalCredits + requisitions.PaidAmount.Value;//requisitions.PaidAmount
                                                //totalReqCredits = totalCredits;
                                            }
                                        }
                                    }
                                    else if (item.Type.ToLower() == "bill")
                                    {
                                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (bills != null)
                                        {
                                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "issuebag");
                                            HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
                                            if (bills.PaymentType.ToLower() == "cash" || bills.PaymentType.ToLower() == "discount" || bills.PaymentType == "")
                                            {
                                                CashDetails Details = new CashDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";//callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "cash";
                                                Details.Billtype = "bill";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = "";// item.InvoiceNumber;
                                                Details.amount = bills.NetAmount.Value;
                                                cashesList.Add(Details);
                                            }
                                            else if (bills.PaymentType.ToLower() == "upi")
                                            {
                                                UpiDetails Details = new UpiDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";//callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "upi";
                                                Details.Billtype = "bill";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = "";// item.InvoiceNumber;
                                                Details.amount = bills.NetAmount.Value;
                                                upiList.Add(Details);

                                            }
                                            else if (bills.PaymentType == "credit")
                                            {
                                                CreaditDetails Details = new CreaditDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";// callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "credit";
                                                Details.Billtype = "bill";
                                                Details.InvoiceNumber = "";//item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = bills.NetAmount.Value;
                                                creaditsList.Add(Details);
                                            }
                                        }
                                    }
                                    else if (item.Type.ToLower() == "storage")
                                    {

                                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (bills != null)
                                        {
                                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "storage");
                                            // HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
                                            if (bills.PaymentType.ToLower() == "credit")
                                            {
                                                StorageDetails Details = new StorageDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = callDetails.PatientName;
                                                Details.Billtype = "storage";
                                                Details.type = "credit";
                                                //// Details.Billtype = item.Type.ToLower();
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = bills.NetAmount.Value;
                                                storageList.Add(Details);
                                            }

                                        }
                                    }
                                    else if (item.Type.ToLower() == "plasmafractionation")
                                    {
                                        PlasmaFractionation plasmaFractionation = this.DBContext.PlasmaFractionation.FirstOrDefault(x => x.PlasmaFractionationId == item.TypeId &&x.FkBloodbankId==bloodbankId);
                                        if (plasmaFractionation != null)
                                        {
                                            CreaditDetails Details = new CreaditDetails();
                                            Details.isCancel = "";
                                            Details.patientName = "";// callDetails.PatientName;
                                            Details.hospitalId = 0;
                                            Details.hospitalName = "";
                                            Details.type = "credit";
                                            Details.Billtype = "plasmafractionation";
                                            Details.InvoiceNumber = "";//item.InvoiceNumber;
                                            Details.InvoiceDate = item.InvoiceDate;
                                            Details.amount = (int)plasmaFractionation.NetAmount;
                                            creaditsList.Add(Details);
                                        }
                                    }
                                }
                            }
                            if (request.hospitalId != null && request.hospitalId > 0)
                            {
                                cashesList = cashesList.Where(x => x.hospitalId == request.hospitalId).ToList();
                                creaditsList = creaditsList.Where(x => x.hospitalId == request.hospitalId).ToList();
                                upiList = upiList.Where(x => x.hospitalId == request.hospitalId).ToList();
                            }
                            if (!string.IsNullOrEmpty(request.Billtype))
                            {
                                if (request.Billtype.ToLower() == "requisition")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "requisition").ToList();

                                    //totalCredits = totalReqCredits;
                                }
                                else if (request.Billtype.ToLower() == "bill")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "bill").ToList();

                                }
                                else if (request.Billtype.ToLower() == "plasmafractionation")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                }
                            }
                            //end new code
                            response.totalStorage = storageList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCash = cashesList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCredit = creaditsList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalUpi = upiList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalExpense = expenses1.Sum(x => x.Amount);
                            response.totalCount = expenses1.Count();
                            response.expenseDetails = expenses1.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
                            response.type = "expense";
                        }
                        else if (request.type.ToLower() == "upi")
                        {
                            if (request.fromDate.HasValue && request.toDate.HasValue)
                            {
                                DateTime startDate = request.fromDate.Value;
                                DateTime endDate = request.toDate.Value;
                                invoiceNosList = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date >= startDate.Date && x.InvoiceDate.Value.Date <= endDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.InvoiceDate).ToList();
                                expensesList = this.DBContext.Expense.Where(x => x.DateTime.Value.Date >= startDate.Date && x.DateTime.Value.Date <= endDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.DateTime).ToList();
                            }
                            else
                            {
                                DateTime startDate = request.fromDate.Value;
                                invoiceNosList = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date == startDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.InvoiceDate).ToList();
                                expensesList = this.DBContext.Expense.Where(x => x.DateTime.Value.Date == startDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.DateTime).ToList();
                            }

                            if (request.fromTime.HasValue && request.toTime.HasValue)
                            {
                                DateTime dateTime1 = request.fromTime.Value;
                                DateTime dateTime2 = request.toTime.Value;
                                invoiceNosList = invoiceNosList.Where(x => x.InvoiceDate.Value.TimeOfDay >= dateTime1.TimeOfDay && x.InvoiceDate.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
                                expensesList = expensesList.Where(x => x.DateTime.Value.TimeOfDay >= dateTime1.TimeOfDay && x.DateTime.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
                            }
                            if (invoiceNosList != null && invoiceNosList.Count > 0)
                            {
                                foreach (var item in invoiceNosList)
                                {
                                    if (item.Type.ToLower() == "requisition")
                                    {
                                        Requisition requisitions = this.DBContext.Requisition.FirstOrDefault(x => x.RequisitionId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (requisitions != null)
                                        {
                                            CallDetails calldetailsRecords = this.DBContext.CallDetails.FirstOrDefault(x => (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "requisition") || (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "samplecollection"));
                                            HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == calldetailsRecords.FkHospitalId);
                                            if (requisitions.PaymentType.ToLower() == "cash" || requisitions.PaymentType.ToLower() == "discount" || requisitions.PaymentType == "")
                                            {

                                                CashDetails Details = new CashDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "cash";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.Billtype = "requisition";
                                                Details.amount = requisitions.PaidAmount.Value;
                                                cashesList.Add(Details);

                                            }
                                            else if (requisitions.PaymentType.ToLower() == "upi")
                                            {
                                                UpiDetails Details = new UpiDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "upi";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.Billtype = "requisition";
                                                Details.amount = requisitions.PaidAmount.Value;
                                                upiList.Add(Details);

                                            }
                                            else if (requisitions.PaymentType.ToLower() == "credit")
                                            {
                                                CreaditDetails Details = new CreaditDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = "";//calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "credit";
                                                Details.Billtype = "requisition";
                                                Details.InvoiceNumber = "";// item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = requisitions.PaidAmount.Value;
                                                creaditsList.Add(Details);
                                                //totalCredits = totalCredits + requisitions.PaidAmount.Value;//requisitions.PaidAmount
                                                //totalReqCredits = totalCredits;
                                            }
                                        }
                                    }
                                    else if (item.Type.ToLower() == "bill")
                                    {
                                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (bills != null)
                                        {
                                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "issuebag");
                                            HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
                                            if (bills.PaymentType.ToLower() == "cash" || bills.PaymentType.ToLower() == "discount" || bills.PaymentType == "")
                                            {
                                                CashDetails Details = new CashDetails();
                                                Details.isCancel = "";
                                                Details.patientName = callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "cash";
                                                Details.Billtype = "bill";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.amount = bills.NetAmount.Value;
                                                cashesList.Add(Details);
                                            }
                                            else if (bills.PaymentType.ToLower() == "upi")
                                            {
                                                UpiDetails Details = new UpiDetails();
                                                Details.isCancel = "";
                                                Details.patientName = callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "upi";
                                                Details.Billtype = "bill";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.amount = bills.NetAmount.Value;
                                                upiList.Add(Details);

                                            }
                                            else if (bills.PaymentType == "credit")
                                            {
                                                CreaditDetails Details = new CreaditDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";// callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "credit";
                                                Details.Billtype = "bill";
                                                Details.InvoiceNumber = "";//item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = bills.NetAmount.Value;
                                                creaditsList.Add(Details);
                                            }
                                        }
                                    }
                                    else if (item.Type.ToLower() == "storage")
                                    {
                                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (bills != null)
                                        {
                                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "storage");
                                            // HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
                                            if (bills.PaymentType.ToLower() == "credit")
                                            {
                                                StorageDetails Details = new StorageDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = callDetails.PatientName;
                                                Details.Billtype = "storage";
                                                Details.type = "credit";
                                                //// Details.Billtype = item.Type.ToLower();
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = bills.NetAmount.Value;
                                                storageList.Add(Details);
                                            }

                                        }
                                    }
                                    else if (item.Type.ToLower() == "plasmafractionation")
                                    {
                                        PlasmaFractionation plasmaFractionation = this.DBContext.PlasmaFractionation.FirstOrDefault(x => x.PlasmaFractionationId == item.TypeId &&x.FkBloodbankId==bloodbankId);
                                        if (plasmaFractionation != null)
                                        {
                                            CreaditDetails Details = new CreaditDetails();
                                            Details.isCancel = "";
                                            Details.patientName = "";// callDetails.PatientName;
                                            Details.hospitalId = 0;
                                            Details.hospitalName = "";
                                            Details.type = "credit";
                                            Details.Billtype = "plasmafractionation";
                                            Details.InvoiceNumber = "";//item.InvoiceNumber;
                                            Details.InvoiceDate = item.InvoiceDate;
                                            Details.amount = (int)plasmaFractionation.NetAmount;
                                            creaditsList.Add(Details);
                                        }
                                    }
                                }
                            }
                            if (request.hospitalId != null && request.hospitalId > 0)
                            {
                                cashesList = cashesList.Where(x => x.hospitalId == request.hospitalId).ToList();
                                creaditsList = creaditsList.Where(x => x.hospitalId == request.hospitalId).ToList();
                                upiList = upiList.Where(x => x.hospitalId == request.hospitalId).ToList();
                            }
                            if (!string.IsNullOrEmpty(request.Billtype))
                            {
                                if (request.Billtype.ToLower() == "requisition")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    //totalCredits = totalReqCredits;
                                }
                                else if (request.Billtype.ToLower() == "bill")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "bill").ToList();

                                }
                                else if (request.Billtype.ToLower() == "plasmafractionation")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                }
                            }
                            response.totalStorage = storageList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCash = cashesList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCredit = creaditsList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalUpi = upiList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCount = upiList.Count();
                            response.upiDetails = upiList.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
                            response.type = "upi";
                            response.totalExpense = expensesList.Sum(x => x.Amount);
                        }
                        else
                        {
                            if (request.fromDate.HasValue && request.toDate.HasValue)
                            {
                                DateTime startDate = request.fromDate.Value;
                                DateTime endDate = request.toDate.Value;
                                invoiceNosList = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date >= startDate.Date && x.InvoiceDate.Value.Date <= endDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.InvoiceDate).ToList();
                                expensesList = this.DBContext.Expense.Where(x => x.DateTime.Value.Date >= startDate.Date && x.DateTime.Value.Date <= endDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.DateTime).ToList();
                            }
                            else
                            {
                                DateTime startDate = request.fromDate.Value;
                                invoiceNosList = this.DBContext.InvoiceNo.Where(x => x.InvoiceDate.Value.Date == startDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.InvoiceDate).ToList();
                                expensesList = this.DBContext.Expense.Where(x => x.DateTime.Value.Date == startDate.Date && x.FkBloodbankId == bloodbankId).OrderByDescending(x => x.DateTime).ToList();
                            }

                            if (request.fromTime.HasValue && request.toTime.HasValue)
                            {
                                DateTime dateTime1 = request.fromTime.Value;
                                DateTime dateTime2 = request.toTime.Value;
                                invoiceNosList = invoiceNosList.Where(x => x.InvoiceDate.Value.TimeOfDay >= dateTime1.TimeOfDay && x.InvoiceDate.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
                                expensesList = expensesList.Where(x => x.DateTime.Value.TimeOfDay >= dateTime1.TimeOfDay && x.DateTime.Value.TimeOfDay <= dateTime2.TimeOfDay).ToList();
                            }
                            if (invoiceNosList != null && invoiceNosList.Count > 0)
                            {
                                foreach (var item in invoiceNosList)
                                {
                                    if (item.Type.ToLower() == "requisition")
                                    {
                                        Requisition requisitions = this.DBContext.Requisition.FirstOrDefault(x => x.RequisitionId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (requisitions != null)
                                        {
                                            CallDetails calldetailsRecords = this.DBContext.CallDetails.FirstOrDefault(x => (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "requisition") || (x.CallDetailsId == requisitions.FkCallId && x.ReqBillType.ToLower() == "samplecollection"));
                                            HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == calldetailsRecords.FkHospitalId);
                                            if (requisitions.PaymentType.ToLower() == "cash" || requisitions.PaymentType.ToLower() == "discount" || requisitions.PaymentType == "")
                                            {

                                                CashDetails Details = new CashDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "cash";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.Billtype = "requisition";
                                                Details.amount = requisitions.PaidAmount.Value;
                                                cashesList.Add(Details);

                                            }
                                            else if (requisitions.PaymentType.ToLower() == "upi")
                                            {
                                                UpiDetails Details = new UpiDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = "";
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = "";
                                                Details.type = "upi";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.Billtype = "requisition";
                                                Details.amount = requisitions.PaidAmount.Value;
                                                upiList.Add(Details);
                                            }

                                            else if (requisitions.PaymentType.ToLower() == "credit")
                                            {
                                                CreaditDetails Details = new CreaditDetails();
                                                Details.isCancel = requisitions.IsActive == "cancel" ? "cancelled" : "";
                                                Details.patientName = "";//calldetailsRecords.PatientName;
                                                Details.hospitalId = (int)calldetailsRecords.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "credit";
                                                Details.Billtype = "requisition";
                                                Details.InvoiceNumber = "";// item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = requisitions.PaidAmount.Value;
                                                creaditsList.Add(Details);
                                                //totalCredits = totalCredits + requisitions.PaidAmount.Value;//requisitions.PaidAmount
                                                //totalReqCredits = totalCredits;
                                            }
                                        }
                                    }
                                    else if (item.Type.ToLower() == "bill")
                                    {
                                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (bills != null)
                                        {
                                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "issuebag");
                                            HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
                                            if (bills.PaymentType.ToLower() == "cash" || bills.PaymentType.ToLower() == "discount" || bills.PaymentType == "")
                                            {
                                                CashDetails Details = new CashDetails();
                                                Details.isCancel = "";
                                                Details.patientName = callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "cash";
                                                Details.Billtype = "bill";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.amount = bills.NetAmount.Value;
                                                cashesList.Add(Details);
                                            }
                                            else if (bills.PaymentType.ToLower() == "upi")
                                            {
                                                UpiDetails Details = new UpiDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";   // callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = " ";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "upi";
                                                Details.Billtype = "bill";
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.amount = bills.NetAmount.Value;
                                                upiList.Add(Details);

                                            }
                                            else if (bills.PaymentType == "credit")
                                            {
                                                CreaditDetails Details = new CreaditDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";// callDetails.PatientName;
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = "";// Details.hospitalId == 0 ? "" : hospitalMaster.HospitalName;
                                                Details.type = "credit";
                                                Details.Billtype = "bill";
                                                Details.InvoiceNumber = "";//item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = bills.NetAmount.Value;
                                                creaditsList.Add(Details);
                                            }
                                        }
                                    }
                                    else if (item.Type.ToLower() == "storage")
                                    {

                                        Bill bills = this.DBContext.Bill.FirstOrDefault(x => x.BillId == item.TypeId && x.FkBloodbankId == bloodbankId);
                                        if (bills != null)
                                        {
                                            CallDetails callDetails = this.DBContext.CallDetails.FirstOrDefault(x => x.BillReqId == bills.BillId && x.ReqBillType.ToLower() == "storage");
                                            // HospitalMaster hospitalMaster = this.DBContext.HospitalMaster.FirstOrDefault(x => x.HospitalMasterId == callDetails.FkHospitalId);
                                            if (bills.PaymentType.ToLower() == "credit")
                                            {
                                                StorageDetails Details = new StorageDetails();
                                                Details.isCancel = "";
                                                Details.patientName = "";
                                                Details.hospitalId = (int)callDetails.FkHospitalId;
                                                Details.hospitalName = callDetails.PatientName;
                                                Details.Billtype = "storage";
                                                Details.type = "credit";
                                                //// Details.Billtype = item.Type.ToLower();
                                                Details.InvoiceNumber = item.InvoiceNumber;
                                                Details.InvoiceDate = item.InvoiceDate;
                                                Details.amount = bills.NetAmount.Value;
                                                storageList.Add(Details);
                                            }

                                        }
                                    }
                                    else if (item.Type.ToLower() == "plasmafractionation")
                                    {
                                        PlasmaFractionation plasmaFractionation = this.DBContext.PlasmaFractionation.FirstOrDefault(x => x.PlasmaFractionationId == item.TypeId &&x.FkBloodbankId==bloodbankId);
                                        if (plasmaFractionation != null)
                                        {
                                            CreaditDetails Details = new CreaditDetails();
                                            Details.isCancel = "";
                                            Details.patientName = "";// callDetails.PatientName;
                                            Details.hospitalId = 0;
                                            Details.hospitalName = "";
                                            Details.type = "credit";
                                            Details.Billtype = "plasmafractionation";
                                            Details.InvoiceNumber = "";//item.InvoiceNumber;
                                            Details.InvoiceDate = item.InvoiceDate;
                                            Details.amount = (int)plasmaFractionation.NetAmount;
                                            creaditsList.Add(Details);
                                        }
                                    }
                                }
                            }
                            if (request.hospitalId != null && request.hospitalId > 0)
                            {
                                cashesList = cashesList.Where(x => x.hospitalId == request.hospitalId).ToList();
                                creaditsList = creaditsList.Where(x => x.hospitalId == request.hospitalId).ToList();
                                upiList = upiList.Where(x => x.hospitalId == request.hospitalId).ToList();
                            }
                            if (!string.IsNullOrEmpty(request.Billtype))
                            {
                                if (request.Billtype.ToLower() == "requisition")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "requisition").ToList();
                                    //totalCredits = totalReqCredits;
                                }
                                else if (request.Billtype.ToLower() == "bill")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "bill").ToList();
                                }
                                else if (request.Billtype.ToLower() == "plasmafractionation")
                                {
                                    cashesList = cashesList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                    creaditsList = creaditsList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                    upiList = upiList.Where(x => x.Billtype.ToLower() == "plasmafractionation").ToList();
                                }
                            }
                            response.totalStorage = storageList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCash = cashesList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCredit = creaditsList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalUpi = upiList.Where(x => x.isCancel != "cancelled").Sum(x => x.amount);
                            response.totalCount = cashesList.Count();
                            response.cashDetails = cashesList.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
                            response.type = "cash";
                            response.totalExpense = expensesList.Sum(x => x.Amount);

                        }

                        response.Message = "Accounts fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Page number must be greater than zero(0)";
                        response.Success = false;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "bloodbank not found";
                    response.Success = false;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 1;
                }

            }
            catch (Exception ex)
            {
                response.Message = "Accounts Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }


        public AddCampResponse AddCamp(AddCampRequest request)
        {
            AddCampResponse response = new AddCampResponse();
            try
            {

                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    string bloodbankId = request.bloodbankId;
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {
                        Camp camp = new Camp();
                        camp.CampName = request.CampName;
                        camp.Address = string.IsNullOrEmpty(request.Address) ? "" : request.Address;
                        camp.FkOrgniserId = request.OrgniserId;
                        camp.FkProId = request.ProId;
                        camp.FkMedicalOfficerId = request.MedicalOfficerId;
                        camp.CampDate = request.CampDate;
                        camp.TimeFrom = request.TimeFrom.Value;
                        camp.TimeTo = request.TimeTo.Value;
                        camp.CampCode = CampCode();
                        camp.NoOfDonors = 0;
                        camp.FkBloodbankId = bloodbankId;
                        camp.IsSpecialCertificate = request.isSpecialCertificate.ToLower();

                        this.DBContext.Camp.Add(camp);
                        int affectedRows = this.DBContext.SaveChanges();
                        response.Success = affectedRows == 1;
                        if (response.Success)
                        {
                            response.CampCode = camp.CampCode;
                            response.Message = "Camp inserted successfully";
                            response.Success = true;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "Failed to insert Camp";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "BloodBank not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "AddCamp Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public string CampCode()
        {
            Random random = new Random();
            string characters = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
            string CampCode = "";
            while (true)
            {
                CampCode = new string(Enumerable.Repeat(characters, 4).Select(s => s[random.Next(s.Length)]).ToArray());
                if (this.DBContext.Camp.FirstOrDefault(x => x.CampCode == CampCode) == null)
                    break;
            }

            return CampCode;

        }

        public ViewCampResponse ViewCamp(ViewCampRequest request)
        {
            ViewCampResponse response = new ViewCampResponse();
            List<CampList> camplist = new List<CampList>();
            int pageSize = 10;
            try
            {
                string bloodbankId = request.bloodbankId;
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    if (request.pageNumber >= 1)
                    {
                        List<Camp> camps = this.DBContext.Camp.Where(x => x.FkBloodbankId == bloodbankId).ToList();
                        if (camps != null && camps.Count > 0)
                        {
                            foreach (var item in camps)
                            {
                                CampList campList = new CampList();
                                campList.CampId = item.CampId;
                                campList.CampName = item.CampName;
                                campList.dateTime = (DateTime)item.CampDate;
                                campList.Address = string.IsNullOrEmpty(item.Address) ? "" : item.Address;
                                campList.CampDate = item.CampDate.Value.ToString("dd/MM/yyyy");
                                campList.TimeFrom = item.TimeFrom.Value.ToString("hh:mm tt");
                                campList.TimeTo = item.TimeTo.Value.ToString("hh:mm tt");
                                campList.CampCode = item.CampCode;
                                campList.NoOfDonors = item.NoOfDonors;
                                campList.OrgniserName = item.FkOrgniserId == 0 ? "" : this.DBContext.Orgniser.FirstOrDefault(x => x.OrgniserId == item.FkOrgniserId).Name;
                                campList.ProName = item.FkProId == 0 ? "" : this.DBContext.Pro.FirstOrDefault(x => x.ProId == item.FkProId).ProName;
                                campList.MedicalOfficerName = item.FkMedicalOfficerId == 0 ? "" : this.DBContext.MedicalOfficer.FirstOrDefault(x => x.MedicalOfficerId == item.FkMedicalOfficerId).MedicalOfficerName;
                                DateTime today = GetLocalDateTime();
                                if (item.CampDate.Value.Date == today.Date)
                                {
                                    campList.CampStatus = "today";
                                }
                                else if (item.CampDate.Value.Date < today.Date)
                                {
                                    campList.CampStatus = "history";
                                }
                                else if (item.CampDate.Value.Date > today.Date)
                                {
                                    campList.CampStatus = "upcoming";
                                }
                                // response.campLists.Add(campList);
                                camplist.Add(campList);
                            }


                            if (!string.IsNullOrEmpty(request.campCode))
                            {
                                camplist = camplist.Where(x => x.CampCode.ToLower() == request.campCode.ToLower()).ToList();
                            }

                            if (!string.IsNullOrEmpty(request.type))
                            {
                                if (request.type.ToLower() == "upcoming")
                                {
                                    camplist = camplist.Where(x => x.CampStatus.ToLower() == "upcoming").ToList();
                                    camplist = camplist.Where(x => x.CampStatus.ToLower() == "upcoming").OrderBy(x => x.dateTime).ToList();
                                    response.totalCount = camplist.Count();
                                }
                                else if (request.type.ToLower() == "today")
                                {
                                    camplist = camplist.Where(x => x.CampStatus.ToLower() == "today").ToList();
                                    response.totalCount = camplist.Count();
                                }
                                else if (request.type.ToLower() == "history")
                                {
                                    camplist = camplist.Where(x => x.CampStatus.ToLower() == "history").ToList();
                                    camplist = camplist.Where(x => x.CampStatus.ToLower() == "history").OrderByDescending(x => x.dateTime).ToList();

                                    response.totalCount = camplist.Count();
                                }

                            }

                            // response.totalCount = camplist.Count();
                            response.campLists = camplist.Skip((request.pageNumber - 1) * pageSize).Take(pageSize).ToList();
                            response.Message = "All Camp fetched successfully";
                            response.Success = true;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "Camp list not found";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "Page number must be greater than zero(0)";
                        response.Success = false;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 1;

                    }
                }
                else
                {
                    response.Message = "bloodbank Not Found";
                    response.Success = false;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "ViewCamp Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public AddOrgniserResponse AddOrgniser(AddOrgniserRequest request)
        {
            AddOrgniserResponse response = new AddOrgniserResponse();
            try
            {

                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    string bloodbankId = request.bloodbankId;
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {
                        string PostImageUrlAwsS3 = "";
                        string PostImageBase64 = request.logo;
                        if (StringExtensions.HasValue(PostImageBase64))
                        {
                            string BucketKeyPosts = s3Repository.s3Settings.Value.BucketChildKeyPosts;
                            UploadPhotoModel uploadPhotoResponse = s3Repository.UploadObject(
                                s3Repository.s3Settings,
                                PostImageBase64,
                                BucketKeyPosts).Result;
                            PostImageUrlAwsS3 = uploadPhotoResponse.FileName;
                        }

                        Orgniser orgniser = new Orgniser();
                        orgniser.Name = string.IsNullOrEmpty(request.Name) ? "" : request.Name;
                        orgniser.EmailId = string.IsNullOrEmpty(request.EmailId) ? "" : request.EmailId;
                        orgniser.ContactNo = string.IsNullOrEmpty(request.ContactNo) ? "" : request.ContactNo;
                        orgniser.CompanyName = string.IsNullOrEmpty(request.companyName) ? "" : request.companyName;
                        orgniser.Description = string.IsNullOrEmpty(request.description) ? "" : request.description;
                        orgniser.Subtitle = string.IsNullOrEmpty(request.subtitle) ? "" : request.subtitle;
                        orgniser.Logo = PostImageUrlAwsS3;
                        orgniser.FkBloodbankId = bloodbankId;
                        this.DBContext.Orgniser.Add(orgniser);
                        int affectedRows = this.DBContext.SaveChanges();
                        response.Success = affectedRows == 1;
                        if (response.Success)
                        {
                            response.Message = "Orgniser inserted successfully";
                            response.Success = true;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "Failed to insert Orgniser";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "BloodBank Not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;

                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "AddOrgniser Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }


        public ViewOrgniserResponse ViewOrgniser(ViewOrgniserRequest request)
        {
            ViewOrgniserResponse response = new ViewOrgniserResponse();

            try
            {
                string bloodbankId = request.bloodbankId;
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    List<Orgniser> orgnisers = this.DBContext.Orgniser.Where(x => x.FkBloodbankId == bloodbankId).ToList();
                    if (orgnisers != null && orgnisers.Count > 0)
                    {
                        foreach (var item in orgnisers)
                        {
                            OrgniserList orgniserList = new OrgniserList();
                            orgniserList.OrgniserId = item.OrgniserId;
                            orgniserList.Name = item.Name;

                            response.orgniserLists.Add(orgniserList);

                        }
                        response.Message = "All Orgniser fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Orgniser list not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;

                    }
                }
                else
                {
                    response.Message = "BloodBank not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "ViewOrgniser Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public Questionnaire isDonorAnsSame(int _qId, string _ans,string bloodbankId)
        {
            Questionnaire isSameQuestionnaire = this.DBContext.Questionnaire.FirstOrDefault(x => x.QueId == _qId && x.Ans.ToLower() == _ans.ToLower() &&x.FkBloodbankId==bloodbankId);
            return isSameQuestionnaire;
        }



        public AddDonorResponse AddDonor(AddDonorRequest request)
        {
            AddDonorResponse response = new AddDonorResponse();
            int donorId = 0, intDonarID = 0;

            try
            {

                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                if (userProfileRecord != null)
                {
                    Camp camp1 = this.DBContext.Camp.FirstOrDefault(x => x.CampCode == request.CampCode);
                    if (camp1 != null)
                    {
                        string bloodbankId = camp1.FkBloodbankId;
                        var donorData = this.DBContext.Donor.FirstOrDefault(x => x.Contact == request.Contact);
                        if (donorData != null)
                            intDonarID = donorData.DonorId;
                        else
                            intDonarID = 0;

                        CampDonorDetails campDonorDetails3 = this.DBContext.CampDonorDetails.FirstOrDefault(x => x.FkDonorId == intDonarID && x.CampCode == request.CampCode);
                        if (campDonorDetails3 == null)
                        {

                            string PostImageUrlAwsS3 = "";
                            string PostImageBase64 = request.Sign;
                            if (StringExtensions.HasValue(PostImageBase64))
                            {
                                string BucketKeyPosts = s3Repository.s3Settings.Value.BucketChildKeyPosts;
                                UploadPhotoModel uploadPhotoResponse = s3Repository.UploadObject(
                                    s3Repository.s3Settings,
                                    PostImageBase64,
                                    BucketKeyPosts).Result;
                                PostImageUrlAwsS3 = uploadPhotoResponse.FileName;
                            }
                            if (userProfileRecord.Logintype.ToLower() == "user")
                            {
                                Donor donor = new Donor();
                                donor.DonorName = request.DonorName;
                                donor.Address = string.IsNullOrEmpty(request.Address) ? "" : request.Address;
                                donor.Contact = string.IsNullOrEmpty(request.Contact) ? "" : request.Contact;
                                if (request.CampCode == "1000")
                                {
                                    donor.DonationDate = GetLocalDateTime().Date;
                                }
                                else
                                {
                                    DateTime? dateTime = this.DBContext.Camp.FirstOrDefault(x => x.CampCode == request.CampCode).CampDate;
                                    donor.DonationDate = dateTime;
                                }
                                donor.Email = string.IsNullOrEmpty(request.Email) ? "" : request.Email;
                                donor.Dob = request.Dob;
                                donor.Age = request.Age;
                                donor.Gender = string.IsNullOrEmpty(request.Gender) ? "" : request.Gender;
                                donor.Weight = request.Weight;
                                donor.Height = string.IsNullOrEmpty(request.Height) ? "" : request.Height;
                                donor.BloodGroup = string.IsNullOrEmpty(request.BloodGroup) ? "" : request.BloodGroup;
                                donor.Bp = string.IsNullOrEmpty(request.BloodPressure) ? "" : request.BloodPressure;
                                donor.Sign = PostImageUrlAwsS3;
                                donor.UserId = "";
                                this.DBContext.Donor.Add(donor);
                                int affectedRows = this.DBContext.SaveChanges();
                                donorId = donor.DonorId;
                                response.Success = affectedRows == 1;
                            }
                            else if (userProfileRecord.Logintype.ToLower() == "donor")
                            {

                                Donor donor = this.DBContext.Donor.FirstOrDefault(x => x.UserId == userProfileRecord.Id);
                                if (donor != null)
                                {
                                    donorId = donor.DonorId;
                                    donor.DonorName = request.DonorName;
                                    donor.Address = string.IsNullOrEmpty(request.Address) ? "" : request.Address;
                                    donor.Contact = string.IsNullOrEmpty(request.Contact) ? "" : request.Contact;
                                    //donor.DonationDate = request.DonationDate;
                                    if (request.CampCode == "1000")
                                    {
                                        donor.DonationDate = GetLocalDateTime().Date;
                                    }
                                    else
                                    {
                                        DateTime? dateTime = this.DBContext.Camp.FirstOrDefault(x => x.CampCode == request.CampCode).CampDate;
                                        donor.DonationDate = dateTime;
                                    }
                                    donor.Email = string.IsNullOrEmpty(request.Email) ? "" : request.Email;
                                    donor.Dob = request.Dob;
                                    donor.Age = request.Age;
                                    donor.Gender = string.IsNullOrEmpty(request.Gender) ? "" : request.Gender;
                                    donor.Weight = request.Weight;
                                    donor.Height = string.IsNullOrEmpty(request.Height) ? "" : request.Height;
                                    donor.Bp = string.IsNullOrEmpty(request.BloodPressure) ? "" : request.BloodPressure;
                                    donor.BloodGroup = string.IsNullOrEmpty(request.BloodGroup) ? "" : request.BloodGroup;
                                    donor.Sign = PostImageUrlAwsS3;

                                    int updatedRows = this.DBContext.SaveChanges();
                                    response.Success = updatedRows == 1;
                                }


                            }
                            if (response.Success)
                            {
                                CampDonorDetails campDonorDetails = new CampDonorDetails();
                                campDonorDetails.CampCode = request.CampCode;
                                campDonorDetails.FkDonorId = donorId;
                                campDonorDetails.BagNo = "";
                                campDonorDetails.Status = "waiting";
                                campDonorDetails.Segment = "";
                                campDonorDetails.FkBatchId = 0;
                                campDonorDetails.FkBloodbankId = bloodbankId;
                                this.DBContext.CampDonorDetails.Add(campDonorDetails);
                                int affectedRows1 = this.DBContext.SaveChanges();
                                response.Success = affectedRows1 == 1;
                                if (response.Success)
                                {
                                    Camp camp = this.DBContext.Camp.FirstOrDefault(x => x.CampCode == request.CampCode);
                                    int no_ofDonors = (int)camp.NoOfDonors;
                                    camp.NoOfDonors = no_ofDonors + 1;
                                    int updatedRows = this.DBContext.SaveChanges();
                                    response.Success = updatedRows == 1;
                                    if (response.Success)
                                    {
                                        bool updateFlag = false;
                                        int Count = 0;
                                        List<DonorQuestionAnswer> donorQuestionAnswers = new List<DonorQuestionAnswer>();
                                        foreach (var item in request.questions)
                                        {
                                            int _qId = item.QueId;
                                            string _ans = item.Ans.ToLower();

                                            Questionnaire questionnaire = isDonorAnsSame(_qId, _ans,bloodbankId);
                                            if (questionnaire != null)
                                            {
                                                Count = Count + 1;
                                                if (Count == request.questions.Count())
                                                {
                                                    CampDonorDetails campDonorDetails1 = this.DBContext.CampDonorDetails.FirstOrDefault(x => x.CampDonorDetailsId == campDonorDetails.CampDonorDetailsId);
                                                    campDonorDetails1.QueStatus = "ok";
                                                    int updatedStockRows = this.DBContext.SaveChanges();
                                                    updateFlag = updatedStockRows == 1;
                                                }
                                            }
                                            else
                                            {
                                                // Insert
                                                DonorQuestionAnswer donorQuesAns = new DonorQuestionAnswer();
                                                donorQuesAns.FkDonorId = donorId;
                                                donorQuesAns.CampCode = request.CampCode;
                                                donorQuesAns.FkQueId = item.QueId;
                                                donorQuesAns.Ans = item.Ans;
                                                donorQuestionAnswers.Add(donorQuesAns);
                                            }
                                        }

                                        if (updateFlag)
                                        {
                                            response.Success = true;
                                        }
                                        if (donorQuestionAnswers.Count > 0)
                                        {
                                            this.DBContext.DonorQuestionAnswer.AddRange(donorQuestionAnswers);
                                            int affectedRows3 = this.DBContext.SaveChanges();
                                            response.Success = affectedRows3 >= 1;
                                            if (response.Success)
                                            {
                                                CampDonorDetails campDonorDetails2 = this.DBContext.CampDonorDetails.FirstOrDefault(x => x.CampDonorDetailsId == campDonorDetails.CampDonorDetailsId);
                                                campDonorDetails2.QueStatus = "notok";
                                                int updatedRows1 = this.DBContext.SaveChanges();
                                                response.Success = updatedRows1 == 1;
                                            }
                                        }

                                        if (response.Success)
                                        {
                                            response.Message = "Donor Added successfully and Donor Question Added/Update Sucefully";
                                            response.Success = true;
                                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                            response.ResponseCode = 0;
                                        }
                                        else
                                        {
                                            response.Message = "Failed to add Donor Question Answer";
                                            response.Success = false;
                                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                            response.ResponseCode = 1;
                                        }

                                    }
                                    else
                                    {
                                        response.Message = "Failed to update No of Donors count ";
                                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                        response.Success = false;
                                        response.ResponseCode = 1;
                                    }
                                }
                                else
                                {
                                    response.Message = "Failed to CampDonorDetails";
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.Success = false;
                                    response.ResponseCode = 1;
                                }
                            }
                            else
                            {
                                response.Message = "Failed to insert Donor";
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.Success = false;
                                response.ResponseCode = 1;
                            }

                        }
                        else
                        {
                            response.Message = " Already Registration  Done For this Camp";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "Camp not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;

                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "AddDonor Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }


        public ViewDonorResponse ViewDonor(ViewDonorRequest request)
        {
            ViewDonorResponse response = new ViewDonorResponse();

            try
            {
                List<Donor> donors = this.DBContext.Donor.ToList();
                if (donors != null && donors.Count > 0)
                {
                    foreach (var item in donors)
                    {
                        DonorList donorList = new DonorList();
                        donorList.DonorId = item.DonorId;
                        donorList.DonorName = item.DonorName;
                        donorList.Address = string.IsNullOrEmpty(item.Address) ? " " : item.Address;
                        donorList.Contact = string.IsNullOrEmpty(item.Contact) ? " " : item.Contact;
                        donorList.DonationDate = item.DonationDate.Value.ToString("dd/MM/yyyy");
                        donorList.Email = string.IsNullOrEmpty(item.Email) ? " " : item.Email;
                        donorList.Dob = item.Dob.Value.ToString("dd/MM/yyyy");
                        donorList.Age = item.Age;
                        donorList.Gender = item.Gender;
                        donorList.Weight = item.Weight;
                        donorList.Height = string.IsNullOrEmpty(item.Height) ? " " : item.Height;
                        donorList.BloodGroup = string.IsNullOrEmpty(item.BloodGroup) ? " " : item.BloodGroup;
                        donorList.BloodPressure = string.IsNullOrEmpty(item.Bp) ? " " : item.Bp;

                        response.donorLists.Add(donorList);

                    }
                    response.Message = "All Donor fetched successfully";
                    response.Success = true;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 0;

                }
                else
                {
                    response.Message = "Donor list not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }

            }
            catch (Exception ex)
            {
                response.Message = "ViewDonor Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;

            }
            return response;
        }


        public ViewDonorByCampCodeResponse ViewDonorByCampCode(ViewDonorByCampCodeRequest request)
        {
            ViewDonorByCampCodeResponse response = new ViewDonorByCampCodeResponse();

            try
            {
                List<CampDonorDetails> details = this.DBContext.CampDonorDetails.Where(x => x.CampCode == request.CampCode).ToList();
                if (details != null && details.Count > 0)
                {
                    foreach (var item in details.OrderBy(x => x.BagNo))
                    {
                        Donor donor = this.DBContext.Donor.FirstOrDefault(x => x.DonorId == item.FkDonorId);
                        DonarListByCampCode donarListByCampCode = new DonarListByCampCode();
                        donarListByCampCode.DonorId = donor.DonorId;
                        donarListByCampCode.DonorName = donor.DonorName;
                        donarListByCampCode.Address = string.IsNullOrEmpty(donor.Address) ? " " : donor.Address;
                        donarListByCampCode.Contact = string.IsNullOrEmpty(donor.Contact) ? " " : donor.Contact;
                        donarListByCampCode.DonationDate = donor.DonationDate.Value.ToString("dd/MM/yyyy");
                        donarListByCampCode.Email = string.IsNullOrEmpty(donor.Email) ? " " : donor.Email;
                        donarListByCampCode.Dob = donor.Dob.Value.ToString("dd/MM/yyyy");
                        donarListByCampCode.Age = donor.Age;
                        donarListByCampCode.Gender = donor.Gender;
                        donarListByCampCode.Weight = donor.Weight;
                        donarListByCampCode.Height = string.IsNullOrEmpty(donor.Height) ? " " : donor.Height;
                        donarListByCampCode.BloodGroup = string.IsNullOrEmpty(donor.BloodGroup) ? " " : donor.BloodGroup;
                        donarListByCampCode.BloodPressure = string.IsNullOrEmpty(donor.Bp) ? " " : donor.Bp;
                        donarListByCampCode.Status = item.Status;
                        donarListByCampCode.Segment = string.IsNullOrEmpty(item.Segment) ? " " : item.Segment;
                        donarListByCampCode.BagNo = string.IsNullOrEmpty(item.BagNo) ? " " : item.BagNo;
                        donarListByCampCode.CampCode = item.CampCode;
                        response.donorLists.Add(donarListByCampCode);

                    }
                    response.Message = "All Donor fetched successfully";
                    response.Success = true;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 0;

                }
                else
                {
                    response.Message = "Donor list not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "viewDonorByCampCode Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;

            }
            return response;
        }

        public ViewDonorByIdResponse ViewDonorById(ViewDonorByIdRequest request)
        {
            ViewDonorByIdResponse response = new ViewDonorByIdResponse();

            try
            {

                CampDonorDetails campDonorDetails = this.DBContext.CampDonorDetails.FirstOrDefault(x => x.FkDonorId == request.DonorId && x.CampCode == request.CampCode);
                if (campDonorDetails != null)
                {
                    Donor donor = this.DBContext.Donor.FirstOrDefault(x => x.DonorId == request.DonorId);
                    if (donor != null)
                    {

                        response.campCode = campDonorDetails.CampCode;
                        response.DonorId = donor.DonorId;
                        // response.DonorId = donor.DonorId;
                        response.DonorName = donor.DonorName;
                        response.Address = string.IsNullOrEmpty(donor.Address) ? " " : donor.Address;
                        response.Contact = string.IsNullOrEmpty(donor.Contact) ? " " : donor.Contact;
                        response.DonationDate = donor.DonationDate.Value.ToString("dd/MM/yyyy"); ;
                        response.Email = string.IsNullOrEmpty(donor.Email) ? " " : donor.Email;
                        response.Dob = donor.Dob.Value.ToString("dd/MM/yyyy");
                        response.Age = donor.Age;
                        response.Gender = string.IsNullOrEmpty(donor.Gender) ? " " : donor.Gender;
                        response.Weight = donor.Weight;
                        response.Height = string.IsNullOrEmpty(donor.Height) ? " " : donor.Height;
                        response.BloodGroup = string.IsNullOrEmpty(donor.BloodGroup) ? " " : donor.BloodGroup;
                        response.BloodPressure = string.IsNullOrEmpty(donor.Bp) ? " " : donor.Bp;
                        response.BagNo = string.IsNullOrEmpty(campDonorDetails.BagNo) ? " " : campDonorDetails.BagNo;
                        response.Segment = string.IsNullOrEmpty(campDonorDetails.Segment) ? " " : campDonorDetails.Segment;
                        response.QueStatus = campDonorDetails.QueStatus;
                        response.DonorSign = donor.Sign;
                        response.BatchNo = campDonorDetails.FkBatchId == 0 ? "" : this.DBContext.Batch.FirstOrDefault(x => x.BatchId == campDonorDetails.FkBatchId).BatchNumber; response.Message = " Donor fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;

                    }

                }
                else
                {
                    response.Message = "Donor not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }

            }
            catch (Exception ex)
            {
                response.Message = "viewDonorById Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;

            }
            return response;
        }

        public string CouponCode()
        {
            Random random = new Random();
            string characters = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
            string CouponCode = "";
            while (true)
            {
                CouponCode = new string(Enumerable.Repeat(characters, 6).Select(s => s[random.Next(s.Length)]).ToArray());
                if (this.DBContext.Certificate.FirstOrDefault(x => x.CouponCode == CouponCode) == null)
                    break;
            }

            return CouponCode;

        }

        public string BloodPressure()
        {
            string bloodPressure = "";
            Random RandString = new Random();
            int min = 110;
            int max = 130;

            int rand = RandString.Next(min, max - 1);
            while (rand % 2 == 1) // odd
            {
                rand = RandString.Next(min, max - 1);
            }
            Random RandString1 = new Random();
            int min1 = 70;
            int max1 = 90;

            int rand1 = RandString.Next(min1, max1 - 1);
            while (rand1 % 2 == 1) // odd
            {
                rand1 = RandString.Next(min1, max1 - 1);
            }

            bloodPressure = rand + "/" + rand1;
            return bloodPressure;

        }


        public int Pulse()
        {
            //Pluse rate Random Generated between 72 to 86 even number

            Random RandString = new Random();
            int min = 72;
            int max = 86;

            int rand = RandString.Next(min, max - 1);
            while (rand % 2 == 1) // odd
            {
                rand = RandString.Next(min, max - 1);
            }

            return rand;

        }


        public UpdateDonorResponse UpdateDonor(UpdateDonorRequest request)
        {
            UpdateDonorResponse response = new UpdateDonorResponse();
            try
            {
              
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName== UserName);
                if (userProfileRecord != null)
                {
                    string bloodbankId = request.bloodbankId;
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {

                        Donor donor = this.DBContext.Donor.FirstOrDefault(x => x.DonorId == request.DonorId);
                        if (donor != null)
                        {
                            string PostImageUrlAwsS3 = "";
                            string PostImageBase64 = request.Sign;
                            if (StringExtensions.HasValue(PostImageBase64))
                            {
                                string BucketKeyPosts = s3Repository.s3Settings.Value.BucketChildKeyPosts;
                                UploadPhotoModel uploadPhotoResponse = s3Repository.UploadObject(
                                    s3Repository.s3Settings,
                                    PostImageBase64,
                                    BucketKeyPosts).Result;
                                PostImageUrlAwsS3 = uploadPhotoResponse.FileName;
                            }

                            donor.DonorName = request.DonorName;
                            donor.Address = string.IsNullOrEmpty(request.Address) ? " " : request.Address;
                            donor.Contact = string.IsNullOrEmpty(request.Contact) ? " " : request.Contact;
                            donor.DonationDate = request.DonationDate;
                            donor.Email = string.IsNullOrEmpty(request.Email) ? " " : request.Email;
                            donor.Dob = request.Dob;
                            donor.Age = request.Age;
                            donor.Gender = string.IsNullOrEmpty(request.Gender) ? " " : request.Gender;
                            donor.Weight = request.Weight;
                            donor.Height = string.IsNullOrEmpty(request.Height) ? " " : request.Height;
                            donor.BloodGroup = string.IsNullOrEmpty(request.BloodGroup) ? " " : request.BloodGroup;
                            donor.Bp = string.IsNullOrEmpty(request.BloodPressure) ? BloodPressure() : request.BloodPressure;
                            donor.Pulse = Pulse();
                            donor.Sign = PostImageUrlAwsS3;
                            int updatedRows = this.DBContext.SaveChanges();
                            response.Success = updatedRows == 1;
                            if (response.Success)
                            {
                                if (!string.IsNullOrEmpty(request.BagNo) && request.batchId != null && request.batchId > 0)
                                {

                                    StockDetails stockDetails = this.DBContext.StockDetails.FirstOrDefault(x => x.BagNo == request.BagNo &&x.FkBloodbankId==bloodbankId);
                                    if (stockDetails == null)
                                    {
                                        CampDonorDetails campDonorDetails = this.DBContext.CampDonorDetails.FirstOrDefault(x => x.BagNo == request.BagNo && x.FkDonorId == request.DonorId && x.CampCode == request.CampCode && x.FkBatchId == request.batchId);
                                        if (campDonorDetails == null)
                                        {
                                            CampDonorDetails details = this.DBContext.CampDonorDetails.FirstOrDefault(x => x.FkDonorId == request.DonorId && x.CampCode == request.CampCode);
                                            details.BagNo = request.BagNo;
                                            details.Status = string.IsNullOrEmpty(request.BagNo) ? "waiting" : "donated";
                                            details.Segment = string.IsNullOrEmpty(request.Segment) ? " " : request.Segment;
                                            details.FkBatchId = request.batchId;
                                            int updatedRows1 = this.DBContext.SaveChanges();
                                            response.Success = updatedRows == 1;
                                            if (response.Success)
                                            {
                                                Batch batch = this.DBContext.Batch.FirstOrDefault(x => x.BatchId == details.FkBatchId && x.RemainingQuantity > 0 && x.ExpiryDate.Value.Date >= GetLocalDateTime().Date);
                                                if (batch != null)
                                                {
                                                    batch.RemainingQuantity = batch.RemainingQuantity - 1;
                                                    int updatedRows2 = this.DBContext.SaveChanges();
                                                    response.Success = updatedRows2 == 1;
                                                    if (response.Success)
                                                    {
                                                        Certificate certificate = new Certificate();
                                                        int campId = this.DBContext.Camp.FirstOrDefault(x => x.CampCode == request.CampCode).CampId;
                                                        certificate.FkCampId = campId;
                                                        certificate.FkDonorId = request.DonorId;
                                                        certificate.CouponCode = CouponCode();
                                                        certificate.AllocatedDate = GetLocalDateTime();
                                                        certificate.ExpiryDate = certificate.AllocatedDate.Value.AddYears(1);
                                                        certificate.IsActive = "yes";
                                                        certificate.CertificateDate = GetLocalDateTime();
                                                        certificate.FkBloodbankId = bloodbankId;
                                                        this.DBContext.Certificate.Add(certificate);
                                                        int affectedcetifiicaterows = this.DBContext.SaveChanges();
                                                        response.Success = affectedcetifiicaterows == 1;
                                                        if (response.Success)
                                                        {

                                                            List<StockDetails> stockDetailsObjectList = new List<StockDetails>();

                                                            for (int _statrtNumber = 1; _statrtNumber <= 2; _statrtNumber++)
                                                            {
                                                                string _bagNumber = request.BagNo.ToString().ToLower().Trim();
                                                                int _productId = _statrtNumber;

                                                                StockDetails stockExist = CheckStockExistOrNot(_bagNumber, _productId,bloodbankId);
                                                                if (stockExist != null)
                                                                {
                                                                }
                                                                else
                                                                {
                                                                    // Insert
                                                                    StockDetails stockDetailsObject = new StockDetails();
                                                                    stockDetailsObject.BagNo = _bagNumber;
                                                                    stockDetailsObject.ProductId = _productId;
                                                                    stockDetailsObject.BloodGroup = request.BloodGroup;
                                                                    stockDetailsObject.CollectionDate = GetLocalDateTime();
                                                                    stockDetailsObject.Solution = batch.SolutionType.ToLower();
                                                                    DateTime collectiondate = GetLocalDateTime();
                                                                    string solution = batch.SolutionType.ToLower();
                                                                    stockDetailsObject.ExpiryDate = GetExpiryDateByProductId(_productId, collectiondate, solution);
                                                                    stockDetailsObject.TestResult = "false";
                                                                    stockDetailsObject.DiscardReason = "";
                                                                    stockDetailsObject.TestDate = GetLocalDateTime();
                                                                    stockDetailsObject.Remark = "";
                                                                    stockDetailsObject.IssueFlag = "available";
                                                                    stockDetailsObject.Volume = "";
                                                                    stockDetailsObject.CollectedBy = userProfileRecord.UserName;
                                                                    stockDetailsObject.TestedBy = "";
                                                                    stockDetailsObject.Segment = request.Segment;
                                                                    stockDetailsObject.FkBloodbankId = bloodbankId;
                                                                    stockDetailsObjectList.Add(stockDetailsObject);

                                                                }

                                                            }//End of For loop
                                                            if (stockDetailsObjectList.Count > 0)
                                                            {
                                                                this.DBContext.StockDetails.AddRange(stockDetailsObjectList);
                                                                int affectedStockRows = this.DBContext.SaveChanges();
                                                                response.Success = affectedStockRows >= 1;
                                                            }

                                                            if (response.Success)
                                                            {
                                                                response.Message = "stocks added and Donor Update successfully";
                                                                response.Success = true;
                                                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                                response.ResponseCode = 0;
                                                            }
                                                            else
                                                            {
                                                                response.Message = "Failed to add stocks";
                                                                response.Success = false;
                                                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                                response.ResponseCode = 1;
                                                            }

                                                        }
                                                    }

                                                }
                                                else
                                                {
                                                    response.Message = "batch Not found";
                                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                    response.Success = false;
                                                    response.ResponseCode = 1;
                                                }
                                            }
                                            else
                                            {
                                                response.Message = "Failed  to update CampDoor Details";
                                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                                response.Success = false;
                                                response.ResponseCode = 1;
                                            }
                                        }
                                        else
                                        {
                                            response.Message = "Bag already available ";
                                            response.Success = false;
                                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                            response.ResponseCode = 1;

                                        }
                                    }
                                    else
                                    {
                                        response.Message = "  Bag already available in Stock Details ";
                                        response.Success = false;
                                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                        response.ResponseCode = 1;
                                    }
                                }
                                else
                                {
                                    response.Message = " Please Provide Bag No and BatchNo ";
                                    response.Success = false;
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.ResponseCode = 1;
                                }
                            }

                            else
                            {
                                response.Message = "Failed to update Donor Details";
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.Success = false;
                                response.ResponseCode = 1;
                            }

                        }
                        else
                        {
                            response.Message = "DonorID not found";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Update Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public RejectDonorResponse RejectDonor(RejectDonorRequest request)
        {
            RejectDonorResponse response = new RejectDonorResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName==UserName);
                if (userProfileRecord != null)
                {
                    string bloodbankId = request.bloodbankId;
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {

                        CampDonorDetails campDonorDetails = this.DBContext.CampDonorDetails.FirstOrDefault(x => x.FkDonorId == request.DonorId && x.CampCode == request.CampCode &&x.FkBloodbankId==bloodbankId);
                        if (campDonorDetails != null)
                        {
                            if (campDonorDetails.Status == "waiting")
                            {
                                campDonorDetails.Status = "rejected";
                                campDonorDetails.BagNo = "";
                                campDonorDetails.Segment = "";

                                int updatedRows = this.DBContext.SaveChanges();
                                response.Success = updatedRows == 1;

                                if (response.Success)
                                {

                                    response.Message = "Rejected successfully";
                                    response.Success = true;
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.ResponseCode = 0;
                                }
                                else
                                {

                                    response.Message = "Failed Reject Donor";
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.Success = false;
                                    response.ResponseCode = 1;
                                }
                            }
                            else if (campDonorDetails.Status == "donated")
                            {
                                string bagNo = campDonorDetails.BagNo;
                                campDonorDetails.Status = "rejected";
                                campDonorDetails.BagNo = "";
                                campDonorDetails.Segment = "";
                                int updatedRows = this.DBContext.SaveChanges();
                                response.Success = updatedRows == 1;
                                if (response.Success)
                                {

                                    List<StockDetails> stockDetails = this.DBContext.StockDetails.Where(x => x.BagNo == bagNo &&x.FkBloodbankId==bloodbankId).ToList();
                                    this.DBContext.StockDetails.RemoveRange(stockDetails);
                                    int affectrows = this.DBContext.SaveChanges();
                                    response.Success = affectrows >= 1;
                                    if (response.Success)
                                    {
                                        int campId = this.DBContext.Camp.FirstOrDefault(x => x.CampCode == request.CampCode).CampId;
                                        Certificate certificate = this.DBContext.Certificate.FirstOrDefault(x => x.FkDonorId == request.DonorId && x.FkCampId == campId &&x.FkBloodbankId==bloodbankId);
                                        this.DBContext.Certificate.Remove(certificate);
                                        int affectrows1 = this.DBContext.SaveChanges();
                                        response.Success = affectrows1 == 1;
                                        if (response.Success)
                                        {
                                            response.Message = "Rejected successfully";
                                            response.Success = true;
                                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                            response.ResponseCode = 0;
                                        }
                                        else
                                        {
                                            response.Message = "Failed Delete certficate";
                                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                            response.Success = false;
                                            response.ResponseCode = 1;

                                        }
                                    }
                                    else
                                    {
                                        response.Message = "Failed Delete Stockdetetails";
                                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                        response.Success = false;
                                        response.ResponseCode = 1;
                                    }
                                }
                                else
                                {

                                    response.Message = "Failed Reject Donor";
                                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                    response.Success = false;
                                    response.ResponseCode = 1;
                                }

                            }

                        }
                        else
                        {
                            response.Message = "Donor not found";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "RejectDonor Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }

        public ViewProResponse ViewPro(ViewProRequest request)
        {
            ViewProResponse response = new ViewProResponse();
            try
            {
                string bloodbankId = request.bloodbankId;
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    List<Pro> pros = this.DBContext.Pro.Where(x => x.FkBloodbankId==bloodbankId).ToList();
                    if (pros != null && pros.Count > 0)
                    {
                        foreach (var item in pros)
                        {
                            ProList proList = new ProList();
                            proList.ProId = item.ProId;
                            proList.ProName = item.ProName;
                            response.proLists.Add(proList);
                        }
                        response.Message = "All Pro fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Pro list not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;

                    }
                }
                else
                {
                    response.Message = "BloodBank Not Found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "ViewPro Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public ViewMedicalOfficerResponse ViewMedicalOfficer(ViewMedicalOfficerRequest request)
        {
            ViewMedicalOfficerResponse response = new ViewMedicalOfficerResponse();
            try
            {
                string bloodbankId = request.bloodbankId;
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    List<MedicalOfficer> medicalOfficers = this.DBContext.MedicalOfficer.Where(x=>x.FkBloodbankId==bloodbankId).ToList();
                    if (medicalOfficers != null && medicalOfficers.Count > 0)
                    {
                        foreach (var item in medicalOfficers)
                        {
                            MedicalOfficerList medicalOfficerList = new MedicalOfficerList();
                            medicalOfficerList.MedicalOfficerId = item.MedicalOfficerId;
                            medicalOfficerList.MedicalOfficerName = item.MedicalOfficerName;
                            response.medicalOfficerLists.Add(medicalOfficerList);
                        }
                        response.Message = "All Medical Oficer fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Medical Officer list not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;

                    }
                }
                else
                {
                    response.Message = "Bloodbank not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "ViewMedicalOfficer Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public AddProResponse AddPro(AddProRequest request)
        {
            AddProResponse response = new AddProResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName== UserName);
                if (userProfileRecord != null)
                {
                    string bloodbankId = request.bloodbankId;
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {
                        Pro pro = new Pro();
                        pro.ProName = request.ProName;
                        pro.ContactNo = request.ContactNo;
                        pro.FkBloodbankId = bloodbankId;
                        this.DBContext.Pro.Add(pro);
                        int affectedRows = this.DBContext.SaveChanges();
                        response.Success = affectedRows == 1;
                        if (response.Success)
                        {
                            response.Message = "Pro inserted successfully";
                            response.Success = true;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "Failed to insert Pro";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "BloodBank Not Found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "AddPro Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public AddMedicalOfficerResponse AddMedicalOfficer(AddMedicalOfficerRequest request)
        {
            AddMedicalOfficerResponse response = new AddMedicalOfficerResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName== UserName);
                if (userProfileRecord != null)
                {
                    string bloodbankId = request.bloodbankId;
                    BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                    if (bloodbankMaster != null)
                    {

                        string PostImageUrlAwsS3 = "";
                        string PostImageBase64 = request.Sign;
                        if (StringExtensions.HasValue(PostImageBase64))
                        {
                            string BucketKeyPosts = s3Repository.s3Settings.Value.BucketChildKeyPosts;
                            UploadPhotoModel uploadPhotoResponse = s3Repository.UploadObject(
                                s3Repository.s3Settings,
                                PostImageBase64,
                                BucketKeyPosts).Result;
                            PostImageUrlAwsS3 = uploadPhotoResponse.FileName;
                        }
                        MedicalOfficer medicalOfficer = new MedicalOfficer();
                        medicalOfficer.MedicalOfficerName = request.MedicalOfficerName;
                        medicalOfficer.ContactNo = request.ContactNo;
                        medicalOfficer.Sign = PostImageUrlAwsS3;
                        medicalOfficer.FkBloodbankId = bloodbankId;
                        this.DBContext.MedicalOfficer.Add(medicalOfficer);
                        int affectedRows = this.DBContext.SaveChanges();
                        response.Success = affectedRows == 1;
                        if (response.Success)
                        {
                            response.Message = "Medical Officer inserted successfully";
                            response.Success = true;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "Failed to insert Medical Officer";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "bloodbank not Found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "AddMedical Officer Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }


        public GetAllQuestionAnswerResponse GetAllQuestionAnswer(GetAllQuestionAnswerRequest request)
        {
            GetAllQuestionAnswerResponse response = new GetAllQuestionAnswerResponse();
            try
            {
                Camp camp = this.DBContext.Camp.FirstOrDefault(x => x.CampCode == request.campCode);
                if (camp != null)
                {
                    string bloodbankId = camp.FkBloodbankId;

                    List<Questionnaire> questionnaires = this.DBContext.Questionnaire.Where(x=>x.FkBloodbankId==bloodbankId).ToList();
                    if (questionnaires != null && questionnaires.Count > 0)
                    {
                        foreach (var item in questionnaires)
                        {
                            QuestionAnswerList questionAnswerList = new QuestionAnswerList();
                            questionAnswerList.QueId = item.QueId;
                            questionAnswerList.Question = item.Question;
                            questionAnswerList.Answer = item.Ans;
                            response.questionAnswerLists.Add(questionAnswerList);
                        }
                        response.Message = "All Question Ans fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Question Ans list not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;

                    }
                }
                else
                {
                    response.Message = "camp Not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "GetAllQuestionAnswer Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public GetDonorQuestionAnswerResponse GetDonorQuestionAnswer(GetDonorQuestionAnswerRequest request)
        {
            GetDonorQuestionAnswerResponse response = new GetDonorQuestionAnswerResponse();
            try
            {
                CampDonorDetails details = this.DBContext.CampDonorDetails.FirstOrDefault(x => x.CampCode == request.CampCode && x.FkDonorId == request.DonorId);
                if (details != null)
                {
                    Camp camp = this.DBContext.Camp.FirstOrDefault(x => x.CampCode == details.CampCode);
                    if (camp != null)
                    {
                        string bloodbankId = camp.FkBloodbankId;
                        if (details.QueStatus.ToLower() == "ok")
                        {
                            
                            List<Questionnaire> questionnaires = this.DBContext.Questionnaire.Where(x=>x.FkBloodbankId==bloodbankId).ToList();
                            if (questionnaires != null && questionnaires.Count > 0)
                            {
                                foreach (var item in questionnaires)
                                {
                                    QuestionAnswerList questionAnswerList = new QuestionAnswerList();
                                    questionAnswerList.QueId = item.QueId;
                                    questionAnswerList.Question = item.Question;
                                    questionAnswerList.Answer = item.Ans;
                                    response.questionAnswerLists.Add(questionAnswerList);
                                }
                                response.Message = "All Question Ans fetched successfully";
                                response.Success = true;
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.ResponseCode = 0;
                            }
                            else
                            {
                                response.Message = "Question Ans list not found";
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.Success = false;
                                response.ResponseCode = 1;

                            }

                        }
                        else if (details.QueStatus.ToLower() == "notok")
                        {

                            List<Questionnaire> questionnaires = this.DBContext.Questionnaire.Where(x=>x.FkBloodbankId==bloodbankId).ToList();
                            if (questionnaires != null && questionnaires.Count > 0)
                            {
                                foreach (var item in questionnaires)
                                {
                                    DonorQuestionAnswer donorQuestionAnswers = this.DBContext.DonorQuestionAnswer.FirstOrDefault(x => x.FkDonorId == request.DonorId && x.CampCode == request.CampCode && x.FkQueId == item.QueId);

                                    QuestionAnswerList questionAnswerList = new QuestionAnswerList();
                                    questionAnswerList.QueId = item.QueId;
                                    questionAnswerList.Question = item.Question;
                                    questionAnswerList.Answer = donorQuestionAnswers == null ? item.Ans : donorQuestionAnswers.Ans;
                                    response.questionAnswerLists.Add(questionAnswerList);
                                }
                                response.Message = "All Question Ans fetched successfully";
                                response.Success = true;
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.ResponseCode = 0;
                            }
                            else
                            {
                                response.Message = "Question Ans list not found";
                                response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                                response.Success = false;
                                response.ResponseCode = 1;

                            }

                        }
                    }
                    else
                    {
                        response.Message = "Camp not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }

                }
                else
                {
                    response.Message = "Donor not not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }


            }
            catch (Exception ex)
            {
                response.Message = "GetDonorQuestionAnswer Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }


        public VerifyDonorAnswerResponse VerifyDonorAnswer(VerifyDonorAnswerRequest request)
        {
            VerifyDonorAnswerResponse response = new VerifyDonorAnswerResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName== UserName);
                if (userProfileRecord != null)
                {
                    Camp camp = this.DBContext.Camp.FirstOrDefault(x => x.CampCode == request.CampCode);
                    if (camp != null)
                    {
                        string bloodbankId = camp.FkBloodbankId;
                        bool updateFlag = false;
                        int Count = 0;
                        List<DonorQuestionAnswer> donorQuestionAnswers = new List<DonorQuestionAnswer>();
                        foreach (var item in request.questions)
                        {
                            int _qId = item.QueId;
                            string _ans = item.Ans.ToLower();

                            Questionnaire questionnaire = isDonorAnsSame(_qId, _ans,bloodbankId);
                            if (questionnaire != null)
                            {
                                DonorQuestionAnswer donorQuestionAnswer = this.DBContext.DonorQuestionAnswer.FirstOrDefault(x => x.CampCode == request.CampCode && x.FkDonorId == request.DonorId && x.FkQueId == item.QueId);
                                if (donorQuestionAnswer != null)
                                {
                                    this.DBContext.DonorQuestionAnswer.Remove(donorQuestionAnswer);
                                    int deltedRow = this.DBContext.SaveChanges();
                                }
                                Count = Count + 1;
                                if (Count == request.questions.Count())
                                {
                                    CampDonorDetails campDonorDetails1 = this.DBContext.CampDonorDetails.FirstOrDefault(x => x.FkDonorId == request.DonorId && x.CampCode == request.CampCode);
                                    campDonorDetails1.QueStatus = "ok";
                                    int updatedRows = this.DBContext.SaveChanges();
                                    updateFlag = updatedRows == 1;
                                    response.queStatus = "ok";

                                }
                            }
                            else
                            {
                                //insert if donor queestion answer
                                DonorQuestionAnswer donorQuestionAnswer = this.DBContext.DonorQuestionAnswer.FirstOrDefault(x => x.CampCode == request.CampCode && x.FkDonorId == request.DonorId && x.FkQueId == item.QueId);
                                if (donorQuestionAnswer == null)
                                {
                                    DonorQuestionAnswer donorQuesAns = new DonorQuestionAnswer();
                                    donorQuesAns.FkDonorId = request.DonorId;
                                    donorQuesAns.CampCode = request.CampCode;
                                    donorQuesAns.FkQueId = item.QueId;
                                    donorQuesAns.Ans = item.Ans;
                                    donorQuestionAnswers.Add(donorQuesAns);
                                }
                            }
                        }

                        if (updateFlag)
                        {
                            response.Success = true;
                        }
                        if (donorQuestionAnswers.Count > 0)
                        {
                            this.DBContext.DonorQuestionAnswer.AddRange(donorQuestionAnswers);
                            int affectedRows3 = this.DBContext.SaveChanges();
                            response.Success = affectedRows3 >= 1;
                            CampDonorDetails campDonorDetails2 = this.DBContext.CampDonorDetails.FirstOrDefault(x => x.FkDonorId == request.DonorId && x.CampCode == request.CampCode);
                            if (campDonorDetails2.QueStatus == "ok")
                            {
                                campDonorDetails2.QueStatus = "notok";
                                int updatedRows = this.DBContext.SaveChanges();
                            }
                            response.queStatus = "notok";
                        }

                        if (response.Success)
                        {
                            response.Message = " Donor Question Added/Update Sucefully";
                            response.Success = true;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "Failed to Add/Update Donor Question Answer";
                            response.Success = false;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 1;
                        }
                    }
                    else
                    {
                        response.Message = "Camp not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }


                else
                {
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Verify Donor Answer Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }


        public GetAllBatchNoResponse GetAllBatchNo(GetAllBatchNoRequest request)
        {
            GetAllBatchNoResponse response = new GetAllBatchNoResponse();
            try
            {
                string bloodbankId = request.bloodbankId;
                BloodbankMaster bloodbankMaster = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId);
                if (bloodbankMaster != null)
                {
                    DateTime dateTime = GetLocalDateTime();
                    List<Batch> batches = this.DBContext.Batch.Where(x => x.RemainingQuantity > 0 && x.ExpiryDate.Value.Date >= dateTime.Date &&x.FkBloodbankId==bloodbankId).ToList();
                    if (batches != null && batches.Count > 0)
                    {
                        foreach (var item in batches)
                        {
                            BatchNoList batchNoList = new BatchNoList();
                            batchNoList.batchId = item.BatchId;
                            batchNoList.batchNo = item.BatchNumber;
                            batchNoList.type = string.IsNullOrEmpty(item.BatchProductType) ? "" : item.BatchProductType;
                            batchNoList.totalQuantity = (int)item.TotalQuantity;
                            batchNoList.remaininigQuantity = (int)item.RemainingQuantity;
                            batchNoList.solution = item.SolutionType;
                            batchNoList.bagWeight = (int)item.SolutionQuantity;
                            batchNoList.manufacturingDate = item.ManufactureDate.Value.ToString("dd/MM/yyyy");
                            batchNoList.expiryDate = item.ExpiryDate.Value.ToString("dd/MM/yyyy");
                            batchNoList.companyName = string.IsNullOrEmpty(item.CompanyName) ? "" : item.CompanyName;
                            response.batchNoLists.Add(batchNoList);
                        }
                        response.Message = "All batch fetched successfully";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                    else
                    {
                        response.Message = "Batch list not found";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Bloodbank not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "getAllBatchNo Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public GetDonorCertificateResponse GetDonorCertificate(GetDonorCertificateRequest request)
        {
            GetDonorCertificateResponse response = new GetDonorCertificateResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                string id = this.DBContext.Users.FirstOrDefault(x => x.UserName.Contains(UserName)).Id;
                int donorid = this.DBContext.Donor.FirstOrDefault(x => x.UserId == id).DonorId;
                List<Certificate> certificates = this.DBContext.Certificate.Where(x => x.FkDonorId == donorid).ToList();
                if (certificates != null && certificates.Count > 0)
                {
                    foreach (var item in certificates)
                    {
                        Camp camp = this.DBContext.Camp.FirstOrDefault(x => x.CampId == item.FkCampId);
                        if (camp != null)
                        {
                            DonorCertificateList donorCertificateList = new DonorCertificateList();
                            donorCertificateList.donorName = this.DBContext.Donor.FirstOrDefault(x => x.DonorId == item.FkDonorId).DonorName;
                            donorCertificateList.bloodGroup = this.DBContext.Donor.FirstOrDefault(x => x.DonorId == item.FkDonorId).BloodGroup;
                            donorCertificateList.donationDate = item.CertificateDate.Value.ToString("dd/MM/yyyy");
                            donorCertificateList.campName = camp.CampName;
                            donorCertificateList.bagNo = this.DBContext.CampDonorDetails.FirstOrDefault(x => x.FkDonorId == item.FkDonorId && x.CampCode == camp.CampCode).BagNo;
                            donorCertificateList.isSpecialCetificate = camp.IsSpecialCertificate.ToLower();
                            if (camp.IsSpecialCertificate.ToLower() == "yes")
                            {
                                //string CompanName = this.DBContext.Orgniser.FirstOrDefault(x => x.OrgniserId == camp.FkOrgniserId).CompanyName;
                                donorCertificateList.companyName = camp.FkOrgniserId == 0 ? "" : this.DBContext.Orgniser.FirstOrDefault(x => x.OrgniserId == camp.FkOrgniserId).CompanyName;
                                donorCertificateList.subtitle = camp.FkOrgniserId == 0 ? "" : this.DBContext.Orgniser.FirstOrDefault(x => x.OrgniserId == camp.FkOrgniserId).Subtitle;
                                donorCertificateList.description = camp.FkOrgniserId == 0 ? "" : this.DBContext.Orgniser.FirstOrDefault(x => x.OrgniserId == camp.FkOrgniserId).Description;
                                donorCertificateList.logo = camp.FkOrgniserId == 0 ? "" : this.DBContext.Orgniser.FirstOrDefault(x => x.OrgniserId == camp.FkOrgniserId).Logo;

                            }
                            else
                            {
                                donorCertificateList.companyName = "";
                                donorCertificateList.description = "";
                                donorCertificateList.subtitle = "";
                                donorCertificateList.logo = "";
                            }
                            donorCertificateList.medicalOfficerName = camp.FkMedicalOfficerId == 0 ? "" : this.DBContext.MedicalOfficer.FirstOrDefault(x => x.MedicalOfficerId == camp.FkMedicalOfficerId).MedicalOfficerName;
                            donorCertificateList.medicalOfficerSign = camp.FkMedicalOfficerId == 0 ? "" : this.DBContext.MedicalOfficer.FirstOrDefault(x => x.MedicalOfficerId == camp.FkMedicalOfficerId).Sign;
                            donorCertificateList.couponCode = item.CouponCode;
                            donorCertificateList.expiryDate = item.ExpiryDate.Value.ToString("dd/MM/yyyy");
                            string bloodbankId = item.FkBloodbankId;
                            donorCertificateList.bloodbankName = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId).BloodbankName;
                            donorCertificateList.bloodbankAddress = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId).Address;
                            donorCertificateList.bloodbanklicenceno = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId).LicenseNumber;
                            donorCertificateList.bloodbankPhonenumber = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId).PhoneNumber;
                            donorCertificateList.bloodbankLogo = this.DBContext.BloodbankMaster.FirstOrDefault(x => x.BloodbankId == bloodbankId).Logo;
                            response.donorCertificateLists.Add(donorCertificateList);
                        }
                    }
                    response.Message = "cerficate fetched successfully";
                    response.Success = true;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 0;
                }
                else
                {
                    response.Message = " Donated Donor not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "GetDonorCertificate Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public DeleteCampResponse DeleteCamp(DeleteCampRequest request)
        {
            DeleteCampResponse response = new DeleteCampResponse();
            try
            {

                Camp camp = this.DBContext.Camp.FirstOrDefault(x => x.CampId == request.campId);
                if (camp != null)
                {
                    if (camp.NoOfDonors == 0)
                    {
                        this.DBContext.Camp.Remove(camp);
                        int affectrows = this.DBContext.SaveChanges();
                        response.Success = affectrows == 1;
                        if (response.Success)
                        {
                            response.Message = " Camp Delete successfully";
                            response.Success = true;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "Failed Delete Camp";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }


                    }
                    else if (camp.NoOfDonors > 0)
                    {
                        response.Message = "Donor are Avilable Can't Delete";
                        response.Success = true;
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.ResponseCode = 0;
                    }
                }
                else
                {
                    response.Message = "camp not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "deletecamp Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public DeleteDonorResponse DeleteDonor(DeleteDonorRequest request)
        {
            DeleteDonorResponse response = new DeleteDonorResponse();
            try
            {
                CampDonorDetails campDonorDetails = this.DBContext.CampDonorDetails.FirstOrDefault(x => x.CampCode == request.campCode && x.FkDonorId == request.donorId && x.Status == "waiting");
                if (campDonorDetails != null)
                {
                    this.DBContext.CampDonorDetails.Remove(campDonorDetails);
                    int affectrows = this.DBContext.SaveChanges();
                    response.Success = affectrows == 1;
                    if (response.Success)
                    {

                        List<DonorQuestionAnswer> donorQuestionAnswers = this.DBContext.DonorQuestionAnswer.Where(x => x.CampCode == request.campCode && x.FkDonorId == request.donorId).ToList();
                        if (donorQuestionAnswers != null && donorQuestionAnswers.Count > 0)
                        {
                            this.DBContext.DonorQuestionAnswer.RemoveRange(donorQuestionAnswers);
                            int affect = this.DBContext.SaveChanges();
                        }
                        Camp camp = this.DBContext.Camp.FirstOrDefault(x => x.CampCode == request.campCode);
                        int no_ofDonors = (int)camp.NoOfDonors;
                        camp.NoOfDonors = no_ofDonors - 1;
                        int updatedRows = this.DBContext.SaveChanges();
                        response.Success = updatedRows == 1;
                        if (response.Success)
                        {
                            response.Message = " Donor Delete  and  campdonorDetils update successfully";
                            response.Success = true;
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.ResponseCode = 0;
                        }
                        else
                        {
                            response.Message = "Failed to Update Donor Count";
                            response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                            response.Success = false;
                            response.ResponseCode = 1;
                        }

                    }
                    else
                    {
                        response.Message = "Failed Delete CampDonorDetils";
                        response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                        response.Success = false;
                        response.ResponseCode = 1;
                    }
                }
                else
                {
                    response.Message = "Donor not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "DeleteDonor Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public GetDonorInfoResponse GetDonorInfo(GetDonorInfoRequest request)
        {
            GetDonorInfoResponse response = new GetDonorInfoResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                string id = this.DBContext.Users.FirstOrDefault(x => x.UserName== UserName).Id;
                Donor donor = this.DBContext.Donor.FirstOrDefault(x => x.UserId == id);
                if (donor != null)
                {
                    response.donorName = donor.DonorName;
                    response.address = string.IsNullOrEmpty(donor.Address) ? "" : donor.Address;
                    response.contact = string.IsNullOrEmpty(donor.Contact) ? "" : donor.Contact;
                    response.donationDate = donor.DonationDate.Value.ToString("dd/MM/yyyy");
                    response.email = string.IsNullOrEmpty(donor.Email) ? "" : donor.Email;
                    response.dob = donor.Dob.Value.ToString("dd/MM/yyyy");
                    response.age = donor.Age;
                    response.gender = string.IsNullOrEmpty(donor.Gender) ? "" : donor.Gender;
                    response.weight = donor.Weight;
                    response.height = string.IsNullOrEmpty(donor.Height) ? "" : donor.Height;
                    response.bloodPressure = string.IsNullOrEmpty(donor.Bp) ? "" : donor.Bp;
                    response.bloodGroup = string.IsNullOrEmpty(donor.BloodGroup) ? "" : donor.BloodGroup;
                    response.donorSign = donor.Sign;
                    response.Message = "Donor fetched successfully";
                    response.Success = true;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 0;
                }
                else
                {
                    response.Message = "Donor not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;

                }
            }
            catch (Exception ex)
            {
                response.Message = "getDonorInfo Exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }

            return response;
        }

        public LogoutResponse Logout(LogoutRequest request)
        {
            LogoutResponse response = new LogoutResponse();
            try
            {
                string UserName = GetCurrentIdentityUser(request).Name;
                AlcoholUser userProfileRecord = this.DBContext.Users.FirstOrDefault(x => x.UserName == UserName);
                string id = userProfileRecord.Id;

                if (userProfileRecord != null)
                {
                    if (userProfileRecord.DeviceId != null)
                    {
                        userProfileRecord.DeviceId = null;
                        int affectedRows = this.DBContext.SaveChanges();
                        response.Success = affectedRows == 1;
                    }
                    response.Message = "Logout successfully";
                    response.Success = true;
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.ResponseCode = 0;

                }
                else
                {
                    //User Not found
                    response.Message = "Logged In user not found";
                    response.HttpStatusCode = System.Net.HttpStatusCode.OK;
                    response.Success = false;
                    response.ResponseCode = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Logout exception : " + ex.Message;
                response.Success = false;
                response.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.ResponseCode = 1;
            }
            return response;
        }










        public string GetResponse(string sURL)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURL);
            request.MaximumAutomaticRedirections = 4;
            request.Credentials = CredentialCache.DefaultCredentials;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string sRes = readStream.ReadToEnd();
            }
            catch (Exception ex)
            {

            }
            return sResponse;
        }


        protected override void Disposing()
        {
            this.DBContext.Dispose();
        }
    }
}
