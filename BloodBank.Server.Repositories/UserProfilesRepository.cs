﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using BloodBank.Server.Entities;
using BloodBank.Server.Extensions;
using BloodBank.Server.Identity;
using BloodBank.Server.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BloodBank.Server.Repositories
{
    public class UserProfilesRepository : BaseRepository
    {

        private BloodBankDBContext DBContext;
        private readonly UserManager<AlcoholUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IPasswordHasher<AlcoholUser> passwordHasher;
        private readonly IConfiguration configuration;
        private S3Repository s3Repository;

        public UserProfilesRepository(
                            BloodBankDBContext BUTDBContext,
                            UserManager<AlcoholUser> userManager,
                              RoleManager<IdentityRole> roleManager,
                              S3Repository s3Repository,
                              IPasswordHasher<AlcoholUser> passwordHasher,
                             IConfiguration configuration)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.passwordHasher = passwordHasher;
            this.configuration = configuration;
            this.DBContext = BUTDBContext;
            this.s3Repository = s3Repository;
        }

     
        protected override void Disposing()
        {
            this.userManager.Dispose();
            this.roleManager.Dispose();
        }
    }
}
