﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BloodBank.Server.Entities;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace BloodBank.Server.Logging
{
    /// <summary>
    /// This class provides helper method pertaining to logging Web related details. In specific, those coming from 
    /// HttpContext, Http Session etc.
    /// </summary>
    public static class WebLoggingHelper
    {
        public static void FillLogEventWithRequestInfoFromHttpContext(HttpContext context, BloodBankLogEvent logEvent)
        {
            // Fill in the context's request info
            PopulateRequestData(context, logEvent);

            // set other details. Not strictly request info, but works as part of request
            logEvent.Hostname = Environment.MachineName;
        }

        public static void FillLogEventWithUserInfoFromHttpContext(HttpContext context, BloodBankLogEvent logEvent)
        {
            // populate user data
            PopulateUserData(context, logEvent);
        }
        

        private static void PopulateRequestData(HttpContext context, BloodBankLogEvent logEvent)
        {
            var request = context.Request;

            if (request == null)
            {
                // is it even possible?
                return;
            }

            if (string.IsNullOrWhiteSpace(logEvent.Message))
            {
                var activity = $"{request.Method} {request.Path}";
                logEvent.Message = activity;
            }

            if (string.IsNullOrEmpty(logEvent.CorrelationId))
            {
                logEvent.CorrelationId = context.TraceIdentifier;
            }

            logEvent.Location = request.Path;

            // Add Query String
            var pueryStringDictionary = Microsoft.AspNetCore.WebUtilities
                .QueryHelpers.ParseQuery(request.QueryString.ToString());

            foreach (var key in pueryStringDictionary.Keys)
            {
                //logEvent.AdditionalInfo.Add($"QueryString-{key}", pueryStringDictionary[key]);
            }

            // Add Headers
            foreach (string headerKey in request.Headers.Keys)
            {
                // We may want to filter some here.
                /*logEvent.AdditionalInfo.Add($"Header-{headerKey}",
                                          request.Headers[headerKey]);*/
            }


            // TODO :Revisit --- RemoteIP should be only that, not all the other properties

            //// Remote IP Address
            //var remoteAddress = context.Connection.RemoteIpAddress;
            //object remote = remoteAddress.

            //logEvent.AdditionalInfo.Add("CallerIP", remote);
            //// logEvent.AdditionalInfo.Add("CallerIP-CIDR", remote + "/32");
        }

        private static void PopulateUserData(HttpContext context, BloodBankLogEvent logEvent)
        {
            var userId = string.Empty;
            var user = context.User;  // ClaimsPrincipal.Current is not sufficient

            if (user != null)
            {
                var i = 1; // i included in dictionary key to ensure uniqueness
                foreach (var claim in user.Claims)
                {
                    if (claim.Type == ClaimTypes.NameIdentifier)
                    {
                        userId = claim.Value;
                    }
                    else
                    {
                        // example dictionary key: UserClaim-4-role 
                        //logEvent.AdditionalInfo.Add($"UserClaim-{i++}-{claim.Type}", claim.Value);
                    }
                }
            }

            logEvent.UserId = userId;
        }

    }
}
