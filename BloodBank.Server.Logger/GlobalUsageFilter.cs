﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using BloodBank.Server.Entities;
using BloodBank.Server.Messages;
using BloodBank.Server.Resources;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;

namespace BloodBank.Server.Logging
{
    public class GlobalUsageFilter : IActionFilter
    {
        private string product, layer;
        private BloodBankLogEvent logEvent;

        public GlobalUsageFilter(string product, string layer)
        {
            this.product = product;
            this.layer = layer;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            // This function executes just before *any* endpoint code executes. 
            // That's how it is configured in Startup class. 
            // Remember, even before this function executes the GlobalLoggerMiddleware has
            // already executed.
            // We use this function to populate the logevent with 

            // Retrieve the reference of the global log event created by GlobalLoggerMiddleware 
            this.logEvent =
                context.HttpContext.Items[Strings.Keys.GlobalLogEvent] as BloodBankLogEvent;

            // add to the log event all the relevant HTTP Context info that is useful for troubleshooting, such as
            // request data, user data etc. 
            // This function must be called in this place only. It seems ASP.NET pipeline resoved jwt token into User Info
            // bit late. Hence if called in GlobalLoggerMiddleware, you would not get any user info
            WebLoggingHelper.FillLogEventWithUserInfoFromHttpContext(context.HttpContext, logEvent);

            // add Route Data to log event
            foreach (var key in context.RouteData.Values?.Keys)
            {
                this.logEvent.AdditionalInfo.Add($"RouteData-{key}",
                                                 (string)context.RouteData.Values[key]);
            }

            // Add Request Parameters Info to log event
            string requestJson = string.Empty;
            // For correlating different events of this request together
            string correlationId = string.Empty;

            // ActionArguments contain POSTed, PUTed data and also
            // RouteData.
            // TODO: We may not want to log route data since it is getting logged
            // distinctly as "RouteData-xyz"
            foreach (var item in context.ActionArguments)
            {
                requestJson = JsonConvert.SerializeObject(item.Value);
                this.logEvent.Payload.Add(item.Key, requestJson);

                // Add additional info if any
                BloodBankMessage skoRequest = item.Value as BloodBankMessage;

                if (skoRequest != null)
                {
                    // we have our request object
                    // check if request wants to log additional data
                    skoRequest.PopulateAdditionalDetails(this.logEvent.AdditionalInfo);
                }
            }

            // do the first entry to log. This is the start point of the request
            // we will log http context info, request details, route data etc now
            // so even of the request pipeline blows up (it happens in case of 504), it least would have 
            // this entry in the log
            BloodBankLogger.WriteUsage(logEvent);


            // Check if the Request is invalid. Do not proceed if it is
            // We use DataAnnotations to make this happen
            if (!context.ModelState.IsValid)
            {
                // invalid request. Simply return bad request and stop
                // don't bother the controller
                context.Result = new BadRequestObjectResult(context.ModelState);

                // TODO: Do we want to put entire ModelState or 
                // some meaningful reduced version of it?
                this.logEvent.AdditionalInfo.Add(Strings.Keys.BadRequest, context.ModelState);
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            var result = context.Result;
            var objectResult = result as ObjectResult;

            if (objectResult != null)
            {
                // we do have some return value
                var returnValue = objectResult.Value;

                var value = JsonConvert.SerializeObject(returnValue);

                // Add Response Parameters Info to log event
                this.logEvent.Response = value;

                // Add additional info if any, but before that
                // check if the response is our created response and not some other (crash, etc?)
                BloodBankMessage skoResponse = returnValue as BloodBankMessage;
                if (skoResponse != null)
                {
                    // we are sending some SKOMessage back
                    // check if response object has any additional info to log
                    skoResponse.PopulateAdditionalDetails(this.logEvent.AdditionalInfo);
                }
            }

        }

    }
}
