﻿using Elasticsearch.Net;
using Elasticsearch.Net.Aws;
using Microsoft.AspNetCore.Http;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Formatting.Json;
using Serilog.Sinks.Elasticsearch;
using BloodBank.Server.Entities;
using BloodBank.Server.Extensions;
using System;
using System.Data.SqlClient;

namespace BloodBank.Server.Logging
{
    public static class BloodBankLogger
    {
        public static void WriteUsage(BloodBankLogEvent logEvent)
        {
            using (Logger usageLogger = CreateLogger())
            {
                usageLogger.Write(LogEventLevel.Information, "{@BloodBankLogEvent}", logEvent);
            }
        }

        public static void WriteUsage(string product, string layer, string message)
        {
            using (Logger usageLogger = CreateLogger())
            {
                usageLogger.Write(LogEventLevel.Information,
                    "{@BloodBankLogEvent} Product: " + product + " Layer: " + layer + " Message: " + message);
            }
        }

        public static void WriteError(BloodBankLogEvent logEvent)
        {
            logEvent.Message = logEvent.Exception.GetMessage();

            using (Logger errorLogger = CreateLogger())
            {
                errorLogger.Write(LogEventLevel.Error, "{@BloodBankLogEvent}", logEvent);
            }

        }

        public static void WriteDiagnostic(BloodBankLogEvent logEvent)
        {
            var writeDiagnostics =
                Convert.ToBoolean(Environment.GetEnvironmentVariable("DIAGNOSTICS_ON"));

            if (!writeDiagnostics)
            {
                return;
            }

            using (Logger diagnosticLogger = CreateLogger())
            {
                diagnosticLogger.Write(LogEventLevel.Error, "{@BloodBankLogEvent}", logEvent);
            }
        }


        private static Logger CreateLogger()
        {

           return CreateConsoleLogger();

          // TODO: See if there is a better way of doing this, 
          // return CreateElasticSearchLogger();
          //   return CreateDebugLogger();
          //  return CreateElasticSearchLogger();

        }

        private static Logger CreateDebugLogger()
        {
            var logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.With(new EnvrionmentInfoEncricher())
                .MinimumLevel.Verbose()         // verbose default

                // File log for development
                .WriteTo.RollingFile(new JsonFormatter(), "verbose-but-{Date}.log").      // verbose default
                CreateLogger();

            Serilog.Debugging.SelfLog.Enable((serilog_error) => Console.WriteLine("[Serilog Error]: " + serilog_error));

            return logger;
        }

        private static Logger CreateConsoleLogger()
        {
            var logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.With(new EnvrionmentInfoEncricher())
                .MinimumLevel.Verbose()         // verbose default

                // File log for development
                .WriteTo.Console(LogEventLevel.Verbose)
                .CreateLogger();

            Serilog.Debugging.SelfLog.Enable((serilog_error) => Console.WriteLine("[Serilog Error]: " + serilog_error));

            return logger;
        }

       
    }
}