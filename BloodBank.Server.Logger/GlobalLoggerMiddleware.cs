﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using BloodBank.Server.Entities;
using BloodBank.Server.Resources;

namespace BloodBank.Server.Logging
{
    public static class GlobalLoggerMiddlewareExtensions
    {
        /// <summary>
        /// Uses the SKO Exception handler.
        /// </summary>
        /// <returns>The SKOE xception handler.</returns>
        /// <param name="builder">Builder.</param>
        /// <param name="product">Product.</param>
        /// <param name="layer">Layer.</param>
        /// <param name="suppressException">If set to <c>true</c> suppress exception. This will
        /// be useful when we don't want the user to know of internal details and unfriendly message - the situation when we go live. However
        /// for development, we want to bubble the exception without suppressing it all the way to the crash screen</param>
        public static IApplicationBuilder UseGlobalLoggerMiddleware(
            this IApplicationBuilder builder, string product, string layer)
        {
            return builder.UseMiddleware<GlobalLoggerMiddleware>
                (product, layer);
        }
    }

    public class GlobalLoggerMiddleware
    {
        private readonly RequestDelegate next;
        private readonly string product;
        private readonly string layer;
        private BloodBankLogEvent logEvent;
        private Stopwatch stopwatch;

        public GlobalLoggerMiddleware(string product, string layer,
                                          RequestDelegate next)
        {
            this.next = next;
            this.product = product;
            this.layer = layer;
        }

        // public async Task Invoke(HttpContext context /* other dependencies */)
        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            this.logEvent = new BloodBankLogEvent(this.product, this.layer);

            // start time tracking
            this.stopwatch = Stopwatch.StartNew();

            // kick off create the global log event, add when started "Started" and the 
            // unique identifier (TraceIdentifer) of this request 

            // this is a global log event that will keep accumulating data over the period of requests life cycle
            // and finally writes to log target when request is finished. Remember - this middleware is the first in the line
            // to execute and that means last to revisit in the chain while tarversing return path of registered middlewares
            // hence it is capable of containing all the info from the inception of request to end.
            
            // add date time stamp when did the request start
            this.logEvent.AdditionalInfo.Add($"Started - {context.TraceIdentifier}",
                                             DateTime.Now.ToString(CultureInfo.InvariantCulture));

            // add to the log event all the relevant HTTP Context info that is useful for troubleshooting, such as
            // request data, user data etc. 
            WebLoggingHelper.FillLogEventWithRequestInfoFromHttpContext(context, this.logEvent);

            // Store the global event in context so it can be available to all components participating in this http request. 
            // They just need to access HttpContext and retrieve this event from Items collection
            context.Items[Strings.Keys.GlobalLogEvent] = logEvent;

            // Trigger next middleware. The next middleware will do its work and will in turn trigger the next in the chain and so on
            // finally when last one has done its job, all the middlewares will start post execution on way back
            await next(context);

            // all middlewares have executed and we are back to this middleware. Lets write the Log event now
            // post execution for this middleware
            await WriteLogEvent(context);
        }

        private Task WriteLogEvent(HttpContext context)
        {
            // know what's the status code of Response. 
            // (repository must have returned it which controller took and assigned to Response. Note capital R)
            int statusCode = context.Response.StatusCode;

            // let the log even know the response code
            this.logEvent.AdditionalInfo.Add(Strings.Keys.HttpStatusCode, 
                                        context.Response.StatusCode);

            // add date time stamp when did the request ended
            this.logEvent.AdditionalInfo.Add($"Ended - {context.TraceIdentifier}",
                                             DateTime.Now.ToString(CultureInfo.InvariantCulture));

            // end time tracking. But remember that this is the closest we go to consider "end" of request processing. But
            // this is not the end. After this middleware ends, a lot happens including deserialization and finally 
            // network transfer. We cannot measure that here, so time taken you see in Postman is a lot more than this number
            this.stopwatch.Stop();
            this.logEvent.ElapsedMilliseconds = stopwatch.ElapsedMilliseconds;

            // finally - do the actual writing to log target
            BloodBankLogger.WriteUsage(logEvent);

            // mark the task completed so waiting thread goes ahead
            return Task.CompletedTask;

        }
    }
}


