﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using Serilog.Core;
using Serilog.Events;

namespace BloodBank.Server.Logging
{
    public class EnvrionmentInfoEncricher : ILogEventEnricher
    {
        public EnvrionmentInfoEncricher()
        {
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            // Middleware
            LogEventProperty middlewareVersion =
                propertyFactory.CreateProperty("middlewareVersion", "2018.10.10-Dev");

            logEvent.AddPropertyIfAbsent(middlewareVersion);

            // Runtime
            LogEventProperty os =
                propertyFactory.CreateProperty("os", RuntimeInformation.OSDescription);

            logEvent.AddPropertyIfAbsent(os);

            LogEventProperty runtimeVersion =
                propertyFactory.CreateProperty("runtimeVersion", Environment.Version);

            logEvent.AddPropertyIfAbsent(runtimeVersion);

            LogEventProperty runtimeDirectory =
                propertyFactory.CreateProperty("runtimeDirectory",
                                               RuntimeEnvironment.GetRuntimeDirectory());

            logEvent.AddPropertyIfAbsent(runtimeDirectory);

            // Entry Assembly
            Assembly entryAssembly = Assembly.GetEntryAssembly();

            LogEventProperty entryAssemblyName =
                propertyFactory.CreateProperty("entryAssemblyName", 
                                               entryAssembly.GetName().Name);

            logEvent.AddPropertyIfAbsent(entryAssemblyName);

            LogEventProperty entryAssemblyVersion =
                propertyFactory.CreateProperty("entryAssemblyVersion",
                                               entryAssembly.GetName().Version);

            logEvent.AddPropertyIfAbsent(entryAssemblyVersion);

            // Executing Assembly
            Assembly executingAssembly = Assembly.GetExecutingAssembly();

            LogEventProperty executingAssemblyName =
                propertyFactory.CreateProperty("executingAssemblyName",
                                               executingAssembly.GetName().Name);
            logEvent.AddPropertyIfAbsent(executingAssemblyName);

            LogEventProperty executignAssemblyVersion =
                propertyFactory.CreateProperty("executingAssemblyVersion",
                                               executingAssembly.GetName().Version);

            logEvent.AddPropertyIfAbsent(executignAssemblyVersion);

        }
    }
}
