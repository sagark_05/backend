﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using BloodBank.Server.Entities;
using BloodBank.Server.Resources;

namespace BloodBank.Server.Logging
{
    public static class SKOExceptionMiddlewareExtensions
    {
        /// <summary>
        /// Uses the SKO Exception handler.
        /// </summary>
        /// <returns>The SKOE xception handler.</returns>
        /// <param name="builder">Builder.</param>
        /// <param name="product">Product.</param>
        /// <param name="layer">Layer.</param>
        /// <param name="suppressException">If set to <c>true</c> suppress exception. This will
        /// be useful when we don't want the user to know of internal details and unfriendly message - the situation when we go live. However
        /// for development, we want to bubble the exception without suppressing it all the way to the crash screen</param>
        public static IApplicationBuilder UseSKOExceptionMiddleware(
            this IApplicationBuilder builder, string product, string layer)
        {
            return builder.UseMiddleware<BloodBankExceptionMiddleware>
                (product, layer);
        }
    }

    public class BloodBankExceptionMiddleware
    {
        private readonly RequestDelegate next;
        private readonly string product;
        private readonly string layer;

        public BloodBankExceptionMiddleware(string product, string layer,
                                          RequestDelegate next)
        {
            this.next = next;
            this.product = product;
            this.layer = layer;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                BloodBankLogEvent logEvent = 
                    context.Items[Strings.Keys.GlobalLogEvent] as BloodBankLogEvent;
                logEvent.Exception = exception;

                // Send back user friendly message, not exception message
                string errorMessage = string.Format(
                    "Error Occurred. We are looking into it. Reference Number : {0}",
                    context.TraceIdentifier);

                var result = JsonConvert.SerializeObject(new { error = errorMessage });
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                await context.Response.WriteAsync(result);
             }
        }
    }
}


