﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Identity
{
    public class AlcoholUser : IdentityUser
    {
        public bool InitialPasswordChanged { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        // add more as we need
        public string Address { get; set; }
        public int? IsDelete { get; set; }
        public string DeleteReason { get; set; }
        public string DeviceId { get; set; }

        public string Logintype { get; set; }  ///added by sk on 5/10/2021
        
    }
}
