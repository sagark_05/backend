﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using BloodBank.Server.Entities;
using BloodBank.Server.Logging;
using BloodBank.Server.Resources;
using System;

namespace BloodBank.Server.Identity
{
    // IdentityDbContext has impmementation for Users as well as Roles
    public class BloodBankDBContext : IdentityDbContext<AlcoholUser>
    {
        private IHttpContextAccessor httpContextAccessor;
        private IConfiguration configuration;

        // These are the tables that will get created  
        public virtual DbSet<Attendance> Attendance { get; set; }
        public virtual DbSet<Batch> Batch { get; set; }
        public virtual DbSet<Bill> Bill { get; set; }
        public virtual DbSet<BillDetails> BillDetails { get; set; }
        public virtual DbSet<BloodInterest> BloodInterest { get; set; }
        public virtual DbSet<BloodRequest> BloodRequest { get; set; }
        public virtual DbSet<BloodbankMaster> BloodbankMaster { get; set; }
        public virtual DbSet<CallDetails> CallDetails { get; set; }
        public virtual DbSet<CallDetailsItem> CallDetailsItem { get; set; }
        public virtual DbSet<CallSettlement> CallSettlement { get; set; }
        public virtual DbSet<Camp> Camp { get; set; }
        public virtual DbSet<CampDonor> CampDonor { get; set; }
        public virtual DbSet<CampDonorDetails> CampDonorDetails { get; set; }
        public virtual DbSet<Certificate> Certificate { get; set; }
        public virtual DbSet<Donor> Donor { get; set; }
        public virtual DbSet<DonorBatch> DonorBatch { get; set; }
        public virtual DbSet<DonorQuestionAnswer> DonorQuestionAnswer { get; set; }
        public virtual DbSet<Expense> Expense { get; set; }
        public virtual DbSet<HospitalMaster> HospitalMaster { get; set; }
        public virtual DbSet<InvoiceNo> InvoiceNo { get; set; }
        public virtual DbSet<Mainmenu> Mainmenu { get; set; }
        public virtual DbSet<Kmrate> Kmrate { get; set; }
        public virtual DbSet<MedicalOfficer> MedicalOfficer { get; set; }
        public virtual DbSet<Orgniser> Orgniser { get; set; }
        public virtual DbSet<OtpTracking> OtpTracking { get; set; }
        public virtual DbSet<PlasmaFractionation> PlasmaFractionation { get; set; }
        public virtual DbSet<Pro> Pro { get; set; }
        public virtual DbSet<ProductMaster> ProductMaster { get; set; }
        public virtual DbSet<ProductRate> ProductRate { get; set; }
        public virtual DbSet<Questionnaire> Questionnaire { get; set; }
        public virtual DbSet<Requisition> Requisition { get; set; }
        public virtual DbSet<RequisitionPaymentAttachment> RequisitionPaymentAttachment { get; set; }
        public virtual DbSet<RequisitionProducts> RequisitionProducts { get; set; }
        public virtual DbSet<ScheduleCall> ScheduleCall { get; set; }
        public virtual DbSet<StockDetails> StockDetails { get; set; }
        public virtual DbSet<Submenu> Submenu { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }

        public virtual DbSet<Usermainmenumapping> Usermainmenumapping { get; set; }
        public virtual DbSet<Usersubmenumapping> Usersubmenumapping { get; set; }
        public virtual DbSet<UsersbloodbankMaster> UsersbloodbankMaster { get; set; }
        public virtual DbSet<VersionDetails> VersionDetails { get; set; }

        public BloodBankDBContext(IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connString = configuration.GetConnectionString("BLOODBANKDBContext");
            optionsBuilder.UseMySql(connString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Attendance>(entity =>
            {
                entity.ToTable("attendance");

                entity.Property(e => e.Image)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveAttendanceStatus)
                  .HasMaxLength(45)
                  .IsUnicode(false);

                entity.Property(e => e.Location)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasMaxLength(255)
                    .IsUnicode(false);


                entity.Property(e => e.FkBloodbankId)
                    .HasColumnName("Fk_Bloodbank_Id")
                    .HasMaxLength(255)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<Batch>(entity =>
            {
                entity.ToTable("batch");

                entity.Property(e => e.BatchId)
                    .HasColumnName("Batch_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Amount)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.BatchNumber)
                    .HasColumnName("Batch_Number")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.BatchProductType)
                    .HasColumnName("Batch_Product_Type")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.BillDate)
                    .HasColumnName("Bill_Date")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.BillNo)
                    .HasColumnName("Bill_No")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate).HasColumnName("Expiry_Date");

                entity.Property(e => e.FkBloodbankId)
                 .HasColumnName("Fk_Bloodbank_Id")
                 .HasMaxLength(255)
                 .IsUnicode(false);

                entity.Property(e => e.GstinNo)
                    .HasColumnName("Gstin_No")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ManufactureDate).HasColumnName("Manufacture_Date");

                entity.Property(e => e.RemainingQuantity)
                    .HasColumnName("Remaining_Quantity")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SolutionQuantity)
                    .HasColumnName("Solution_Quantity")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SolutionType)
                    .HasColumnName("Solution_Type")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierName)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.TotalQuantity)
                    .HasColumnName("Total_Quantity")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Bill>(entity =>
            {
                entity.ToTable("bill");

                entity.Property(e => e.BillId).HasColumnName("Bill_Id");

                entity.Property(e => e.AttachmentUrl)
                    .HasColumnName("Attachment_URL")
                    .HasColumnType("longtext");

                entity.Property(e => e.AuthorizedbyId)
                    .HasColumnName("AuthorizedbyID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.BillDate).HasColumnName("Bill_Date");

                entity.Property(e => e.BillPaidDate).HasColumnName("Bill_PaidDate");

                entity.Property(e => e.BillType)
                    .HasColumnName("Bill_Type")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BtSetCharge)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FkBloodbankId)
                 .HasColumnName("Fk_Bloodbank_Id")
                 .HasMaxLength(255)
                 .IsUnicode(false);

                entity.Property(e => e.FkRequisitionId).HasColumnName("Fk_Requisition_Id");

                entity.Property(e => e.IsNat)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NetAmount).HasColumnName("Net_Amount");

                entity.Property(e => e.PaymentType)
                    .HasColumnName("Payment_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Remark).HasColumnType("longtext");

                entity.Property(e => e.ServiceBoyId)
                .HasColumnName("ServiceBoyID")
                .HasMaxLength(255)
                .IsUnicode(false);


                entity.Property(e => e.ServiceCharge)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalAmount).HasColumnName("Total_amount");

                entity.Property(e => e.UpiId)
                   .HasMaxLength(45)
                   .IsUnicode(false);
            });
            //modelBuilder.Entity<Bill>(entity =>
            //{
            //    entity.ToTable("bill");

            //    entity.Property(e => e.BillId).HasColumnName("Bill_Id");

            //    entity.Property(e => e.AttachmentUrl)
            //        .HasColumnName("Attachment_URL")
            //        .HasColumnType("longtext");

            //    entity.Property(e => e.AuthorizedbyId)
            //        .HasColumnName("AuthorizedbyID")
            //        .HasMaxLength(255)
            //        .IsUnicode(false);

            //    entity.Property(e => e.BillDate).HasColumnName("Bill_Date");

            //    entity.Property(e => e.BtSetCharge)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.FkRequisitionId).HasColumnName("Fk_Requisition_Id");

            //    entity.Property(e => e.IsNat)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.NetAmount).HasColumnName("Net_Amount");

            //    entity.Property(e => e.PaymentType)
            //        .HasColumnName("Payment_Type")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Remark).HasColumnType("longtext");

            //    entity.Property(e => e.ServiceCharge)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.TotalAmount).HasColumnName("Total_amount");
            //});


            modelBuilder.Entity<BillDetails>(entity =>
            {
                entity.ToTable("bill_details");

                entity.Property(e => e.BillDetailsId).HasColumnName("Bill_Details_Id");

                entity.Property(e => e.BagNumber)
                    .HasColumnName("Bag_Number")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FkBillId).HasColumnName("Fk_Bill_Id");

                entity.Property(e => e.FkProductId).HasColumnName("Fk_Product_Id");

                entity.Property(e => e.NewCollectionDate).HasColumnName("New_Collection_Date");

                entity.Property(e => e.NewExpiryDate).HasColumnName("New_Expiry_Date");

                entity.Property(e => e.NewTestingDate).HasColumnName("New_Testing_Date");

                entity.Property(e => e.PrintStatus)
                    .HasColumnName("Print_Status")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProductAmount).HasColumnName("Product_Amount");
            });



            modelBuilder.Entity<BloodInterest>(entity =>
            {
                entity.HasKey(e => e.InterestId)
                    .HasName("PRIMARY");

                entity.ToTable("blood_interest");

                entity.Property(e => e.InterestId)
                    .HasColumnName("Interest_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FkRequestId)
                    .HasColumnName("Fk_Request_Id")
                    .HasColumnType("int(11)");


                entity.Property(e => e.InterestDate).HasColumnName("Interest_Date");

                entity.Property(e => e.UserId)
                    .HasColumnName("User_Id")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BloodRequest>(entity =>
            {
                entity.HasKey(e => e.RequestId)
                    .HasName("PRIMARY");

                entity.ToTable("blood_request");

                entity.Property(e => e.RequestId)
                    .HasColumnName("Request_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BloodGroup)
                    .HasColumnName("Blood_Group")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersonMobileNumber)
                    .HasColumnName("Contact_Person_Mobile_Number")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersonName)
                    .HasColumnName("Contact_Person_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HospitalName)
                    .HasColumnName("Hospital_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PatientName)
                    .HasColumnName("Patient_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);


                entity.Property(e => e.RequestDate).HasColumnName("Request_Date");

                entity.Property(e => e.ProductName)
                    .HasColumnName("Product_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BloodbankMaster>(entity =>
            {
                entity.HasKey(e => e.BloodbankId)
                    .HasName("PRIMARY");

                entity.ToTable("bloodbank_master");

                entity.Property(e => e.BloodbankId)
                    .HasColumnName("Bloodbank_Id")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.BloodbankName)
                    .HasColumnName("Bloodbank_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BloodbankShortName)
                 .HasMaxLength(45)
                 .IsUnicode(false);

                entity.Property(e => e.LicenseNumber)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("Phone_Number")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Logo).HasColumnType("longtext");

            });


            modelBuilder.Entity<CallDetails>(entity =>
            {
                entity.ToTable("call_details");

                entity.Property(e => e.CallDetailsId).HasColumnName("Call_Details_Id");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Age)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BillReqId).HasColumnName("Bill_req_ID");

                entity.Property(e => e.BloodGroup)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CallDetails1)
                    .HasColumnName("Call_Details")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedUserId)
                    .HasColumnName("Created_User_Id")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FkCallId).HasColumnName("Fk_Call_ID");

                entity.Property(e => e.FkHospitalId).HasColumnName("Fk_Hospital_Id");

                entity.Property(e => e.Gender)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PatientContact)
                    .HasColumnName("Patient_Contact")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PatientName)
                    .HasColumnName("Patient_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PatientStatus)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ReqBillType)
                    .HasColumnName("Req_Bill_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });


            modelBuilder.Entity<CallDetailsItem>(entity =>
            {
                entity.ToTable("call_details_item");

                entity.Property(e => e.CallDetailsItemId)
                    .HasColumnName("CallDetailsItem_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CallDetailsId)
                    .HasColumnName("CallDetails_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("Product_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Quantity).HasColumnType("int(11)");
            });
            
            modelBuilder.Entity<CallSettlement>(entity =>
            {
                entity.ToTable("call_settlement");

                entity.Property(e => e.CallSettlementId).HasColumnName("Call_Settlement_Id");

                entity.Property(e => e.ApprovedById)
                    .HasColumnName("Approved_by_ID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CollectionSettlementStatus)
                    .HasColumnName("Collection_Settlement_Status")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.DateTime).HasColumnName("Date_time");

                entity.Property(e => e.FkCallId).HasColumnName("Fk_Call_ID");

                entity.Property(e => e.ImageUrl)
                    .HasColumnName("image_Url")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IsKmapproved)
                    .HasColumnName("isKMApproved")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.FkBloodbankId)
           .HasColumnName("Fk_Bloodbank_Id")
           .HasMaxLength(255)
           .IsUnicode(false);

                entity.Property(e => e.Km).HasColumnType("decimal(3,1)");

                entity.Property(e => e.SettlementStatus)
                    .HasColumnName("Settlement_Status")
                    .HasMaxLength(45)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<Camp>(entity =>
            {
                entity.ToTable("camp");

                entity.Property(e => e.CampId)
                    .HasColumnName("Camp_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Address)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CampCode)
                    .HasColumnName("Camp_Code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CampDate).HasColumnName("Camp_Date");

                entity.Property(e => e.CampName)
                    .HasColumnName("Camp_Name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.FkBloodbankId)
              .HasColumnName("Fk_Bloodbank_Id")
              .HasMaxLength(255)
              .IsUnicode(false);


                entity.Property(e => e.FkMedicalOfficerId)
                    .HasColumnName("Fk_Medical_Officer_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FkOrgniserId)
                  .HasColumnName("Fk_Orgniser_Id")
                  .HasColumnType("int(11)");

                entity.Property(e => e.FkProId)
                .HasColumnName("Fk_Pro_Id")
                .HasColumnType("int(11)");

                entity.Property(e => e.IsSpecialCertificate)
                    .HasMaxLength(45)
                    .IsUnicode(false);


                entity.Property(e => e.NoOfDonors)
                    .HasColumnName("No_Of_Donors")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TimeFrom).HasColumnName("Time_From");

                entity.Property(e => e.TimeTo).HasColumnName("Time_To");
            });

            modelBuilder.Entity<CampDonor>(entity =>
            {
                entity.ToTable("camp_donor");

                entity.Property(e => e.CampDonorId)
                    .HasColumnName("Camp_Donor_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BagNo)
                    .HasColumnName("Bag_No")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.FkCampId)
                    .HasColumnName("Fk_Camp_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FkDonorId)
                    .HasColumnName("Fk_Donor_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SignUrl)
                    .HasColumnName("Sign_Url")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CampDonorDetails>(entity =>
            {
                entity.ToTable("camp_donor_details");

                entity.Property(e => e.CampDonorDetailsId)
                    .HasColumnName("CampDonorDetails_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BagNo)
                    .HasColumnName("Bag_No")
                    .HasMaxLength(45)
                    .IsUnicode(false);


                entity.Property(e => e.CampCode)
                    .HasColumnName("Camp_Code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.FkBloodbankId)
            .HasColumnName("Fk_Bloodbank_Id")
            .HasMaxLength(255)
            .IsUnicode(false);

                entity.Property(e => e.FkBatchId)
                    .HasColumnName("Fk_Batch_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FkDonorId)
                    .HasColumnName("Fk_Donor_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.QueStatus)
                 .HasColumnName("Que_Status")
                 .HasMaxLength(45)
                 .IsUnicode(false);

                entity.Property(e => e.Status)
                   .HasMaxLength(45)
                   .IsUnicode(false);

                entity.Property(e => e.Segment)
                 .HasMaxLength(45)
                .IsUnicode(false);



            });

            modelBuilder.Entity<Certificate>(entity =>
            {
                entity.ToTable("certificate");

                entity.Property(e => e.CertificateId)
                    .HasColumnName("Certificate_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.AllocatedDate).HasColumnName("Allocated_Date");

                entity.Property(e => e.CertificateDate).HasColumnName("Certificate_Date");

                entity.Property(e => e.CouponCode)
                    .HasColumnName("Coupon_Code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate).HasColumnName("Expiry_Date");

                entity.Property(e => e.FkCampId)
                    .HasColumnName("Fk_Camp_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FkDonorId)
                    .HasColumnName("Fk_Donor_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.FkBloodbankId)
                   .HasColumnName("Fk_Bloodbank_Id")
                   .HasMaxLength(255)
                   .IsUnicode(false);

            });


            modelBuilder.Entity<Donor>(entity =>
            {
                entity.ToTable("donor");

                entity.Property(e => e.DonorId).HasColumnName("Donor_Id");

                entity.Property(e => e.Address)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.BloodGroup)
                    .HasColumnName("Blood_Group")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Bp)
                  .HasMaxLength(45)
                  .IsUnicode(false);

                entity.Property(e => e.Contact)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Dob).HasColumnName("DOB");

                entity.Property(e => e.DonationDate).HasColumnName("Donation_Date");

                entity.Property(e => e.DonorName)
                    .HasColumnName("Donor_Name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Pulse).HasColumnType("int(11)");

                entity.Property(e => e.Height)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                   .HasMaxLength(255)
                   .IsUnicode(false);

                entity.Property(e => e.Sign).HasColumnType("longtext");
            });

            modelBuilder.Entity<DonorBatch>(entity =>
            {
                entity.ToTable("donor_batch");

                entity.Property(e => e.DonorBatchId)
                    .HasColumnName("Donor_Batch_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BagNo)
                    .HasColumnName("Bag_No")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.BatchId)
                    .HasColumnName("Batch_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DonorId)
                    .HasColumnName("Donor_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SeggmentNo)
                    .HasColumnName("Seggment_No")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DonorQuestionAnswer>(entity =>
            {
                entity.HasKey(e => e.DonorQueAnsId)
                    .HasName("PRIMARY");

                entity.ToTable("donor_question_answer");

                entity.Property(e => e.DonorQueAnsId)
                    .HasColumnName("Donor_Que_Ans_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Ans)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CampCode)
                    .HasColumnName("Camp_Code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.FkDonorId)
                    .HasColumnName("Fk_Donor_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FkQueId)
                    .HasColumnName("Fk_Que_Id")
                    .HasColumnType("int(11)");
            });



            modelBuilder.Entity<Expense>(entity =>
            {
                entity.ToTable("expense");

                entity.Property(e => e.ExpenseBy)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ExpenseOn)
                    .HasMaxLength(45)
                    .IsUnicode(false);


                entity.Property(e => e.FkBloodbankId)
                    .HasColumnName("Fk_Bloodbank_Id")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Image).HasColumnType("longtext");

                entity.Property(e => e.VoucherNo)
                  .HasMaxLength(45)
                  .IsUnicode(false);
            });

            modelBuilder.Entity<HospitalMaster>(entity =>
            {
                entity.ToTable("hospital_master");

                entity.Property(e => e.HospitalMasterId).HasColumnName("Hospital_Master_Id");

                entity.Property(e => e.HospitalName)
                    .HasColumnName("Hospital_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Location)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InvoiceNo>(entity =>
            {
                entity.Property(e => e.InvoiceNoId).HasColumnName("InvoiceNo_Id");

                entity.Property(e => e.InvoiceNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FkBloodbankId)
                 .HasColumnName("Fk_Bloodbank_Id")
                 .HasMaxLength(255)
                 .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Kmrate>(entity =>
            {
                entity.ToTable("kmrate");

                entity.Property(e => e.FkBloodbankId)
               .HasColumnName("Fk_Bloodbank_Id")
               .HasMaxLength(255)
               .IsUnicode(false);

                entity.Property(e => e.Rate).HasColumnType("decimal(10,2)");

                entity.Property(e => e.KmRateId).HasColumnName("KmRate_Id");
            });

            modelBuilder.Entity<Mainmenu>(entity =>
            {
                entity.ToTable("mainmenu");

                entity.Property(e => e.MainMenuId)
                    .HasColumnName("Main_Menu_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MainMenuName)
                    .HasColumnName("Main_Menu_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PlatformType)
                    .HasColumnName("Platform_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MedicalOfficer>(entity =>
            {
                entity.ToTable("medical_officer");

                entity.Property(e => e.MedicalOfficerId)
                    .HasColumnName("Medical_Officer_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ContactNo)
                    .HasColumnName("Contact_No")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.FkBloodbankId)
             .HasColumnName("Fk_Bloodbank_Id")
             .HasMaxLength(255)
             .IsUnicode(false);


                entity.Property(e => e.MedicalOfficerName)
                    .HasColumnName("Medical_Officer_Name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Sign).HasColumnType("longtext");
            });


            modelBuilder.Entity<Orgniser>(entity =>
            {
                entity.ToTable("orgniser");

                entity.Property(e => e.OrgniserId)
                    .HasColumnName("Orgniser_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CompanyName)
                  .HasColumnName("Company_Name")
                  .HasMaxLength(45)
                  .IsUnicode(false);


                entity.Property(e => e.ContactNo)
                    .HasColumnName("Contact_No")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.EmailId)
                    .HasColumnName("Email_Id")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.FkBloodbankId)
          .HasColumnName("Fk_Bloodbank_Id")
          .HasMaxLength(255)
          .IsUnicode(false);

                entity.Property(e => e.Logo).HasColumnType("longtext");

                entity.Property(e => e.Name)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Subtitle)
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });


            modelBuilder.Entity<OtpTracking>(entity =>
            {
                entity.HasKey(e => e.OtpId)
                    .HasName("PRIMARY");

                entity.ToTable("otp_tracking");

                entity.Property(e => e.OtpId)
                    .HasColumnName("OTP_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MobileNo)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Otp)
                    .HasColumnName("OTP")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PlasmaFractionation>(entity =>
            {
                entity.ToTable("plasma_fractionation");

                entity.Property(e => e.PlasmaFractionationId)
                    .HasColumnName("Plasma_Fractionation_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FkBuyerInstituteId)
                    .HasColumnName("Fk_Buyer_Institute_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NetAmount)
                     .HasColumnName("Net_Amount")
                    .HasColumnType("decimal(18,2)");

                entity.Property(e => e.NoOfUnits)
                    .HasColumnName("No_of_Units")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Remark)
                 .HasMaxLength(45)
                 .IsUnicode(false);

                entity.Property(e => e.ServiceCharge).HasColumnType("int(11)");


                entity.Property(e => e.PaymentType)
               .HasColumnName("Payment_Type")
               .HasMaxLength(45)
               .IsUnicode(false);

                entity.Property(e => e.PlasmaFractionationDate).HasColumnName("Plasma_Fractionation_Date");

                entity.Property(e => e.PlasmaFractionationNumber)
    .HasColumnName("Plasma_Fractionation_Number")
    .HasMaxLength(50)
    .IsUnicode(false);

                entity.Property(e => e.ProductId)
                    .HasColumnName("Product_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TotalAmount)
                      .HasColumnName("Total_Amount")
                      .HasColumnType("decimal(18,2)");

                entity.Property(e => e.TotalBatches)
                    .HasColumnName("Total_Batches")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TotalVolumeInGm)
                    .HasColumnName("Total_Volume_In_gm")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TotalVolumeInLiters)
                    .HasColumnName("Total_Volume_In_liters")
                    .HasColumnType("decimal(18,3)");

                entity.Property(e => e.FkBloodbankId)
                  .HasColumnName("Fk_Bloodbank_Id")
                  .HasMaxLength(255)
                  .IsUnicode(false);
            });


            modelBuilder.Entity<Pro>(entity =>
            {
                entity.ToTable("pro");

                entity.Property(e => e.ProId)
                    .HasColumnName("Pro_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ContactNo)
                    .HasColumnName("Contact_No")
                    .HasMaxLength(45)
                    .IsUnicode(false);


                entity.Property(e => e.FkBloodbankId)
          .HasColumnName("Fk_Bloodbank_Id")
          .HasMaxLength(255)
          .IsUnicode(false);

                entity.Property(e => e.ProName)
                    .HasColumnName("Pro_Name")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });


            modelBuilder.Entity<ProductMaster>(entity =>
            {
                entity.HasKey(e => e.ProductId)
                    .HasName("PRIMARY");

                entity.ToTable("product_master");

                entity.Property(e => e.ProductId)
                    .HasColumnName("Product_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("Product_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProductType)
                    .HasColumnName("Product_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProductRate>(entity =>
            {
                entity.HasKey(e => e.ProductId)
                    .HasName("PRIMARY");

                entity.ToTable("product_rate");

                entity.Property(e => e.FkBloodbankId)
                   .HasColumnName("Fk_Bloodbank_Id")
                  .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ProductId).HasColumnName("Product_Id");

                entity.Property(e => e.FkProductMasterId).HasColumnName("FK_ProductMasterId");

                entity.Property(e => e.NatRate).HasColumnName("Nat_Rate");
            });

            modelBuilder.Entity<Questionnaire>(entity =>
            {
                entity.HasKey(e => e.QueId)
                    .HasName("PRIMARY");

                entity.ToTable("questionnaire");

                entity.Property(e => e.QueId)
                    .HasColumnName("Que_Id")
                    .HasColumnType("int(11)");


                entity.Property(e => e.FkBloodbankId)
                    .HasColumnName("Fk_Bloodbank_Id")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Ans)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Question).HasColumnType("longtext");
            });

            modelBuilder.Entity<Requisition>(entity =>
            {
                entity.ToTable("requisition");

                entity.Property(e => e.RequisitionId).HasColumnName("Requisition_Id");

                entity.Property(e => e.AuthorisedById)
                    .HasColumnName("AuthorisedByID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FkBloodbankId)
                 .HasColumnName("Fk_Bloodbank_Id")
                 .HasMaxLength(255)
                 .IsUnicode(false);


                entity.Property(e => e.FkCallId).HasColumnName("Fk_CallID");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.IsNat)
                    .HasMaxLength(45)
                    .IsUnicode(false);
                entity.Property(e => e.PaidDate).HasColumnType("datetime");

                entity.Property(e => e.PaymentType)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceBoyId)
                .HasColumnName("ServiceBoyID")
                .HasMaxLength(255)
                .IsUnicode(false);


                entity.Property(e => e.Remark)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RequisitionDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceCharge).HasColumnType("int(11)");

                entity.Property(e => e.UpiId)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.RequisitionNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RequisitionPaymentAttachment>(entity =>
            {
                entity.ToTable("Requisition_Payment_Attachment");

                entity.Property(e => e.RequisitionPaymentAttachmentId).HasColumnName("Requisition_Payment_Attachment_Id");

                entity.Property(e => e.FkRequisitionId).HasColumnName("Fk_Requisition_Id");

                entity.Property(e => e.ImageUrl)
                    .HasColumnName("Image_Url")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RequisitionProducts>(entity =>
            {
                entity.HasKey(e => e.RequisitionProductId)
                    .HasName("PRIMARY");

                entity.ToTable("requisition_products");

                entity.Property(e => e.RequisitionProductId).HasColumnName("Requisition_Product_Id");

                entity.Property(e => e.FkRequisitionId).HasColumnName("Fk_Requisition_Id");

                entity.Property(e => e.IssueQuantity).HasColumnName("Issue_Quantity");

                entity.Property(e => e.ProductId).HasColumnName("Product_Id");
            });

            modelBuilder.Entity<ScheduleCall>(entity =>
            {
                entity.HasKey(e => e.CallId)
                   .HasName("PRIMARY");

                entity.ToTable("schedule_call");

                entity.Property(e => e.CallId).HasColumnName("Call_Id");

                entity.Property(e => e.AssignedUserId)
                    .HasColumnName("Assigned_User_Id")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CallDateTime).HasColumnName("Call_Date_Time");

                entity.Property(e => e.FkBloodbankId)
                .HasColumnName("Fk_Bloodbank_Id")
                .HasMaxLength(255)
                .IsUnicode(false);


                entity.Property(e => e.Status)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });


            modelBuilder.Entity<StockDetails>(entity =>
            {
                entity.ToTable("stock_details");

                entity.Property(e => e.StockDetailsId).HasColumnName("Stock_Details_Id");

                entity.Property(e => e.BagNo)
                    .HasColumnName("Bag_No")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.BloodGroup)
                    .HasColumnName("Blood_Group")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CollectedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CollectionDate).HasColumnName("Collection_Date");

                entity.Property(e => e.DiscardReason)
                    .HasColumnName("Discard_Reason")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate).HasColumnName("Expiry_Date");

                entity.Property(e => e.FkBloodbankId)
              .HasColumnName("Fk_Bloodbank_Id")
              .HasMaxLength(255)
              .IsUnicode(false);


                entity.Property(e => e.IssueFlag)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProductId).HasColumnName("Product_Id");

                entity.Property(e => e.Remark)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                 entity.Property(e => e.Segment)
                  .HasMaxLength(45)
                 .IsUnicode(false);

                entity.Property(e => e.Solution)
                    .HasMaxLength(45)
                    .IsUnicode(false);


                entity.Property(e => e.TestDate).HasColumnName("Test_Date");

                entity.Property(e => e.TestResult)
                    .HasColumnName("Test_Result")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TestedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Volume)
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<StockDetails>(entity =>
            {
                entity.ToTable("stock_details");

                entity.Property(e => e.StockDetailsId).HasColumnName("Stock_Details_Id");

                entity.Property(e => e.BagNo)
                    .HasColumnName("Bag_No")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.BloodGroup)
                    .HasColumnName("Blood_Group")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CollectionDate).HasColumnName("Collection_Date");

                entity.Property(e => e.DiscardReason)
                    .HasColumnName("Discard_Reason")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate).HasColumnName("Expiry_Date");

                entity.Property(e => e.IssueFlag)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProductId).HasColumnName("Product_Id");

                entity.Property(e => e.Remark)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TestDate).HasColumnName("Test_Date");

                entity.Property(e => e.TestResult)
                    .HasColumnName("Test_Result")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Volume)
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Submenu>(entity =>
            {
                entity.ToTable("submenu");

                entity.Property(e => e.SubMenuId)
                    .HasColumnName("Sub_Menu_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ActionNameText)
                                .HasColumnName("Action_Name_Text")
                                .HasMaxLength(45)
                                .IsUnicode(false);

                entity.Property(e => e.ControllerNameText)
                    .HasColumnName("Controller_Name_Text")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.FkMainMenuId)
                    .HasColumnName("Fk_Main_Menu_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PlatformType)
                    .HasColumnName("Platform_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubMenuName)
                    .HasColumnName("Sub_Menu_Name")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });


            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.HasIndex(e => e.EmailId)
                    .HasName("EmailId_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.Property(e => e.Address)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contact)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmailId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UsersbloodbankMaster>(entity =>
            {
                entity.HasKey(e => e.UserBloodBankMasterId)
                    .HasName("PRIMARY");

                entity.ToTable("usersbloodbank_master");

                entity.Property(e => e.UserBloodBankMasterId)
                    .HasColumnName("UserBloodBank_Master_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FkBloodBankId)
                    .HasColumnName("FK_BloodBank_Id")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("User_Id")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.ToTable("user_role");

                entity.Property(e => e.UserRoleId)
                    .HasColumnName("User_Role_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RoleName)
                    .HasColumnName("Role_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Usermainmenumapping>(entity =>
            {
                entity.ToTable("usermainmenumapping");

                entity.Property(e => e.UserMainMenuMappingId)
                    .HasColumnName("UserMainMenuMapping_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FkMainMenuId)
                    .HasColumnName("Fk_Main_Menu_ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PlatformType)
                    .HasColumnName("Platform_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Usersubmenumapping>(entity =>
            {
                entity.ToTable("usersubmenumapping");

                entity.Property(e => e.UserSubMenuMappingId)
                    .HasColumnName("UserSubMenuMapping_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FkSubMenuId)
                    .HasColumnName("Fk_Sub_Menu_ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PlatformType)
                    .HasColumnName("Platform_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });


            modelBuilder.Entity<VersionDetails>(entity =>
            {
                entity.HasKey(e => e.VersionId)
                    .HasName("PRIMARY");

                entity.Property(e => e.VersionId).HasColumnType("int(11)");

                entity.Property(e => e.AppType)
                  .HasMaxLength(45)
                  .IsUnicode(false);

                entity.Property(e => e.VersionName).HasColumnType("varchar(255)");

                entity.Property(e => e.VersionNumber).HasColumnType("varchar(255)");

                entity.Property(e => e.DeviceType).HasColumnType("varchar(100)");

                entity.Property(e => e.ForceUpdateVerName).HasColumnType("varchar(255)");

                entity.Property(e => e.ForceUpdateVerNumber).HasColumnType("varchar(255)");
            });


        }


    }
}

