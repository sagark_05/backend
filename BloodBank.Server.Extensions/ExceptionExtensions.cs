﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Extensions
{
    public static class ExceptionExtensions
    {
        public static string GetMessage(this Exception exception)
        {
            // we stop at 3 levels of Inner exception
            // this is ugliest and dirtiest implementation but should work

            if (exception == null)
            {
                return string.Empty;
            }

            string message = exception.Message;

            if (exception.InnerException != null)
            {
                message += " Inner 1 : " + exception.InnerException.Message;

                if (exception.InnerException.InnerException != null)
                {
                    message += " Inner 2 : " + exception.InnerException.InnerException.Message;


                    if (exception.InnerException.InnerException.InnerException != null)
                    {
                        message += " Inner 3 : " + exception.InnerException.InnerException.InnerException.Message;
                    }
                }

            }


            return message;
        }
    }
}
