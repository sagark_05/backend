﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BloodBank.Server.Extensions
{
    public static class CollectionExtensions
    {
        public static bool HasElements<T>(this ICollection<T> collection)
        {
            return collection != null && collection.Count > 0;
        }
    }
}
