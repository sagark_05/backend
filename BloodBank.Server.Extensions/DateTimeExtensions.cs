﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Extensions
{
    public static class DateTimeExtensions
    {

        public static string GetCurrentUTCTimestamp()
        {
            
            string UTCTimestamp;

            //Get Current UTC date time
            //DateTime dateTime = DateTime.UtcNow;

            //UTCTimestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString(); //1555667458

            //Int32 unixTimestamp1 = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds; //1555667458
            //string unixTimestamp2 = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds.ToString(); //1555667458.54635

            string unixTimestamp3 = (
                DateTime.UtcNow
                .Subtract(new DateTime(1970, 1, 1))
                .Ticks/TimeSpan.TicksPerSecond)
                .ToString(); //1555667458

            UTCTimestamp = unixTimestamp3;

            return UTCTimestamp;
        }

        public static DateTime GetCurrentUTCDateTime()
        {
            
            //Get Current UTC date time
            DateTime dateTimeUTC = DateTime.UtcNow;
           
            return dateTimeUTC;
        }

    }
}
