﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Extensions
{
    public static class StringExtensions
    {
        public static bool HasValue(this string str)
        {
            return !string.IsNullOrWhiteSpace(str);
        }
        
        public static string RemoveSpaces(this string str)
        {
            string trimmedStr = str.Trim();
            return trimmedStr.Replace(" ", "");
        }
    }
}
