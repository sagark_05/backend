using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Runtime;
using Amazon.S3;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using BloodBank.Server.Entities;
using BloodBank.Server.Identity;
using BloodBank.Server.Logging;
using BloodBank.Server.Repositories;
using BloodBank.Server.Resources;

namespace Roice.Server
{
    public class Startup
    {
        private const string PRODUCT = Strings.PRODUCT;
        private const string LAYER = "BloodBank.Server";

        public const string AppS3BucketKey = "AppS3Bucket";

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            var contentRoot = env.ContentRootPath; 
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddMvc().AddNewtonsoftJson();
            services.AddControllers().AddNewtonsoftJson();
            services.AddControllersWithViews().AddNewtonsoftJson();
            services.AddRazorPages().AddNewtonsoftJson();
            // Http Accessor
            services.AddHttpContextAccessor();
            
            // Add S3 to the ASP.NET Core dependency injection framework.
            // services.AddAWSService<Amazon.S3.IAmazonS3>();
            services.AddScoped<Amazon.S3.IAmazonS3>(s => new AmazonS3Client
            (Strings.AccessKey, Strings.SecretKey, Amazon.RegionEndpoint.APSouth1));
             

            //Configure S3 settings
            services.Configure<S3Settings>(s3Setting =>
            Configuration.GetSection("S3Settings").Bind(s3Setting));

            // Add Mvc options
            services.AddMvc(options =>
            {
                options.Filters.Add(new GlobalUsageFilter(PRODUCT, LAYER));
                options.ReturnHttpNotAcceptable = true;
                options.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
                options.InputFormatters.Add
                       (new XmlDataContractSerializerInputFormatter(new MvcOptions()));
                options.EnableEndpointRouting = false;

            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
                          

            //.AddNewtonsoftJson(options =>
            //{
            //    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            //});
            //.AddJsonOptions(options =>
            //{
            //   // this takes care of cyclic references.no recursive Stack
            //  // overflow now.will ignore recursive infite loops
            //    options.SerializerSettings.ReferenceLoopHandling =
            //      Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            ////});

            //services.AddControllers()
            //            .AddNewtonsoftJson(options =>
            //{
            //    // this takes care of cyclic references.no recursive Stack
            //    // overflow now.will ignore recursive infite loops
            //    options.SerializerSettings.ReferenceLoopHandling =
            //      Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            //});

            // Default : MySQL
            services.AddDbContext<BloodBankDBContext>();
            services.AddIdentity<AlcoholUser, IdentityRole>().
            AddEntityFrameworkStores<BloodBankDBContext>();

            // For DI
            services.AddScoped<UsersRepository>();
            services.AddScoped<UsersRepository>();
            services.AddScoped<AuthRepository>();
            
            services.AddScoped<PostsRepository>();
           
            services.AddScoped<UserProfilesRepository>();
            services.AddScoped<S3Repository>();
            services.AddScoped<StockRepository>();// AU Added 07Dec2020
            services.AddScoped<DonorRepository>();// SK Added 10Oct2021
            // Authorization
            services.AddAuthorization(config =>
            {
                config.AddPolicy(Strings.AccessPolicies.UsersManagementPolicy,
                    p => p.RequireClaim(Strings.Claims.CanCRUDUsers, "true"));
            });

            // Token authentication


            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(cfg =>
            {
                cfg.RequireHttpsMetadata = false;
                cfg.SaveToken = true;

                cfg.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidIssuer = Configuration["Tokens:Issuer"],
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Tokens:Key"])),
                    ValidateIssuer = true,
                    // can't set here. as there can be more than one. hence used customer validator
                    //ValidAudience = Configuration["Tokens:Audience"],
                    ValidateAudience = true,
                    AudienceValidator = RoiceAudienceValidator,
                    ValidateLifetime = true
                };

            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // register Global Logger Middleware
            app.UseGlobalLoggerMiddleware(PRODUCT, LAYER);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // Log Exception
                app.UseSKOExceptionMiddleware(PRODUCT, LAYER);
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseMvc();
            //services.AddMvc(options => options.EnableEndpointRouting = false);

            /*
             * //code tried for turning on MVc with view
             * 
             * routes =>
            {
                routes.MapRoute(
                    "Default",                         // Route name
                    "{controller}/{action}/{id}",      // URL with parameters
                    new { controller = "ResetPasswordController", action = "Index", id = "" });   // Parameter defaults
            }*/
        }


        /// <summary>
        /// Do here custom logic pertaining to validating audience
        /// audience is read from token. and can have one of the fixed values
        /// Let the master list be stored somewhere in cache for fast access
        /// Until all that is decided we are simply returning "true"
        /// This means, one validation will happen at the time of token creation whether the
        /// customer is valid and has active subscription and until we remove this hardcoded
        /// "true", token will be valid till it gets expired
        /// </summary>
        /// <param name="audiences"></param>
        /// <param name="securityToken"></param>
        /// <param name="validationParameters"></param>
        /// <returns></returns>
        private bool RoiceAudienceValidator(IEnumerable<string> audiences, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            return true;
        }

    }
}
