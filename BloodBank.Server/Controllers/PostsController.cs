﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BloodBank.Server.Entities;
using BloodBank.Server.Messages;

using BloodBank.Server.Repositories;
using BloodBank.Server.Resources;
using BloodBank.Server.Controllers;

namespace Roice.Server.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : BaseController
    {
        private PostsRepository repository;

        public PostsController(PostsRepository repository)
        {
            this.repository = repository;
        }

       
        protected override void Disposing()
        {
            this.repository.Dispose();
        }

        protected override BaseResponse Execute(string action, BaseRequest request)
        {
            
            throw new NotImplementedException($"{action} not implemented");
        }
    }
}