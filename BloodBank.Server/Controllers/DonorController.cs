﻿using BloodBank.Server.Identity;
using BloodBank.Server.Messages;
using BloodBank.Server.Messages.Donor;
using BloodBank.Server.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodBank.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DonorController : BaseController
    {
        private DonorRepository repository;
        private BloodBankDBContext DBContext;

        public DonorController(DonorRepository repository, BloodBankDBContext dbContext)
        {
            this.repository = repository;
            this.DBContext = dbContext;
        }



        [HttpPost("AddBloodOrder")]
        public IActionResult AddBloodOrder(AddBloodOrderRequest request)
        {
            return this.ProcessRequest<AddBloodOrderResponse>(request);
        }

        [HttpGet("GetAllBloodOrder")]
        public IActionResult GetAllBloodOrder()
        {
            GetAllBloodOrderRequest request = new GetAllBloodOrderRequest();
            return this.ProcessRequest<GetAllBloodOrderResponse>(request);
        }

        [HttpPost("AddBloodDonateInterest")]
        public IActionResult AddBloodDonateInterest(AddBloodDonateInterestRequest request)
        {
            return this.ProcessRequest<AddBloodDonateInterestResponse>(request);
        }

        [HttpPost("UpdateDonorProfile")]
        public IActionResult UpdateDonorProfile(UpdateDonorProfileRequest request)
        {
            return this.ProcessRequest<UpdateDonorProfileResponse>(request);
        }




        protected override void Disposing()
        {
            this.repository.Dispose();
        }
        protected override BaseResponse Execute(string action, BaseRequest request)
        {
            if (action == nameof(AddBloodOrder))
            {
                return repository.AddBloodOrder(request as AddBloodOrderRequest);
            }
            else if (action == nameof(GetAllBloodOrder))
            {
                return repository.GetAllBloodOrder(request as GetAllBloodOrderRequest);
            }
            else if (action == nameof(AddBloodDonateInterest))
            {
                return repository.AddBloodDonateInterest(request as AddBloodDonateInterestRequest);
            }
            else if (action == nameof(UpdateDonorProfile))
            {
                return repository.UpdateDonorProfile(request as UpdateDonorProfileRequest);
            }


            throw new NotImplementedException();
        }
        }
    }
