﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BloodBank.Server.Identity;
using BloodBank.Server.Messages;
using BloodBank.Server.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BloodBank.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockController : BaseController
    {
        private StockRepository repository;
        private BloodBankDBContext DBContext;

        public StockController(StockRepository repository, BloodBankDBContext dbContext)
        {
            this.repository = repository;
            this.DBContext = dbContext;
        }
        [HttpGet("GetUserMenuList")]
        public IActionResult GetUserMenuList()
        {
            GetUserMenuListRequest request = new GetUserMenuListRequest();
            return this.ProcessRequest<GetUserMenuListResponse>(request);
        }

        [HttpGet("UserBloodbankList")]
        public IActionResult UserBloodbankList()
        {
            UserBloodbankListRequest request = new UserBloodbankListRequest();
            return this.ProcessRequest<UserBloodbankListResponse>(request);
        }

        [HttpPost("AddStock")]
        public IActionResult AddStock(AddStockRequest request)
        {
            return this.ProcessRequest<AddStockResponse>(request);
        }

        [HttpPost("BulkUplaod")]
        public IActionResult BulkUplaod(BulkUplaodRequest request)
        {
            return this.ProcessRequest<BulkUplaodResponse>(request);
        }

        [HttpPost("AddTesting")]
        public IActionResult AddTesting(AddTestingRequest request)
        {
            return this.ProcessRequest<AddTestingResponse>(request);
        }

        [HttpPost("GetCallList")]
        public IActionResult GetCallList(GetCallListRequest request)
        {
            return this.ProcessRequest<GetCallListResponse>(request);
        }

        [HttpPost("GetCallDetails")]
        public IActionResult GetCallDetails(GetCallDetailsRequest request)
        {
           // GetCallDetailsRequest request = new GetCallDetailsRequest();
            return this.ProcessRequest<GetCallDetailsResponse>(request);
        }

        [HttpPost("ViewStock")]
        public IActionResult ViewStock(ViewStockRequest request)
        {
           // ViewStockRequest request = new ViewStockRequest();
            return this.ProcessRequest<ViewStockResponse>(request);
        }

        [HttpPost("ViewStockbyProductId")]
        public IActionResult ViewStockbyProductId(ViewStockbyProductTypeRequest request)
        {
            return this.ProcessRequest<ViewStockbyProductTypeResponse>(request);
        }

        [HttpGet("GetAccount")]
        public IActionResult GetAccount()
        {
            GetAccountRequest request = new GetAccountRequest();
            return this.ProcessRequest<GetAccountResponse>(request);
        }

        [HttpPost("AddCall")]
        public IActionResult AddCall(AddCallRequest request)
        {
            return this.ProcessRequest<AddCallResponse>(request);
        }

        [HttpPost("AddExpenses")]
        public IActionResult AddExpenses(AddExpensesRequest request)
        {
            return this.ProcessRequest<AddExpensesResponse>(request);
        }

        [HttpPost("Assign")]
        public IActionResult Assign(AssignRequest request)
        {
            return this.ProcessRequest<AssignResponse>(request);
        }

        [HttpPost("AddRequisition")]
        public IActionResult AddRequisition(AddRequisitionRequest request)
        {
            return this.ProcessRequest<AddRequisitionResponse>(request);
        }

        [HttpPost("FilteredStock")]
        public IActionResult FilteredStock(FilteredStockRequest request)
        {
            return this.ProcessRequest<FilteredStockResponse>(request);
        }

        [HttpGet("GetQuantityAmount")]
        public IActionResult GetQuantityAmount()
        {
            GetQuantityAmountRequest request = new GetQuantityAmountRequest();
            return this.ProcessRequest<GetQuantityAmountResponse>(request);
        }

        [HttpGet("GetAllHospitals")]
        public IActionResult GetAllHospitals()
        {
            GetAllHospitalsRequest request = new GetAllHospitalsRequest();
            return this.ProcessRequest<GetAllHospitalsResponse>(request);
        }

        [HttpPost("GetAllAssignedUser")]
        public IActionResult GetAllAssignedUser(GetAllAssignedUserRequest request)
        {
           // GetAllAssignedUserRequest request = new GetAllAssignedUserRequest();
            return this.ProcessRequest<GetAllAssignedUserResponse>(request);
        }

        [HttpPost("GetAttendanceStatus")]
        public IActionResult GetAttendanceStatus(GetAttendanceStatusRequest request)
        {
            //GetAttendanceStatusRequest request = new GetAttendanceStatusRequest();
            return this.ProcessRequest<GetAttendanceStatusResponse>(request);
        }

        [HttpPost("MarkAttendance")]
        public IActionResult MarkAttendance(MarkAttendanceRequest request)
        {
            return this.ProcessRequest<MarkAttendanceResponse>(request);
        }

        [HttpPost("GetAllAttendanceList")]
        public IActionResult GetAllAttendanceList(GetAllAttendanceListRequest request)
        {
           // GetAllAttendanceListRequest request = new GetAllAttendanceListRequest();
            return this.ProcessRequest<GetAllAttendanceListResponse>(request);
        }

        [HttpPost("AddCallSettlement")]
        public IActionResult AddCallSettlement(AddCallSettlementRequest request)
        {
            return this.ProcessRequest<AddCallSettlementResponse>(request);
        }

        [HttpPost("GetApproveKilometerList")]
        public IActionResult GetApproveKilometerList(GetApproveKilometerListRequest request)
        {
            return this.ProcessRequest<GetApproveKilometerListResponse>(request);
        }

        [HttpPost("ApproveCallSettlement")]
        public IActionResult ApproveCallSettlement(ApproveCallSettlementRequest request)
        {
            return this.ProcessRequest<ApproveCallSettlementResponse>(request);
        }

        [HttpPost("CalculateRequisitionAmount")]
        public IActionResult CalculateRequisitionAmount(CalculateRequisitionAmountRequest request)
        {
            return this.ProcessRequest<CalculateRequisitionAmountResponse>(request);
        }

        [HttpGet("UserSettlementAmount")]
        public IActionResult UserSettlementAmount()
        {
            UserSettlementAmountRequest request = new UserSettlementAmountRequest();
            return this.ProcessRequest<UserSettlementAmountResponse>(request);
        }

        [HttpPost("getCurrentStockDetails")]
        public IActionResult getCurrentStockDetails(getCurrentStockDetailsRequest request)
        {
            return this.ProcessRequest<getCurrentStockDetailsResponse>(request);
        }

        [HttpPost("GetServiceBoysWithCallDetails")]
        public IActionResult GetServiceBoysWithCallDetails(GetServiceBoysWithCallDetailsRequest request)
        {
            return this.ProcessRequest<GetServiceBoysWithCallDetailsResponse>(request);
        }

        [HttpPost("GetProductRate")]
        public IActionResult GetProductRate(GetProductRateRequest request)
        {
            return this.ProcessRequest<GetProductRateResponse>(request);
        }

        [HttpPost("CalculateCallSettlement")]
        public IActionResult CalculateCallSettlement(CalculateCallSettlementRequest request)
        {
            return this.ProcessRequest<CalculateCallSettlementResponse>(request);
        }

        [HttpPost("Accounts")]
       // [HttpGet("Accounts")]
        public IActionResult Accounts(AccountsRequest request)
        {
            return this.ProcessRequest<CalculateCallSettlementResponse>(request);
        }

        [HttpPost("AddCamp")]
        public IActionResult AddCamp(AddCampRequest request)
        {   
            return this.ProcessRequest<AddCampResponse>(request);
        }


        [HttpPost("ViewCamp")]
        public IActionResult ViewCamp(ViewCampRequest request)
        {
            return this.ProcessRequest<ViewCampResponse>(request);
        }


        [HttpPost("AddOrgniser")]
        public IActionResult AddOrgniser(AddOrgniserRequest request)
        {
            return this.ProcessRequest<AddOrgniserResponse>(request);
        }


        [HttpPost("ViewOrgniser")]
        public IActionResult ViewOrgniser(ViewOrgniserRequest request)
        {
           // ViewOrgniserRequest request = new ViewOrgniserRequest();
            return this.ProcessRequest<ViewOrgniserResponse>(request);
        }

        [HttpPost("AddDonor")]
        public IActionResult AddDonor(AddDonorRequest request)
        {
            return this.ProcessRequest<AddDonorResponse>(request);
        }

        [HttpGet("ViewDonor")]
        public IActionResult ViewDonor()
        {
            ViewDonorRequest request = new ViewDonorRequest();
            return this.ProcessRequest<ViewDonorResponse>(request);
        }

        [HttpPost("ViewDonorByCampCode")]
        public IActionResult ViewDonorByCampCode(ViewDonorByCampCodeRequest request)
        {
            return this.ProcessRequest<ViewDonorByCampCodeResponse>(request);
        }

        [HttpPost("ViewDonorById")]
        public IActionResult ViewDonorById(ViewDonorByIdRequest request)
        {
            return this.ProcessRequest<ViewDonorByIdResponse>(request);
        }

        [HttpPost("UpdateDonor")]
        public IActionResult UpdateDonor(UpdateDonorRequest request)
        {
            return this.ProcessRequest<UpdateDonorResponse>(request);
        }

        [HttpPost("RejectDonor")]
        public IActionResult RejectDonor(RejectDonorRequest request)
        {
            return this.ProcessRequest<RejectDonorResponse>(request);
        }

        [HttpPost("ViewPro")]
        public IActionResult ViewPro(ViewProRequest request)
        {
           // ViewProRequest request = new ViewProRequest();
            return this.ProcessRequest<ViewProResponse>(request);
        }

        [HttpPost("ViewMedicalOfficer")]
        public IActionResult ViewMedicalOfficer(ViewMedicalOfficerRequest request)
        {
          //  ViewMedicalOfficerRequest request = new ViewMedicalOfficerRequest();
            return this.ProcessRequest<ViewMedicalOfficerResponse>(request);
        }

        [HttpPost("AddPro")]
        public IActionResult AddPro(AddProRequest request)
        {
            return this.ProcessRequest<AddProResponse>(request);
        }

        [HttpPost("AddMedicalOfficer")]
        public IActionResult AddMedicalOfficer(AddMedicalOfficerRequest request)
        {
            return this.ProcessRequest<AddMedicalOfficerResponse>(request);
        }

        [HttpPost("GetAllQuestionAnswer")]
        public IActionResult GetAllQuestionAnswer(GetAllQuestionAnswerRequest request)
        {
           //GetAllQuestionAnswerRequest request = new GetAllQuestionAnswerRequest();
            return this.ProcessRequest<GetAllQuestionAnswerResponse>(request);
        }

        [HttpPost("GetDonorQuestionAnswer")]
        public IActionResult GetDonorQuestionAnswer(GetDonorQuestionAnswerRequest request)
        {
            return this.ProcessRequest<GetDonorQuestionAnswerResponse>(request);
        }

        [HttpPost("VerifyDonorAnswer")]
        public IActionResult VerifyDonorAnswer(VerifyDonorAnswerRequest request)
        {
            return this.ProcessRequest<VerifyDonorAnswerResponse>(request);
        }

        [HttpPost("GetAllBatchNo")]
        public IActionResult GetAllBatchNo(GetAllBatchNoRequest request)
        {
           // GetAllBatchNoRequest request = new GetAllBatchNoRequest();
            return this.ProcessRequest<GetAllBatchNoResponse>(request);
        }

        [HttpGet("GetDonorCertificate")]
        public IActionResult GetDonorCertificate()
        {
            GetDonorCertificateRequest request = new GetDonorCertificateRequest();
            return this.ProcessRequest<GetDonorCertificateResponse>(request);
        }

        [HttpPost("DeleteCamp")]
        public IActionResult DeleteCamp(DeleteCampRequest request)
        {
            return this.ProcessRequest<DeleteCampResponse>(request);
        }

        [HttpPost("DeleteDonor")]
        public IActionResult DeleteDonor(DeleteDonorRequest request)
        {
            return this.ProcessRequest<DeleteDonorResponse>(request);
        }

        [HttpGet("GetDonorInfo")]
        public IActionResult GetDonorInfo()
        {
            GetDonorInfoRequest request = new GetDonorInfoRequest();
            return this.ProcessRequest<GetDonorInfoResponse>(request);
        }

        [HttpGet("Logout")]
        public IActionResult Logout()
        {
            LogoutRequest request = new LogoutRequest();
            return this.ProcessRequest<LogoutResponse>(request);
        }


        protected override void Disposing()
        {
            this.repository.Dispose();
        }

        protected override BaseResponse Execute(string action, BaseRequest request)
        {
            if (action == nameof(GetUserMenuList))
            {
                return repository.GetUserMenuList(request as GetUserMenuListRequest);
            }
          else if (action == nameof(UserBloodbankList))
            {
                return repository.UserBloodbankList(request as UserBloodbankListRequest);
            }
           else if (action == nameof(AddStock))
            {
                return repository.AddStock(request as AddStockRequest);
            }
            else if (action == nameof(BulkUplaod))
            {
                return repository.BulkUplaod(request as BulkUplaodRequest);
            }
            else if(action == nameof(AddTesting))
            {
                return repository.AddTesting(request as AddTestingRequest);
            }
            else if(action == nameof(GetCallList))
            {
                return repository.GetCallList(request as GetCallListRequest);
            }
            else if(action == nameof(GetCallDetails))
            {
                return repository.GetCallDetails(request as GetCallDetailsRequest);
            }
            else if(action == nameof(ViewStock))
            {
                return repository.ViewStock(request as ViewStockRequest);
            }
            else if(action == nameof(GetAccount))
            {
                return repository.GetAccount(request as GetAccountRequest);
            }
            else if(action == nameof(AddCall))
            {
                return repository.AddCall(request as AddCallRequest);
            }
            else if(action == nameof(AddExpenses))
            {
                return repository.AddExpenses(request as AddExpensesRequest);
            }
            else if(action == nameof(Assign))
            {
                return repository.Assign(request as AssignRequest);
            }
            else if(action == nameof(AddRequisition))
            {
                return repository.AddRequisition(request as AddRequisitionRequest);
            }
            else if(action == nameof(FilteredStock))
            {
                return repository.FilteredStock(request as FilteredStockRequest);
            }
            else if(action == nameof(GetQuantityAmount))
            {
                return repository.GetQuantityAmount(request as GetQuantityAmountRequest);
            }
            else if(action == nameof(GetAllHospitals))
            {
                return repository.GetAllHospitals(request as GetAllHospitalsRequest);
            }
            else if(action == nameof(GetAllAssignedUser))
            {
                return repository.GetAllAssignedUser(request as GetAllAssignedUserRequest);
            }
            else if(action == nameof(GetAttendanceStatus))
            {
                return repository.GetAttendanceStatus(request as GetAttendanceStatusRequest);
            }
            else if(action == nameof(MarkAttendance))
            {
                return repository.MarkAttendance(request as MarkAttendanceRequest);
            }
            else if (action == nameof(GetAllAttendanceList))
            {
                return repository.GetAllAttendanceList(request as GetAllAttendanceListRequest);
            }
            else if(action == nameof(AddCallSettlement))
            {
                return repository.AddCallSettlement(request as AddCallSettlementRequest);
            }
            else if(action == nameof(GetApproveKilometerList))
            {
                return repository.GetApproveKilometerList(request as GetApproveKilometerListRequest);
            }
            else if(action == nameof(ApproveCallSettlement))
            {
                return repository.ApproveCallSettlement(request as ApproveCallSettlementRequest);
            }
            else if(action == nameof(CalculateRequisitionAmount))
            {
                return repository.CalculateRequisitionAmount(request as CalculateRequisitionAmountRequest);
            }
            else if(action == nameof(UserSettlementAmount))
            {
                return repository.UserSettlementAmount(request as UserSettlementAmountRequest);
            }
            else if(action == nameof(getCurrentStockDetails))
            {
                return repository.getCurrentStockDetails(request as getCurrentStockDetailsRequest);
            }
            else if (action == nameof(GetServiceBoysWithCallDetails))
            {
                return repository.GetServiceBoysWithCallDetails(request as GetServiceBoysWithCallDetailsRequest);
            }
            else if (action == nameof(ViewStockbyProductId))
            {
                return repository.ViewStockbyProductId(request as ViewStockbyProductTypeRequest);
            }
            else if (action == nameof(GetProductRate))
            {
                return repository.GetProductRate(request as GetProductRateRequest);
            }
            else if (action == nameof(CalculateCallSettlement))
            {
                return repository.CalculateCallSettlement(request as CalculateCallSettlementRequest);
            }
            else if (action == nameof(Accounts))
            {
                return repository.Accounts(request as AccountsRequest);
            }
            else if (action == nameof(AddCamp))
            {
                return repository.AddCamp(request as AddCampRequest);
            }
            else if (action == nameof(ViewCamp))
            {
                return repository.ViewCamp(request as ViewCampRequest);
            }
            else if (action == nameof(AddOrgniser))
            {
                return repository.AddOrgniser(request as AddOrgniserRequest);
            }
            else if (action == nameof(ViewOrgniser))
            {
                return repository.ViewOrgniser(request as ViewOrgniserRequest);
            }
            else if (action == nameof(AddDonor))
            {
                return repository.AddDonor(request as AddDonorRequest);
            }
            else if (action == nameof(ViewDonor))
            {
                return repository.ViewDonor(request as ViewDonorRequest);
            }
            else if (action == nameof(ViewDonorByCampCode))
            {
                return repository.ViewDonorByCampCode(request as ViewDonorByCampCodeRequest);
            }
            else if (action == nameof(ViewDonorById))
            {
                return repository.ViewDonorById(request as ViewDonorByIdRequest);
            }
            else if (action == nameof(UpdateDonor))
            {
                return repository.UpdateDonor(request as UpdateDonorRequest);
            }
            else if (action == nameof(RejectDonor))
            {
                return repository.RejectDonor(request as RejectDonorRequest);
            }
            else if (action == nameof(ViewPro))
            {
                return repository.ViewPro(request as ViewProRequest);
            }
            else if (action == nameof(ViewMedicalOfficer))
            {
                return repository.ViewMedicalOfficer(request as ViewMedicalOfficerRequest);
            }
            else if (action == nameof(AddPro))
            {
                return repository.AddPro(request as AddProRequest);
            }
            else if (action == nameof(AddMedicalOfficer))
            {
                return repository.AddMedicalOfficer(request as AddMedicalOfficerRequest);
            }
            else if (action == nameof(GetAllQuestionAnswer))
            {
                return repository.GetAllQuestionAnswer(request as GetAllQuestionAnswerRequest);
            }
            else if (action == nameof(GetDonorQuestionAnswer))
            {
                return repository.GetDonorQuestionAnswer(request as GetDonorQuestionAnswerRequest);
            }
            else if (action == nameof(VerifyDonorAnswer))
            {
                return repository.VerifyDonorAnswer(request as VerifyDonorAnswerRequest);
            }
            else if (action == nameof(GetAllBatchNo))
            {
                return repository.GetAllBatchNo(request as GetAllBatchNoRequest);
            }
            else if (action == nameof(GetDonorCertificate))
            {
                return repository.GetDonorCertificate(request as GetDonorCertificateRequest);
            }
            else if (action == nameof(DeleteCamp))
            {
                return repository.DeleteCamp(request as DeleteCampRequest);
            }
            else if (action == nameof(DeleteDonor))
            {
                return repository.DeleteDonor(request as DeleteDonorRequest);
            }
            else if (action == nameof(GetDonorInfo))
            {
                return repository.GetDonorInfo(request as GetDonorInfoRequest);
            }
            else if (action == nameof(Logout))
            {
                return this.repository.Logout(request as LogoutRequest);
            }

            throw new NotImplementedException();
        }
    }
}