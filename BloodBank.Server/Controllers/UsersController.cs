﻿using System;
using Microsoft.AspNetCore.Mvc;
using BloodBank.Server.Resources;
using BloodBank.Server.Messages;

using BloodBank.Server.Repositories;

using System.Net.Http;
using BloodBank.Server.Identity;
using System.Linq;
using System.Net.Http.Headers;
using Roice.Server.Controllers;

namespace BloodBank.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : BaseController
    {
        private UsersRepository repository;
        private BloodBankDBContext DBContext;

        public UsersController(UsersRepository repository, BloodBankDBContext dbContext)
        {
            this.repository = repository;
            this.DBContext = dbContext;
        }

        // POST Create New User - api/users
        [HttpPost]
        public IActionResult CreateUser(CreateUserRequest request)
        {
            return this.ProcessRequest<CreateUserResponse>(request);
        }

        [HttpPost("ChangePassword")]
        public IActionResult ChangePassword(ChangePasswordRequest request)
        {
            return this.ProcessRequest<ChangePasswordResponse>(request);
        }

        protected override void Disposing()
        {
            this.repository.Dispose();
        }

        protected override BaseResponse Execute(string action, BaseRequest request)
        {
            if (action == nameof(CreateUser))
            {
                return repository.CreateUser(request as CreateUserRequest);
            }
            if (action == nameof(ChangePassword))
            {
                return this.repository.ChangePassword(request as ChangePasswordRequest);
            }
            throw new NotImplementedException($"{action} not implemented");
        }
    }
}