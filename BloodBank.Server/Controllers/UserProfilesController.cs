﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BloodBank.Server.Messages;

using BloodBank.Server.Repositories;
using BloodBank.Server.Controllers;

namespace Roice.Server.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfilesController : BaseController
    {

        private UserProfilesRepository repository;

        public UserProfilesController(UserProfilesRepository repository)
        {
            this.repository = repository;
        }

       

        protected override void Disposing()
        {
            this.repository.Dispose();
        }

        protected override BaseResponse Execute(string action, BaseRequest request)
        {
            
           
            throw new NotImplementedException($"{action} not implemented");
        }
    }
}