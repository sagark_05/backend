﻿using System;
using Microsoft.AspNetCore.Mvc;
using BloodBank.Server.Messages;
using BloodBank.Server.Repositories;
using BloodBank.Server.Controllers;

namespace Roice.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : BaseController
    {
        private AuthRepository repository;

        public AuthController(AuthRepository repository)
        {
            this.repository = repository;
        }

       
        //[HttpGet("TestMethod")]
        //public IActionResult TestMethod()
        //{
        //    TestClassRequest request = new TestClassRequest();
        //    return this.ProcessRequest<TestClassResponse>(request);
        //}


        [HttpPost("token")]
        public IActionResult Login(LoginRequest request)
        {
            if (request.Username == "")
            {
                throw new ArgumentException("Username is required");
            }
            return this.ProcessRequest<LoginResponse>(request, nameof(Login));
        }

        [HttpGet("Test")]
        public IActionResult Test()
        {
            BaseRequest request = new BaseRequest();
            return this.ProcessRequest<BaseResponse>(request);
        }

        [HttpPost("GetVersionDetails")]
        public IActionResult GetVersionDetails(GetVersionDetailsRequest request)
        {
            return this.ProcessRequest<GetVersionDetailsResponse>(request);
        }

        [HttpPost("GetOTP")]
        public IActionResult GetOTP(GetOTPRequest request)
        {
            return this.ProcessRequest<GetOTPResponse>(request);
        }

        [HttpPost("VerifyOTP")]
        public IActionResult VerifyOTP(VerifyOTPRequest request)
        {
            return this.ProcessRequest<VerifyOTPResponse>(request);
        }



        protected override void Disposing()
        {
            this.repository.Dispose();
        }

        protected override BaseResponse Execute(string action, BaseRequest request)
        {
           if (action == nameof(Login))
           {
                return this.repository.Login(request as LoginRequest);
           }
           else if (action == nameof(Test))
           {
               return this.repository.Test(request as BaseRequest);
           }
           else if (action == nameof(GetVersionDetails))
           {
               return this.repository.GetVersionDetails(request as GetVersionDetailsRequest);
           }
            else if (action == nameof(GetOTP))
            {
                return this.repository.GetOTP(request as GetOTPRequest);
            }
            else if (action == nameof(VerifyOTP))
            {
                return this.repository.VerifyOTP(request as VerifyOTPRequest);
            }
            throw new NotImplementedException($"{action} not implemented");

        }
    }
}