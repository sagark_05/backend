﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class ViewDonorByIdRequest:BaseRequest
    {
        public int DonorId { get; set; }

        public string CampCode { get; set; }
    }
}
