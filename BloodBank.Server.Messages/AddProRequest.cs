﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class AddProRequest:BaseRequest
    {
        public string bloodbankId { get; set; }
        public string ProName { get; set; }
        public string ContactNo { get; set; }
    }
}
