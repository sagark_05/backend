﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class VerifyOTPResponse:BaseResponse
    {
        public string UserId { get; set; }
        public string Token { get; set; }

        public string Role { get; set; }
      //  public string UserId { get; set; }
      //  public string fullName { get; set; }
      //  public string Token { get; set; }
        public DateTime ValidTo { get; set; }
    }
}
