﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AddExpensesRequest : BaseRequest
    {
        public string bloodbankId { get; set; }
        public string expenseOn { get; set; }
        
        //public string expenseBy { get; set; }
        public int? amount { get; set; }
        public DateTime expenseDate { get; set; }
        public string image { get; set; }
    }
}
