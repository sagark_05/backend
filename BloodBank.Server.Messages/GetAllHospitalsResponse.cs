﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class GetAllHospitalsResponse : BaseResponse
    {
        public List<HospitalInfo> allHospitals;
        public GetAllHospitalsResponse()
        {
            this.allHospitals = new List<HospitalInfo>();
        }

    }
}
