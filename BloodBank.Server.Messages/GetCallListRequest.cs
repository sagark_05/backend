﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class GetCallListRequest : BaseRequest
    {
        public string bloodbankId { get; set; }
        public string status { get; set; }

        public int pageNumber { get; set; }


    }
}
