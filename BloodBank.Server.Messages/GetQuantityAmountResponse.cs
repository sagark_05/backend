﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class GetQuantityAmountResponse : BaseResponse
    {
       public int wb_Normal { get; set; }
       public int pcv_Normal { get; set; }
       public int ffp_Normal { get; set; } 
       public int rdp_Normal { get; set; } 
       public int sdp_Normal { get; set; }
       public int plasma_Normal { get; set; }
       public int wb_Nat { get; set; }
       public int pcv_Nat { get; set; } 
       public int ffp_Nat { get; set; } 
       public int rdp_Nat { get; set; }
       public int sdp_Nat { get; set; }
       public int plasma_Nat { get; set; }
    }
}
