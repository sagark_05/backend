﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class getCurrentStockDetailsResponse : BaseResponse
    {
        public string Status { get; set; }
        public List<StockDetailsDataList> stockDetailsDataLists { get; set; }

        public getCurrentStockDetailsResponse()
        {
            this.stockDetailsDataLists = new List<StockDetailsDataList>();
        }
    }
}
