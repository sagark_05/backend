﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class GetAllAttendanceListResponse:BaseResponse
    {

        public List<AttendanceList> attendanceLists { get; set; }

        public List<AbsentList> absentLists { get; set; }


        public GetAllAttendanceListResponse()
        {
            this.attendanceLists = new List<AttendanceList>();
            this.absentLists = new List<AbsentList>();
        }
    }
}
