﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class AttendanceList
    {
        public string name { get; set; }

        public string intime { get; set; }

        public string outtime { get; set; }

        public string workhrs { get; set; }

        public string imageurl { get; set; }
    }
}

