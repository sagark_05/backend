﻿using System;
using System.Net;

namespace BloodBank.Server.Messages
{
    public class BaseResponse
    {
        public string Message { get; set; }
        public bool Success { get; set; }
        public int ResponseCode { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
    }
}
