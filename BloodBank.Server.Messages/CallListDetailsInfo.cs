﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class CallListDetailsInfo
    {
        public int callDetailsId { get; set; }
        public bool isSuccess { get; set; }
        public string requisitionStatus { get; set; }
        public string hospitalName { get; set; }
        public string details { get; set; }
        public string callType { get; set; }
        public DateTime? callTime { get; set; }
        public bool isAccept { get; set; }
        public string status { get; set; }

        public string patientName { get; set; }
        public string serviceBoyName { get; set; }
        public string createdByUserId { get; set; }
        public string createdByUserName { get; set; } // <= AU Added on 01Jan2020
        public string assignedUserId { get; set; } // <= AU Added on 01Jan2020
        public string assignedUserName { get; set; } // <= AU Added on 01Jan2020
        public string patientStatus { get; set; } // <= AU Added on 01Jan2020
    }
}
