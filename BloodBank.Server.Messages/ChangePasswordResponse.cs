﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class ChangePasswordResponse : BaseResponse
    {
        public string newPassword { get; set; }
    }
}
