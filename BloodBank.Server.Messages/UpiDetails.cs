﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class UpiDetails
    {

        public string patientName { get; set; }
        public string hospitalName { get; set; }

        public int hospitalId { get; set; }
        public string type { get; set; }

        public int amount { get; set; }
        public string InvoiceNumber { get; set; }

        public DateTime? InvoiceDate { get; set; }   
        public string Billtype { get; set; }
        public string isCancel { get; set; }    //added by sk on 17Sep 2021

    }
}
