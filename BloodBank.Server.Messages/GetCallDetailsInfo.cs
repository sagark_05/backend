﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class GetCallDetailsInfo
    {
       
        public string patientName { get; set; }
        public string mobileNumber { get; set; }
        public int? Amount { get; set; }
        public string servicecharge { get; set; }
        public int discount { get; set; }
        public string btcharge { get; set; }
        public string hospitalName { get; set; }
        public string CallDetails { get; set; }
        public string CallType { get; set; }
        public List<Quantity> quantityRequired { get; set; }
        public string IsNat { get; set; }

        public string patientBloodGroup { get; set; } // AU Added on 22JAN2021
        public string patientGender { get; set; } // AU Added on 22JAN2021
        public string patientAge { get; set; } // AU Added on 22JAN2021
        public int callDetailsId { get; set; } // AU Added on 22JAN2021
        public GetCallDetailsInfo()
        {
            this.quantityRequired = new List<Quantity>();
        }


    }
}
