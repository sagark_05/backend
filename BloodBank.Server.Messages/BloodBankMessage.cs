﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class BloodBankMessage
    {
        public virtual void PopulateAdditionalDetails(Dictionary<string, object> details)
        {
            // nothing to add for now
        }
    }
}
