﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class MedicalOfficerList
    {
        public int MedicalOfficerId { get; set; }
        public string MedicalOfficerName { get; set; }
    }
}
