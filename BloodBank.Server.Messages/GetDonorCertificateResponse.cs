﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class GetDonorCertificateResponse:BaseResponse
    {
        public GetDonorCertificateResponse()
        {
            donorCertificateLists = new List<DonorCertificateList>();
        }

        public List<DonorCertificateList> donorCertificateLists;
    }
}
