﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class ViewOrgniserResponse:BaseResponse
    {

        public ViewOrgniserResponse()
        {
            orgniserLists = new List<OrgniserList>();
        }

        public List<OrgniserList> orgniserLists;
    }
}
