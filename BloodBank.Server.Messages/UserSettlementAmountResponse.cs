﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class UserSettlementAmountResponse : BaseResponse
    {
        public int petrolAmount { get; set; }
        public int collectionAmount { get; set; }
        public int balanceAmount { get; set; }
    }
}
