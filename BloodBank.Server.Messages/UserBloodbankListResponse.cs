﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class UserBloodbankListResponse:BaseResponse
    {
       
        public List<UserBloodbankListInfo> userBloodbankListInfos { get; set; }

        public UserBloodbankListResponse()
        {
            this.userBloodbankListInfos = new List<UserBloodbankListInfo>();
        }

    }
}
