﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class ServiceBoyCallDetailsDataList 
    {
        public string ServiceBoyName { get; set; }
      //  public bool IsFreeNow { get; set; }
        public string CallLocation { get; set; }
        // public DateTime CallTime { get; set; }

        public string CallStatus { get; set; }
        public string CallTime { get; set; }

        public string PatientName { get; set; }
    }
}
