﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AddCallSettlementRequest : BaseRequest
    {
        public string bloodbankId { get; set; }
        public int callId { get; set; }
        public decimal? km { get; set; }
        public string imageUrl { get; set; }
       
    }
}
