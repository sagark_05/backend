﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class ChangePasswordRequest : BaseRequest
    {
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
        public string cofirmPassword { get; set; }
        public string userName { get; set; }
    }
}
