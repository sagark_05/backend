﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class ViewCampResponse:BaseResponse
    {

        public int totalCount { get; set; }
        public ViewCampResponse()
        {

            campLists = new List<CampList>();
        }

        public List<CampList> campLists;

    }
}
