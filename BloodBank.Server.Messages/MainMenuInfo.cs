﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public  class MainMenuInfo
    {
        public int mainMenuId { get; set; }

        public string  mainMenuName { get; set; }

        public List<SubMenuInfo> subMenuInfos { get; set; }

        public MainMenuInfo()
        {
            this.subMenuInfos = new List<SubMenuInfo>();
        }
    }
}
