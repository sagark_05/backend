﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class GetServiceBoysWithCallDetailsResponse : BaseResponse
    {
        public string Status { get; set; }
        public List<ServiceBoyCallDetailsDataList> serviceBoyCallDetailsDataLists { get; set; }

        public List<FreeServiceBoyCallDetailsDataList> freeServiceBoyCallDetailsDataLists { get; set; }

        public List<NotAssignServiceBoyCallDetailsDataList> notAssignServiceBoyCallDetailsDataLists { get; set; }
        public GetServiceBoysWithCallDetailsResponse()
        {
            this.serviceBoyCallDetailsDataLists = new List<ServiceBoyCallDetailsDataList>();
            this.freeServiceBoyCallDetailsDataLists = new List<FreeServiceBoyCallDetailsDataList>();
            this.notAssignServiceBoyCallDetailsDataLists = new List<NotAssignServiceBoyCallDetailsDataList>();
        }

    }
}
