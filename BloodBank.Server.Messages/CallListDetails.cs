﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class CallListDetails
    {
       public int callId { get; set; }
        public List<CallListDetailsInfo> callListDetailsInfos { get; set; }
        public CallListDetails() 
        {
            this.callListDetailsInfos = new List<CallListDetailsInfo>();
        }

    }
}
