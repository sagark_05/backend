﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class CreateUserResponse : BaseResponse
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
