﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class CallSettlementInfo
    {
        public int callId { get; set; }
      
        public int petrolAmount { get; set; }
        public string serviceBoyUserId { get; set; }
   
        public string serviceBoyName { get; set; }

        public List<CallCollectionInfo> callcollectioninfo { get; set; }
        public CallSettlementInfo()
        {
            this.callcollectioninfo = new List<CallCollectionInfo>();
        }

    }
}
