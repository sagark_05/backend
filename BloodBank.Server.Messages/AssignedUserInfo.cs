﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AssignedUserInfo
    {
        public string assignUserId { get; set; }
        public string assignUserName {get; set;}
    }
}
