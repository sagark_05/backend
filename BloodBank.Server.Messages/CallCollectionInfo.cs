﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class CallCollectionInfo
    {
        public string hospitalName { get; set; }
        public int collectedAmount { get; set; }

        public string patientName { get; set; }
        public int callDetailsId { get; set; }
    }
}
