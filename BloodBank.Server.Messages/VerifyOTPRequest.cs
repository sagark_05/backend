﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class VerifyOTPRequest:BaseRequest
    {
        public string OTP { get; set; }

        public string MobileNumber { get; set; }
    }
}
