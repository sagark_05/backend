﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class AddCampRequest:BaseRequest
    {
        public string bloodbankId { get; set; }
        public string CampName { get; set; }
        public string Address { get; set; }
        public DateTime? CampDate { get; set; }
        public DateTime? TimeFrom { get; set; }
        public DateTime? TimeTo { get; set; }
        public int OrgniserId { get; set; }
        public int ProId { get; set; }
        public int MedicalOfficerId { get; set; }
        public int? NoOfDonors { get; set; }

        public string isSpecialCertificate { get; set; }


    }
}
