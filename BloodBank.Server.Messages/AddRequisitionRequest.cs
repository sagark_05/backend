﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AddRequisitionRequest : BaseRequest
    {
        public string bloodbankId { get; set; }
        public string patientName { get; set; }
        public string mobileNumber { get; set; }
        public string details { get; set; }
        public string hospital { get; set; }
        public List<AddRequisitionQuantity> quantityRequired { get; set; }
        public string callType { get; set; }
        public int? Amount { get; set; }
        public string paymentType { get; set; }
       // public List<BillImageInfo> billImages { get; set; }
       public string BillImages { get; set; }

        public string otherImage { get; set; }
        public int callId { get; set; }
        public int callDetailsId { get; set; }
        public string isNat { get; set; }


        public string bloodroup { get; set; } // AU Added this parameter 22JAN2021
        public string gender { get; set; } // AU Added this parameter 22JAN2021
        public string age { get; set; } // AU Added this parameter 22JAN2021

        public int? serviceCharge { get; set; } //sk added this parameter 27aug 2021
        public string upiId { get; set; } //sk adddd this parameter 26Aug2021
        public AddRequisitionRequest()
        {
            this.quantityRequired = new List<AddRequisitionQuantity>();
        //    this.billImages = new List<BillImageInfo>();
        }
    }
}
