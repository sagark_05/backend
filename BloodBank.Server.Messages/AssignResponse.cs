﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AssignResponse : BaseResponse
    {
        public string status { get; set; }
    }
}
