﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class AddCampResponse:BaseResponse
    {
        public string CampCode { get; set; }
    }
}
