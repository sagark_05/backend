﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class StocksListbyProductType
    {
        public bool isSuccess { get; set; }
        public string bloodGroup { get; set; }
        public string bagNumber { get; set; }
        public int productID { get; set; }
        //public DateTime expiryDate { get; set; }
         public string expiryDate { get; set; }

      // public DateTime? expiryDate { get; set; }

      //  public int collectionDate { get; set; }
          public string collectionDate { get; set; }
     //  public DateTime? collectionDate { get; set; }

        public string discardReason { get; set; }

        public string collectedBy { get; set; }
    }
}
