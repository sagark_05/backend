﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class FilteredStockRequest : BaseRequest
    {
        public string bloodbankId { get; set; }
        public string bloodGroup { get; set; }
        public string productType { get; set; }
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
        public string bagNumber { get; set; }
        public int pageNumber { get; set; }
    }
}
