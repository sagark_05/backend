﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class ProductRateList
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }
        public int? Rate { get; set; }
        public int? NatRate { get; set; }
        public int? CrossMatchRate { get; set; }
        public int? CrossMatchNatRate { get; set; }

    }
}
