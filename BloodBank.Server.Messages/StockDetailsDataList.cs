﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class StockDetailsDataList
    {
        public string StockType { get; set; }
        public int OPositiveStock { get; set; }
        public int ONegativeStock { get; set; }
        public int APositiveStock { get; set; }
        public int ANegativeStock { get; set; }
        public int ABPositiveStock { get; set; }
        public int ABNegativeStock { get; set; }
        public int BPositiveStock { get; set; }
        public int BNegativeStock { get; set; }
        public int Total { get; set; }
   
    }
}
