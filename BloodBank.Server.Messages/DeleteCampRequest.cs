﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class DeleteCampRequest:BaseRequest
    {
        public int campId { get; set; }
    }
}
