﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class BatchNoList
    {
        public int batchId { get; set; }
        public string batchNo { get; set; }

        public string type { get; set; }

        public int totalQuantity { get; set; }

        public int remaininigQuantity { get; set; }

        public string solution { get; set; }

        public int bagWeight { get; set; }

        public string manufacturingDate { get; set; }

        public string expiryDate { get; set; }

        public string companyName { get; set; }
    }
}
