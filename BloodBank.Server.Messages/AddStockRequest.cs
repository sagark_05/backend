﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AddStockRequest : BaseRequest
    {
        //public string[] component { get; set; }
        public string barcodeNumber { get; set; }

        public string bloodbankId { get; set; }

        public string segment { get; set; }
        public bool isBagDiscard { get; set; }
        public string discardBag { get; set; }
        public string ifOtherRemark { get; set; }
        public DateTime collectionDate { get; set; }

        public string solution { get; set; }
        public List<componantDetails> componant { get; set; }
        public AddStockRequest()
        {
            this.componant = new List<componantDetails>();
        }
    }
}
