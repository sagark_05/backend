﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class GetAttendanceStatusResponse : BaseResponse
    {
        public string attendanceStatus { get; set; } 
        public DateTime? inTime { get; set; }
        public DateTime? outTime { get; set; }
    }
}
