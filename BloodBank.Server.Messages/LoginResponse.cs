﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class LoginResponse : BaseResponse
    {
        public string Id { get; set; }
        public string Token { get; set; }
        public DateTime ValidTo { get; set; }
        public string roleName {get; set;}

    }
}
