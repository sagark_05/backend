﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class GetAllQuestionAnswerResponse:BaseResponse
    {
        public GetAllQuestionAnswerResponse()
        {
            questionAnswerLists = new List<QuestionAnswerList>();
        }

        public List<QuestionAnswerList> questionAnswerLists;
    }
}
