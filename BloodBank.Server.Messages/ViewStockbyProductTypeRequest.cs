﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class ViewStockbyProductTypeRequest : BaseRequest
    {
        public string bloodbankId { get; set; }
        public int productId { get; set; }
        public string bloodGroup { get; set; }
        // public string productType { get; set; }


        public DateTime? date  { get; set; }
        public string issueFlag { get; set; }

        public string sortType { get; set; }

        

    }
}
