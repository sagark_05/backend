﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AccountsRequest : BaseRequest
    {
        // Filter options
        //  public DateTime? fromDate { get; set; }
        // public DateTime? toDate { get; set; }
        public string bloodbankId { get; set; }

        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }

       
        public int? hospitalId { get; set; }

        public int pageNumber { get; set; }


           public string type { get; set; }

      //  public string fromTime { get; set; }

      //  public string toTime { get; set; }

        public DateTime? fromTime { get; set; }

        public DateTime? toTime { get; set; }

        public string Billtype { get; set; }       //added by sk on 7 may 2021

        


        //  public string hospitalName { get; set; }
        // public string hospitalName { get; set; }
        //   public string type { get; set; }
    }
}
