﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public  class StorageDetails
    {
        public string patientName { get; set; }
        public string hospitalName { get; set; }

        public int hospitalId { get; set; }
        public string type { get; set; }

        public int amount { get; set; }

        public string InvoiceNumber { get; set; }   //added by sk on 7 may 2021

        public DateTime? InvoiceDate { get; set; }   //added by sk on 7 may 2021
        public string Billtype { get; set; }        //added by sk on 7 may 2021

        public string isCancel { get; set; }    //added by sk on 17Sep 2021

    }
}
