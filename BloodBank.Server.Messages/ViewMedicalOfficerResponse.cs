﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class ViewMedicalOfficerResponse:BaseResponse
    {
        public ViewMedicalOfficerResponse()
        {
            medicalOfficerLists = new List<MedicalOfficerList>();
        }

        public List<MedicalOfficerList> medicalOfficerLists;
    }
}
