﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class SubMenuInfo
    {
        public int subMenuId { get; set; }
        public string subMenuName { get; set; }
    }
}
