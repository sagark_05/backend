﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class MarkAttendanceRequest : BaseRequest
    {
        public string bloodbankId { get; set; }
        public string image { get; set; }
    }
}
