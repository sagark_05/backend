﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class GetUserMenuListResponse:BaseResponse
    {

        public List<MainMenuInfo> mainMenuInfos { get; set; }

        public GetUserMenuListResponse()
        {
            this.mainMenuInfos = new List<MainMenuInfo>();
        }
    }
}
