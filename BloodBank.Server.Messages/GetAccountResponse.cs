﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class GetAccountResponse : BaseResponse
    {
        public float totalBalance { get; set; }
        public float income { get; set; }
        public float expenses { get; set; }
        public List<Transaction> recentTransaction { get; set; }

        public GetAccountResponse()
        {
            this.recentTransaction = new List<Transaction>();
        }

    }
}
