﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class DeleteDonorRequest:BaseRequest
    {
        public string campCode { get; set; }

        public int donorId { get; set; }
    }
}
