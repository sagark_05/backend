﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class QuestionAnswerList
    {
        public int QueId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
