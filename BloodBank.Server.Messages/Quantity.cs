﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class Quantity
    {
        public int wb { get; set; }
        public int pcv { get; set; }
        public int ffp { get; set; }
        public int rdp { get; set; }
        public int sdp { get; set; }
        public int plasma { get; set; }

    }
}
