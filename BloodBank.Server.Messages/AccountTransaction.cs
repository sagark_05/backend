﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AccountTransaction
    {
        public string patientName { get; set; }
        public string hospitalName { get; set; }
        public string type { get; set; }
        public string expenseOn { get; set; }
        public int amount { get; set; }
        public DateTime date { get; set; }

    }
}
