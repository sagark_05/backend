﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class RejectDonorRequest:BaseRequest
    {
        public string bloodbankId { get; set; }
        public int DonorId { get; set; }

        public string CampCode { get; set; }
    }
}
