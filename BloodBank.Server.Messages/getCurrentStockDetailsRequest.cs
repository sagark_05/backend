﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class getCurrentStockDetailsRequest : BaseRequest
    {
       public string bloodbankId { get; set; }
    }
}
