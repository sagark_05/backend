﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class MainStockList
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int ProductCount { get; set; }
    }
}
