﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class GetAllAssignedUserResponse : BaseResponse
    {
        public List<AssignedUserInfo> assignedUserInfos;
        public GetAllAssignedUserResponse()
        {
            this.assignedUserInfos = new List<AssignedUserInfo>();
        }
    }
}
