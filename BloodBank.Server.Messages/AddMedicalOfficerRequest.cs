﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class AddMedicalOfficerRequest:BaseRequest
    {
        public string bloodbankId { get; set; }
        public string MedicalOfficerName { get; set; }
        public string ContactNo { get; set; }
        public string Sign { get; set; }
    }
}
