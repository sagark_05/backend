﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class VerifyDonorAnswerRequest:BaseRequest
    {
        public int DonorId { get; set; }
        public string CampCode { get; set; }
        public List<QuestionsDetails> questions { get; set; }

        public VerifyDonorAnswerRequest()
        {
            this.questions = new List<QuestionsDetails>();
        }
    }
}
