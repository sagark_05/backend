﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class GetAllAssignedUserRequest : BaseRequest
    {
        public string bloodbankId { get; set; }
    }
}
