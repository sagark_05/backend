﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AddTestingRequest : BaseRequest
    {
        public string bloodbankId { get; set; }
        public string barcodeNumber { get; set; }
        public string bloodGroup { get; set; }
        public bool isdiscardBag { get; set; }
        public string discardBag { get; set; }
        public string otherRemark { get; set; }

    }
}
