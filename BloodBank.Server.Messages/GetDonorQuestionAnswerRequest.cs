﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class GetDonorQuestionAnswerRequest:BaseRequest
    {
        public int DonorId { get; set; }

        public string CampCode { get; set; }

    }
}
