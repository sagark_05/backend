﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AssignRequest : BaseRequest
    {
        public string bloodbankId { get; set; }
        public int callID { get; set; }
        public string status { get; set; }
    }
}
