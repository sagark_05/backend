﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class HospitalInfo
    {
        public int hospitalId { get; set; }
        public string hospitalName { get; set; }
    }
}
