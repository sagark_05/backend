﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class CalculateCallSettlementResponse : BaseResponse
    {
        public int collectedAmount { get; set; }
        public int petrolAmount { get; set; }
        public int balance { get; set; }
        public List<CallSettlementInfo> callListDetailsInfos { get; set; }
        public CalculateCallSettlementResponse()
        {
            this.callListDetailsInfos = new List<CallSettlementInfo>();
        }
    }
}
