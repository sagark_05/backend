﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class ExpenseDetails
    {
        public int Amount { get; set; }

      //  public string hospitalName { get; set; }
        public string type { get; set; }
        public string ExpenseOn { get; set; }
        public string ExpenseBy { get; set; }

        public DateTime? ExpenseDate { get; set; }

    }
}
