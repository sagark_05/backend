﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class ViewDonorByCampCodeRequest:BaseRequest
    {
       // public string bloodbankId { get; set; }
        public string CampCode { get; set; }
    }
}
