﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class GetVersionDetailsRequest : BaseRequest
    {
        public string DeviceType { get; set; }

        public string Apptype { get; set; }
    }
}
