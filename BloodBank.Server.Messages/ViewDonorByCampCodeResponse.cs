﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class ViewDonorByCampCodeResponse:BaseResponse
    {
        public ViewDonorByCampCodeResponse()
        {
            donorLists = new List<DonarListByCampCode>();
        }

        public List<DonarListByCampCode> donorLists;
    }
}
