﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class FilteredStockResponse : BaseResponse
    {
        public List<Stock> stockCount { get; set; }
        public FilteredStockResponse()
        {
            this.stockCount = new List<Stock>();
        }
    }
}
