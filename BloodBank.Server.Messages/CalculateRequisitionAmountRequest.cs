﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class CalculateRequisitionAmountRequest : BaseRequest
    {
        public List<ClaculateRequisitionByProduct> claculateRequisitionByProducts { get; set; }
        public string isNat { get; set; }
        public string bloodbankId { get; set; }

        public CalculateRequisitionAmountRequest()
        {
            this.claculateRequisitionByProducts = new List<ClaculateRequisitionByProduct>();
        }
    }
}
