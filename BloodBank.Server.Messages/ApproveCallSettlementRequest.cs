﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class ApproveCallSettlementRequest : BaseRequest
    {
        public string bloodbankId { get; set; }
        public int CallId { get; set; }
    }
}
