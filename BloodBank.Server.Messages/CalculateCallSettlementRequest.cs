﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class CalculateCallSettlementRequest : BaseRequest
    {
        public string serviceBoyName { get; set; }

        public string bloodbankId { get; set; }
    }
}
