﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class GetDonorInfoResponse:BaseResponse
    {
        
        public string donorName { get; set; }
        public string address { get; set; }
        public string contact { get; set; }
        
        public string email { get; set; }
     
        public string gender { get; set; }
        public int? weight { get; set; }

        public int? age { get; set; }
        public string height { get; set; }
        public string bloodGroup { get; set; }
        public string bloodPressure { get; set; }

        public string donationDate { get; set; }

        public string dob { get; set; }
        public string donorSign { get; set; }

    }
}
