﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class GetProductRateResponse : BaseResponse
    {
        public List<ProductRateList> productRateLists { get; set; }

        public GetProductRateResponse()
        {
            this.productRateLists = new List<ProductRateList>();
        }
    }
}
