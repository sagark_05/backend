﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class GetApproveKilometerListResponse : BaseResponse
    {
       public List<ApproveKilometerInfo> approveKilometerList { get; set; }
       public GetApproveKilometerListResponse()
       {
            this.approveKilometerList = new List<ApproveKilometerInfo>();
       }
    }
}
