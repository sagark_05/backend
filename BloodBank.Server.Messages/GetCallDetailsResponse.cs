﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class GetCallDetailsResponse : BaseResponse
    {
       
        public List<GetCallDetailsInfo> getCallDetailsInfos { get; set; }
        public GetCallDetailsResponse()
        {
            this.getCallDetailsInfos = new List<GetCallDetailsInfo>();
        }

    }
}
