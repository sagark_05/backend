﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class CampList
    {
        public int CampId { get; set; }
        public string CampName { get; set; }
        public string Address { get; set; }
        public string CampDate { get; set; }

        public DateTime dateTime { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public string CampCode { get; set; }
        public string CampStatus { get; set; }
        public int? NoOfDonors { get; set; }
        public string MedicalOfficerName { get; set; }
        public string OrgniserName { get; set; }
        public string ProName { get; set; }
    }
}
