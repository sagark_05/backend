﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class NotAssignServiceBoyCallDetailsDataList
    {
        public string ServiceBoyName { get; set; }
        public string CallLocation { get; set; }
        public string CallStatus { get; set; }
        public string CallTime { get; set; }
        public string PatientName { get; set; }
    }
}
