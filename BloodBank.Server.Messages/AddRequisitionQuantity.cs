﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AddRequisitionQuantity
    {
        //public int wb { get; set; }
        //public int pcv { get; set; }
        //public int ffp { get; set; }
        //public int rdp { get; set; }
        //public int sdp { get; set; }
        //public int plasma { get; set; }
        public string productName { get; set; }
        public int productCount { get; set; }
    }
}
