﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class UserBloodbankListInfo
    {
        public string bloodbankId { get; set; }
        public string bloodbankName { get; set; }

        public string bloodbankLogo { get; set; }

    }
}
