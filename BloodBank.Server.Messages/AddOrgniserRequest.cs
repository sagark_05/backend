﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class AddOrgniserRequest:BaseRequest
    {
        public string bloodbankId { get; set; }
        public string Name { get; set; }
        public string ContactNo { get; set; }
        public string EmailId { get; set; }
        public string companyName { get; set; }
        public string subtitle { get; set; }
        public string logo { get; set; }
        public string description { get; set; }
    }
}
