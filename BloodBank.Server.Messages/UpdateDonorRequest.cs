﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class UpdateDonorRequest:BaseRequest
    {
        public string bloodbankId { get; set; }
        public int DonorId { get; set; }
        public string DonorName { get; set; }
        public string CampCode { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public DateTime? DonationDate { get; set; }
        public string Email { get; set; }
        public DateTime? Dob { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public int Weight { get; set; }
        public string Height { get; set; }
        public string BloodGroup { get; set; }
        public string BloodPressure { get; set; }
        public string BagNo { get; set; }
        public string Segment { get; set; }
        public string Sign { get; set; }

        public int? batchId { get; set; }
    }
}
