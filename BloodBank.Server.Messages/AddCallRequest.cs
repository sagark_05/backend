﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AddCallRequest : BaseRequest
    {
         public string bloodbankId { get; set; }
        public int callID { get; set; }
        public string patientName { get; set; }
        public string mobileNumber { get; set; }
        public string details { get; set; }
        public int hospitalId { get; set; }
        public string callType { get; set; }
        public string serviceBoyName { get; set; }
        public string address { get; set; }
        public string assignedUserId { get; set; }  

    }
}
