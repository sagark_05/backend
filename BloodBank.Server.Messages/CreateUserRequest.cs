﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class CreateUserRequest : BaseRequest
    {
        public string EmailId { get; set; }

        //[Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        public string Password { get; set; }
        public string MobileNo { get; set; }        
        public string ProfilePhoto { get; set; }

        public string LoginType { get; set; }   //added by sk on 5/10/2021
        public string SignupType { get; set; }

    }
}
