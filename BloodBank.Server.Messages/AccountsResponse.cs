﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class AccountsResponse : BaseResponse
    {
        public int totalCash { get; set; }
        
        public int totalCredit { get; set; }

        public int totalUpi { get; set; }

        public int? totalExpense { get; set; }

        public int? totalStorage { get; set; }

        public string type { get; set; }
        //public int balance { get; set; }

        //    public List<AccountTransaction> transactions { get; set; }

        public int totalCount { get; set; }
        public List<CashDetails> cashDetails;

        public List<ExpenseDetails> expenseDetails;

        public List<CreaditDetails> creaditDetails;

        public List<UpiDetails> upiDetails;

        public List<StorageDetails> storageDetails;
        public AccountsResponse()
        {
           // this.transactions = new List<AccountTransaction>();
            cashDetails = new List<CashDetails>();
            creaditDetails = new List<CreaditDetails>();
            expenseDetails = new List<ExpenseDetails>();
            upiDetails = new List<UpiDetails>();
            storageDetails = new List<StorageDetails>();

        }


    }
}
