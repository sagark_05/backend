﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class GetAllBatchNoResponse:BaseResponse
    {
        public GetAllBatchNoResponse()
        {
            batchNoLists= new List<BatchNoList>();
        }

        public List<BatchNoList> batchNoLists;
    }
}
