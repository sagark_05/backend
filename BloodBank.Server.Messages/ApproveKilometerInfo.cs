﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class ApproveKilometerInfo
    {
        public int? callId { get; set; }
        public string hospitalName { get; set; }
        public string assignedUserId { get; set; }
        public string assignedUserName { get; set; }
        public DateTime callDateTime { get; set; }
        public string kmImage { get; set; }
        public decimal? KM { get; set; }
    }
}
