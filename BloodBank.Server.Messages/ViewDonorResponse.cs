﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class ViewDonorResponse:BaseResponse
    {
        public ViewDonorResponse()
        {
            donorLists = new List<DonorList>();
        }

        public List<DonorList> donorLists;
    }
}
