﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class ViewCampRequest:BaseRequest
    {

        public string bloodbankId { get; set; }
        public int pageNumber { get; set; }

        public string type { get; set; }

        public string campCode { get; set; }
    }
}
