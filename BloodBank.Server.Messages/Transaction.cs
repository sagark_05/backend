﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class Transaction
    {
        public string customerName { get; set; }
        public string customerProfileImage { get; set; }
        public string detail { get; set; }
        public string amount { get; set; }
        public DateTime date { get; set; }
    }
}
