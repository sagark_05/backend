﻿using BloodBank.Server.Entities;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class RequestContext
    {
        public CustomerInfo Customer { get; }
        public ClientApplication ClientApplication { get; }
        // Check if this is required
        public IPrincipal User { get; }

        // is there a better way to set these values than such bloated constructor? :)
        // basically we don't want to all setter. else, client objects can change this
        public RequestContext(BaseRequest request,
            CustomerInfo Customer,
            ClientApplication ClientApplication,
            IPrincipal User)
        {
            this.Customer = Customer;
            this.ClientApplication = ClientApplication;
            this.User = User;
        }
    }

}
