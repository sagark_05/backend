﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class ViewStockResponse : BaseResponse
    { 
        

        public List<MainStockList> mainStockLists { get; set; }
        public ViewStockResponse()
        {
            this.mainStockLists = new List<MainStockList>();
        }

    }
}
