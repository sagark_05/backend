﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class ViewProResponse:BaseResponse
    {
        public ViewProResponse()
        {
            proLists = new List<ProList>();
        }

        public List<ProList> proLists;
    }
}
