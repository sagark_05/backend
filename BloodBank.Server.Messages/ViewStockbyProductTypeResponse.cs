﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
   public class ViewStockbyProductTypeResponse : BaseResponse
    {
        public List<StocksListbyProductType> stocksListbyProductTypes { get; set; }
        public ViewStockbyProductTypeResponse()
        {
            this.stocksListbyProductTypes = new List<StocksListbyProductType>();
        }
    }
}
