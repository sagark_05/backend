﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class GetCallListResponse : BaseResponse
    {
        public List<CallListDetails> callList { get; set; }

        public GetCallListResponse()
        {
            this.callList = new List<CallListDetails>();
        }
        public int totalCount { get; set; }
    }
}
