﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class FacebookLoginRequest : BaseRequest
    {
        public string emailId { get; set; }
    }
}
