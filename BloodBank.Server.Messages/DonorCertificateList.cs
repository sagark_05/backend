﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
  public class DonorCertificateList
    {

        public string donorName { get; set; }

        public string campName { get; set; }

        public string bloodbankName { get; set; }

        public string bloodbankAddress { get; set; }

        public string bloodbankPhonenumber { get; set; }
        public string bloodbanklicenceno { get; set; }
        public string bloodbankLogo { get; set; }

        public string bloodGroup { get; set; }
        public string donationDate { get; set; }
        public string companyName { get; set; }
        public string subtitle { get; set; }
        public string description { get; set; }
        public string logo { get; set; }
        public string medicalOfficerSign { get; set; }
        public string medicalOfficerName { get; set; }

        public string bagNo { get; set; }
        public string couponCode { get; set; }

        public string isSpecialCetificate { get; set; }

        public string expiryDate { get; set; }


    }
}
