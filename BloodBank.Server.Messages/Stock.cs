﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class Stock
    {

        public bool isSuccess { get; set; }
        public string bloodGroup { get; set; }
        public string bagNumber { get; set; }
        public int productID { get; set; }
        //public DateTime expiryDate { get; set; }
        public string expiryDate { get; set; }
        public int collectionDate { get; set; }

        public int PCV { get; set; }
        public int FFP { get; set; }
        public int RDP { get; set; }
        public int WB { get; set; }
        public int SDP { get; set; }
        public int Plasma { get; set; }

    }
}
