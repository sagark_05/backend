﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
 public class GetDonorQuestionAnswerResponse:BaseResponse
    {
        public GetDonorQuestionAnswerResponse()
        {
            questionAnswerLists = new List<QuestionAnswerList>();
        }

        public List<QuestionAnswerList> questionAnswerLists;
    }
}
