﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages.Donor
{
 public class BloodOrderList
    {
        public int requestId { get; set; }
        public string contactPersonName { get; set; }
        public string contactPersonMobileNumber { get; set; }
        public string patientName { get; set; }
        public string hospitalName { get; set; }
        public string bloodGroup { get; set; }
        public string productName { get; set; }

        public string isavilableinterest { get; set; }
    }
}
