﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages.Donor
{
  public class GetAllBloodOrderResponse:BaseResponse
    {

        public GetAllBloodOrderResponse()
        {
            bloodOrderLists = new List<BloodOrderList>();
        }

        public List<BloodOrderList> bloodOrderLists;
    }
}
