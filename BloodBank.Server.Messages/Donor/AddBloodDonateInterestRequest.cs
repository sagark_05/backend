﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages.Donor
{
  public class AddBloodDonateInterestRequest:BaseRequest
    {
        public int requestId { get; set; }
        public string userId { get; set; }
    }
}
