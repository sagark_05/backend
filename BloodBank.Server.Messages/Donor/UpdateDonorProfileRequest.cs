﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages.Donor
{
   public class UpdateDonorProfileRequest:BaseRequest
    {
        public string name { get; set; }
       // public string CampCode { get; set; }
        public string address { get; set; }

        public string contactNo { get; set; }

        public string email { get; set; }

        public DateTime? dob { get; set; }

        public int age { get; set; }

        public string gender { get; set; }

        public int weight { get; set; }

        public string height { get; set; }

        public string bloodGroup { get; set; }

        public string sign { get; set; }
    }
}
