﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Messages
{
    public class CalculateRequisitionAmountResponse : BaseResponse
    {
        public int amount { get; set; }
    }
}
