﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
  public partial  class Mainmenu
    {
        public int MainMenuId { get; set; }
        public string MainMenuName { get; set; }
        public string PlatformType { get; set; }
    }
}
