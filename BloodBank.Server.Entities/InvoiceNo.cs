﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class InvoiceNo
    {
        public int InvoiceNoId { get; set; }
        public string InvoiceNumber { get; set; }
        public string Type { get; set; }
        public int? TypeId { get; set; }
        public int? IsCancelled { get; set; }
        public DateTime? InvoiceDate { get; set; }

        public string FkBloodbankId { get; set; }
    }
}
