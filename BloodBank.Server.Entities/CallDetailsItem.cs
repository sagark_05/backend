﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class CallDetailsItem
    {
        public int CallDetailsItemId { get; set; }
        public int? CallDetailsId { get; set; }
        public int? ProductId { get; set; }
        public int? Quantity { get; set; }
    }
}
