﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class CallSettlement
    {
        public int CallSettlementId { get; set; }
        public int? FkCallId { get; set; }
        public decimal? Km { get; set; }
        public string ImageUrl { get; set; }
        public string IsKmapproved { get; set; }
        public string ApprovedById { get; set; }
        public DateTime? DateTime { get; set; }
        public int? Amount { get; set; }
        public string SettlementStatus { get; set; }
        public string FkBloodbankId { get; set; }
        public string CollectionSettlementStatus { get; set; }
    }
}
