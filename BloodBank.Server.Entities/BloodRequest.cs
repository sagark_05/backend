﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class BloodRequest
    {
        public int RequestId { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonMobileNumber { get; set; }
        public string PatientName { get; set; }
        public string HospitalName { get; set; }
        public string BloodGroup { get; set; }
        public string ProductName { get; set; }
        public string Status { get; set; }

        public DateTime? RequestDate { get; set; }
    }
}
