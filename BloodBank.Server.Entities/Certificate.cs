﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class Certificate
    {
        public int CertificateId { get; set; }
        public int? FkCampId { get; set; }
        public int? FkDonorId { get; set; }
        public string CouponCode { get; set; }
        public DateTime? AllocatedDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string IsActive { get; set; }

        public DateTime? CertificateDate { get; set; }

        public string FkBloodbankId { get; set; }
    }
}
