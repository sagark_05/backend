﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class StockDetails
    {
        public int StockDetailsId { get; set; }
        public string BagNo { get; set; }
        public int? ProductId { get; set; }
        public string BloodGroup { get; set; }
        public DateTime? CollectionDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string TestResult { get; set; }
        public string DiscardReason { get; set; }
        public DateTime? TestDate { get; set; }
        public string Remark { get; set; }
        public string IssueFlag { get; set; }
        public string Volume { get; set; }
        public string CollectedBy { get; set; }
        public string TestedBy { get; set; }
        public string Solution { get; set; }
        public string Segment { get; set; }

        public string FkBloodbankId { get; set; }
    }
}
