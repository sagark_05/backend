﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class RequisitionPaymentAttachment
    {
        public int RequisitionPaymentAttachmentId { get; set; }
        public int? FkRequisitionId { get; set; }
        public string ImageUrl { get; set; }
    }
}
