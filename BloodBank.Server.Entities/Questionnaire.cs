﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class Questionnaire
    {
        public int QueId { get; set; }
        public string Question { get; set; }
        public string Ans { get; set; }

        public string FkBloodbankId { get; set; }
    }
}
