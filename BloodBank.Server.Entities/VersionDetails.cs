﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class VersionDetails
    {
        public int VersionId { get; set; }
        public string VersionName { get; set; }
        public string VersionNumber { get; set; }
        public string DeviceType { get; set; }
        public string ForceUpdateVerName { get; set; }
        public string ForceUpdateVerNumber { get; set; }

        public string AppType { get; set; }
    }
}
