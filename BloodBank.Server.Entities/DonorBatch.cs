﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class DonorBatch
    {
        public int DonorBatchId { get; set; }
        public int? DonorId { get; set; }
        public int? BatchId { get; set; }
        public string SeggmentNo { get; set; }
        public string BagNo { get; set; }
    }
}
