﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class BillDetails
    {
        public int BillDetailsId { get; set; }
        public int? FkBillId { get; set; }
        public int? FkProductId { get; set; }
        public string BagNumber { get; set; }
        public int? ProductAmount { get; set; }
        public string PrintStatus { get; set; }
        public DateTime? NewCollectionDate { get; set; }
        public DateTime? NewExpiryDate { get; set; }
        public DateTime? NewTestingDate { get; set; }
    }
}
