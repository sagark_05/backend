﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class CampDonorDetails
    {
        public int CampDonorDetailsId { get; set; }
        public int? FkDonorId { get; set; }
        public string CampCode { get; set; }
        public string BagNo { get; set; }
        public string Status { get; set; }
        public string Segment { get; set; }
        public string QueStatus { get; set; }

        public string FkBloodbankId { get; set; }
        public int? FkBatchId { get; set; }

    }
}
