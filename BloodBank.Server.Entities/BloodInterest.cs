﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class BloodInterest
    {
        public int InterestId { get; set; }
        public int? FkRequestId { get; set; }
        public string UserId { get; set; }
        public DateTime? InterestDate { get; set; }
    }
}
