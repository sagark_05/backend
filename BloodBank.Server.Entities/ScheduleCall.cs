﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class ScheduleCall
    {
        public int CallId { get; set; }
        public string AssignedUserId { get; set; }
        public DateTime? CallDateTime { get; set; }
        public string Status { get; set; }

        public string FkBloodbankId { get; set; }
    }
}
