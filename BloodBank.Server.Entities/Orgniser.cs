﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class Orgniser
    {
        public int OrgniserId { get; set; }
        public string Name { get; set; }
        public string ContactNo { get; set; }
        public string EmailId { get; set; }
        public string CompanyName { get; set; }
        public string Subtitle { get; set; }
        public string Description { get; set; }
        public string Logo { get; set; }

        public string FkBloodbankId { get; set; }
    }
}
