﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
  public partial  class Usersubmenumapping
    {
        public int UserSubMenuMappingId { get; set; }
        public int? FkSubMenuId { get; set; }
        public string UserId { get; set; }
        public string PlatformType { get; set; }
    }
}
