﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class Attendance
    {
        public int AttendanceId { get; set; }
        public string UserId { get; set; }
        public DateTime? AttendanceDateTime { get; set; }
        public DateTime? InTime { get; set; }
        public DateTime? OutTime { get; set; }
        public string Image { get; set; }
        public string Location { get; set; }
        public string ApproveAttendanceStatus { get; set; }
        public string FkBloodbankId { get; set; }
    }
}
