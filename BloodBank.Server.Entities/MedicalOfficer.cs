﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class MedicalOfficer
    {
        public int MedicalOfficerId { get; set; }
        public string MedicalOfficerName { get; set; }
        public string ContactNo { get; set; }
        public string Sign { get; set; }
        public string FkBloodbankId { get; set; }
    }
}
