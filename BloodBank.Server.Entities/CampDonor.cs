﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class CampDonor
    {
        public int CampDonorId { get; set; }
        public int? FkCampId { get; set; }
        public int? FkDonorId { get; set; }
        public string BagNo { get; set; }
        public string Status { get; set; }
        public string SignUrl { get; set; }
    }
}
