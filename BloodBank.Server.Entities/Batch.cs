﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class Batch
    {
        public int BatchId { get; set; }
        public string BatchNumber { get; set; }
        public string BatchProductType { get; set; }
        public int? TotalQuantity { get; set; }
        public int? RemainingQuantity { get; set; }
        public string SolutionType { get; set; }
        public int? SolutionQuantity { get; set; }
        public DateTime? ManufactureDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string CompanyName { get; set; }
        public string SupplierName { get; set; }
        public string BillNo { get; set; }
        public string Amount { get; set; }
        public string BillDate { get; set; }
        public string GstinNo { get; set; }
        public string FkBloodbankId { get; set; }
    }
}
