﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
   public partial class PlasmaFractionation
    {
        public int PlasmaFractionationId { get; set; }
        public DateTime? PlasmaFractionationDate { get; set; }
        public int? NoOfUnits { get; set; }
        public int? TotalBatches { get; set; }
        public decimal? TotalVolumeInLiters { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? NetAmount { get; set; }
        public int? ProductId { get; set; }
        public string CreatedBy { get; set; }
        public int? FkBuyerInstituteId { get; set; }
        public string FkBloodbankId { get; set; }
        public string PaymentType { get; set; }
        public DateTime? PaidDate { get; set; }
        public int? ServiceCharge { get; set; }
        public string Remark { get; set; }
        public int? TotalVolumeInGm { get; set; }
        public string PlasmaFractionationNumber { get; set; }
    }
}
