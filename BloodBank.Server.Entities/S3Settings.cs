﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public class S3Settings
    {

        public string AWSBaseURL { get; set; }
        public string BucketName { get; set; }
        public string AWSRegion { get; set; }
        public string BucketParentKey { get; set; }
        public string BucketChildKeyPosts { get; set; }
        public string BucketChildKeyUserProfile { get; set; }
        public string BucketChildKeyMediaFiles { get; set; }
    }
}
