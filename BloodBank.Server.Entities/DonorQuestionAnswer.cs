﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class DonorQuestionAnswer
    {
        public int DonorQueAnsId { get; set; }
        public int? FkDonorId { get; set; }
        public string CampCode { get; set; }
        public int? FkQueId { get; set; }
        public string Ans { get; set; }
    }
}
