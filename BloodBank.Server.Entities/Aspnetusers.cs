﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class Aspnetusers
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string NormalizedEmail { get; set; }
        public short EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string PhoneNumber { get; set; }
        public short PhoneNumberConfirmed { get; set; }
        public short TwoFactorEnabled { get; set; }
        public DateTime? LockoutEnd { get; set; }
        public short LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public short InitialPasswordChanged { get; set; }
        public int? IsDelete { get; set; }
        public string DeleteReason { get; set; }
        public string DeviceId { get; set; }

        public string Logintype { get; set; }
    }
}