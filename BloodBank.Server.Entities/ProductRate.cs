﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class ProductRate
    {
        public int ProductId { get; set; }
        public int? Rate { get; set; }
        public int? NatRate { get; set; }
        public int? CrossMatchRate { get; set; }
        public int? CrossMatchNatRate { get; set; }
        public int? FkProductMasterId { get; set; }

        public string FkBloodbankId { get; set; }
    }
}
