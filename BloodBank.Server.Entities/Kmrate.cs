﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class Kmrate
    {
        public int KmRateId { get; set; }
        public decimal? Rate { get; set; }

        public string FkBloodbankId { get; set; }
    }
}
