﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class Bill
    {
        public int BillId { get; set; }
        public int? FkRequisitionId { get; set; }
        public DateTime? BillDate { get; set; }
        public string ServiceCharge { get; set; }
        public string BtSetCharge { get; set; }
        public int? TotalAmount { get; set; }
        public int? Discount { get; set; }
        public int? NetAmount { get; set; }
        public string AttachmentUrl { get; set; }
        public string PaymentType { get; set; }
        public string Remark { get; set; }
        public string AuthorizedbyId { get; set; }
        public string IsNat { get; set; }
        public DateTime? BillPaidDate { get; set; }
        public string BillType { get; set; }
        public string UpiId { get; set; }
        public string ServiceBoyId { get; set; }
        public string FkBloodbankId { get; set; }
    }
}
