﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
 public partial   class Submenu
    {
        public int SubMenuId { get; set; }
        public string SubMenuName { get; set; }
        public string ControllerNameText { get; set; }
        public string ActionNameText { get; set; }
        public int? FkMainMenuId { get; set; }
        public string PlatformType { get; set; }
    }
}
