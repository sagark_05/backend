﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class Expense
    {
        public int ExpenseId { get; set; }
        public int? Amount { get; set; }
        public DateTime? DateTime { get; set; }
        public string ExpenseOn { get; set; }
        public string ExpenseBy { get; set; }
        public string Image { get; set; }

        public string VoucherNo { get; set; }

        public string FkBloodbankId { get; set; }
    }
}
