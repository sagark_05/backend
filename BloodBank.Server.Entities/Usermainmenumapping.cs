﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
  public partial  class Usermainmenumapping
    {
        public int UserMainMenuMappingId { get; set; }
        public int? FkMainMenuId { get; set; }
        public string UserId { get; set; }
        public string PlatformType { get; set; }
    }
}
