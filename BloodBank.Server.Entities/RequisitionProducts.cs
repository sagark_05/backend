﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class RequisitionProducts
    {
        public int RequisitionProductId { get; set; }
        public int? FkRequisitionId { get; set; }
        public int? ProductId { get; set; }
        public int? Quantity { get; set; }
        public int? IssueQuantity { get; set; }
    }
}
