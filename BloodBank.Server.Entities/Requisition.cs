﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class Requisition
    {
        public int RequisitionId { get; set; }
        public DateTime? RequisitionDate { get; set; }
        public int? FkCallId { get; set; }
        public int? PaymentAmount { get; set; }
        public int? PaidAmount { get; set; }
        public string PaymentType { get; set; }
        public string IsActive { get; set; }
        public int? Discount { get; set; }
        public string Remark { get; set; }
        public string AuthorisedById { get; set; }
        public string IsNat { get; set; }
        public DateTime? PaidDate { get; set; }
        public int? ServiceCharge { get; set; }
        public string UpiId { get; set; }
        public string ServiceBoyId { get; set; }

        public string RequisitionNumber { get; set; }
        public string FkBloodbankId { get; set; }
    }
}
