﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class UserRole
    {
        public int UserRoleId { get; set; }
        public string RoleName { get; set; }
    }
}
