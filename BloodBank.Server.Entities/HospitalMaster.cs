﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class HospitalMaster
    {
        public int HospitalMasterId { get; set; }
        public string HospitalName { get; set; }
        public string Location { get; set; }
    }
}
