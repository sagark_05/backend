﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class Camp
    {
        public int CampId { get; set; }
        public string CampName { get; set; }
        public string Address { get; set; }
        public int? FkOrgniserId { get; set; }
        public DateTime? CampDate { get; set; }
        public DateTime? TimeFrom { get; set; }
        public DateTime? TimeTo { get; set; }
        public string CampCode { get; set; }
        public int? NoOfDonors { get; set; }
        public int? FkMedicalOfficerId { get; set; }
        public int? FkProId { get; set; }
        public string IsSpecialCertificate { get; set; }

        public string FkBloodbankId { get; set; }
    }
}
