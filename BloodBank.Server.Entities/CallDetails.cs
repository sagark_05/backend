﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class CallDetails
    {
        public int CallDetailsId { get; set; }
        public int? FkCallId { get; set; }
        public int? BillReqId { get; set; }
        public DateTime? DateTime { get; set; }
        public string ReqBillType { get; set; }
        public int? FkHospitalId { get; set; }
        public string CallDetails1 { get; set; }
        public string PatientName { get; set; }
        public string PatientContact { get; set; }
        public string Address { get; set; }
        public string CreatedUserId { get; set; }
        public string BloodGroup { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
        public string PatientStatus { get; set; }
    }
}
