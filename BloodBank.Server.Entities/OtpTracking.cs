﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
    public partial class OtpTracking
    {
        public int OtpId { get; set; }
        public string MobileNo { get; set; }
        public int? Otp { get; set; }
        public DateTime? DateTime { get; set; }
        public string Status { get; set; }
    }
}
