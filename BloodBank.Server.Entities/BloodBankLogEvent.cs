﻿using System;
using System.Collections.Generic;

namespace BloodBank.Server.Entities
{
    public class BloodBankLogEvent
    {
        public string Message { get; set; }
        // WHERE
        public string Product { get; set; }
        public string Layer { get; set; }
        public string Location { get; set; }
        public string Hostname { get; set; }

        // WHO
        public string UserId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }

        // EVERYTHING ELSE
        public long? ElapsedMilliseconds { get; set; }  // only for performance entries
        public Exception Exception { get; set; }  // the exception for error logging
        public string CorrelationId { get; set; } // exception shielding from server to client

        public Dictionary<string, object> Payload { get; set; }  // Payload
        public string Response { get; set; }  // Response
        public Dictionary<string, object> AdditionalInfo { get; set; }  // everything else


        public BloodBankLogEvent()
        {
            this.AdditionalInfo = new Dictionary<string, object>();
            this.Payload = new Dictionary<string, object>();
        }

        public BloodBankLogEvent(string product, string layer, string message):this()
        {
            this.Product = product;
            this.Layer = Layer;
            this.Message = message;
        }

        public BloodBankLogEvent(string product, string layer) : this()
        {
            this.Product = product;
            this.Layer = layer;
        }
    }
}
