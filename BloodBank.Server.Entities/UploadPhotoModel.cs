﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
   public class UploadPhotoModel
    {
        public bool Success { get; set; }
        public string FileName { get; set; }
        public string data { get; set; }
    }
}
