﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodBank.Server.Entities
{
 public partial class BloodbankMaster
    {
        public string BloodbankId { get; set; }
        public string BloodbankName { get; set; }
        public string Address { get; set; }
        public string BloodbankShortName { get; set; }
        public string LicenseNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string Logo { get; set; }

    }
}
