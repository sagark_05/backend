﻿using System;


namespace BloodBank.Server.Resources
{
    public static class Strings
    {

        public static string RoiceContextName = "RoiceDBContext";
        public static string RoiceStoreType = "RoiceStore";
        public const string PRODUCT = "Roice.Middleware";

        public static string AccessKey = Environment.GetEnvironmentVariable("ACCESS_KEY");
        public static string SecretKey = Environment.GetEnvironmentVariable("SECRET_KEY");
       

        public static class MBarkStores
        {
            public const string MySQL = "MySQL";
            public const string SQLServer = "SQLServer";
        }

        public static class AccessPolicies
        {
            public const string UsersManagementPolicy = "UsersManagementPolicy";
        }

        public static class Claims
        {
            public const string CanCRUDUsers = "CanCRUDUsers";
        }


        public static class Keys
        {
            public const string HttpStatusCode = "Http-Status-Code";
            public const string BadRequest = "BadRequest";
            public const string GlobalLogEvent = "GlobalLogEvent";
        }

        public static class Roles
        {
            public const string Admin = "Admin";
            public const string User = "User";
            public const string GuestServices = "GuestServices";

            public static bool IsKnownRole(string role)
            {
                return
                    Strings.Roles.Admin.Equals(role, StringComparison.InvariantCultureIgnoreCase) ||
                    Strings.Roles.User.Equals(role, StringComparison.InvariantCultureIgnoreCase) ||
                    Strings.Roles.GuestServices.Equals(role, StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public static class HttpResponseMessages
        {
            public const string CONFLICTS = "Email Id already exists.";
        }
    }
}
